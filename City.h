//
//  City.h
//  Health
//
//  Created by hq on 12-9-14.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol CityDelegate <NSObject>

-(void)FrushDataOfCity:(NSMutableDictionary*)mDict;

@end

@interface City : UITableViewController<HttpFormatRequestDeleagte>
{
    id<CityDelegate>Delegate;
}
@property(nonatomic,retain)NSMutableArray *mCitys;
@property(nonatomic,assign)int provinceID;
@property(nonatomic,assign)id<CityDelegate>Delegate;
@property(nonatomic,assign)BOOL isSeekFood;
@end
