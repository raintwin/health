//
//  ProvinceCity.h
//  Health
//
//  Created by hq on 12-9-13.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProvinceCity : NSObject

@property(nonatomic,retain)NSString *province,*city;
@property(nonatomic,assign)int provinceID,cityID;
@end
