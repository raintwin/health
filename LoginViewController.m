//
//  LoginViewController.m
//  Health
//
//  Created by hq on 12-6-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "LoginViewController.h"
#import "BulidController.h"
#import "RegistersViewController.h"
#import "JSON.h"
#import "Instance.h"
#import "LoginCharacter.h"
#import "BulidController.h"
#import "IndexsViewController.h"
#import "HBTEventDetailHttp.h"


#define USERNAME @"prmlf"
#define USERPASSWORD @"123456"
//#define USERNAME @""
//#define USERPASSWORD @""

//#define USERNAME @"tccxl"
//#define USERNAME @"15015566370"


#define USENAMETAG 10000
#define USEPASSTAG 10001
#define TEXTVIEW 10002

@implementation LoginViewController
@synthesize delegate;
@synthesize rootDelegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        NSLog(@"登录");
        self.rootDelegate = (DMAppDelegate *)[[UIApplication sharedApplication] delegate];

    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)removeViewController
{
    [self.view removeFromSuperview];
    [self release];
}
#pragma mark dealloc


#pragma mark 登录
-(void)LoadSignIn
{    
    NSString *name     = ((UITextField *)[self.view viewWithTag:USENAMETAG]).text;
    NSString *password = ((UITextField *)[self.view viewWithTag:USEPASSTAG]).text;

    if ([name length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入用户名" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        return;
    }

     if ([password length] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入密码	" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        return;
    }
    [self defaultSignIn:name password:password];
}

-(void)defaultSignIn:(NSString *)userName password:(NSString *)userPassWord
{
    NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *UDID = [defaults objectForKey:NSLocalizedString(@"PS_deviceSerialID", nil)];
    
    if ([UDID length] >0) {
        [mDict setObject:UDID forKey:NSLocalizedString(@"HT_DeviceSerialID", nil)];
    }
    [mDict setObject:NSLocalizedString(@"PORT_SigIN", nil) forKey:NSLocalizedString(@"PORT_ReqType", nil)];
    [mDict setObject:[NSNumber numberWithInt:1] forKey:NSLocalizedString(@"HT_MoblieType", nil)];
    [mDict setObject:userName forKey:NSLocalizedString(@"HT_UserNo", nil)];
    [mDict setObject:userPassWord forKey:NSLocalizedString(@"HT_Password", nil)];
    
    [self httpSignIn:[mDict JSONRepresentation]];
    [mDict release];
    
}
-(void)httpSignIn:(NSString *)JSONString
{
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,NSLocalizedString(@"URL_Expand",nil)];
    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate =self;
    [mHttpRequest CustomFormDataRequestDict:JSONString tag:HTTPSIGN url:urlstr];
    [self.view addSubview:mHttpRequest];
    [mHttpRequest release];
    
}
-(void)markuserDetial
{
    NSString *name     = ((UITextField *)[self.view viewWithTag:USENAMETAG]).text;
    NSString *password = ((UITextField *)[self.view viewWithTag:USEPASSTAG]).text;

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:name forKey:NSLocalizedString(@"HT_UserNo", nil)];
    [defaults setObject:password forKey:NSLocalizedString(@"HT_Password", nil)];
    [defaults synchronize];
}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    if (tag == 1) {
        NSLog(@"ResultContent :%@",ResultContent);
        UIAlertView *alertView =[ [UIAlertView alloc]initWithTitle:nil message:ResultContent delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
        [alertView release];
        return;
    }
    NSDictionary *dict = [ResultContent JSONValue];

    if ([dict objectForKey:@"isTimeOut"]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:[dict objectForKey:@"timeOutMsg"] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"马上付支", nil];
        alert.delegate = self.rootDelegate.viewController;
        alert.tag = 100;
        [alert show];
        [alert release];
        [[NSUserDefaults standardUserDefaults] setObject:[dict objectForKey:@"userID"] forKey:@"userID"];

        return;
    }
    if ([[dict objectForKey:@"timeOutMsg"] length] > 0 && ![dict objectForKey:@"isTimeOut"]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:[dict objectForKey:@"timeOutMsg"] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"马上付支", nil];
        alert.delegate = self.rootDelegate.viewController;
        alert.tag = 101;
        [alert show];
        [alert release];
    }
    
    [self markuserDetial];
    [HBTEventDetailHttp eventSigninDetail:[ResultContent JSONValue]];
    [delegate GoLoginViewController];
    [self removeViewController];
    return;

}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        NSLog(@"支付宝");
        [self goPaymentViewController];
    }
    if (buttonIndex == 0 && alertView.tag == 100) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[NSNumber numberWithBool:NO] forKey:NSLocalizedString(@"PS_Sign", nil)];
        [defaults synchronize];
        [delegate GoLoginViewController];
    }
    
}
- (void)goPaymentViewController
{
    PaymentViewController *paymentViewController = [[PaymentViewController alloc]initWithNibName:@"PaymentViewController" bundle:nil];
    [self.view addSubview:paymentViewController.view];
}

-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"网络错误，重请登录" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alertView show];
    [alertView release];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithBool:NO] forKey:NSLocalizedString(@"PS_Sign", nil)];
    [defaults synchronize];
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.navigationItem.hidesBackButton = YES;
    
    [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"登录" target:self selector:@selector(UserLogin)];
    
    [ViewTool addUIImageView:self.view imageName:@"login.jpg" type:@"" x:0 y:0 x1:320 y1:460];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *name = [defaults objectForKey:NSLocalizedString(@"HT_UserNo", nil)];
    NSString *pass = [defaults objectForKey:NSLocalizedString(@"HT_Password", nil)];

    {
        UITextField *textView = [[UITextField alloc]initWithFrame:CGRectMake(85, 133, 200, 30)];
        textView.backgroundColor = [UIColor clearColor];
        textView.placeholder = @"用户名";
        textView.text = @"";
        textView.delegate = self;
        textView.text = name;
        textView.tag = USENAMETAG;
        [self.view addSubview:textView];
    }
    {
        UITextField *textView = [[UITextField alloc]initWithFrame:CGRectMake(85, 190, 200, 30)];
        textView.backgroundColor = [UIColor clearColor];
        textView.placeholder = @"密码";
        textView.text = @"";
        textView.delegate = self;
        textView.secureTextEntry = YES;
        textView.text = pass;
        textView.tag = USEPASSTAG;
        [self.view addSubview:textView];
    }
    
    {
        UIButton *button = [ViewTool addUIButton:self.view imageName:@"signinButton.png" type:@"" x:34 y:269 x1:152 y1:50];
        [button addTarget:self action:@selector(LoadSignIn) forControlEvents:64];
    }
    {
        UIButton *button = [ViewTool addUIButton:self.view imageName:@"signinCancel.png" type:@"" x:200 y:269 x1:90 y1:50];
        [button addTarget:self action:@selector(removeViewController) forControlEvents:64];

    }
    {
        UITextView *textView = [[UITextView alloc]initWithFrame:CGRectMake(10, 5, 300, 70)];
        textView.tag = TEXTVIEW;
        textView.editable = NO;
        textView.text = [NSString stringWithFormat:@"提示: %@",[Instance getSignInPrompt]];
        textView.textColor =[UIColor whiteColor];
        textView.font = [UIFont fontWithName:@"STHeitiTC-Medium" size:16.0];
        [textView setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:textView];
        [textView release];
    }
}
-(void)refreashTextView:(NSString *)text
{
    UITextView *textView = (UITextView *)[self.view viewWithTag:TEXTVIEW];
    textView.text = [NSString stringWithFormat:@"提示: %@",text];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textField.text = nil;
    return YES;
}
- (BOOL)textFieldShouldClear:(UITextField *)textField              // called when clear button pressed. return NO to ignore 
{
    textField.text = nil;
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField              // called when 'return' key pressed. return NO to ignore.
{
   
    [textField resignFirstResponder];
    return YES;
}

-(void)handleSingleTapFrom
{
    NSLog(@"dd");
}
-(void)dealloc
{
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
