//
//  PaymentViewController.h
//  Health
//
//  Created by dinghao on 13-8-11.
//
//

#import <UIKit/UIKit.h>

@interface PaymentViewController : UIViewController<HttpFormatRequestDeleagte>

@property (retain, nonatomic) IBOutlet UITextView *userContent;

@property (retain, nonatomic) IBOutlet UILabel *alipayDate;
@property (retain, nonatomic) IBOutlet UILabel *alipayMoney;


@end
