//
//  JsonFormat.m
//  Health
//
//  Created by hq on 12-7-25.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "JsonFormat.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "JSON.h"
@implementation JsonFormat
-(void)JsonForNotitySend:(NSMutableDictionary*)mDict NSURL:(NSURL*)url trage:(id)trage;

{
    NSString *JSONString = [mDict JSONRepresentation];
    NSLog(@"JSONString :%@",JSONString);

    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:trage];
    [request setTimeOutSeconds:20];
    [request setPostValue:JSONString forKey:@"requestContent"];
    [request startSynchronous];
    
    NSError *error = [request error];
    if (error) {
        NSLog(@"error -- :%@",[request responseString]);
    }

}
-(void)JsonForPhotoSend:(NSMutableDictionary*)mDict photoarray:(NSMutableArray*)photos NSURL:(NSURL*)url trage:(id)trage;
{
    NSString *JSONString = [mDict JSONRepresentation];
    NSLog(@"JSONString :%@",JSONString);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:trage];
    [request setTimeOutSeconds:20];
    [request setPostValue:JSONString forKey:@"requestContent"];
    [request startSynchronous];
    
    NSError *error = [request error];
    if (error) {
        NSLog(@"error -- :%@",[request responseString]);
    }
    
}

- (void)requestFinished:(ASIHTTPRequest *)request {
    //    NSString *responseStr = [request responseString];
    NSLog(@"sub--requestFinished");
    NSData *responeseData = [request responseData];
    NSString *responseStr = [[NSString alloc] initWithData:responeseData encoding:NSUTF8StringEncoding];
    
    responseStr =   [responseStr stringByReplacingOccurrencesOfString: @"\r" withString:@""];
    responseStr =   [responseStr stringByReplacingOccurrencesOfString: @"\n" withString:@""];
    
    
    NSMutableDictionary *tmpArray = [responseStr JSONValue];
    NSLog(@"requestFinished -- :%@",responseStr);
    
    NSLog(@"resultStr -%@  ",[tmpArray objectForKey:@"ResultStuates"]);
    NSMutableDictionary *Description = [tmpArray objectForKey:@"ResultStuates"];
    NSLog(@"Description -%@  ",[Description objectForKey:@"ResultDescription"]);
    NSLog(@"ResultCode -%d  ",[[Description objectForKey:@"ResultCode"] intValue]);
    
}

@end
