//
//  Recipe.m
//  Health
//
//  Created by hq on 12-9-7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "Recipe.h"

@implementation Recipe
@synthesize dayOfWeek,dateForFindCondition,dateForRepresentation;
-(void)dealloc
{
    [dateForRepresentation release]; dateForRepresentation = nil;
    [dateForFindCondition release]; dateForFindCondition = nil;
    [dayOfWeek release];  dayOfWeek = nil; 
    [super dealloc];
}
@end


#pragma mark 食谱类别    餐次

@implementation RecipeClasses

@synthesize mealName,mealTime;
@synthesize arrangeMealOrNot;
@synthesize mealType;
-(void)dealloc
{
    [mealName release];  mealName = nil;
    [mealTime release];  mealTime = nil;
    [super dealloc];
}
@end



#pragma mark 食谱类别    餐次

@implementation RecipeClassesName

@synthesize menuName,menuPicUrl;
@synthesize menuID,menuWeight;
-(void)dealloc
{
    [menuName release];  menuName = nil;
    [menuPicUrl release];  menuPicUrl = nil;
    [super dealloc];
}
@end



#pragma mark 食谱类别    餐次

@implementation RecipeMenuList

@synthesize foodKindName,foodName,foodPicUrl;
@synthesize foodNutrientDistributionID,foodWeight;
-(void)dealloc
{
    [foodKindName release];  foodKindName = nil;
    [foodName release];  foodName = nil;
    [foodPicUrl release];  foodPicUrl = nil;

    [super dealloc];
}
@end


#pragma mark 查询结果

@implementation FoodResult
@synthesize foodNutrientDistributionID,foodKindIDBySSBT;
@synthesize foodName,foodKindNameBySSBT,foodPicUrl;
@synthesize isMark;
-(void)dealloc
{
    [foodName release]; foodName = nil;
    [foodKindNameBySSBT release]; foodKindNameBySSBT = nil;
    [foodPicUrl release]; foodPicUrl = nil;

    [super dealloc];
}
@end



#pragma mark 查询结果

@implementation recommendRecipe
@synthesize menuName,menuPicUrl;
@synthesize menuID,mealId;
@synthesize flag;
-(void)dealloc
{
    [menuName release]; menuName = nil;
    [menuPicUrl release]; menuPicUrl = nil;
    
    [super dealloc];
}
@end

#pragma mark 推存食谱 日期类
@implementation DateTableClass


@synthesize mDate;
@synthesize ID;
-(void)dealloc
{
    [mDate release]; mDate = nil;
    [super dealloc];
}

@end


#pragma mark 餐次类别
@implementation MealTableClass
@synthesize ID,FatherID,MealType;
-(void)dealloc
{
    [super dealloc];
}

@end



@implementation SaveAllClass

@synthesize mFoods,mRecommnectFoods,mRecommentRecipes;
@synthesize isDiv;
@synthesize mealID;
-(void)dealloc
{
    [mFoods release]; mFoods = nil;
    [mRecommnectFoods release]; mRecommnectFoods = nil;
    [mRecommnectFoods release]; mRecommentRecipes = nil;
    [super dealloc];
}

@end

#pragma mark 餐次类别
@implementation sysMeal
@synthesize ID,uType,name;
-(void)dealloc
{
    [uType release];
    [name release];
    [super dealloc];
}
@end


#pragma mark 菜谱成份
@implementation FoodNutritiont
@synthesize foodName,nutritionName,foodAmount,nutritionAmount,unit;
-(void)dealloc
{
    [foodName release];
    [foodAmount release];

    [nutritionName release];
    [nutritionAmount release];
    [unit release];
    [super dealloc];
}

@end



#pragma mark 历史记录列表
@implementation MealList
@synthesize date;
-(void)dealloc
{
    [date release];
    [super dealloc];
}

@end

