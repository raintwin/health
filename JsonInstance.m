//
//  JsonInstance.m
//  Health
//
//  Created by hq on 12-7-25.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "JsonInstance.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
@implementation JsonInstance
-(void)JsonForNotitySend:(NSMutableDictionary*)mDict NSURL:(NSURL*)url trage:(id)trage;

{
    NSString *JSONString = [mDict JSONRepresentation];
    NSLog(@"JSONString :%@",JSONString);

    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:trage];
    [request setTimeOutSeconds:20];
    [request setPostValue:JSONString forKey:@"requestContent"];
    [request startSynchronous];
    
    NSError *error = [request error];
    if (error) {
        NSLog(@"error -- :%@",[request responseString]);
    }

}
-(void)JsonForNotitySend:(NSMutableDictionary*)mDict NSURL:(NSURL*)url;

{
    NSString *JSONString = [mDict JSONRepresentation];
    NSLog(@"JSONString :%@",JSONString);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setTimeOutSeconds:20];
    [request setPostValue:JSONString forKey:@"requestContent"];
    [request startSynchronous];
    
    NSError *error = [request error];
    if (error) {
        NSLog(@"error -- :%@",[request responseString]);
    }
    
}
-(void)JsonForPhotoSend:(NSMutableDictionary*)mDict photoarray:(NSMutableArray*)photos NSURL:(NSURL*)url trage:(id)trage;
{
    NSString *JSONString = [mDict JSONRepresentation];
    NSLog(@"JSONString :%@",JSONString);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:trage];
    [request setTimeOutSeconds:20];
    [request setPostValue:JSONString forKey:@"requestContent"];
    for (int i = 0; i<[photos count]; i++) {
        NSLog(@"photoname :%@",[photos objectAtIndex:i]);
        NSString *str = [photos objectAtIndex:i];
        [request setFile:str forKey:[NSString stringWithFormat:@"phone%d",i]];
    }
    [request startSynchronous];
    
    NSError *error = [request error];
    if (error) {
        NSLog(@"error -- :%@",[request responseString]);
    }
}
-(void)JsonForPhotoSend:(NSMutableDictionary*)mDict photoarray:(NSMutableArray*)photos NSURL:(NSURL*)url;
{
    NSString *JSONString = [mDict JSONRepresentation];
    NSLog(@"JSONString :%@",JSONString);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setTimeOutSeconds:20];
    [request setPostValue:JSONString forKey:@"requestContent"];
    for (int i = 0; i<[photos count]; i++) {
        NSLog(@"photoname :%@",[photos objectAtIndex:i]);
        NSString *str = [photos objectAtIndex:i];
        [request setFile:str forKey:[NSString stringWithFormat:@"phone%d",i]];
    }
    [request startSynchronous];
    
    NSError *error = [request error];
    if (error) {
        NSLog(@"error -- :%@",[request responseString]);
    }
    
}
- (void)requestFinished:(ASIHTTPRequest *)request {
    //    NSString *responseStr = [request responseString];
    NSData *responeseData = [request responseData];
    NSString *responseStr = [[NSString alloc] initWithData:responeseData encoding:NSUTF8StringEncoding];
    
    responseStr =   [responseStr stringByReplacingOccurrencesOfString: @"\r" withString:@""];
    responseStr =   [responseStr stringByReplacingOccurrencesOfString: @"\n" withString:@""];
    
    
    NSMutableDictionary *tmpArray = [responseStr JSONValue];
    
    int ResultCode = [[[tmpArray objectForKey:@"ResultStuates"] objectForKey:@"ResultCode"] intValue];
    NSString *Description = [[tmpArray objectForKey:@"ResultStuates"] objectForKey:@"ResultDescription"];
    NSLog(@"Description -%@  ",[[tmpArray objectForKey:@"ResultStuates"] objectForKey:@"ResultDescription"]);
    NSLog(@"ResultCode -%d  ",[[[tmpArray objectForKey:@"ResultStuates"] objectForKey:@"ResultCode"] intValue]);
    if (ResultCode == 0 && [Description isEqualToString:@"处理正确"]) {
        NSLog(@"发送正确，完成发送");
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"网络错误，发送失败" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        
    }
    [responseStr release];
    [SVProgressHUD dismiss];
}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSData *data = [request responseData];
    NSString *responseStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"requestFailed -- :%@",responseStr);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"网络错误，发送失败" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
    [responseStr release];
    [SVProgressHUD dismiss];

}
-(void)dealloc
{
    [super dealloc];
}
@end
