//
//  ChildListViewController.h
//  Health
//
//  Created by hq on 12-6-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DownLoadTableImage.h"

@protocol ChileListDelegate <NSObject>
@optional

-(void)SetPitchs:(NSMutableArray *)childs;

@end


@interface ChildListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,DownLoadImageDelegate,HttpFormatRequestDeleagte>
{
   id<ChileListDelegate> _chliddelgelagte;
    
    BOOL isOver;
}

@property(nonatomic,retain) NSMutableArray *mChilds;

@property(nonatomic,retain)UITableView *mTable;
@property(nonatomic,assign) id<ChileListDelegate>_chliddelgelagte;

@property(nonatomic,retain)NSMutableDictionary *mDownLoadImageDict;

@end

