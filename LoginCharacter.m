//
//  LoginCharacter.m
//  Health
//
//  Created by hq on 12-8-3.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "LoginCharacter.h"
#import "BulidController.h"
#import "ASIFormDataRequest.h"
@implementation LoginCharacter
@synthesize mChilds;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


-(void)navBack
{

    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];
    [self.navigationItem setHidesBackButton:YES];
    // Do any additional setup after loading the view from its nib.
    
    UITableView *tableview  = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 372) style:UITableViewStyleGrouped];
    [tableview setBackgroundColor:[UIColor clearColor]];
    [tableview setScrollEnabled:NO];
    tableview.delegate = self;
    tableview.dataSource = self;
    [self.view addSubview:tableview];
    [tableview release];
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [mChilds count];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
 	static NSString * cellName = @"CityManagerViewController";
	
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier: cellName] autorelease];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    
    NSMutableDictionary *mDict = [mChilds objectAtIndex:[indexPath row]];

    cell.textLabel.text = [mDict objectForKey:@"childName"];
    
    return cell;
}
-(void)InitTabBar
{
    self.navigationController.navigationBarHidden = YES;
    
    BulidController *build = [[BulidController alloc]init];
    UITabBarController *tabBarController = [build BuildsController];
    
    tabBarController.delegate = self;
    tabBarController.customizableViewControllers = nil;
    [tabBarController.view setFrame:[self.view bounds]];    
    [self.navigationController pushViewController:tabBarController animated:YES];
    [build release];
    [tabBarController release];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *mDict = [mChilds objectAtIndex:[indexPath row]];
    
    NSLog(@"mDict: %@",[mDict JSONRepresentation]);
    [mDict writeToFile:[Instance GetDocumentByString:@"initlalize"] atomically:YES];

    [self performSelector:@selector(DownIconImage:) withObject:[mDict objectForKey:@"photo"]];
}
#pragma mark 下载图标
-(void)DownIconImage:(NSString*)Url
{
    if ([Url length] == 0) {
        return;
    }
    
    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate = self;
    [mHttpRequest CustomFormDataRequestDownFile:[Instance GetDocumentByString:[NSString stringWithFormat:@"%@.png",[Instance GetChildID]]] tag:FILEDOWNID url:Url];
    [self.view addSubview:mHttpRequest];
    [mHttpRequest release];
    
//    [mHttpFormatRequest CustomFormDataRequestDownFile:[Instance GetDocumentByString:[NSString stringWithFormat:@"%@.png",[Instance GetChildID]]] tag:FILEDOWNID url:Url];

}

-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{
}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    if (tag == FILEDOWNID) {
        [self InitTabBar];

    }
}

-(void)dealloc
{
    [mChilds release];
//    [tabBarController release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mChilds = nil;
//    tabBarController = nil;
}
-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{

}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
