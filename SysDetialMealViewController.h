//
//  SysDetialMealViewController.h
//  Health
//
//  Created by dinghao on 13-4-19.
//
//

#import <UIKit/UIKit.h>
#import "HttpFormatRequest.h"
#import "ViewController.h"
@interface SysDetialMealViewController : ViewController<HttpFormatRequestDeleagte,UITableViewDataSource,UITableViewDelegate>

@property(retain,nonatomic)HttpFormatRequest *mHttpRequest;
@property(retain,nonatomic)NSMutableArray *foodDetails,*nutritionDetails;
@property(retain,nonatomic)NSMutableDictionary *sysDetialMealsDict;
@property(retain,nonatomic)sysMeal *sysmeal;

@property(retain,nonatomic)UITableView *mTableView;
@end
