//
//  FoodView.m
//  Health
//
//  Created by hq on 12-9-12.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "FoodView.h"
#define mTableHeight 46
#define mTableTextSize 13
#define mTablePieHeight 400

@implementation FoodView
@synthesize mAnalysisResultDict;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)navBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}
-(void)dealloc
{
    [mAnalysisResultDict release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mAnalysisResultDict = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 12;
}
-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return mTableHeight;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
    }
    
    for (UIView *sub in [cell.contentView subviews]) {
        [sub removeFromSuperview];
    }
    
    {
        UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 46)];
        [img setImage:[UIImage imageNamed:[indexPath row]%2 == 0 ? @"xia.jpg":@"shang.jpg"]];  
        img.tag = 121211;
        [cell.contentView addSubview:img];
        [img release];
        
    }
    
    {
        UILabel *label = [Instance addLable:CGRectMake(10, 0, 160, mTableHeight) tag:100011 size:mTableTextSize string:@""];
        [cell.contentView addSubview:label];
        [label release];
    }
    {
        UILabel *label = [Instance addLable:CGRectMake(170, 0, 300, mTableHeight) tag:100022 size:mTableTextSize string:@""];
        [cell.contentView addSubview:label];
        [label release];
    }

    // Configure the cell...
    int mIndexRow = [indexPath row];

        if (mIndexRow == 0) {
            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"杂粮      %@",[mAnalysisResultDict objectForKey:@"miscellaneousGrainCrop"]]; //2
            ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"细粮      %@",[mAnalysisResultDict objectForKey:@"wheatFlourAndRice"]];//3
        }
        if (mIndexRow == 1) {
            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"代乳食品      %@",[mAnalysisResultDict objectForKey:@"milkStuff"]];//4
            ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"糕点      %@",[mAnalysisResultDict objectForKey:@"cakeFood"]];//5
            
        }
        if (mIndexRow == 2) {
            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"干豆类      %@",[mAnalysisResultDict objectForKey:@"drySoyBeanFood"]];//6
            ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"豆制品      %@",[mAnalysisResultDict objectForKey:@"soyBeanProduct"]];//7
            
        }
        if (mIndexRow == 3) {
            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"浅色蔬菜      %@",[mAnalysisResultDict objectForKey:@"lightColoredVegetable"]];//8
            ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"绿橙蔬菜      %@",[mAnalysisResultDict objectForKey:@"greenOrangeVegetable"]];//9
            
        }
        if (mIndexRow == 4) {
            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"干菜      %@",[mAnalysisResultDict objectForKey:@"dryVegetable"]];//10
            ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"海菜      %@",[mAnalysisResultDict objectForKey:@"seaVegetable"]];//11
            
        }
        if (mIndexRow == 5) {
            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"咸菜      %@",[mAnalysisResultDict objectForKey:@"saltVegetable"]];//12
            ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"水果      %@",[mAnalysisResultDict objectForKey:@"fruit"]];//13
            
        }
        if (mIndexRow == 6) {
            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"坚壳类      %@",[mAnalysisResultDict objectForKey:@"solidShellFood"]];//14
            ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"乳类      %@",[mAnalysisResultDict objectForKey:@"milkFood"]];//15
            
        }
        if (mIndexRow == 7) {
            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"蛋类      %@",[mAnalysisResultDict objectForKey:@"eggFood"]];//16
            ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"肉类      %@",[mAnalysisResultDict objectForKey:@"meatFood"]];//17
            
        }
        if (mIndexRow == 8) {
            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"肝类      %@",[mAnalysisResultDict objectForKey:@"liverFood"]];//18
            ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"鱼虾      %@",[mAnalysisResultDict objectForKey:@"fishAndShrimp"]];//19
            
        }
        if (mIndexRow == 9) {
            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"淀粉与糖      %@",[mAnalysisResultDict objectForKey:@"starchAndSugur"]];//20
            ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"食油      %@",[mAnalysisResultDict objectForKey:@"cookingOil"]];//21
            
        }
        if (mIndexRow == 10) {
            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"酱油      %@",[mAnalysisResultDict objectForKey:@"soySauceOil"]];//22
            ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"食盐      %@",[mAnalysisResultDict objectForKey:@"cookingSalt"]];//23
            
        }
        if (mIndexRow == 11) {
            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"其它      %@",[mAnalysisResultDict objectForKey:@"otherFood"]];//23
            ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//23
        }
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end
