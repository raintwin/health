//
//  RegisterViewController.h
//  Health
//
//  Created by hq on 12-6-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController<UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    UITabBarController *tabBarController;
    UITableView *mytable;

}
@property(nonatomic,retain)NSString *nameStr ,*passwordStr;
@end
