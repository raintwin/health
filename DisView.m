//
//  DisView.m
//  Health
//
//  Created by hq on 12-8-15.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "DisView.h"

@implementation DisView
@synthesize btnTag;
@synthesize btns;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.btnTag  = 0;
        // Initialization code
        
        UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(10, 20, 59, 80)];
        image.tag = 11000;
        [self addSubview:image];
        [image release];
        
        CGRect rect = image.frame;
        NSMutableArray *array = [[NSMutableArray alloc]init];
        NSArray *btnTitles = [[NSArray alloc] initWithObjects:@"很好",@"一般",@"差", nil];
        for (int i = 0; i<3; i++) {
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(rect.origin.x + rect.size.width + 10,rect.origin.y + 30 * i, 23, 23)];
            
            if (i == 0) [btn setImage:[UIImage imageNamed:@"tu_14.png"] forState:0];

            else  [btn setImage:[UIImage imageNamed:@"tu_04.png"] forState:0];
    
            UILabel *title = [Instance addLablePoint:CGPointMake(28, 0) text:[btnTitles objectAtIndex:i] textsize:15];
            title.text = [btnTitles objectAtIndex:i];
            [btn addSubview:title];
            [title release];
            
            btn.tag = i;
            
            [self addSubview:btn];
            [array addObject:btn];

            [btn addTarget:self action:@selector(SetDisNumber:) forControlEvents:64];
            [btn release];
        }
        self.btns = array;
        [btnTitles release];
        [array release];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)SetRefresh:(NSString *)imgstr
{
    UIImageView *imge = (UIImageView*) [self viewWithTag:11000];
    if (imge!=nil) {
        [imge setImage:[UIImage imageNamed:imgstr]];
    }
    self.btnTag = 0;
}

-(void)SetDisNumber:sender
{
    for (UIButton *btn  in btns) {
        [btn setImage:[UIImage imageNamed:@"tu_04.png"] forState:0];
    }
    UIButton *tmp = sender;
    self.btnTag = tmp.tag;
    NSLog(@"btnTag %d",btnTag);
    [tmp setImage:[UIImage imageNamed:@"tu_14.png"] forState:0];
}
-(void)dealloc
{
    [btns release];
    [super dealloc];
}
@end



@implementation DisViewImage
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(10, 20, 59, 80)];
        image.tag = 11000;
        [self addSubview:image];
        [image release];
        
    }
    return self;
}



-(void)SetRefresh:(NSString *)imgstr
{
    UIImageView *imge = (UIImageView*) [self viewWithTag:11000];
    [imge setImage:[UIImage imageNamed:imgstr]];
}

-(void)SetDisNumber:(int)tag
{
    UIImageView *imge = (UIImageView*) [self viewWithTag:11000];
    if (imge == nil) {
        return;
    }
    CGRect rect = imge.frame;
    
    
    NSArray *btnTitles = [[NSArray alloc] initWithObjects:@"很好",@"一般",@"差", nil];
    
    for (int i = 0; i<3; i++) {
        UIImageView *imagev = [[UIImageView alloc]initWithFrame:CGRectMake(rect.origin.x + rect.size.width + 10,rect.origin.y + 30 * i, 23, 23)];
        imagev.image = nil;
        [imagev setImage:[UIImage imageNamed:@"tu_04.png"]];

        if (tag == i) {
            [imagev setImage:[UIImage imageNamed:@"tu_14.png"]];
        }
        
        
        UILabel *title = [Instance addLablePoint:CGPointMake(28, 0) text:[btnTitles objectAtIndex:i] textsize:15];
        title.text = [btnTitles objectAtIndex:i];
        [imagev addSubview:title];
        [title release];
        [self addSubview:imagev];
        [imagev release];
        
    }
    [btnTitles release];    
}
-(void)dealloc
{
    [super dealloc];
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */
@end

