//
//  RecommendDinnerView.h
//  Health
//
//  Created by hq on 12-9-15.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SeekFoodView.h"

@interface RecommendDinnerView : UIViewController<UITableViewDelegate,UITableViewDataSource,HttpFormatRequestDeleagte>
{
    int mealID;
}

@property(nonatomic,retain)UITableView *mFoodTable,*mRecommendFoodTable,*mRecommendRecipeTable;
@property(nonatomic,retain)NSMutableArray *mFoods,*mRecommendFoods,*mRecommendRecipes;

@property(nonatomic,retain)NSMutableDictionary *mConditionDict,*mAnalysisResultDict;

@end
