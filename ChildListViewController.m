//
//  ChildListViewController.m
//  Health
//
//  Created by hq on 12-6-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ChildListViewController.h"
#import "JSON.h"
#import "CChild.h"
#import "DownLoadTableImage.h"


@implementation ChildListViewController
@synthesize _chliddelgelagte;
@synthesize mChilds;
@synthesize mTable;
@synthesize mDownLoadImageDict;

#define tableWidths 50
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)CancelDownLoad
{
    for (int i = 0; i<[mChilds count]; i++) {
        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:i inSection:0];
        DownLoadTableImage *imageDownLoad = [mDownLoadImageDict objectForKey:indexpath];
        if (imageDownLoad != nil) {
            [imageDownLoad CancelDownloadImage];
        }
        
    }
    [mDownLoadImageDict removeAllObjects];
}
#pragma mark - View lifecycle
-(void)navBack
{

    [self CancelDownLoad];
    if ([mChilds count]>0) {
        [_chliddelgelagte SetPitchs:mChilds];
    }
    
   [self.navigationController popViewControllerAnimated:NO];
}
-(void)PitchOn
{

    if (isOver) {
        for (CChild *obj  in mChilds) {
            obj.isCheck = NO;
        }
        isOver = NO;
    }
    else{
        for (CChild *obj  in mChilds) {
            obj.isCheck = YES;
        }
        isOver = YES;
    }
    [mTable reloadData];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    isOver = NO;
    [ViewTool addUIImageViewWithAutoSize:self.view imageName:@"viewblackgroup.jpg" type:@"" x:0 y:0 x1:0 y1:0];

    // Do any additional setup after loading the view from its nib.

    [Instance setLeftImageAndNameForNavigationItem:self.navigationItem Image:@"blankbutton.png" title:@"确定" target:self selector:@selector(navBack)];
    [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"全选" target:self selector:@selector(PitchOn)];
//    [Instance setRightImageBtnForNavigationItem:self.navigationItem Image:@"quanxuan.png" target:self selector:@selector(PitchOn)];

    
    
    NSMutableDictionary *mDictionary = [[NSMutableDictionary alloc]init];
    self.mDownLoadImageDict = mDictionary;
    [mDictionary release];
    
    NSLog(@"e mChilds count=%d",[mChilds count]);
    
 
    
    {
        UITableView *table = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,372)];
        table.delegate = self;
        table.dataSource = self;
        table.backgroundColor = [UIColor clearColor];
        self.mTable = table;
        [table release];

        [self.view addSubview:mTable];
    }

    if (mChilds ==nil) {
        [NSThread detachNewThreadSelector:@selector(GetChildList) toTarget:self withObject:nil];
    }

}
-(void)GetChildList
{
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/UserManager/TeacherFetchChildList"];
    
    NSMutableDictionary *mDic = [[NSMutableDictionary alloc] init];
    
    [mDic setObject:[Instance GetUseID] forKey:@"UserID"];
    
    NSString *JSONString = [mDic JSONRepresentation];
    [mDic release];
    
    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate = self;
    [mHttpRequest CustomFormDataRequestDict:JSONString tag:0 url:urlstr];
    [self.view addSubview:mHttpRequest];
    [mHttpRequest release];
}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    NSArray *array = [[[ResultContent JSONValue] valueForKey:@"childList"] retain];    
    NSMutableArray *tmpArray = [[NSMutableArray alloc]init];
    for (int i = 0; i < [array count]; i++) {
        CChild *sif = [[CChild alloc] init];
        sif.cID         = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childID"]] intValue];
        sif.cName   = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childName"]];
        sif.cPhotoUrl    = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"photo"]];  
        
        sif.isLoad = NO;
        sif.image = nil;
        sif.isCheck = NO;
        [tmpArray addObject:sif];
        [sif release];
    }
    
    self.mChilds = tmpArray;
    [tmpArray release];
    [array release];
    
    if (mChilds ==nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"读取幼儿列表失败" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    [mTable reloadData];

}
#pragma mark  load imageview 
-(void)StartDownLoadImage:(NSString*)url IndexPath:(NSIndexPath*)indexpath 
{
    DownLoadTableImage *imageDownLoad = [mDownLoadImageDict objectForKey:indexpath];
    if (imageDownLoad == nil) {
        imageDownLoad = [[DownLoadTableImage alloc] init];
        imageDownLoad.ImageURL = url;
        imageDownLoad.ImageHeight = 40;
        imageDownLoad.ImageWidth = 40;
        imageDownLoad.IndexPathTo = indexpath;
        imageDownLoad.Delegate =self;
        [mDownLoadImageDict setObject:imageDownLoad forKey:indexpath];
        [imageDownLoad StartDownloadImage];
        [imageDownLoad release];
    }
}
#pragma mark finish download image
-(void)FinishDownLoadImage:(NSIndexPath*)indexpath Image:(UIImage*)image IsFailed:(BOOL)isFaild
{
    DownLoadTableImage *imageDownLoad = [mDownLoadImageDict objectForKey:indexpath];
    if (imageDownLoad != nil) {
        UITableViewCell *cell = [self.mTable cellForRowAtIndexPath:indexpath];
        CChild *obj = [mChilds objectAtIndex:[indexpath row]];
        
        if (isFaild) {
            obj.image = [UIImage imageNamed:@"acquiesce.jpg"];
            obj.isLoad = NO;
        }
        else
        {
            obj.image = image;
            obj.isLoad = YES;
        }
        
        ((UIImageView *)[cell.contentView viewWithTag:110000]).image = obj.image ;
    }
}
- (void)loadImagesForOnscreenRows
{
    if ([mChilds count] > 0)
    {
        NSArray *visiblePaths = [self.mTable indexPathsForVisibleRows];
        for (NSIndexPath *indexPath in visiblePaths)
        {
            if ([indexPath row]<[mChilds count]) {
                CChild *item = [mChilds objectAtIndex:indexPath.row];
                
                if (!item.isLoad ) // avoid the app icon download if the app already has an icon
                {
                    [self StartDownLoadImage:item.cPhotoUrl IndexPath:indexPath];
                }
                
            }
        }
    }
}
#pragma mark -
#pragma mark Deferred image loading (UIScrollViewDelegate)
#pragma mark -
// Load images for all onscreen rows when scrolling is finished
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	if (!decelerate)
	{
        [self loadImagesForOnscreenRows];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadImagesForOnscreenRows];
}
#pragma mark tableview 

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   if (tableView == mTable) {
       
       if ([mChilds count]<[indexPath row]) {
           return;
       }
       CChild *sif = [mChilds objectAtIndex:[indexPath row]];

       
      if ([[tableView cellForRowAtIndexPath:indexPath] accessoryType] == UITableViewCellAccessoryCheckmark) {
         [[tableView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryNone];
          sif.isCheck = NO;

      }
      else
      {
         [[tableView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryCheckmark];
          sif.isCheck = YES;

      }
      [tableView deselectRowAtIndexPath:indexPath animated:NO];
   }
     
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return tableWidths;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [mChilds count];	
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 
    
    
    
    static NSString *cellName = @"cellstr";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    
    
    if (cell== nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellName];
    }
    
    
    NSInteger row = [indexPath row];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.imageView.image = nil;
    
    for (UIView *ctr in [cell.contentView subviews]) {
        [ctr removeFromSuperview];
    }

    
        
    if (tableView == mTable) {
        if ([mChilds count] == 0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"读取幼儿列表失败" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            return cell;
        }
        CChild *sif = [mChilds objectAtIndex:row];
        
        NSString *childStr =  [NSString stringWithFormat:@"%@",sif.cName];
        
        UIImageView *markImg = [[UIImageView alloc]initWithFrame:CGRectMake(292, 13, 23, 23)];
        [markImg setImage:[UIImage imageNamed:@"tu_04.png"]];
        [cell addSubview:markImg];
        [markImg release];

        // --------------------- load image
        {
            UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 40, 40)];
            image.tag = 110000;
            [image setImage:[UIImage imageNamed:@"acquiesce.jpg"]];
            [cell.contentView addSubview:image];
            [image release];
            
            if (!sif.isLoad) {
                if (self.mTable.dragging == NO && self.mTable.decelerating == NO) {
                    //                    NSLog(@"obj.nPhoto :%@",obj.nPhoto);
                    [self StartDownLoadImage:sif.cPhotoUrl IndexPath:indexPath];
                }
            }
            else
            {
                UIImageView *img =(UIImageView *)[cell.contentView viewWithTag:110000];
                [img setImage:sif.image];
            }
        }

        {
            CGSize rect1 = [childStr sizeWithFont:[UIFont boldSystemFontOfSize:13]];
            
            UILabel *parentText = [[UILabel alloc]initWithFrame:CGRectMake(70 , 11, rect1.width, rect1.height)];
            parentText.font = [UIFont boldSystemFontOfSize:13];
            [parentText setBackgroundColor:[UIColor clearColor]];
            parentText.text = childStr;
            [cell.contentView addSubview:parentText];
            [parentText release];
            
            

            
        }

       [cell setAccessoryType:UITableViewCellAccessoryNone];
        
        if (sif.isCheck) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;

        } 
    }
    return cell;

}

-(void)dealloc
{

    [mDownLoadImageDict release]; 
    [mChilds release]; 
    [mTable release]; 
   [super dealloc];
}


- (void)viewDidUnload
{
    self.mDownLoadImageDict = nil;
    self.mTable = nil;
    self.mChilds = nil ;

    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
