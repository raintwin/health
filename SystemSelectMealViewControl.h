//
//  SystemSelectMealViewControl.h
//  Health
//
//  Created by jiaodian on 12-12-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PantrytMianViewController.h"
#import "SystemComPantryViewControl.h"
@interface SystemSelectMealViewControl : UIViewController<UITableViewDataSource,UITableViewDelegate,SystemComPantryViewControlDelegate>


@property(retain,nonatomic)UITableView *mTableView;
@property(retain,nonatomic)NSMutableArray *mSysComPantryMeals;
@property(retain,nonatomic)PantrytMianViewController *mSuperView;
@end
