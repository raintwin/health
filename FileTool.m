//
//  FileTool.m
//  LingNan HD
//
//  Created by osu on 11-1-28.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FileTool.h"


@implementation FileTool

//获取document目录
+(NSString *)getDocumentsPath{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	return documentsDirectory;
}
//获取document路径下的file
+(NSString *)getDocumentsPath:(NSString *)fileName{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:fileName];
	return path;
}
//获取Bundle目录下的文件
+(NSString *)getBundlePath:(NSString *)fileName type:(NSString *)type{
	NSString *path = [[NSBundle mainBundle]pathForResource:fileName ofType:type];
	return path;
}

	//获取tempath目录下的文件
+(NSString *)getTemPath:(NSString *)fileName{
	NSString *tempPath = NSTemporaryDirectory();
	NSString *path = [tempPath stringByAppendingPathComponent:fileName];
	return path;
}

//创建文件夹
+(void )createDirectoryAtPath:(NSString *)fileName{
	[[NSFileManager defaultManager] createDirectoryAtPath:fileName withIntermediateDirectories:YES attributes:nil error:nil];
}
 
//判断docment是否存在此file
+(BOOL)isExistAtDocPath:(NSString *)fileName fileManager:(NSFileManager *)fm docPath:(NSString *)docPath {
	return [fm fileExistsAtPath:[docPath stringByAppendingPathComponent:fileName]];
}
//判断bundle是否存在此file
+(BOOL)isExistAtBundle:(NSString *)fileName fileManager:(NSFileManager *)fm  {
	return [fm fileExistsAtPath:[FileTool getBundlePath:fileName type:@""]];
}

//判断tempaht是否存在此file
+(BOOL)isExistAtTem:(NSString *)fileName fileManager:(NSFileManager *)fm  {
	return [fm fileExistsAtPath:[FileTool getTemPath:fileName]];
}
+(BOOL)deleteFileAtDocPath:(NSString *)fileName fileManager:(NSFileManager *)fm docPath:(NSString *)docPath  {
	NSString *path=[docPath stringByAppendingPathComponent:fileName];
	if ([fm fileExistsAtPath:path]) {
		[fm removeItemAtPath:path error:nil];
	}
		return YES;
}

+(BOOL)deleteFileWithFile:(NSString *)fileName fileManager:(NSFileManager *)fm {
	 
	if ([fm fileExistsAtPath:fileName]) {
		[fm removeItemAtPath:fileName error:nil];
	}
	return YES;
}

+(BOOL)deleteFileAtBundlePath:(NSString *)fileName fileManager:(NSFileManager *)fm {
	NSString *path=[FileTool getBundlePath:fileName type:@""];
	if ([fm fileExistsAtPath:path]) {
		[fm removeItemAtPath:path error:nil];
	}
		return YES;
}

	//保存游戏数据  
	//参数介绍：  
	//   (NSMutableArray *)data ：保存的数据  
	//   (NSString *)fileName ：存储的文件名  
-(BOOL) saveData:(NSMutableArray *)data  saveFileName:(NSString *)fileName  
{  
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);  
    NSString *documentsDirectory = [paths objectAtIndex:0];  
    if (!documentsDirectory) {  
        NSLog(@"Documents directory not found!");  
        return NO;  
    }  
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:fileName];  
    return ([data writeToFile:appFile atomically:YES]);  
}  
	//读取游戏数据  
	//参数介绍：  
	//   (NSString *)fileName ：需要读取数据的文件名  
-(id) loadData:(NSString *)fileName  
{  
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);  
    NSString *documentsDirectory = [paths objectAtIndex:0];  
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:fileName];  
    NSMutableArray *myData = [[[NSMutableArray alloc] initWithContentsOfFile:appFile] autorelease];  
    return myData;  
}  

@end
