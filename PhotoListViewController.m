//
//  PhotoListViewController.m
//  Health
//
//  Created by hq on 12-7-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "PhotoListViewController.h"
#import "DMAppDelegate.h"
#import "InFoViewController.h"
#import "CChild.h"
#import "MyScrollView.h"

#import "SHKItem.h"
#import "SHKActionSheet.h"
#import "shareWeiBoViewController.h"

#define ShowImage @"instanceimg.png"

#define TAGTMEP 120000
@interface PhotoListViewController ()
@property(nonatomic,retain)UINavigationController *weiboNavController;
@end


@implementation PhotoListViewController
@synthesize ScrollerPages;
@synthesize mPhotos;
@synthesize mChildId;
@synthesize isInhomeOrSchool;
@synthesize mPhotoScrollView;
@synthesize mFunctionView;
@synthesize mCommentView;
@synthesize downCount;
@synthesize weiboNavController;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        ScrollerPages = -1;
        downCount = 0;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"low memory" message:@"low memory memory" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
    

}
-(void)CloseSeltfView
{
    
    if (downCount ==0) {
        [SVProgressHUD dismiss];
        [self performSelectorOnMainThread:@selector(CloseSeltfViewThread) withObject:nil waitUntilDone:YES];
    }
}
-(void)navBack
{
    DMViewController *rootController=(DMViewController*) [UIApplication sharedApplication].keyWindow.rootViewController;
    [self CancelDownLoad];
    self.navigationController.navigationBarHidden = NO;
    [rootController setButtonBringSubviewToFront];
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)CancelDownLoad
{

    for (MyScrollView *subView in [mPhotoScrollView subviews]) {
        if ([subView isKindOfClass:[MyScrollView class]]) {
            subView.viewdelegate = nil;
            [subView removeFromSuperview];
        }

    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];

    DMViewController *rootController=(DMViewController*) [UIApplication sharedApplication].keyWindow.rootViewController;
    [rootController setButtonsendSubviewToBack];

    if (mPhotos != nil){

        {
            UIView *mView = [[UIView alloc]initWithFrame:CGRectMake(0, 490, 320, 48)];
            self.mFunctionView = mView;
            [self.view addSubview:mFunctionView];
            [mView release];
            [ViewTool addUIImageView:mFunctionView imageName:@"img_46" type:@"png" x:0 y:0 x1:320 y1:48];
             
        }

        ChildOfPhotoInfo *pObj = [mPhotos objectAtIndex:ScrollerPages];

        self.title = [NSString stringWithFormat:@"描述:%@",pObj.childPhotoComment];


        
        {
            UIScrollView *mScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, 320, 460)];
            [mScrollView setBackgroundColor:[UIColor blackColor]];
            mScrollView.delegate = self;
            [mScrollView setPagingEnabled:YES];
            [mScrollView setContentSize:CGSizeMake(320*[mPhotos count], 0)];
            self.mPhotoScrollView = mScrollView;
            [mScrollView release];
            
            [self.view addSubview:mPhotoScrollView];
            NSLog(@"ScrollerPages %d",ScrollerPages);
            [self loadScrollViewWithPage:ScrollerPages-1];
            [self loadScrollViewWithPage:ScrollerPages];
            [self loadScrollViewWithPage:ScrollerPages+1];

            [mPhotoScrollView setContentOffset:CGPointMake(mPhotoScrollView.frame.size.width * ScrollerPages, 0)];
            NSLog(@"mPhotoScrollView Wid:%f",mPhotoScrollView.frame.size.width * ScrollerPages);
        }
        
        
        
       
        {
            UIButton *btn = [ViewTool addUIButton:mFunctionView imageName:@"第一张.png" type:@"" x:50 y:7 x1:30 y1:30];
            [btn addTarget:self action:@selector(SetFirstPhoto) forControlEvents:64];
  
        }
        {
            UIButton *btn = [ViewTool addUIButton:mFunctionView imageName:@"最后一张.png" type:@"" x:240 y:7 x1:30 y1:30];
            [btn addTarget:self action:@selector(SetEndPhoto) forControlEvents:64];
            
        }
        {
            UIButton *btn = [ViewTool addUIButton:mFunctionView imageName:@"微博分享.png" type:@"" x:0 y:7 x1:26 y1:26];
            [btn addTarget:self action:@selector(weiboFunction) forControlEvents:64];
            
        }
        UIButton *leftPhoto = [ViewTool addUIButton:mFunctionView imageName:@"上一个.png" type:@"" x:100 y:7 x1:30 y1:30];
        [leftPhoto addTarget:self action:@selector(SetPhotoLeft) forControlEvents:64];
        
        UIButton *rightPhoto = [ViewTool addUIButton:mFunctionView imageName:@"下一个.png" type:@"" x:180 y:7 x1:30 y1:30];
        [rightPhoto addTarget:self action:@selector(SetPhotoRight) forControlEvents:64];
      
        
        if ((([Instance GetUseType] == LOGIN_BIND_P || [Instance GetUseType] == LOGIN_NOBIND_P) && isInhomeOrSchool == NO) || ([Instance GetUseType] == LOGIN_TEACH && isInhomeOrSchool == YES)) {
            UIButton *delPhoto = [ViewTool addUIButton:mFunctionView imageName:@"删除桶.png" type:@"" x:280 y:7 x1:30 y1:30];
            [delPhoto addTarget:self action:@selector(DelegatePhoto) forControlEvents:64];
        }
 
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"加载图片失败" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    
}

#pragma mark  file
-(NSString*)setFileFormat:(NSString *)String
{
    NSArray *list=[String componentsSeparatedByString:@"/"];
    
    return [list objectAtIndex:[list count]-1];
    
    
}
///------------------     获取路径　并创建文件夹
-(NSString*)filePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    documentDirectory =  [documentDirectory stringByAppendingPathComponent:@"big"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:documentDirectory]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:documentDirectory withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return documentDirectory;
}
////------------------ 获取文件路径
-(NSString *)documentFilePath:(NSString *)String
{
    NSString *document = [self filePath];
    return [document stringByAppendingPathComponent:[self setFileFormat:String]];
}
-(BOOL)isFileExistPath:(NSString *)String
{
    NSString *documentDirectory = [self filePath];
    
    documentDirectory =  [documentDirectory stringByAppendingPathComponent:[self setFileFormat:String]];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:documentDirectory]) {
        return YES;
    }
    else
        return NO;
    
}
#pragma mark

-(void)DownLoadButtonImage:(NSString*)UrlString
{
    
    
}

#pragma mark  load imageview 
-(void)StartDownLoadImage:(NSString*)url  tag:(int)index
{

    
    downCount = -1;
    ASIHTTPRequest * request ;
    request =[ASIHTTPRequest requestWithURL:[NSURL URLWithString:url]];
    [request setDownloadDestinationPath:[self documentFilePath:url]];
    request.tag = index;
	[request setDelegate:self];
	[request setTimeOutSeconds:120];
	[request setNumberOfTimesToRetryOnTimeout:10];
	[request setDownloadProgressDelegate:self];
	[request startAsynchronous];

    

}
- (void)requestFinished:(ASIHTTPRequest *)request
{
    MyScrollView *Myimageview = (MyScrollView*)[mPhotoScrollView viewWithTag:request.tag];
    ChildOfPhotoInfo *pObj = [mPhotos objectAtIndex:request.tag-TAGTMEP];

    if (Myimageview !=nil) {
        if ([self isFileExistPath:pObj.picUrl]) {
            [Myimageview setImage:[UIImage imageWithContentsOfFile:[self documentFilePath:pObj.picUrl]]];
        }
    }
    downCount = 0;
}
#pragma mark finish download image

- (void)loadScrollViewWithPage:(int)page
{
    if (page < 0)
        return;
    if (page >= [mPhotos count])
        return;

    CGRect frame = mPhotoScrollView.frame;
    frame.origin.x = frame.size.width * page;
    ChildOfPhotoInfo *pObj = [mPhotos objectAtIndex:page];
    
    MyScrollView *MyAscrView = (MyScrollView *)[mPhotoScrollView viewWithTag:TAGTMEP+page];
    if (MyAscrView == nil) {
        MyScrollView *ascrView = [[MyScrollView alloc] initWithFrame:frame];
        ascrView.viewdelegate = self;
        ascrView.tag = TAGTMEP+page;
        [mPhotoScrollView addSubview:ascrView];
        [ascrView release];
        
        if ([self isFileExistPath:pObj.picUrl]) {
            [ascrView setImage:[UIImage imageWithContentsOfFile:[self documentFilePath:pObj.picUrl]]];
        }
        
        else
        {
            [SVProgressHUD show];
            [self StartDownLoadImage:pObj.picUrl tag:TAGTMEP+page];

        }

    }

}
-(void)MyScrollViewSingTouch
{
    
    [self.view bringSubviewToFront:mFunctionView];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3f];
    if (mFunctionView.frame.origin.y == 490) {
        [mFunctionView setFrame:CGRectMake(0, 460-48-44, 320, 48)];
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
    else {
        [mFunctionView setFrame:CGRectMake(0, 490, 320, 48)];
        [self.navigationController setNavigationBarHidden:YES animated:YES];

    }
    [UIView commitAnimations];
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    // Switch the indicator when more than 50% of the previous/next page is visible
    if (!decelerate) {
        [self LoadImageForScroll:scrollView];
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self LoadImageForScroll:scrollView];
}
-(void)LoadImageForScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    ScrollerPages = page;
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
    [self FrushDescription];
    
 
}
-(void) setNormalSize;
{}
-(void) setNotNormalSize ;
{}
-(void)setsizeimage;
{}

-(void)SetPhotoToShare:(int)index
{
    
    UIActionSheet *pickerSheet = [[UIActionSheet alloc]initWithTitle:@"请选择图片" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"新浪微博",@"腾讯微博",nil ];
    pickerSheet.delegate = self;
    pickerSheet.tag = 10000001;
	[pickerSheet showFromToolbar:self.navigationController.toolbar];
    [pickerSheet release];
    
    mShareIndex = index;
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
//    ChildOfPhotoInfo *pObj = [mPhotos objectAtIndex:mShareIndex];
    
//    NSArray *shareList = [NSArray arrayWithObjects:pObj.picUrl, nil];
    
//    SHKShareItemController * shareController = [[SHKShareItemController alloc] initWithImageList:shareList];
//    if (buttonIndex == 0) {
//        shareController.defaultItem = 1;
//    }   
//    if (buttonIndex == 1) {
//        shareController.defaultItem = 3;
//    }
//    [self.navigationController setNavigationBarHidden:NO];
//    [self.navigationController.navigationBar setTintColor:[UIColor blackColor]];
//    [self.navigationController pushViewController:shareController animated:YES];
//    [shareController release];

}

-(void)FrushDescription
{
    if (ScrollerPages < 0 || ScrollerPages>=[mPhotos count]-1) {
        return;
    }
    ChildOfPhotoInfo *mChildInfo = [mPhotos objectAtIndex:ScrollerPages];
    if ((UITextView *)[mCommentView viewWithTag:10101010]!=nil) {
        ((UITextView *)[mCommentView viewWithTag:10101010]).text = [NSString stringWithFormat:@"描述:%@",mChildInfo.childPhotoComment];
    }
}

-(void)ScrollerContentOffset:(int)page onAinmate:(BOOL)onainmate
{
    if (page < 0 || page>=[mPhotos count]) {
        return;
    }
    if (onainmate) {
        [mPhotoScrollView setContentOffset:CGPointMake(page *mPhotoScrollView.frame.size.width, 0 ) animated:YES];
    }
    else
        [mPhotoScrollView setContentOffset:CGPointMake(page *mPhotoScrollView.frame.size.width, 0 )];
    [self FrushDescription];
}
#pragma mark 左边 --

-(void)SetPhotoLeft
{
    if (ScrollerPages == 0){
        return;
    }
    ScrollerPages--;
    [self loadScrollViewWithPage:ScrollerPages-1];
    [self loadScrollViewWithPage:ScrollerPages];
    [self loadScrollViewWithPage:ScrollerPages+1];
    
    [self ScrollerContentOffset:ScrollerPages onAinmate:YES];

}
#pragma mark 右边 ++

-(void)SetPhotoRight
{
    if (ScrollerPages >= [mPhotos count]-1) {
        return;
    }
    ScrollerPages++;
    [self loadScrollViewWithPage:ScrollerPages-1];
    [self loadScrollViewWithPage:ScrollerPages];
    [self loadScrollViewWithPage:ScrollerPages+1];
    
    [self ScrollerContentOffset:ScrollerPages onAinmate:YES];
}
-(void)SetFirstPhoto
{
    ScrollerPages = 0;
    NSLog(@"pageIndex  -- :%d",ScrollerPages);
    [self loadScrollViewWithPage:ScrollerPages-1];
    [self loadScrollViewWithPage:ScrollerPages];
    [self loadScrollViewWithPage:ScrollerPages+1];
    [self ScrollerContentOffset:ScrollerPages onAinmate:NO];

}
-(void)SetEndPhoto
{
    ScrollerPages= [mPhotos count]-1 ;
    NSLog(@"pageIndex  -- :%d",ScrollerPages);
    [self loadScrollViewWithPage:ScrollerPages-1];
    [self loadScrollViewWithPage:ScrollerPages];
    [self loadScrollViewWithPage:ScrollerPages+1];
    [self ScrollerContentOffset:ScrollerPages onAinmate:NO];

}
#pragma mark 删除图片
-(void)DelegatePhoto
{
    if (ScrollerPages <0 || ScrollerPages >= [mPhotos count]-1) {
        return;
    }
    
    ChildOfPhotoInfo *pObj = [mPhotos objectAtIndex:ScrollerPages];
    if (pObj == nil) {
        return;
    }
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/Photo/TPFetchChildPhotoRecordDetailOfOneBaby"];
    
    NSMutableDictionary *mDic = [[NSMutableDictionary alloc] init];
    
    [mDic setObject:[NSNumber numberWithInt:mChildId] forKey:@"ID"];
    [mDic setObject:[NSNumber numberWithInt:5] forKey:@"type"];
    [mDic setObject:[NSNumber numberWithInt:pObj.childPhotoID] forKey:@"photoID"];
    [mDic setObject:[NSNumber numberWithBool:isInhomeOrSchool] forKey:@"inSchoolOrInHome"];
    
    NSString *JSONString = [mDic JSONRepresentation];
    [mDic release];
    
    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate = self;
    [mHttpRequest CustomFormDataRequestDict:JSONString tag:0 url:urlstr];
    [self.view addSubview:mHttpRequest];
    [mHttpRequest release];
}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    [mPhotos removeObjectAtIndex:ScrollerPages];
    for (MyScrollView *subView in [mPhotoScrollView subviews]) {
        [subView removeFromSuperview];
    }
    [self loadScrollViewWithPage:ScrollerPages-1];
    [self loadScrollViewWithPage:ScrollerPages];
    [self loadScrollViewWithPage:ScrollerPages+1];

}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
    }
}

#pragma mark weibo 

-(void)weiboFunction
{
    ChildOfPhotoInfo *ChildeInfo = [mPhotos objectAtIndex:ScrollerPages];
    shareWeiBoViewController *exampleShareImage = [[shareWeiBoViewController alloc]initWithNibName:nil bundle:nil];
    exampleShareImage.shareInfo = ChildeInfo;
    [self.navigationController pushViewController:exampleShareImage animated:YES];
    [exampleShareImage release];
    [self performSelector:@selector(testOffline) withObject:nil afterDelay:0.5];

}
- (void)testOffline
{
	[SHK flushOfflineQueue];
}

-(void)dealloc
{
    [mFunctionView release];
    [mCommentView release];
    
    [mPhotos release];   
    [mPhotoScrollView release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mFunctionView = nil;
    self.mCommentView = nil;
    
    self.mPhotos = nil;
    self.mPhotoScrollView = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
