//
//  LoadingTable.h
//  Health
//
//  Created by hq on 12-6-26.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingTable : UITableViewController
{
    UIView *refreshHeaderView;
    UIImageView *refreshArrow;
    UILabel *refreshLabel;
    UIActivityIndicatorView *refreshSpinner;
    NSString *textPull;
    NSString *textRelease;
    NSString *textLoading;
    
    BOOL isDragging;
    BOOL isLoading;

}
- (void)setupStrings;
-(void)refresh;
- (void)startLoading;
@end
