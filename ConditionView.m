//
//  ConditionView.m
//  Health
//
//  Created by hq on 12-9-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ConditionView.h"


#define CompareType  1010011
#define NutritionType 1010001
#define FoodType 1010002
#define ProvinceType 1010003
#define CityType 1010004
#define SexyType 1010005
#define AgeType 1010006

@implementation ConditionView
@synthesize ConditionID;
@synthesize Delegate;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)backNav
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(backNav)];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (ConditionID == CompareType) {
        return 6;
    }
    if (ConditionID == NutritionType) {
        return 12;
    }
    if (ConditionID == AgeType) {
        return 8;
    }
    if (ConditionID == FoodType) {
        return 10;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    int mIndexRow = [indexPath row];
    // Configure the cell...
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    if (ConditionID == NutritionType) {
        NSArray *array = [[NSArray alloc] initWithObjects:@"不限",@"能量",@"碳水化合物",@"脂肪",@"蛋白质",@"钙", @"锌",@"铁",@"胡萝卜素",@"维生素A",@"维生素C",@"维生素E",nil];
        cell.textLabel.text =  [array objectAtIndex:mIndexRow];
        [array release];
    }

    if (ConditionID == CompareType) {
        NSArray *array = [[NSArray alloc] initWithObjects:@"不限",@"大于",@"大于等于",@"小于",@"小于等于",@"等于", nil];
        cell.textLabel.text =  [array objectAtIndex:mIndexRow];
        [array release];
    }
    if (ConditionID == AgeType) {
        NSArray *array = [[NSArray alloc] initWithObjects:@"不限",@"1岁",@"2岁",@"3岁",@"4岁",@"5岁",@"6岁",@"7岁", nil];
        cell.textLabel.text =  [array objectAtIndex:mIndexRow];
        [array release];
    }
    if (ConditionID == FoodType) {
        NSArray *array = [[NSArray alloc] initWithObjects:@"不限",@"谷类薯类及杂豆",@"蔬菜",@"水果",@"鱼虾",@"蛋类",@"畜禽肉类",@"大豆类及坚果", @"奶类及制品",@"油脂类",nil];
        cell.textLabel.text =  [array objectAtIndex:mIndexRow];
        [array release];
    }
    return cell;
}
#pragma mark - Table view delegate
-(void)DelegateData:(NSArray*)array index:(int)mIndexRow
{
    NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
    [mDict setObject:[array objectAtIndex:mIndexRow] forKey:@"value"];
    [mDict setObject:[NSNumber numberWithInt:mIndexRow] forKey:@"ID"];
    [mDict setObject:[NSNumber numberWithInt:ConditionID] forKey:@"Type"];
    [Delegate FrushData:mDict];
    [mDict release];

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    

    
    int mIndexRow = [indexPath row];
    // Configure the cell...
    
    

    if (ConditionID == NutritionType) {
        NSArray *array = [[NSArray alloc] initWithObjects:@"不限",@"能量",@"碳水化合物",@"脂肪",@"蛋白质",@"钙", @"锌",@"铁",@"胡萝卜素",@"维生素A",@"维生素C",@"维生素E",nil];
        [self DelegateData:array index:mIndexRow];
        [array release];
    }
    
    if (ConditionID == CompareType) {
        NSArray *array = [[NSArray alloc] initWithObjects:@"不限",@"大于",@"大于等于",@"小于",@"小于等于",@"等于", nil];
        [self DelegateData:array index:mIndexRow];
        [array release];
    }
    if (ConditionID == AgeType) {
        NSArray *array = [[NSArray alloc] initWithObjects:@"不限",@"1岁",@"2岁",@"3岁",@"4岁",@"5岁",@"6岁",@"7岁", nil];
        [self DelegateData:array index:mIndexRow];
        [array release];
    }
    if (ConditionID == FoodType) {
        NSArray *array = [[NSArray alloc] initWithObjects:@"不限",@"谷类薯类及杂豆",@"蔬菜",@"水果",@"鱼虾",@"蛋类",@"畜禽肉类",@"大豆类及坚果", @"奶类及制品",@"油脂类",nil];
        [self DelegateData:array index:mIndexRow];
        [array release];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


@end
