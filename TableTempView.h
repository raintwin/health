//
//  TableTempView.h
//  Health
//
//  Created by jiaodian on 12-11-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TableTempViewDelegate<NSObject>

-(void)ReturnHttpData:(NSString*)ResultContent container:(NSMutableArray*)arrays tag:(int)tag isFinish:(BOOL)isFinish;
-(void)SetTableData:(UITableViewCell*)cell indexpath:(NSIndexPath*)indexPath datas:(NSMutableArray *)mDatas;
-(void)TableDidSelect:(NSIndexPath*)indexPath Datas:(NSMutableArray*)mDatas;
-(void)TableDownLoadCellImage;
-(void)deselectRow:(NSIndexPath *)indexPath;
@end
@interface TableTempView : UIView<HttpFormatRequestDeleagte,UITableViewDataSource,UITableViewDelegate>
{
    BOOL isLoading,isLoadOver,isCase;
    int mPageStart;
    id<TableTempViewDelegate>delegate;
}

@property(nonatomic,assign) int TableHeightForRow;
@property(nonatomic,assign) BOOL isLoading,isLoadOver,isLoadImage,isCase;
@property(nonatomic,assign) BOOL commitEditing;
@property(nonatomic,retain) NSString *UrlString;
@property(nonatomic,retain) NSMutableArray *requestDatas;
@property(nonatomic,retain) NSMutableDictionary *DataDict;
@property(nonatomic,retain) UITableView *mTableView;

@property(nonatomic,assign) id<TableTempViewDelegate>delegate;

@property(nonatomic,retain) NSMutableDictionary *mKeyDict;

-(void)GetDataForHttp;
-(void)deleteRow:(NSMutableDictionary *)dict;
@end
