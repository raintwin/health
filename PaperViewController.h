//
//  PaperViewController.h
//  Health
//
//  Created by hq on 12-6-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface PaperViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate>
{
    
    int ePageStart;
//------------------
    

    
    BOOL isDragging;
    BOOL isLoading;
    
    
    UITapGestureRecognizer *mTapRecognizer;
    
}
///-----
@property(nonatomic,retain)UIView *refreshHeaderView;
@property(nonatomic,retain)UIImageView *refreshArrow;
@property(nonatomic,retain)UIActivityIndicatorView *refreshSpinner;
@property(nonatomic,retain)UILabel *refreshLabel;

//-----
@property(nonatomic,retain)	NSMutableArray		*chatArray;
@property(nonatomic,assign)int notReadCount,childid;





@property(nonatomic,retain)NSMutableDictionary *mIconDownDict;
-(void) autoMovekeyBoard: (BOOL) h;

//--------
-(void)refresh;
- (void)startLoading;


@end
