//
//  NotifyListViewController.h
//  Health
//
//  Created by hq on 12-6-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SendNotifyViewController.h"
#import "DownLoadTableImage.h"
#import "TableTemplateView.h"
#import "TableTempView.h"
@interface NotifyListViewController : UIViewController<DownLoadImageDelegate,SendNotifyDelegate,TableTempViewDelegate>
{

    
}

-(void)StartDownLoadImage:(NSString*)url IndexPath:(NSIndexPath*)indexpath ;
-(void)FinishDownLoadImage:(NSIndexPath*)indexpath Image:(UIImage*)image IsFailed:(BOOL)isFaild;


@property(nonatomic,retain)NSMutableDictionary *mDownLoadImageDict;



@property(nonatomic,retain)TableTempView *mTableTemplateView;

@end
