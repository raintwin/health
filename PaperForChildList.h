//
//  PaperForChildList.h
//  Health
//
//  Created by hq on 12-6-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DownLoadTableImage.h"




@interface PaperForChildList : UIViewController<UITableViewDelegate,UITableViewDataSource,DownLoadImageDelegate,HttpFormatRequestDeleagte>
{
    
    BOOL isSuccess;
}

@property(nonatomic,retain) NSMutableArray *mChilds;

@property(nonatomic,retain)UITableView *mTable;
@property(nonatomic,retain)NSMutableDictionary *mDownLoadImageDict;

@end

