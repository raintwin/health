//
//  DisViewShow.m
//  Health
//
//  Created by hq on 12-8-22.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "DisViewShow.h"
#import "JSON.h"
#import "Display.h"
#import "CChild.h"
#import "DisView.h"
@implementation DisViewShow
@synthesize mChildOfDisID;
@synthesize mChildID;
@synthesize TYPE;
@synthesize mDisContens,mChilds;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        TYPE = -1;
        mChildID = -1 ;
        mChildOfDisID = -1;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)backBtn
{

    [self.navigationController popViewControllerAnimated:YES];
}
-(NSString*)DisType:(int)type
{
    switch (type) {
        case 0:
            return @"日评价";
            break;
        case 1:
            return @"周评价";
            break;
        case 2:
            return @"期末评价";
            break;
            
        default:
            break;
    }
    return nil;
}
-(NSString*)CommonTypeImage:(int)tag
{
    switch (tag) {
        case 0:
            return  @"日评价.png";
            break;
        case 1:
            return @"周评价.png";
            break;
        case 2:
            return @"期末评价.png";
            break;
        default:
            break;
    }
    return nil;
}
#pragma mark refresh
-(void)RefreshView:(NSMutableArray*)item display:(DisplayInfo*)dis
{
    //------------------- 类别
//    self.title = dis.m
    if ((UITextView*)[self.view viewWithTag:100001]!=nil) {
        UITextView *textview =  (UITextView*)[self.view viewWithTag:100001];
        textview.text = [NSString stringWithFormat:@"类别:%@",[self DisType:dis.mDisType]];
        if ((UIImageView *)[textview viewWithTag:1000011]) {
            ((UIImageView *)[textview viewWithTag:1000011]).image = [UIImage imageNamed:[self CommonTypeImage:dis.mDisType]];
        }
    }
    //------------------- 表现
    UIScrollView *scroll = (UIScrollView*)[self.view viewWithTag:100002];
    if (scroll!=nil) {
        NSString * desc = dis.mDisComment;  
        CGSize size = [desc sizeWithFont : [UIFont  systemFontOfSize:14]  //设置计算size的字体与字号                           
                       constrainedToSize : CGSizeMake(280, 2000)           //根据显示控件宽度不同，285需要调整                              
                           lineBreakMode : UILineBreakModeWordWrap];   //设置换行的截断方式  
        UILabel *lblLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 280,  size.height+10)];  
        [lblLabel setNumberOfLines : 0];                                                             //设置lable的最大行数，设置为0表示无行数限制
        [lblLabel setFont : [UIFont  systemFontOfSize : 30]];  
        [lblLabel setText : desc]; 
        
        UITextView *conText =(UITextView*)[scroll viewWithTag:1000021];
        if (conText != nil) {
            [conText setFrame: CGRectMake(0, 0, 280,  size.height+10)];
            [conText setFont : [UIFont systemFontOfSize : 14]];               
            [conText setText : desc];
        }
        [lblLabel release];
        
        
        {
            UIScrollView *scrollDis = [[UIScrollView alloc] initWithFrame:CGRectMake(10, conText.frame.size.height +30, 300, 120)];
            [scrollDis setBackgroundColor:HEXCOLOR(0xf4f4f4)];
            [scrollDis setPagingEnabled:YES];
            [scroll addSubview:scrollDis];
            scrollDis.delegate = self;
            [scrollDis release];
            NSArray *array;
            
            if (TYPE == INSCHOOL ) {
                array = [[NSArray alloc] initWithObjects:@"礼貌2.png",@"卫生2.png",@"学习2.png",@"锻炼2.png",@"游戏2.png",@"睡眠2.png",@"劳动2.png",@"进餐2.png", nil];
            }
            if (TYPE == INHOME) {
                array = [[NSArray alloc] initWithObjects:@"敬长辈.png",@"不挑食.png",@"会收拾.png",@"爱清洁.png",@"讲道理.png",@"自己睡.png", nil];
                
                
            }
            [scrollDis setContentSize:CGSizeMake([array count] *150,0)];
            
            UIPageControl *pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(10, scrollDis.frame.origin.y + scrollDis.frame.size.height, 300, 20)];
            pageControl.tag = 111001;
            pageControl.numberOfPages = [array count]/2;
            pageControl.currentPage = 0;
            pageControl.backgroundColor = [UIColor blackColor];
            [scroll addSubview:pageControl];
            [pageControl release];
            
            
            for (int i =0; i<[array count]; i++) {
                NSString *str = [array objectAtIndex:i];
                DisViewImage *disv = [[DisViewImage alloc]initWithFrame:CGRectMake(150*i , 0, 160, 100)];
                [disv SetRefresh:str];
                [disv SetDisNumber:[[item objectAtIndex:i]intValue]];
                [scrollDis addSubview:disv];
                [disv release];
            }
            [array release];
            [scroll setContentSize:CGSizeMake(0, scrollDis.frame.origin.y + scroll.frame.size.height)];

        }
    }
    // -------------- 发布时间
    {
        if ((UITextView*)[self.view viewWithTag:100003]!=nil) {
            ((UITextView*)[self.view viewWithTag:100003]).text = [NSString stringWithFormat:@"%@ 于 %@",dis.mDiserName,dis.mDisDate];
            ((UITextView*)[self.view viewWithTag:100003]).textAlignment = UITextAlignmentRight;
        }
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;

    UIScrollView *scroll = (UIScrollView*)[self.view viewWithTag:100002];
    if (scroll !=nil) {
        UIPageControl *pageControl = (UIPageControl*)[scroll viewWithTag:111001];
        if (pageControl!=nil) {
            pageControl.currentPage = page;
        }
    }
    
}
-(void)InitDisView
{
    {
        UIScrollView *scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 40, 320, 300)];
        scroll.tag = 100002;
        scroll.delegate = nil;
        [scroll setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:scroll];
        [scroll release];
        
        {
            UITextView *textview = [[UITextView alloc]initWithFrame:CGRectMake(20, 0, 320, 50)];
            textview.font = [UIFont boldSystemFontOfSize:15];
            textview.textAlignment = UITextAlignmentLeft;
            textview.backgroundColor = [UIColor whiteColor];
            textview.editable = NO;
            [textview setUserInteractionEnabled:NO];
            textview.text = @"";
            textview.tag = 1000021;
            [scroll addSubview:textview];
            [textview release];
        }
        
        
        
    }   
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(backBtn)];
//    [Instance baiduApi:@"lookdiv" eventlabel:@"lookdiv"];

    //BaiduMobStat* statTracker = [BaiduMobStat defaultStat];    [statTracker logEvent:@"lookdiv" eventLabel:@"lookdiv"];
    
    {
        UITextView *textview = [[UITextView alloc]initWithFrame:CGRectMake(0, 0, 320, 40)];
        textview.font = [UIFont boldSystemFontOfSize:15];
        textview.textAlignment = UITextAlignmentLeft;
        textview.backgroundColor = [UIColor grayColor];
        textview.editable = NO;
        [textview setUserInteractionEnabled:NO];
        textview.text = @"";
        textview.tag = 100001;
        [self.view addSubview:textview];
        [textview release];
        
        {
            UIImageView *markImg = [[UIImageView alloc]initWithFrame:CGRectMake(260, 3, 26, 32)];
            [markImg setImage:[UIImage imageNamed:@"日.gif"]];
            markImg.tag = 1000011;
            [textview addSubview:markImg];
            [markImg release];
        }

    }
    [self InitDisView];
    {
        UITextView *textview = [[UITextView alloc]initWithFrame:CGRectMake(0, 340, 320, 30)];
        textview.font = [UIFont boldSystemFontOfSize:15];
        textview.textAlignment = UITextAlignmentLeft;
        textview.backgroundColor = [UIColor grayColor];
        textview.editable = NO;
        [textview setUserInteractionEnabled:NO];
        textview.text = @"";
        textview.tag = 100003;
        [self.view addSubview:textview];
        [textview release];
        
        
    }
    [self performSelector:@selector(GetChildDisRecorder)];
}
#pragma mark - View lifecycle
-(void)GetChildDisRecorder
{    
    NSMutableDictionary*mDict = [[NSMutableDictionary alloc] init];
    
    NSString *urlstr;
    if (TYPE == INHOME) {
        urlstr= [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/ChildRepresentation/TPFetchChildRepresentationOneRecordDetailOfOneBabyInHome"];

    }
    if (TYPE == INSCHOOL) {
        urlstr= [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/ChildRepresentation/TPFetchChildRepresentationOneRecordDetailOfOneBabyInSchool"];

    }
    
    [mDict setObject:[NSNumber numberWithInt:mChildOfDisID] forKey:@"RepresentationID"];
    [mDict setObject:[NSNumber numberWithInt:mChildID] forKey:@"ChildID"];
    
    
    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate = self;
    [mHttpRequest CustomFormDataRequestDict:[mDict JSONRepresentation] tag:0 url:urlstr];
    [self.view addSubview:mHttpRequest];
    [mDict release];
    [mHttpRequest release];
    

}

#pragma  mark requestFinished
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag{
    
    NSMutableArray *mDisInfos = [[NSMutableArray alloc] init];
    
    if (TYPE == INSCHOOL) {
        NSArray *array = [[[ResultContent JSONValue] valueForKey:@"personOfChildRepresentationInSchool"] retain];
        
        DisplayInfo *disinfo = [[DisplayInfo alloc]init];
        disinfo.mDisDate = [NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"publishDateTime"]];
        disinfo.mDisType = [[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"type"]] intValue];
        disinfo.mDisComment = [NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"comment"]];
        disinfo.mDiserID = [[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"publishPersonID"]] intValue];
        disinfo.mDiserName = [NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"publishPersonName"]];
        
        for (int i = 0; i<8; i++) {
            [mDisInfos addObject:[NSNull null]];
        }
        [mDisInfos insertObject:[NSNumber numberWithInt:[[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"courtesy"]] intValue] ] atIndex:0];
        [mDisInfos insertObject:[NSNumber numberWithInt:[[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"hygienism"]] intValue] ] atIndex:1];
        [mDisInfos insertObject:[NSNumber numberWithInt:[[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"study"]] intValue] ] atIndex:2];
        [mDisInfos insertObject:[NSNumber numberWithInt:[[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"takeExercise"]] intValue] ] atIndex:3];
        [mDisInfos insertObject:[NSNumber numberWithInt:[[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"game"]] intValue] ] atIndex:4];
        [mDisInfos insertObject:[NSNumber numberWithInt:[[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"sleep"]] intValue] ] atIndex:5];
        [mDisInfos insertObject:[NSNumber numberWithInt:[[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"labour"]] intValue] ] atIndex:6];
        [mDisInfos insertObject:[NSNumber numberWithInt:[[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"eatAbility"]] intValue] ] atIndex:7];

        
        self.mDisContens = mDisInfos;
        [mDisInfos release];
        NSMutableArray *Childs = [[NSMutableArray alloc]init];
        
        for (int i = 0; i< [array count]; i++) {
            CChild *nobj = [[CChild alloc] init];
            nobj.cID = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"ID"]] intValue];
            nobj.cPhotoUrl = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"photo"]]; 
            nobj.cName = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"name"]];
            [Childs addObject:nobj];
            [nobj release];
        }
        
        int isSave = [[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"pairRepresentationIDInHome"]] intValue];
        if (isSave!=-1) {
            mChildOfDisID = isSave;
            [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"在家" target:self selector:@selector(GoDisInHome)];
        }

        self.mChilds = Childs;
        [Childs release];
        [array release];

        [self RefreshView:mDisContens display:disinfo];
        [disinfo release];

        return;
    }
    if (TYPE == INHOME) {
        
        DisplayInfo *disinfo = [[DisplayInfo alloc]init];
        disinfo.mDisDate = [NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"publishDateTime"]];
        disinfo.mDisType = [[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"type"]] intValue];
        disinfo.mDisComment = [NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"comment"]];
        disinfo.mDiserID = [[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"publishPersonID"]] intValue];
        disinfo.mDiserName = [NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"publishPersonName"]];
        
        
        for (int i = 0; i<6; i++) {
            [mDisInfos addObject:[NSNull null]];
        }
        [mDisInfos insertObject:[NSNumber numberWithInt:[[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"respectOlder"]] intValue] ] atIndex:0];
        [mDisInfos insertObject:[NSNumber numberWithInt:[[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"notPickerFood"]] intValue] ] atIndex:1];
        [mDisInfos insertObject:[NSNumber numberWithInt:[[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"likeActivity"]] intValue] ] atIndex:2];
        [mDisInfos insertObject:[NSNumber numberWithInt:[[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"likeClean"]] intValue] ] atIndex:3];
        [mDisInfos insertObject:[NSNumber numberWithInt:[[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"speekTruth"]] intValue] ] atIndex:4];
        [mDisInfos insertObject:[NSNumber numberWithInt:[[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"selfSleep"]] intValue] ] atIndex:5];

        int isSave = [[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"pairRepresentationIDInSchool"]] intValue];
        if (isSave!=-1) {
            mChildOfDisID = isSave;
            [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"在园" target:self selector:@selector(GoDisInScholl)];
        }
        self.mDisContens = mDisInfos;
        [mDisInfos release];
        

        [self RefreshView:mDisContens display:disinfo];
        [disinfo release];
        return;
    }

}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{}

-(void)GoDisInScholl
{
    TYPE = INSCHOOL;
    UIScrollView *scroll = (UIScrollView*)[self.view viewWithTag:100002];
    if (scroll != nil) {
        [scroll removeFromSuperview];
        [self InitDisView];
    }
    [self performSelector:@selector(GetChildDisRecorder)];
}
-(void)GoDisInHome
{
    TYPE = INHOME;
    UIScrollView *scroll = (UIScrollView*)[self.view viewWithTag:100002];
    if (scroll != nil) {
        [scroll removeFromSuperview];
        [self InitDisView];
    }

    [self performSelector:@selector(GetChildDisRecorder)];

}

-(void)dealloc
{
    [mDisContens release];
    [mChilds release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mDisContens = nil;
    self.mChilds = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
