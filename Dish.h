//
//  Dish.h
//  Health
//
//  Created by hq on 12-7-31.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Dish : NSObject
@property(nonatomic,retain) NSString *DishName,*DishWeight;
@property(nonatomic,assign) int DishID;
@end
