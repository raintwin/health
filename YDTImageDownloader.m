//
//  YDTImageDownloader.m
//  YDTClient
//
//  Created by  hq on 11-10-27.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "YDTImageDownloader.h"


@implementation YDTImageDownloader

@synthesize ImageURL = strImageURL;

@synthesize IndexPathTo = objIndexPathTo,Delegate = objDelegate;

@synthesize ImageData = objImageData;
@synthesize Connection = objConnection;

@synthesize ImageWidth = nImageWidth, ImageHeight = nImageHeight;
@synthesize mImage;
@synthesize mImageView;
#pragma mark -
#pragma mark override function
#pragma mark -

-(id) init
{
	if (self = [super init]) 
	{
		nImageWidth = nImageHeight = 30;
	}
	
	return self;
}
-(void) dealloc
{
	if (self.Connection) 
	{
		[self.Connection cancel];
	}
	self.ImageData = nil;
	self.Connection = nil;
	
	[strImageURL release];

	
	[objIndexPathTo release];
	


	
	[super dealloc];
}


#pragma mark interface function to outside
#pragma mark -
-(BOOL) StartDownloadImage
{
    self.ImageData = [NSMutableData data];

    objConnection = [[NSURLConnection alloc] initWithRequest:
					   [NSURLRequest requestWithURL:
						[NSURL URLWithString:self.ImageURL] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30] delegate:self];

//	NSLog1(@"laod image from url:%@", self.ImageURL);
	
	return YES;
}
-(BOOL) CancelDownloadImage
{
	if (self.Connection) 
	{
		[self.Connection cancel];
	}
	self.ImageData = nil;
	self.Connection = nil;
	
	return YES;
}






#pragma mark Download support (NSURLConnectionDelegate)
#pragma mark -
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.ImageData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	self.ImageData = nil;
	self.Connection = nil;
	
//	[self.Delegate FinishededImageLoad:self.IndexPathTo Image:nil IsFailed:YES];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // Set appIcon and clear temporary data/image
    UIImage *image = [[UIImage alloc] initWithData:self.ImageData];

	self.ImageData = nil;
	self.Connection = nil;
	
	
	if (image.size.width == 0)
	{
//		[self.Delegate FinishededImageLoad:self.IndexPathTo Image:nil IsFailed: YES];
	}
	else 
	{
		if (image.size.width != nImageWidth && image.size.height != nImageHeight)
		{
			CGSize itemSize = CGSizeMake(nImageWidth, nImageHeight);
			UIGraphicsBeginImageContext(itemSize);
			CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
			[image drawInRect:imageRect];
			[image release];
			image = UIGraphicsGetImageFromCurrentImageContext();
			[image retain];
			UIGraphicsEndImageContext();
		}	
		[self.Delegate SetImageView:image];
	}
//    self.mImage = image;
    mImageView.image = image;
	[image release]; 

}

@end
