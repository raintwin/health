//
//  HBTEventDetailHttp.m
//  Health
//
//  Created by dinghao on 13-4-15.
//
//

#import "HBTEventDetailHttp.h"

@implementation HBTEventDetailHttp

+(void)DictAddDataString:(NSUserDefaults *)defaults Key:(NSString*)KeyString Value:(NSString*)ValueString
{
    if ([ValueString class] == [NSNull class]) {
        return;
    }
    if ([ValueString length] > 0) {
        [defaults setObject:ValueString forKey:KeyString];
    }
}

+(void)eventSigninDetail:(NSMutableDictionary *)valueData;
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    
    [defaults setObject:[NSNumber numberWithBool:YES] forKey:NSLocalizedString(@"PS_Sign", nil)];

    NSLog(@"[valueString objectForKey:%@",[[valueData objectForKey:@"userID"] stringValue]);
    // ----------- 幼儿信息
    [defaults setObject:[valueData objectForKey:@"userID"] forKey:@"userID"];
    [defaults setObject:[valueData objectForKey:@"childSchoolID"] forKey:@"childSchoolID"];
    [defaults setObject:[valueData objectForKey:@"provinceID"] forKey:@"provinceID"];
    [defaults setObject:[valueData objectForKey:@"childTeacher"] forKey:@"childTeacher"];
    
    [defaults setObject:[valueData objectForKey:@"cityID"] forKey:@"cityID"];
    [defaults setObject:[valueData objectForKey:@"userID"] forKey:@"userID"];
    [defaults setObject:[valueData objectForKey:@"childClassID"] forKey:@"childClassID"];
    
    [defaults setObject:[valueData objectForKey:@"uarentOrTeacher"] forKey:@"uarentOrTeacher"];
    
    [defaults setObject:[valueData objectForKey:@"uarentOrTeacher"] forKey:@"uarentOrTeacher"];
    [defaults setObject:[valueData objectForKey:@"openUser"] forKey:@"openUser"];

    
    [self DictAddDataString:defaults Key:@"childClassName" Value:[valueData objectForKey:@"childClassName"]];
    [self DictAddDataString:defaults Key:@"childName" Value:[valueData objectForKey:@"childName"]];
    [self DictAddDataString:defaults Key:@"cityName" Value:[valueData objectForKey:@"cityName"]];
    [self DictAddDataString:defaults Key:@"provinceName" Value:[valueData objectForKey:@"provinceName"]];
    
    [self DictAddDataString:defaults Key:@"photo" Value:[valueData objectForKey:@"photo"]];
    [self DictAddDataString:defaults Key:@"childGender" Value:[valueData objectForKey:@"childGender"]];
    [self DictAddDataString:defaults Key:@"childBirthday" Value:[valueData objectForKey:@"childBirthday"]];
    [self DictAddDataString:defaults Key:@"childSchoolName" Value:[valueData objectForKey:@"childSchoolName"]];
    [self DictAddDataString:defaults Key:@"timeOutMsg" Value:[valueData objectForKey:@"timeOutMsg"]];

    [defaults setObject:[valueData objectForKey:@"isTimeOut"] forKey:@"isTimeOut"];

    [defaults synchronize];
}
+ (BOOL)accountsPastAlert
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    BOOL isPast = [[defaults objectForKey:@"isTimeOut"] boolValue];
    return isPast;
}
+(NSString *)getTimeOutMsg;
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *timeOutMsg = [defaults objectForKey:@"timeOutMsg"];
    if ([timeOutMsg length] > 0) {
        return timeOutMsg;
    }
    return nil;
}
@end
