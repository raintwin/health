//
//  PhotoFolderViewController.h
//  Health
//
//  Created by hq on 12-7-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DownLoadTableImage.h"


@interface PhotoFolderViewController : UIViewController<DownLoadImageOfScrollDelegate,UIScrollViewDelegate,HttpFormatRequestDeleagte>
{
    int mPageStart;
}
@property(nonatomic,retain)NSMutableDictionary *mDownLoadImageDict;
@property(nonatomic,retain)NSMutableArray *photoArray;

@property(nonatomic,assign)int mChildID;
@property(nonatomic,assign)BOOL isInhomeOrSchool;


-(void)InitScrollView:(NSMutableArray*)array;
-(void)StartDownLoadImage:(NSString*)url  tag:(int)index;

-(void)FinishDownLoadImageOfScrooll:(int)index Image:(UIImage*)image IsFailed:(BOOL)isFaild;


@end
