//
//  ChildListPhotoViewCtr.h
//  Health
//
//  Created by jiaodian on 12-11-22.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableTemplateView.h"
#import "DownLoadTableImage.h"
@interface ChildListPhotoViewCtr : UIViewController<DownLoadImageDelegate,TableTemplateViewDelegate>
{
    int mPageStart;

}

@property(nonatomic,assign)BOOL OnHome, isInhomeOrSchool;

@property(nonatomic,retain)NSMutableDictionary *mDownLoadImageDict;

@property(nonatomic,retain)TableTemplateView *mTableTemplateView;
@end
