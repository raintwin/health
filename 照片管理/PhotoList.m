//
//  PhotoList.m
//  Health
//
//  Created by dinghao on 13-3-26.
//
//

#import "PhotoList.h"
#import "CChild.h"
#import "SendPhotoViewController.h"

#define mark 10000


#define PhotoUrl     @"/Photo/TPFetchChildPhotoRecordsOfOneBaby"
#define mPageCount 100000
#define mPageStart 0
@interface PhotoList ()
{

}

@end

@implementation PhotoList
@synthesize mDictParameter;
@synthesize mPhotoLists;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        NSMutableArray *array = [[NSMutableArray alloc]init];
        self.mPhotoLists  = array;
        [array release];
        
        
        isLoading = NO;
    }
    return self;
}
-(void)dealloc
{
    [mDictParameter release];
    [mPhotoLists release];
    [super dealloc];
}
-(void)viewDidUnload
{
    self.mDictParameter = nil;
    self.mPhotoLists    = nil;
    [super viewDidUnload];
}
-(void)navigationBack
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)TakePhoto
{
    SendPhotoViewController *detailViewController = [[SendPhotoViewController alloc] initWithNibName:@"SendPhotoViewController" bundle:nil];
    
    detailViewController.title = @"发照片";
    
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];
    
}
-(void)itemRight:sender
{
    UIButton *btn = sender;
    switch (btn.tag) {

            [mDictParameter setObject:[NSNumber numberWithBool:false] forKey:@"inSchoolOrInHome"];

            break;
        case 1:

            [mDictParameter setObject:[NSNumber numberWithBool:TRUE] forKey:@"inSchoolOrInHome"];


            break;
        default:
            break;
    }
    [NSThread detachNewThreadSelector:@selector(nsthread) toTarget:self withObject:nil];

}

- (void)viewDidLoad
{
    [super viewDidLoad];

    {
        NSMutableDictionary *mDic = [[NSMutableDictionary alloc] init];
        [mDic setObject:[NSNumber numberWithInt:[[Instance GetUseID] intValue]] forKey:@"ID"];
        [mDic setObject:[NSNumber numberWithInt:mPageCount] forKey:@"PageCount"];
        [mDic setObject:[NSNumber numberWithInt:mPageStart] forKey:@"PageStart"];
        [mDic setObject:[NSNumber numberWithBool:TRUE] forKey:@"inSchoolOrInHome"];
        self.mDictParameter = mDic ;
        [mDic release];
    }
    

//    [Instance setRightImageBtnForNavigationItem:self.navigationItem Image:@"camera.png" target:self selector:@selector(TakePhoto)];
//
    //----------------------------------------
    {
        NSMutableArray *array = [[NSMutableArray alloc]init];
        
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        [mDict setObject:@"在家" forKey:@"title"];
        [mDict setObject:FOURENSURE forKey:@"imageName"];
        [array addObject:mDict];
        [mDict release];
        
        mDict = [[NSMutableDictionary alloc]init];
        [mDict setObject:@"在园" forKey:@"title"];
        [mDict setObject:FOURENSURE forKey:@"imageName"];
        [array addObject:mDict];
        [mDict release];
        
        [Instance setNavigationItemRightTwo:self.navigationItem buttons:array target:self selector:@selector(itemRight:)];
        

    }
    

    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navigationBack)];


	
	// set icons for each segment
//	[iconControl setImage:[UIImage imageNamed:@"photoclass.jpg"] forSegmentAtIndex:0];
//	[iconControl setImage:[UIImage imageNamed:@"photochild.jpg"] forSegmentAtIndex:1];

    self.tableView.delegate = self;
    [self.tableView setBackgroundColor:[UIColor blueColor]];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];

    [NSThread detachNewThreadSelector:@selector(nsthread) toTarget:self withObject:nil];
}
-(void)nsthread
{
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,PhotoUrl];
    
    
    if (mDictParameter != nil) {
        HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
        mHttpRequest.Delegate = self;
        [mHttpRequest CustomFormDataRequestDict:[mDictParameter JSONRepresentation] tag:0 url:urlstr];
        [self.view addSubview:mHttpRequest];
        [mHttpRequest release];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"加载图片列表出错" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    
}
#pragma mark HttpFormatRequestDelegate;
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    [mPhotoLists removeAllObjects];
    NSArray *array = [[[ResultContent JSONValue] valueForKey:@"childPhotoRecordsOfOneBaby"] retain];
    
    for (int i = 0; i < [array count]; i++) {
        ChildOfPhotoInfo *mChildObj = [[ChildOfPhotoInfo alloc] init];
        mChildObj.childPhotoID = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childPhotoID"]] intValue];
        mChildObj.childPhotoComment = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childPhotoComment"]];
        mChildObj.publishDateTime = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"publishDateTime"]];
        mChildObj.publishPersonID = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"publishPersonID"]] intValue];
        mChildObj.publishPersonName = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"publishPersonName"]];
        
        mChildObj.picUrl = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"picUrl"]];
        mChildObj.smallPicUrl = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"smallPicUrl"]];
        
        mChildObj.smallphotoimage  = nil;
        mChildObj.bigphotoimage = nil;
        
        mChildObj.isSmallLoad = NO;
        mChildObj.isBigLoad = NO;
        
        [mPhotoLists addObject:mChildObj];
        [mChildObj release];
    }
    [array release];
    [self.tableView reloadData];

    
}
#pragma mark 
-(NSString*)setFileFormat:(NSString *)String
{
    NSArray *list=[String componentsSeparatedByString:@"/"];

    return [list objectAtIndex:[list count]-1];
    
    
}
#pragma mark 获取路径　并创建文件夹
-(NSString*)filePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    documentDirectory =  [documentDirectory stringByAppendingPathComponent:@"small"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:documentDirectory]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:documentDirectory withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return documentDirectory;
}
#pragma mark 获取文件路径
-(NSString *)documentFilePath:(NSString *)String
{
    NSString *document = [self filePath];
    return [document stringByAppendingPathComponent:[self setFileFormat:String]];
}
-(BOOL)isFileExistPath:(NSString *)String
{
    NSString *documentDirectory = [self filePath];
    
    documentDirectory =  [documentDirectory stringByAppendingPathComponent:[self setFileFormat:String]];

    if ([[NSFileManager defaultManager] fileExistsAtPath:documentDirectory]) {
        return YES;
    }
    else
        return NO;
    
}
#pragma mark

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if ([mPhotoLists count]>0 && [mPhotoLists count]<4) {
        return 1;
    }
    return [mPhotoLists count]/4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    int row = [indexPath row];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    


    
    for (UIView *ctrView in [cell.contentView subviews]) {
        [ctrView removeFromSuperview];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    for (int i = 0; i<4; i++) {
        int index = row *4 +i;
        
        if ( index >= [mPhotoLists count]) {
            return cell;
        }
        [self loadPhotoImage:cell count:index between:i];
    }
    
    
    return cell;
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	if (!decelerate)
	{
        [self LoadPhoto];
    }

}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self LoadPhoto];
}

#pragma mark - Table view

-(void)LoadPhoto
{

        NSArray *indexPaths =  [self.tableView indexPathsForVisibleRows];
        for (NSIndexPath *indexPath in indexPaths) {
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            for (int position = 0; position<4; position++) {
                int index = [indexPath row] *4 +position;
                
                if ( index >= [mPhotoLists count]) {
                    return ;
                }
                [self loadPhotoImage:cell count:index between:position];
            }
        }
}
-(void)loadPhotoImage:(UITableViewCell*)cell count:(int)index between:(int)position
{
    UIImage *image = [UIImage imageNamed:@"相框.png"];
    float width  = image.size.width/2;
    float height = image.size.height/2;
    
    

        
    UIButton *mButton = [[UIButton alloc]initWithFrame:CGRectMake(15+(width+15)*position, 0, width, height)];
    [mButton setBackgroundImage:[UIImage imageNamed:@"相框.png"] forState:0];
    mButton.tag = index ;
    [cell.contentView addSubview:mButton];
    [mButton addTarget:self action:@selector(GoBigPhotot:) forControlEvents:64];
    [mButton release];
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(1.5, 1.5, 58, 58)];
    [mButton addSubview:imageView];
    [imageView release];
    
    ChildOfPhotoInfo *mChildObj = [mPhotoLists objectAtIndex:index];
    
    if ([self isFileExistPath:mChildObj.smallPicUrl]) {
        UIImage *image = [UIImage imageWithContentsOfFile:[self documentFilePath:mChildObj.smallPicUrl]];
        [imageView setImage:image];
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
    }
    else {
        if (self.tableView.dragging == NO && self.tableView.decelerating == NO) {
            
            [NSThread detachNewThreadSelector:@selector(DownLoadButtonImage:) toTarget:self withObject:mChildObj.smallPicUrl];
        }
    }
    

}
-(void)DownLoadButtonImage:(NSString*)UrlString
{
    
    ASIHTTPRequest * request ;

    request =[ASIHTTPRequest requestWithURL:[NSURL URLWithString:UrlString]];
    [request setDownloadDestinationPath:[self documentFilePath:UrlString]];
	[request setDelegate:self];
	[request setTimeOutSeconds:120];
	[request setNumberOfTimesToRetryOnTimeout:10];
	[request setDownloadProgressDelegate:self];
	[request startAsynchronous];

}
- (void)requestFinished:(ASIHTTPRequest *)request
{
    [self.tableView reloadData];
}

#pragma mark 大图
-(void)GoBigPhotot:sender
{
    
    UIButton *tmp = sender;
    PhotoListViewController *detailViewController = [[PhotoListViewController alloc] initWithNibName:@"PhotoListViewController" bundle:nil];
    detailViewController.mChildId = [[mDictParameter objectForKey:@"ID"] integerValue];
    detailViewController.mPhotos = mPhotoLists;
    detailViewController.isInhomeOrSchool = [[mDictParameter objectForKey:@"inSchoolOrInHome"] boolValue];
    detailViewController.ScrollerPages = tmp.tag;
    
    [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview:detailViewController.view];
    [detailViewController.view setFrame:CGRectMake(0, 0, 320, 480)];
    
    
    
    //    [detailViewController release];
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end
