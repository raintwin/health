//
//  PhotoViewController.m
//  Health
//
//  Created by dinghao on 13-4-11.
//
//

#import "PhotoViewController.h"
#import "CChild.h"
#import "SendPhotoViewController.h"

#define PhotoUrl     @"http://www.ayayababy.cn/Photo/TPFetchChildPhotoRecordsOfOneBaby"
#define HomeUrl      @"http://www.ayayababy.cn/Photo/TeacherFetchChildPhotoRecordsOfEveryBaby"


//#define PhotoUrl     @"http://192.168.1.250/Photo/TPFetchChildPhotoRecordsOfOneBaby"
//#define HomeUrl      @"http://192.168.1.250/Photo/TeacherFetchChildPhotoRecordsOfEveryBaby"



//


#define Teach 100023
#define Produ 100024
#define mPageCount 100000
#define mPageStart 0

#define TABLEHEIGHT 30
@interface PhotoViewController ()

@property(nonatomic,retain)UIButton *classButton;
@property(nonatomic,retain)UIButton *personalButton;
@property(nonatomic,retain)UITableView *photoTableView;
@property(nonatomic,retain)NSMutableDictionary *photoDict;
@property(nonatomic,retain)NSMutableArray *proPhotoLists,*classChilds;
@property(nonatomic,retain)HttpFormatRequest *httpFormatRequest;
@end



@implementation PhotoViewController
@synthesize photoTableView;

@synthesize photoDict;
@synthesize proPhotoLists;
@synthesize classChilds;
@synthesize classButton;
@synthesize personalButton;
@synthesize httpFormatRequest;
typedef enum _ASIAuthenticationType {
	ClassType = 0,
    ChildType = 1
} PhotoType;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}
-(void)navigationBack
{
    
}

-(void)dealloc
{
    [self.classButton release];
    [self.personalButton release];
    [self.photoTableView release];
    [self.photoDict release];
    [self.proPhotoLists release];
    [self.classChilds release];
    [self.httpFormatRequest release];
    [super dealloc];
}
-(void)viewDidUnload
{
    self.classButton = nil;
    self.personalButton = nil;
    self.photoDict = nil;
    self.proPhotoLists = nil;
    self.classChilds = nil;
    self.photoTableView = nil;
    self.httpFormatRequest = nil;
    [super viewDidUnload];
}
-(void)TakePhoto
{

    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"HT_UserNo :%@",[defaults objectForKey:NSLocalizedString(@"HT_UserNo", nil)]);
    if ([[defaults objectForKey:NSLocalizedString(@"HT_UserNo", nil)]  isEqualToString:@"10000"]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"测试帐号没有此功能" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        return;
    }
    
    SendPhotoViewController *detailViewController = [[SendPhotoViewController alloc] initWithNibName:@"SendPhotoViewController" bundle:nil];
    
    detailViewController.title = @"发照片";
    
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    isLoading = NO;
    isTeachHome = NO;
    downIndex = 0;
    if (![Instance getCharacter]) {
        [Instance setRightImageBtnForNavigationItem:self.navigationItem Image:@"camera.png" target:self selector:@selector(TakePhoto)];
    }

    self.proPhotoLists = [[NSMutableArray alloc]init];
    self.classChilds   = [[NSMutableArray alloc]init];
    


    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"photoviewback.jpg"]]];



    self.classButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 160, 30)];
    [self.classButton setBackgroundImage:[UIImage imageNamed:@"photoclass"] forState:0];
    [self.classButton addTarget:self action:@selector(setClassPhoto) forControlEvents:64];
    [self.view addSubview:self.classButton];
    

    
    self.personalButton = [[UIButton alloc]initWithFrame:CGRectMake(160, 0, 160, 30)];
    [self.personalButton setBackgroundImage:[UIImage imageNamed:@"photochild"] forState:0];
    [self.personalButton addTarget:self action:@selector(setPersonalPhoto) forControlEvents:64];
    [self.view addSubview:self.personalButton];


    self.photoTableView= [[UITableView alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y +30, self.view.bounds.size.width, 342.0f) style:UITableViewStylePlain];
    [self.photoTableView setBackgroundColor:[UIColor clearColor]];
    self.photoTableView.delegate   = self;
    self.photoTableView.dataSource = self;
    [self.view addSubview:self.photoTableView];

    
    self.photoDict = [[NSMutableDictionary alloc] init];
    [self.photoDict setObject:[NSNumber numberWithInt:[[Instance GetUseID] intValue]] forKey:@"ID"];
    [self.photoDict setObject:[NSNumber numberWithInt:mPageCount] forKey:@"PageCount"];
    [self.photoDict setObject:[NSNumber numberWithInt:mPageStart] forKey:@"PageStart"];
    [self.photoDict setObject:[NSNumber numberWithBool:TRUE] forKey:@"inSchoolOrInHome"];
    
    self.httpFormatRequest = [[HttpFormatRequest alloc]init];
    self.httpFormatRequest.Delegate = self;
    [self.view addSubview:self.httpFormatRequest];
    [self.httpFormatRequest CustomFormDataRequestDict:[self.photoDict JSONRepresentation] tag:0 url:PhotoUrl];

}
#pragma mark 班级相片
-(void)setClassPhoto
{
    if (![Instance getCharacter]) {
        [Instance setRightImageBtnForNavigationItem:self.navigationItem Image:@"camera.png" target:self selector:@selector(TakePhoto)];
    }
    else
    {
        self.navigationItem.rightBarButtonItem = nil;
    }
    isTeachHome = NO;

    [self.classButton setImage:[UIImage imageNamed:@"photoclass"] forState:0];
    [self.personalButton setImage:[UIImage imageNamed:@"photochild"] forState:0];

    [self.photoDict setObject:[NSNumber numberWithBool:TRUE] forKey:@"inSchoolOrInHome"];
    [self.httpFormatRequest CustomFormDataRequestDict:[self.photoDict JSONRepresentation] tag:0 url:PhotoUrl];
    [self.photoTableView setBackgroundColor:[UIColor clearColor]];

}
-(void)setPersonalPhoto
{
    if ([Instance getCharacter]) {
        [Instance setRightImageBtnForNavigationItem:self.navigationItem Image:@"camera.png" target:self selector:@selector(TakePhoto)];
    }
    else{
        self.navigationItem.rightBarButtonItem = nil;
    }

    [self.classButton setImage:[UIImage imageNamed:@"photoclassgray"] forState:0];
    [self.personalButton setImage:[UIImage imageNamed:@"photochildyuo"] forState:0];
    [self.photoTableView setBackgroundColor:[UIColor clearColor]];

    if (![Instance getCharacter]) {
        isTeachHome = YES;
        [self.photoDict setObject:[NSNumber numberWithBool:FALSE] forKey:@"inSchoolOrInHome"];
        [self.httpFormatRequest CustomFormDataRequestDict:[self.photoDict JSONRepresentation] tag:Teach url:HomeUrl];
        [self.photoTableView setBackgroundColor:[UIColor whiteColor]];

    }
    else
    {
        isTeachHome = NO;
        [self.photoDict setObject:[NSNumber numberWithBool:FALSE] forKey:@"inSchoolOrInHome"];
        [self.httpFormatRequest CustomFormDataRequestDict:[self.photoDict JSONRepresentation] tag:0 url:PhotoUrl];

    }
}
#pragma mark



#pragma mark HttpFormatRequestDelegate;
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    if (![Instance getCharacter] && tag == Teach && isTeachHome) {
        [self.classChilds removeAllObjects];

        NSArray *array = [[[ResultContent JSONValue] valueForKey:@"childPhotoRecordsOfEveryBody"] retain];
                for (int i = 0; i < [array count]; i++) {
            ChildOfPhoto *mChildObj = [[ChildOfPhoto alloc] init];
            
            mChildObj.cID = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childID"]] intValue];
            mChildObj.cName = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childName"]];
            mChildObj.cPhotoUrl = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childPhoto"]];
            mChildObj.mRecorder = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childPhotoRecords"]] intValue];
            [self.classChilds addObject:mChildObj];
            [mChildObj release];
        }    
        [array release];
    }
    else
    {
        [self.proPhotoLists removeAllObjects];
        NSArray *array = [[[ResultContent JSONValue] valueForKey:@"childPhotoRecordsOfOneBaby"] retain];
        
        for (int i = 0; i < [array count]; i++) {
            ChildOfPhotoInfo *mChildObj = [[ChildOfPhotoInfo alloc] init];
            mChildObj.childPhotoID = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childPhotoID"]] intValue];
            mChildObj.childPhotoComment = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childPhotoComment"]];
            mChildObj.publishDateTime = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"publishDateTime"]];
            mChildObj.publishPersonID = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"publishPersonID"]] intValue];
            mChildObj.publishPersonName = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"publishPersonName"]];
            
            mChildObj.picUrl = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"picUrl"]];
            mChildObj.smallPicUrl = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"smallPicUrl"]];
            
            mChildObj.smallphotoimage  = nil;
            mChildObj.bigphotoimage = nil;
            
            mChildObj.isSmallLoad = NO;
            mChildObj.isBigLoad = NO;
            
            [self.proPhotoLists addObject:mChildObj];
            [mChildObj release];
        }
        [array release];

    }
    [self.photoTableView reloadData];
    
    
}
#pragma mark
-(NSString*)setFileFormat:(NSString *)String
{
    NSArray *list=[String componentsSeparatedByString:@"/"];
    
    return [list objectAtIndex:[list count]-1];
    
    
}
#pragma mark 获取路径　并创建文件夹
-(NSString*)filePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    documentDirectory =  [documentDirectory stringByAppendingPathComponent:@"small"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:documentDirectory]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:documentDirectory withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return documentDirectory;
}
#pragma mark 获取文件路径
-(NSString *)documentFilePath:(NSString *)String
{
    NSString *document = [self filePath];
    return [document stringByAppendingPathComponent:[self setFileFormat:String]];
}
-(BOOL)isFileExistPath:(NSString *)String
{
    NSString *documentDirectory = [self filePath];
    
    documentDirectory =  [documentDirectory stringByAppendingPathComponent:[self setFileFormat:String]];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:documentDirectory]) {
        return YES;
    }
    else
        return NO;
    
}
#pragma mark


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isTeachHome && ![Instance getCharacter]) {
        return TABLEHEIGHT;
    }
        
    return 85.5f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (![Instance getCharacter] && isTeachHome) {
        return [self.classChilds count];
    }
    else
    {
        if ([self.proPhotoLists count]>0 && [self.proPhotoLists count]<4) {
            return 1;
        }
        return [self.proPhotoLists count]/4;
    }

    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }

    for (UIView *ctrView in [cell.contentView subviews]) {
        [ctrView removeFromSuperview];
    }
    if (![Instance getCharacter] && isTeachHome) {
        if ([self.classChilds count] == 0) {
            return cell;
        }
        [tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];

        ChildOfPhoto *mChildObj = [self.classChilds objectAtIndex:[indexPath row]];
                
        UILabel *nameLabel = [ViewTool addUILable:cell.contentView x:0 y:0 x1:160 y1:TABLEHEIGHT fontSize:15 lableText:mChildObj.cName];
        nameLabel.font = [UIFont boldSystemFontOfSize:15];
        UILabel *recorderLabel = [ViewTool addUILable:cell.contentView x:160 y:0 x1:160 y1:TABLEHEIGHT fontSize:15 lableText:@""];
        recorderLabel.text = [NSString stringWithFormat:@"相片: %d 张",mChildObj.mRecorder];
    }
    else  {
        int row = [indexPath row];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        for (int i = 0; i<4; i++) {
            int index = row *4 +i;
            
            if ( index >= [self.proPhotoLists count]) {
                return cell;
            }
            [self loadPhotoImage:cell count:index between:i];
        }
    }

    
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (isTeachHome) {
        ChildOfPhoto *mChildObj = [self.classChilds objectAtIndex:[indexPath row]];

        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        dict = [[NSMutableDictionary alloc] init];
        [dict setObject:[NSNumber numberWithInt:mChildObj.cID] forKey:@"ID"];
        [dict setObject:[NSNumber numberWithInt:mPageCount] forKey:@"PageCount"];
        [dict setObject:[NSNumber numberWithInt:mPageStart] forKey:@"PageStart"];
        [dict setObject:[NSNumber numberWithBool:FALSE] forKey:@"inSchoolOrInHome"];

        [self.httpFormatRequest CustomFormDataRequestDict:[dict JSONRepresentation] tag:0 url:PhotoUrl];
        [dict release];
        isTeachHome = NO;
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (isTeachHome) {
        return;
    }
	if (!decelerate)
	{
        [self LoadPhoto];
    }
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (isTeachHome) {
        return;
    }
    [self LoadPhoto];
}

#pragma mark - Table view

-(void)LoadPhoto
{
    
    NSArray *indexPaths =  [self.photoTableView indexPathsForVisibleRows];
    for (NSIndexPath *indexPath in indexPaths) {
        UITableViewCell *cell = [self.photoTableView cellForRowAtIndexPath:indexPath];
        for (int position = 0; position<4; position++) {
            int index = [indexPath row] *4 +position;
            
            if ( index >= [self.proPhotoLists count]) {
                return ;
            }
            [self loadPhotoImage:cell count:index between:position];
        }
    }
}
-(void)loadPhotoImage:(UITableViewCell*)cell count:(int)index between:(int)position
{
    UIImage *image = [UIImage imageNamed:@"相框.png"];
    float width  = image.size.width/2;
    float height = image.size.height/2;
    
    
    
    
    UIButton *mButton = [[UIButton alloc]initWithFrame:CGRectMake(15+(width+15)*position, 0, width, height)];
    [mButton setBackgroundImage:[UIImage imageNamed:@"相框.png"] forState:0];
    mButton.tag = index ;
    [cell.contentView addSubview:mButton];
    [mButton addTarget:self action:@selector(GoBigPhotot:) forControlEvents:64];
    [mButton release];
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(1.5, 1.5, 58, 58)];
    [mButton addSubview:imageView];
    [imageView release];
    
    ChildOfPhotoInfo *mChildObj = [self.proPhotoLists objectAtIndex:index];
    
    if ([self isFileExistPath:mChildObj.smallPicUrl]) {
        UIImage *image = [UIImage imageWithContentsOfFile:[self documentFilePath:mChildObj.smallPicUrl]];
        [imageView setImage:image];
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
    }
    else {
        if (self.photoTableView.dragging == NO && self.photoTableView.decelerating == NO) {
            
            [NSThread detachNewThreadSelector:@selector(downLoadButtonImage:) toTarget:self withObject:mChildObj.smallPicUrl];
        }
    }
    
    
}
-(void)downLoadButtonImage:(NSString*)UrlString
{
    
    ASIHTTPRequest * request ;
    
    request =[ASIHTTPRequest requestWithURL:[NSURL URLWithString:UrlString]];
    [request setDownloadDestinationPath:[self documentFilePath:UrlString]];
	[request setDelegate:self];
	[request setTimeOutSeconds:120];
	[request setNumberOfTimesToRetryOnTimeout:10];
	[request setDownloadProgressDelegate:self];
	[request startAsynchronous];
    
}
- (void)requestFinished:(ASIHTTPRequest *)request
{
    if (downIndex >3) {
        [self.photoTableView reloadData];
        downIndex = 0;
    }
    downIndex ++;
}

#pragma mark 大图
-(void)GoBigPhotot:sender
{
    
    UIButton *tmp = sender;
    PhotoListViewController *detailViewController = [[PhotoListViewController alloc] initWithNibName:@"PhotoListViewController" bundle:nil];
    detailViewController.mChildId = [[self.photoDict objectForKey:@"ID"] integerValue];
    detailViewController.mPhotos = self.proPhotoLists;
    detailViewController.isInhomeOrSchool = [[self.photoDict objectForKey:@"inSchoolOrInHome"] boolValue];
    detailViewController.ScrollerPages = tmp.tag;
    
    DMViewController *rootController=(DMViewController*) [UIApplication sharedApplication].keyWindow.rootViewController;
    [self.navigationController pushViewController:detailViewController animated:YES];
    self.navigationController.navigationBarHidden = YES;
    
    [rootController setButtonsendSubviewToBack];
    NSLog(@"detailViewController.view :%@",NSStringFromCGRect(detailViewController.view.bounds));
  
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
