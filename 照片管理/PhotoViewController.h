//
//  PhotoViewController.h
//  Health
//
//  Created by dinghao on 13-4-11.
//
//

#import <UIKit/UIKit.h>

#import "PhotoListViewController.h"

@class DMViewController;

@interface PhotoViewController : UIViewController<HttpFormatRequestDeleagte,ASIHTTPRequestDelegate,UITableViewDataSource,UITableViewDelegate>
{
    BOOL isLoading;
    BOOL isTeachHome;
    int downIndex;
}

@end
