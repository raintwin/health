//
//  ChildDisViewController.m
//  Health
//
//  Created by hq on 12-6-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "ChildDisViewController.h"
#import "ChildListOfCom.h"
#import "Reachability.h"
#import "JsonInstance.h"
#import "ASIHTTPRequest.h"
#import "JSON.h"
#import "CChild.h"
#import "DisView.h"


#define EPersonIDsOfNotice @"PersonIDsOfNotice"

#define IViewTag 100022

@implementation ChildDisViewController
@synthesize mChilds,mChildID;
@synthesize mCNameText;
@synthesize mDisText;
@synthesize isDis,isAnd,isPhoto,isText,isTitle,isUsers;
@synthesize _Delegate;
@synthesize mDisViews;
@synthesize EvaluateType;

@synthesize mDateDict;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_01.png"]]];
        isDis = isPhoto = isText = isTitle = isAnd = isUsers = NO;
        EvaluateType = -1;
    }
    return self;
}
// ----------------

-(void)SetnotityTitle
{
    if ((UIView*)[self.view viewWithTag:IViewTag]!=nil) {
        UIView *view = (UIView*)[self.view viewWithTag:IViewTag];
        if ((UILabel*)[view viewWithTag:IViewTag+10]!=nil) {
            ((UILabel*)[view viewWithTag:IViewTag+10]).text = @"标题";
        }
    }
    isSelect = YES;
    [self.view bringSubviewToFront:[self.view viewWithTag:IViewTag]];
    [self InitScrollerDis];
}
-(void)SetNotifyContent
{
    if ((UIView*)[self.view viewWithTag:IViewTag]!=nil) {
        UIView *view = (UIView*)[self.view viewWithTag:IViewTag];
        if ((UILabel*)[view viewWithTag:IViewTag+10]!=nil) {
            ((UILabel*)[view viewWithTag:IViewTag+10]).text = @"内容";
        }
    }

    isSelect = YES;
    [self.view bringSubviewToFront:[self.view viewWithTag:IViewTag]];
    [self.view bringSubviewToFront:mDisText];
    [mDisText becomeFirstResponder];
    [self InitScrollerDis];

}
-(void)SetDisView
{
    if ((UIView*)[self.view viewWithTag:IViewTag]!=nil) {
        UIView *view = (UIView*)[self.view viewWithTag:IViewTag];
        if ((UILabel*)[view viewWithTag:IViewTag+10]!=nil) {
            ((UILabel*)[view viewWithTag:IViewTag+10]).text = @"表现";
        }
    }
    
    isSelect = NO;
    [self performSelector:@selector(InitScrollerDis)];
}
-(void)InitScrollerDis
{
    UIScrollView *scroller = (UIScrollView *)[self.view viewWithTag:111000];
    UIPageControl *pageControl = (UIPageControl*)[self.view viewWithTag:111001];
    
    if (isSelect) {
        [self.view bringSubviewToFront:scroller];
        [self.view bringSubviewToFront:pageControl];
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3f];
        CGRect rect = scroller.frame;
        CGRect rectPagte = pageControl.frame;
        rect.origin.y = 300;
        rectPagte.origin.y = 420;
        [scroller setFrame:rect];
        [pageControl setFrame:rectPagte];
        [UIView commitAnimations];

        return;
    }

    
    if (scroller == nil) {
        
        NSMutableArray *array = [[NSMutableArray alloc] init];
        if (![Instance getCharacter]) { /// 老师
            for (int i = 0; i<8; i++) {
                [array addObject:[NSNull null]];
            }
            [array replaceObjectAtIndex:0 withObject:@"礼貌2.png"];
            [array replaceObjectAtIndex:1 withObject:@"卫生2.png"];
            [array replaceObjectAtIndex:2 withObject:@"学习2.png"];
            [array replaceObjectAtIndex:3 withObject:@"锻炼2.png"];
            [array replaceObjectAtIndex:4 withObject:@"游戏2.png"];
            [array replaceObjectAtIndex:5 withObject:@"睡眠2.png"];
            [array replaceObjectAtIndex:6 withObject:@"劳动2.png"];
            [array replaceObjectAtIndex:7 withObject:@"进餐2.png"];

            
        }
        if ([Instance getCharacter]) {//家长
            for (int i = 0; i<6; i++) {
                [array addObject:[NSNull null]];
            }
            
            [array replaceObjectAtIndex:0 withObject:@"敬长辈.png"];
            [array replaceObjectAtIndex:1 withObject:@"不挑食.png"];
            [array replaceObjectAtIndex:2 withObject:@"会收拾.png"];
            [array replaceObjectAtIndex:3 withObject:@"爱清洁.png"];
            [array replaceObjectAtIndex:4 withObject:@"讲道理.png"];
            [array replaceObjectAtIndex:5 withObject:@"自己睡.png"];



        }
        
        UIScrollView *scroller = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 300, 320, 200)];
        scroller.tag = 111000;
        [scroller setBackgroundColor:HEXCOLOR(0xf4f4f4)];
        scroller.delegate = self;
        [scroller setShowsHorizontalScrollIndicator:NO];
        [scroller setShowsVerticalScrollIndicator:NO];
        [scroller setPagingEnabled:YES];
        [scroller setContentSize:CGSizeMake(320* ([array count]/2), 200)];
        [self.view addSubview:scroller];
        [scroller release];
        
        UIPageControl *pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 420, 320, 20)];
        pageControl.tag = 111001;
        pageControl.numberOfPages = [array count]/2;
        pageControl.currentPage = 0;
        pageControl.backgroundColor = [UIColor blackColor];
        [self.view addSubview:pageControl];
        [pageControl release];
        

        NSMutableArray *tmps = [[NSMutableArray alloc] init];
        for (int i =0; i<[array count]; i++) {
            NSString *str =(NSString *)[array objectAtIndex:i];
            DisView *disv = [[DisView alloc]initWithFrame:CGRectMake(160*i , 0, 160, 100)];
            [disv SetRefresh:str];
            [tmps addObject:disv];
            [disv release];
        }
        [array release];
        self.mDisViews = tmps;
        [tmps release];
        for (DisView *objv in mDisViews) {
            [scroller addSubview:objv];
        }
        
        [self.view bringSubviewToFront:scroller];
        [self.view bringSubviewToFront:pageControl];
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3f];
        CGRect rect = scroller.frame;
        CGRect rectPagte = pageControl.frame;

            rect.origin.y = 0;
            rectPagte.origin.y = 120 ;
            
            [scroller setFrame:rect];
            [pageControl setFrame:rectPagte];
            
        [UIView commitAnimations];

    }
    else
    {
        [self.view bringSubviewToFront:scroller];
        [self.view bringSubviewToFront:pageControl];

        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3f];
        CGRect rect = scroller.frame;
        CGRect rectPagte = pageControl.frame;
        if (scroller.frame.origin.y == 0) {
            rect.origin.y = 300;
            rectPagte.origin.y = 420 ;
            [scroller setFrame:rect];
            [pageControl setFrame:rectPagte];
            
        }   
        else
        {
            rect.origin.y = 0;
            rectPagte.origin.y = 120 ;
            
            [scroller setFrame:rect];
            [pageControl setFrame:rectPagte];
            
        }
        [UIView commitAnimations];
    }
    [self.view bringSubviewToFront:[self.view viewWithTag:IViewTag]];

}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    UIPageControl *pageControl = (UIPageControl*)[self.view viewWithTag:111001];

    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    pageControl.currentPage = page;

}
// ----------------
#define IconWidth 20
-(void)InitTools
{

    UIView *tempview = (UIView *)[self.view viewWithTag:IViewTag];
    if (tempview != nil) {
        return;
    }
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 25)];
    view.tag = IViewTag;
    [view setBackgroundColor:[UIColor whiteColor]];
    //    [view setUserInteractionEnabled:YES];
    [self.view addSubview:view];
    [view release];
    int i = 0;
    
    
    if (isAnd) {
        UIButton *btn = [Instance addUIButtonByImage:view image:@"人物.png" rect:CGRectMake(10 + i*55, 0, IconWidth, IconWidth)];
        [btn addTarget:self action:@selector(SlectChild) forControlEvents:64];
        i++;
    }
    if (isTitle) {
        
        UIButton *btn = [Instance addUIButtonByImage:view image:@"标题.png" rect:CGRectMake(10 + i*55, 0, IconWidth, IconWidth)];
        [btn addTarget:self action:@selector(SetnotityTitle) forControlEvents:64];
        i++;
    }
    if (isText) {
        UIButton *btn = [Instance addUIButtonByImage:view image:@"内容.png" rect:CGRectMake(10 + i*55, 0, IconWidth, IconWidth)];

        [btn addTarget:self action:@selector(SetNotifyContent) forControlEvents:64];
        i++;
    }
    if (isDis) {
        UIButton *btn = [Instance addUIButtonByImage:view image:@"表现.png" rect:CGRectMake(10 + i*55, 0, IconWidth, IconWidth)];
        
        [btn addTarget:self action:@selector(SetDisView) forControlEvents:64];
        i++;
    }

    UILabel *TextType = [[UILabel alloc]initWithFrame:view.frame];
    TextType.font = [UIFont systemFontOfSize:15];
    TextType.backgroundColor = [UIColor clearColor];
    TextType.textColor = [UIColor blackColor];
    TextType.textAlignment = UITextAlignmentRight;
    TextType.tag = IViewTag+10;
    [view addSubview:TextType];
    [TextType release];

}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)navBack
{

    [self.navigationController popViewControllerAnimated:YES];
    
}



-(void)dealloc
{
    NSLog(@"mChilds3 retiancoutn: %d",[mChilds retainCount]);
    [mChildID release]; 
    [mChilds release]; 
    [mCNameText release];
    [mDisText release]; 
    [mDisViews release];
    [mDateDict release];
    
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mChilds = nil;
    self.mChildID = nil;
    self.mCNameText = nil;
    self.mDisViews = nil;
    self.mDisText = nil;
    self.mDateDict = nil;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:@"backbutton.png" target:self selector:@selector(navBack)];
    [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"发送" target:self selector:@selector(SaveNotifyInfo)];
    
    UIFont *font = [UIFont systemFontOfSize:16];
    
    
    
    {
        UITextView *textview = [Instance addTextView:self frame:CGRectMake(0,0, 320, 200)];
        [textview setScrollEnabled:YES];
        textview.backgroundColor = [UIColor whiteColor];
        textview.font = font;
        textview.tag = 1;
        textview.text = @"";
        self.mDisText = textview;
        [textview release];
        [self.view addSubview:mDisText];
        [mDisText becomeFirstResponder];
        
    }
    

    
    //监听键盘高度的变换 
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];  
    
    // 键盘高度变化通知，ios5.0新增的    
#ifdef __IPHONE_5_0  
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];  
    if (version >= 5.0) {  
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];  
    }  
#endif  
}

#pragma mark 发送
-(void)SaveNotifyInfo
{

    
    [self performSelector:@selector(NotityInformartion)];
    
}
-(void)GetChildsId
{

    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (CChild *obj in mChilds) {
        if (obj.isCheck == YES) {
            [array addObject:[NSNumber numberWithInt:obj.cID]];
            
        }
    }
    self.mChildID = array;
    NSLog(@"发送幼儿的数目 ----:%d",[mChildID count]);
    [array release];
}

-(void)NotityInformartion
{
    NSString *NotityConten = mDisText.text;
    
    [self GetChildsId];
    
    if (isAnd) {
        if ([mChildID count] == 0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请选择幼儿姓名" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            return;
        }
    }   
    if ([NotityConten isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"评价内容不能为空" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        return;
    }
    NSMutableDictionary*mDict = [[NSMutableDictionary alloc] init];
    
    [mDict setObject:[NSNumber numberWithInt:EvaluateType] forKey:@"Type"];
    [mDict setObject:NotityConten forKey:@"Comment"];
    [mDict setObject:[mDateDict objectForKey:@"date"] forKey:@"PublishDateTime"];
    [mDict setObject:[Instance GetUseID] forKey:@"PublishPersonID"];
    
    
    
    NSString *sss = [mDict JSONRepresentation];
    
    NSLog(@"JSONString sss  :%@",sss);
    NSString *urlstr;
    if (![Instance getCharacter]) { // 老师
       urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/ChildRepresentation/TeacherAddChildRepresentation"];

        
        [mDict setObject:mChildID forKey:@"PersonIDOfChildRepresentationInSchool"];
        [mDict setObject:[NSNumber numberWithInt:((DisView*)[mDisViews objectAtIndex:0]).btnTag] forKey:@"Courtesy"];
        [mDict setObject:[NSNumber numberWithInt:((DisView*)[mDisViews objectAtIndex:1]).btnTag] forKey:@"Hygienism"];
        [mDict setObject:[NSNumber numberWithInt:((DisView*)[mDisViews objectAtIndex:2]).btnTag] forKey:@"Study"];
        [mDict setObject:[NSNumber numberWithInt:((DisView*)[mDisViews objectAtIndex:3]).btnTag] forKey:@"TakeExercise"];
        [mDict setObject:[NSNumber numberWithInt:((DisView*)[mDisViews objectAtIndex:4]).btnTag] forKey:@"Game"];
        [mDict setObject:[NSNumber numberWithInt:((DisView*)[mDisViews objectAtIndex:5]).btnTag] forKey:@"Sleep"];
        [mDict setObject:[NSNumber numberWithInt:((DisView*)[mDisViews objectAtIndex:6]).btnTag] forKey:@"Labour"];
        [mDict setObject:[NSNumber numberWithInt:((DisView*)[mDisViews objectAtIndex:7]).btnTag] forKey:@"EatAbility"];

    }
    else { //家长
       urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/ChildRepresentation/ParentAddChildRepresentation"];
        [mDict setObject:[Instance GetChildID] forKey:@"PersonIDOfChildRepresentationInHome"];
        
        [mDict setObject:[NSNumber numberWithInt:((DisView*)[mDisViews objectAtIndex:0]).btnTag] forKey:@"RespectOlder"];
        [mDict setObject:[NSNumber numberWithInt:((DisView*)[mDisViews objectAtIndex:1]).btnTag] forKey:@"NotPickerFood"];
        [mDict setObject:[NSNumber numberWithInt:((DisView*)[mDisViews objectAtIndex:2]).btnTag] forKey:@"LikeActivity"];
        [mDict setObject:[NSNumber numberWithInt:((DisView*)[mDisViews objectAtIndex:3]).btnTag] forKey:@"LikeClean"];
        [mDict setObject:[NSNumber numberWithInt:((DisView*)[mDisViews objectAtIndex:4]).btnTag] forKey:@"SpeekTruth"];
        [mDict setObject:[NSNumber numberWithInt:((DisView*)[mDisViews objectAtIndex:5]).btnTag] forKey:@"SelfSleep"];

    }

    NSString *JSONString = [mDict JSONRepresentation];
    
    NSLog(@"JSONString :%@",JSONString);
    [mDict release];
    
    
    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate = self;
    [mHttpRequest CustomFormDataRequestDict:JSONString tag:0 url:urlstr];
    [self.view addSubview:mHttpRequest];
    [mHttpRequest release];
    
}
#pragma mark requestFinished 
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    [self navBack];
    [_Delegate ReloDataList];

}



#pragma mark ChildListOfCom 

-(void)SlectChild
{
    ChildListOfCom *nlvcCtr  = [[ChildListOfCom alloc]initWithNibName:@"ChildListOfCom" bundle:nil];
    nlvcCtr._chliddelgelagte = self;

    
    nlvcCtr.title = @"幼儿姓名";
    
    NSMutableDictionary *mComInfo = [[NSMutableDictionary alloc]init];
    [mComInfo setObject:[mDateDict objectForKey:@"date"] forKey:@"date"];
    [mComInfo setObject:[NSNumber numberWithInt:EvaluateType] forKey:@"type"];
    nlvcCtr.mComInfo = mComInfo;
    [mComInfo release];
    
    [self.navigationController pushViewController:nlvcCtr animated:YES];

    [nlvcCtr release];
    
}
#pragma mark 重置
-(void)resetNotity
{
    mCNameText.text= @"";
//    mNotifyTitle.text = @"";
    mDisText.text = @"";
    
}
#pragma mark delegateFunction 
-(void)SetPitchs:(NSMutableArray *)childs
{

    self.mChilds = childs;
    
}
#pragma mark 响应输入
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
	textField.text = @"";	
	return NO;
}



-(void)ChangeRectOfView:(CGRect)keyboardRect
{
    CGRect rect = keyboardRect;
    rect.origin.x = 0.0f;
    rect.origin.y = 0.0f;
    rect.size.height = 416 - keyboardRect.size.height-25;
    [mDisText setFrame:rect];
//    mNotifyTitle.frame = mDisText.frame;
    
    UIView *view = [self.view viewWithTag:IViewTag];
    rect = view.frame;
    rect.origin.y = mDisText.frame.size.height;
    [view setFrame:rect];
}

#pragma mark -  
#pragma mark Responding to keyboard events  
- (void)keyboardWillShow:(NSNotification *)notification {  
    
    /*  
     Reduce the size of the text view so that it's not obscured by the keyboard.  
     Animate the resize so that it's in sync with the appearance of the keyboard.  
     */  
    
    NSDictionary *userInfo = [notification userInfo];  
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];  
    
    CGRect keyboardRect = [aValue CGRectValue];  
//    NSLog(@"keyboardRect width :%f  height :%f",keyboardRect.size.width,keyboardRect.size.height);
    
    [self InitTools];
    [self ChangeRectOfView:keyboardRect];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];  
    NSTimeInterval animationDuration;  
    [animationDurationValue getValue:&animationDuration];  
    
    // Animate the resize of the text view's frame in sync with the keyboard's appearance.  
}  


- (void)keyboardWillHide:(NSNotification *)notification {  
    
    NSDictionary* userInfo = [notification userInfo];  
    
    /*  
     Restore the size of the text view (fill self's view).  
     Animate the resize so that it's in sync with the disappearance of the keyboard.  
     */  
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];  
    NSTimeInterval animationDuration;  
    [animationDurationValue getValue:&animationDuration];  
    
}  




- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
