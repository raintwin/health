//
//  PhotoList.h
//  Health
//
//  Created by dinghao on 13-3-26.
//
//

#import <UIKit/UIKit.h>
#import "PhotoListViewController.h"
@interface PhotoList : UITableViewController<HttpFormatRequestDeleagte,ASIHTTPRequestDelegate>

{
    BOOL isLoading;
}
@property(nonatomic,retain)NSMutableDictionary *mDictParameter;
@property(nonatomic,retain)NSMutableArray *mPhotoLists;
@end
