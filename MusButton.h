//
//  MusButton.h
//  Health
//
//  Created by hq on 12-9-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MusButton : UIView

{
    int mCount;
}
-(void)setTitles:(NSMutableArray*)titles;
-(int)GetBtnValue;
-(void)SetBtn:(int)tag;

@property(nonatomic,assign)int BTNTAG;
@property(nonatomic,retain)NSMutableArray *mRemarkBtns;
@end
