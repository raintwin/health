//
//  PaperViewController.m
//  Health
//
//  Created by hq on 12-6-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "PaperViewController.h"
#import "Reachability.h"
#import <QuartzCore/QuartzCore.h>
#import "JSON.h"
#import "CChild.h"
#import "DownLoadTableImage.h"
#import "ChatCustomCell.h"
#import "CustomCell.h"

#import "ChatViewCell.h"

#define REFRESH_HEADER_HEIGHT 52.0f
#define mTABLEHEIGHT 50

//static NSString *const UIBubbleIdentifier = @"UIBubbleIdentifier";

@interface PaperViewController ()

@property (nonatomic,retain) UITableView *chatTableView;
@property (nonatomic,retain) UITextField *chatTextFiled;


@end

@implementation PaperViewController
@synthesize chatTableView;
@synthesize chatArray;


@synthesize refreshHeaderView;
@synthesize refreshArrow;
@synthesize refreshSpinner;
@synthesize refreshLabel;

@synthesize notReadCount,childid;
@synthesize chatTextFiled;
@synthesize mIconDownDict;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        NSLog(@"留言");
        ePageStart = -1;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(NSString*)IconFilePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
//    NSString *filePng = [documentDirectory stringByAppendingPathComponent:@"icon.png"];
    NSString *fileJpg = [documentDirectory stringByAppendingPathComponent:@"acquiesce.jpg"];
    

    {return fileJpg; }
    
    return fileJpg;
}

#pragma mark Table view methods

- (UIView *)bubbleView:(PaperInfo*)obj {

	// build single chat bubble cell with given text
    float FontSize = 16;
    float iconBounds = 54.0f;
    float textWidth = 250.0f;
//    PaperInfo *obj = [chatArray objectAtIndex:row];
    BOOL fromSelf;
    fromSelf = obj.teacherOrParent;
    if (![Instance getCharacter]) { //老师
        fromSelf = !obj.teacherOrParent;
    }
    NSString *text = obj.content;
    NSString *time = obj.publishDateTime;

    
	UIView *returnView = [[UIView alloc] initWithFrame:CGRectZero];
    returnView.tag = 10001;
	returnView.backgroundColor = [UIColor clearColor];
	UIImage *bubble = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:fromSelf?@"ly":@"lyl" ofType:@"png"]];
	UIImageView *bubbleImageView = [[UIImageView alloc] initWithImage:[bubble stretchableImageWithLeftCapWidth:fromSelf? 28:9 topCapHeight:25]];
	[returnView addSubview:bubbleImageView];
    bubbleImageView.tag = 20001;
    
        UIFont *font = [UIFont systemFontOfSize:FontSize];
        CGSize size = [text sizeWithFont:font constrainedToSize:CGSizeMake(textWidth - 50.f , 900.0f) lineBreakMode:UILineBreakModeCharacterWrap];
        UILabel *bubbleText = [[UILabel alloc] initWithFrame:CGRectMake(fromSelf? 21.f:15.0f, 5, (int)size.width+10, (int)size.height+10)];
        bubbleText.backgroundColor = [UIColor clearColor];
        bubbleText.font = font;
        bubbleText.numberOfLines = 0;
        bubbleText.lineBreakMode = UILineBreakModeCharacterWrap;
        bubbleText.text = text;
        bubbleImageView.frame = CGRectMake(0.0f, 0.0f, 200.0f, size.height+21.0f);
        
        CGSize textSize = [text sizeWithFont:font];
        if (textSize.width > textWidth - 20) 
            bubbleImageView.frame = CGRectMake(iconBounds+10, 10.0f, textWidth, size.height+21.0f);
        else
            bubbleImageView.frame = CGRectMake(iconBounds+10, 10.0f, textSize.width + 21*2, size.height+21.0f);
            
        UIImage *iconImgage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"acquiesce.jpg" ofType:@""]];
        UIImageView *icon = [[UIImageView alloc] initWithImage:iconImgage];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[Instance GetDocumentByString:[NSString stringWithFormat:@"%d.jpg",obj.publishPersonID]]]) {
        icon.image = [UIImage imageWithContentsOfFile:[Instance GetDocumentByString:[NSString stringWithFormat:@"%d.jpg",obj.publishPersonID]]];
        if (icon.image.size.width < 10) {
            icon.image = [UIImage imageNamed:@"acquiesce.jpg"];
        }
    }
        if(fromSelf)
        {
            returnView.frame = CGRectMake(0.0f, 10.0f, 320.0f, size.height+70.0f);
            [icon setFrame:CGRectMake(5, bubbleImageView.frame.origin.y-5, iconBounds, iconBounds)];
         }

        else
        {
            returnView.frame = CGRectMake(120.0f + (200 - bubbleImageView.frame.size.width)-(iconBounds+10)*2, 10.0f, 320, size.height+70.0f);
            [icon setFrame:CGRectMake( bubbleImageView.frame.size.width + iconBounds+10 , bubbleImageView.frame.origin.y-5, iconBounds, iconBounds)];
        }
                   
        [returnView addSubview:icon];
        [bubbleImageView addSubview:bubbleText];
        [bubbleImageView release];
        [bubbleText release];

    
	
    
    
    {
        UIFont *font = [UIFont systemFontOfSize:FontSize];
        CGSize timeSize = [time sizeWithFont:font];
        float originx = fromSelf? icon.frame.origin.x:icon.frame.origin.x + icon.frame.size.width -timeSize.width;
        float BubbleImageHeight = bubbleImageView.frame.size.height;
        if (BubbleImageHeight <40.0f) {
            BubbleImageHeight = 41.0f;
            CGRect rect = returnView.frame;
            rect.size.height = rect.size.height + 20;
            returnView.frame = rect;
        }
        UILabel *timeText = [[UILabel alloc] initWithFrame:CGRectMake(originx, bubbleImageView.frame.origin.y + BubbleImageHeight +10, timeSize.width, timeSize.height)];
  
        timeText.backgroundColor = [UIColor clearColor];
        timeText.font = font;
        timeText.numberOfLines = 0;
        timeText.lineBreakMode = UILineBreakModeCharacterWrap;
        timeText.text = time;
        [returnView addSubview:timeText];
        [timeText release];
    }

    return returnView;
}
-(void)DownPaperIcon:(NSString*)photoUrl Name:(NSString*)name
{
    [self addTapRecognizer];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:photoUrl]];
    [request setDownloadDestinationPath:[Instance GetDocumentByString:[NSString stringWithFormat:@"%@.jpg",name]]];
    [request setDelegate:self];
    [request setTimeOutSeconds:20];
    [request startAsynchronous];
    request.tag = 1000222;
    NSError *error = [request error];
    if (error) {
        NSLog(@"%@",[request responseString]);
        [SVProgressHUD dismiss];
        
    }
}


#pragma mark 刷新
-(void)refreshMessage
{

    
    ePageStart = 0;
    notReadCount = 5;
    [chatArray removeAllObjects];

    [NSThread detachNewThreadSelector:@selector(GetChildPaperMessage) toTarget:self withObject:nil];
    
}

#pragma mark 返回
-(void)navBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addTapRecognizer
{
    if (mTapRecognizer != nil) {
        [[UIApplication sharedApplication].keyWindow  removeGestureRecognizer:mTapRecognizer];
        mTapRecognizer = nil;
    }
    
    mTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapFrom)];
    mTapRecognizer.numberOfTapsRequired = 1; // 单击
    mTapRecognizer.numberOfTouchesRequired = 1; //手指数
    
    [[UIApplication sharedApplication].keyWindow addGestureRecognizer:mTapRecognizer];
    [mTapRecognizer release];
}
-(void)removeTapRecognizer
{
    if (mTapRecognizer != nil) {
        [[UIApplication sharedApplication].keyWindow  removeGestureRecognizer:mTapRecognizer];
        mTapRecognizer = nil;
    }
}
-(NSString *)setFormatID:(int)mID;
{

    return [NSString stringWithFormat:@"M%d",mID];
}

-(void)viewDidLoad
{
    [super viewDidLoad];
//    [Instance baiduApi:@"paper" eventlabel:@"paper"];

    
    
    
    if (![Instance getCharacter]) { //老师
        [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];
    }
    
    [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"刷新" target:self selector:@selector(refreshMessage)];

    
    
    NSMutableArray *muarray = [[NSMutableArray alloc] init];
    self.chatArray = muarray;
    [muarray release];
    
    NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
    self.mIconDownDict = mDict;
    [mDict release];
    
    [mIconDownDict setObject:@"nil"  forKey:[self setFormatID:BACKRESULTCODE]];

    


    self.chatTextFiled = [[UITextField alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 300.0f, 31.0f)];
    self.chatTextFiled.tag = 1000;
    self.chatTextFiled.delegate = self;
    self.chatTextFiled.autocorrectionType = UITextAutocorrectionTypeNo;
    self.chatTextFiled.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.chatTextFiled.borderStyle = UITextBorderStyleRoundedRect;
    self.chatTextFiled.returnKeyType = UIReturnKeySend;
    
    

    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f, [Instance getScreenHeight]-44, 320.0f, 44.0f)];
    toolBar.tag = 2000;
    
    NSMutableArray* allitems = [[NSMutableArray alloc] init];
    [allitems addObject:[[[UIBarButtonItem alloc] initWithCustomView:self.chatTextFiled] autorelease]];
    [toolBar setItems:allitems];
    [allitems release];
    
    [self.view addSubview:toolBar];
    [toolBar release];

    
    
    self.chatTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, [Instance getScreenHeight]-44)];
    self.chatTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.chatTableView.backgroundColor = [UIColor colorWithRed:0.859f green:0.886f blue:0.929f alpha:1.0f];
    [self.chatTableView setScrollEnabled:YES];
    self.chatTableView.delegate = self;
    self.chatTableView.dataSource = self;
    [self.view addSubview:self.chatTableView];
    
    {
        {
            UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0-REFRESH_HEADER_HEIGHT, 320, REFRESH_HEADER_HEIGHT)];
            //    [view setBackgroundColor:[UIColor clearColor]];
            view.backgroundColor = [UIColor colorWithRed:0.859f green:0.886f blue:0.929f alpha:1.0f];
            self.refreshHeaderView = view ; 
            [view release];
        }
        
        {
            UILabel *mRefreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, REFRESH_HEADER_HEIGHT)];
            mRefreshLabel.backgroundColor = [UIColor clearColor];
            [mRefreshLabel setFont:[UIFont systemFontOfSize:14]];
            [mRefreshLabel setTextAlignment:UITextAlignmentCenter];
            mRefreshLabel.text = @"刷新";
            mRefreshLabel.textColor = [UIColor blackColor];
            self.refreshLabel = mRefreshLabel;
            [mRefreshLabel release];
        }
        {
            UIImageView *mRefreshArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow.png"]];
            [mRefreshArrow setFrame:CGRectMake(floorf((REFRESH_HEADER_HEIGHT-27) / 2), floorf((REFRESH_HEADER_HEIGHT - 44 ) / 2), 27, 44)];
            self.refreshArrow = mRefreshArrow;
            [mRefreshArrow release];
        }
        {
            UIActivityIndicatorView *mRefreshSpinner = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            [mRefreshSpinner setFrame:CGRectMake(floorf(floorf(REFRESH_HEADER_HEIGHT - 20) / 2), floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20)];
            [mRefreshSpinner setHidesWhenStopped:YES];
            self.refreshSpinner = mRefreshSpinner;
            [mRefreshSpinner release];
        }
        
        [refreshHeaderView addSubview:refreshLabel];
        [refreshHeaderView addSubview:refreshArrow];
        [refreshHeaderView addSubview:refreshSpinner];
        
        
        [self.chatTableView addSubview:refreshHeaderView];

    }



    
    ePageStart = 0;
    [NSThread detachNewThreadSelector:@selector(GetChildPaperMessage) toTarget:self withObject:nil];

    
    
    //监听键盘高度的变换 
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    

}


-(void)GetChildPaperMessage
{

    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    [self getMessageList];
    [pool release];
    
}
-(void)getMessageList
{
    [self addTapRecognizer];
    
    
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/LeaveMessage/TPFetchLeaveMessageList"];
    NSLog(@"urlstr :%@",urlstr);
    NSURL *url = url = [NSURL URLWithString:urlstr];
    
    NSMutableDictionary *mDic = [[NSMutableDictionary alloc] init];
    
    [mDic setObject:[NSNumber numberWithInt:[[Instance GetUseID] intValue]] forKey:@"ID"];

    if ([Instance getCharacter]) {
        [mDic setObject:[NSNumber numberWithInt:[[Instance GetUseID] intValue]] forKey:@"childID"];
    }
    else
    {
        [mDic setObject:[NSNumber numberWithInt:childid] forKey:@"childID"];
    }
    
    [mDic setObject:[NSNumber numberWithInt:ePageStart] forKey:@"pageStart"];
    [mDic setObject:[NSNumber numberWithInt:10] forKey:@"pageCount"];

    NSString *JSONString = [mDic JSONRepresentation];
    [mDic release];
    
    NSLog(@"JSONString :%@",JSONString);
    [SVProgressHUD show];


    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setTimeOutSeconds:20];
    [request setPostValue:JSONString forKey:@"requestContent"];
    [request startAsynchronous];
    

}

-(BOOL)MatchPaperId:(int)papearId
{
    for (PaperInfo *obj in chatArray) {
        if (obj.messageid == papearId) {
            return NO;
        }
    }
    return YES;
}

#pragma mark requestFinished 
- (void)requestFinished:(ASIHTTPRequest *)request {
    [SVProgressHUD dismiss];
    
    [self removeTapRecognizer];
    
    if (request.tag == 1000222) {
        [self.chatTableView reloadData];
        return;
    }

    if (request.tag == 100011) {

        if ([HealthHeaders DisposeRequest:request]) {
            return;
        }
    }
    if (![HealthHeaders DisposeRequest:request]) {
        return;
    }
    
    [self.chatTableView reloadData];

    
    NSData *responeseData = [request responseData];
    NSString *responseStr = [[NSString alloc] initWithData:responeseData encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *mDict = [responseStr JSONValue];
    NSString *ResultContent = [[mDict objectForKey:@"ResultContent"] JSONRepresentation];
    [responseStr release];

    if ([ResultContent isEqualToString:@""]) {
        return;
    }
    
    NSLog(@"ResultContent :%@",ResultContent);
    NSArray *array = [[[ResultContent JSONValue] valueForKey:@"leaveMessageList"] retain];
    if ([array count] == 0) {
        [array release];
        return;
    }
    
    for (int i = 0; i < [array count]; i++) {
        PaperInfo *nobj = [[PaperInfo alloc] init];

        nobj.messageid = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"ID"]] intValue];

        nobj.content = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"content"]]; 
        nobj.publishDateTime = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"publishDateTime"]];
        nobj.publishPersonUserName = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"publishPersonUserName"]];
        nobj.publishPhoto = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"publishPhoto"]];

        nobj.publishPersonID = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"publishPersonID"]] intValue];
        nobj.teacherOrParent = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"teacherOrParent"]] boolValue];
        nobj.isLoad = NO;
        nobj.image = nil;
        
        BOOL isMark = NO;
        
        for (NSString *string in mIconDownDict) {
            if (nobj.publishPersonID == [[mIconDownDict objectForKey:string] intValue]) {
                isMark = YES;
                break;
            }
        }
        if (!isMark) {
            NSLog(@"publishPersonID :%d",nobj.publishPersonID);

            [self DownPaperIcon:nobj.publishPhoto Name:[NSString stringWithFormat:@"%d",nobj.publishPersonID]];
            [mIconDownDict setObject:[NSNumber numberWithInt:nobj.publishPersonID] forKey:[self setFormatID:nobj.publishPersonID]];
        }
        
        
        if ([self MatchPaperId:nobj.messageid]) {
            if ([chatArray count] == 0 ) {
                [chatArray addObject:nobj];
            }
            else
            {[chatArray insertObject:nobj atIndex:0];}

        }
        [nobj release];
        
        [self.chatTableView reloadData];

    }


    [array release];
    

    


}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    [self removeTapRecognizer];
    [SVProgressHUD dismiss];

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"网络超时，请刷新" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
    
    if (request.tag == 1000222) {
        return;
    }
    if (request.tag == 100011) {
        return;
    }


    

    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [chatArray count];	

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([chatArray count] == 0) {
        return mTABLEHEIGHT;
    }
//    UIView *chatView = [self bubbleView:[chatArray objectAtIndex:[indexPath row]] index:[indexPath row]];
    UIView *chatView = [self bubbleView:[chatArray objectAtIndex:[indexPath row]]];

    float tableHeight = chatView.frame.size.height +10;
    [chatView release];
	return tableHeight;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

//    static NSString *ChatViewCellIdentifier = @"ChatViewCellIdentifier";
//    
//    static BOOL nibsRegistered = NO;
//    if (!nibsRegistered) {
//        UINib *nib = [UINib nibWithNibName:@"ChatViewCell" bundle:nil];
//        [tableView registerNib:nib forCellReuseIdentifier:ChatViewCellIdentifier];
//        nibsRegistered = YES;
//    }
//    
//    ChatViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ChatViewCellIdentifier];
//    if (cell == nil) {
//        cell = [[ChatViewCell alloc]
//                initWithStyle:UITableViewCellStyleDefault
//                reuseIdentifier:ChatViewCellIdentifier];
//    }

    static NSString *cellName = @"cellstr";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    
    
    if (cell== nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellName];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([chatArray count] == 0) {
        return cell;
    }

    
    PaperInfo *obj = [chatArray objectAtIndex:[indexPath row]];


    for(UIView *subview in [cell.contentView subviews])
		[subview removeFromSuperview];
    
    UIView *view = [self bubbleView:obj];
	[cell.contentView addSubview:view];
    [view release];
    
    return cell;

}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (isLoading) return;
    else  isDragging = NO;    
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y < 0) {
        [UIView beginAnimations:nil context:nil];
        if (scrollView.contentOffset.y < -(REFRESH_HEADER_HEIGHT)) {
            refreshLabel.text = @"下拉松开即可刷新...";
            [refreshArrow layer].transform =CATransform3DMakeRotation(M_PI, 0, 0, 1);
        }
        else
        {
            refreshLabel.text = @"下拉更新....";
            [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI*2, 0, 0, 1);
        }
        [UIView commitAnimations];
    }
    
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView.contentOffset.y < 0) {
        if (scrollView.contentOffset.y < -(REFRESH_HEADER_HEIGHT+20)) {
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.3f];
            [refreshSpinner startAnimating];
            refreshArrow.hidden = YES;
            [UIView commitAnimations];
             [self startLoading];       
        }
    }
}
- (void)startLoading {
    isLoading = YES;
    
    // Show the header
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    self.chatTableView.contentInset = UIEdgeInsetsMake(REFRESH_HEADER_HEIGHT, 0, 0, 0);
    refreshLabel.text = @"加载中...";
    refreshArrow.hidden = YES;
    [refreshSpinner startAnimating];
    [UIView commitAnimations];
    
    // Refresh action!
    [self refresh];
}
-(void)refresh
{
//    ePageStart = [chatArray count]/notReadCount;
    [self performSelector:@selector(stopLoading) withObject:nil];
    [NSThread detachNewThreadSelector:@selector(GetChildPaperMessage) toTarget:self withObject:nil];

}
-(void)stopLoading
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.8];
    [UIView setAnimationDidStopSelector:@selector(stopLoadingComplete:finished:context:)];
    self.chatTableView.contentInset = UIEdgeInsetsZero;
    UIEdgeInsets tableContentInset = self.chatTableView.contentInset;
    tableContentInset.top = 0.0;
    
    [refreshArrow layer].transform =CATransform3DMakeRotation(M_PI, 0, 0, 1);
    [UIView commitAnimations];
}
- (void)stopLoadingComplete:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    // Reset the header
    refreshLabel.text = @"Loading";
    refreshArrow.hidden = NO;
    [refreshSpinner stopAnimating];
}

#pragma mark text file methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
	// return NO to disallow editing.

	return YES;
}
-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text = @"";
    return YES;
}
-(BOOL)isBlankString:(NSString *)string{  
    
    if (string == nil) {  
        
        return YES;  
        
    }  
    
    if (string == NULL) {  
        
        return YES;  
        
    }  
    
    if ([string isKindOfClass:[NSNull class]]) {  
        
        return YES;  
        
    }  
    
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0) {  
        
        return YES;  
        
    }  
    if ([string length] == 0) {
        return YES;
    }
    return NO;  
    
}  
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
	// return YES to allow editing to stop and to resign first responder status. NO to disallow the editing session to end
    [textField resignFirstResponder];
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	// called when 'return' key pressed. return NO to ignore.
//    textField.text = @"";
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"HT_UserNo :%@",[defaults objectForKey:NSLocalizedString(@"HT_UserNo", nil)]);
    if ([[defaults objectForKey:NSLocalizedString(@"HT_UserNo", nil)]  isEqualToString:@"10000"]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"测试帐号没有此功能" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        [textField resignFirstResponder];
        return YES;
    }
    
    if ([self isBlankString:textField.text]) {
        return YES;
    }
//    [Instance baiduApi:@"sendpaper" eventlabel:@"sendpaper"];

    
    //BaiduMobStat* statTracker = [BaiduMobStat defaultStat];    [statTracker logEvent:@"sendpaper" eventLabel:@"sendpaper"];


    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //    [dateFormatter setTimeStyle:NSDateFormatterLongStyle];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *now = [dateFormatter stringFromDate:[NSDate date]];
    [dateFormatter release];
    
    
    PaperInfo *obj = [[PaperInfo alloc] init];
    obj.teacherOrParent = FALSE;
    if (![Instance getCharacter]) {// 老师
        obj.teacherOrParent = TRUE;

    }
    obj.publishPersonID = [[Instance GetUseID] intValue];
    obj.content = textField.text;
    obj.publishDateTime = now ;

    [chatArray addObject:obj];
    [obj release];
    
    [self.chatTableView reloadData];
    [self.chatTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[chatArray count]-1 inSection:0]
							  atScrollPosition: UITableViewScrollPositionBottom 
									  animated:NO];
    [textField resignFirstResponder];

    [self performSelector:@selector(SendMessage:) withObject:textField.text afterDelay:0.1f];
    textField.text = @"";
	return YES;
}
#pragma mark send message
-(void)SendMessage:(NSString*)connect
{
    [self addTapRecognizer];
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/LeaveMessage/TPSendLeaveMessage"];
    NSLog(@"urlstr :%@",urlstr);
    NSURL *url = url = [NSURL URLWithString:urlstr];
    
    NSMutableDictionary *mDic = [[NSMutableDictionary alloc] init];
    
    [mDic setObject:[Instance GetUseID] forKey:@"publishPersonID"];
    [mDic setObject:[Instance GetUseID]  forKey:@"childID"];
    [mDic setObject:connect forKey:@"content"];
    
    NSString *JSONString = [mDic JSONRepresentation];
    [mDic release];
    
    NSLog(@"JSONString :%@",JSONString);
    [SVProgressHUD show];

    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    request.tag = 100011;
    [request setDelegate:self];
    [request setTimeOutSeconds:20];
    [request setPostValue:JSONString forKey:@"requestContent"];
    [request startAsynchronous];
    

    

}
#pragma mark Responding to keyboard events
- (void)keyboardWillShow:(NSNotification *)notification {
    
    /*
     Reduce the size of the text view so that it's not obscured by the keyboard.
     Animate the resize so that it's in sync with the appearance of the keyboard.
     */
    
    NSDictionary *userInfo = [notification userInfo];
    
    // Get the origin of the keyboard when it's displayed.
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    // Get the top of the keyboard as the y coordinate of its origin in self's view's coordinate system. The bottom of the text view's frame should align with the top of the keyboard's final position.
    CGRect keyboardRect = [aValue CGRectValue];
    
    // Get the duration of the animation.
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    // Animate the resize of the text view's frame in sync with the keyboard's appearance.
    [self autoMovekeyBoard:keyboardRect.size.height];
}


- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    /*
     Restore the size of the text view (fill self's view).
     Animate the resize so that it's in sync with the disappearance of the keyboard.
     */
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    
    [self autoMovekeyBoard:FALSE];
}
-(void) autoMovekeyBoard: (BOOL) h{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3f];
    [self.view setFrame:CGRectMake(0, h? -200:0, self.view.frame.size.width, self.view.frame.size.height)];
    [UIView commitAnimations];
    
    
}
-(void)handleSingleTapFrom
{
    [self.chatTextFiled resignFirstResponder];
}
-(void)dealloc
{
    [mIconDownDict release];
    
    
    [refreshLabel release];
    [refreshArrow release];
    [refreshSpinner release];
    [refreshHeaderView release];
    
    [chatArray release];
    
    [self.chatTableView release];
    [self.chatTextFiled release];

    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mIconDownDict = nil;
    
    self.chatTextFiled = nil;

    self.refreshLabel = nil;
    self.refreshArrow = nil;
    self.refreshSpinner = nil;
    self.refreshHeaderView = nil;
    
    self.chatArray = nil;
    self.chatTableView = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
