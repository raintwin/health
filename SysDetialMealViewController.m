//
//  SysDetialMealViewController.m
//  Health
//
//  Created by dinghao on 13-4-19.
//
//

#import "SysDetialMealViewController.h"

@interface SysDetialMealViewController ()

@end

@implementation SysDetialMealViewController
@synthesize mHttpRequest;
@synthesize foodDetails,nutritionDetails;
@synthesize sysDetialMealsDict;
@synthesize sysmeal;
@synthesize mTableView;
#define TableViewHeiht 30
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}
-(void)dealloc
{
    [super dealloc];
    [self.foodDetails release];
    [self.nutritionDetails release];
    [self.sysDetialMealsDict release];

    [self.mTableView release];
    
    
}
-(void)viewDidUnload
{
    [super viewDidUnload];
    self.foodDetails = nil;
    self.nutritionDetails = nil;
    self.sysDetialMealsDict = nil;
    self.mTableView = nil;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.foodDetails = [[NSMutableArray alloc]init];
    self.nutritionDetails = [[NSMutableArray alloc]init];
    self.sysDetialMealsDict = [[NSMutableDictionary alloc]init];

    NSLog(@"self.view.frame = %@",NSStringFromCGRect(self.view.frame));
    self.mTableView = [[UITableView alloc]initWithFrame:self.view.frame style:UITableViewStylePlain];
    self.mTableView.delegate = self;
    self.mTableView.dataSource = self;
    [self.view addSubview:self.mTableView];
    [self performSelector:@selector(RequestData) withObject:nil afterDelay:0.1f];

}
-(void)RequestData
{

    
    {
        [self.sysDetialMealsDict setObject:NSLocalizedString(@"PORT_SysMealDetail", nil) forKey:NSLocalizedString(@"PORT_ReqType", nil)];
        [self.sysDetialMealsDict setObject:sysmeal.uType forKey:@"uType"];
        [self.sysDetialMealsDict setObject:[NSNumber numberWithInt:sysmeal.ID] forKey:@"id"];
        
        HttpFormatRequest *httprequest = [[HttpFormatRequest alloc]init];
        httprequest.Delegate = self;
        self.mHttpRequest = httprequest;
        [self.view addSubview:mHttpRequest];
        [httprequest release];
        
        NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,NSLocalizedString(@"URL_Expand",nil)];
        [mHttpRequest CustomFormDataRequestDict:[sysDetialMealsDict JSONRepresentation] tag:0 url:urlstr];
        
    }
    
}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{
}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    NSArray *foodList = [[[ResultContent JSONValue] valueForKey:@"foodList"] retain];
    if (foodList >0) {
        for (int i = 0 ; i < [foodList count]; i++) {
            FoodNutritiont *food = [[FoodNutritiont alloc]init];
            food.foodName = [NSString stringWithFormat:@"%@",[[foodList objectAtIndex:i] valueForKey:@"foodname"]];
            food.foodAmount = [NSString stringWithFormat:@"%@",[[foodList objectAtIndex:i] valueForKey:@"amount"]];
            [self.foodDetails addObject:food];
            [food release];
        }
    }
    [foodList release];
    
    NSArray *nutritionList = [[[ResultContent JSONValue] valueForKey:@"nutrition"] retain];
    if ([nutritionList count] >0) {
        for (int i = 0 ; i < [nutritionList count]; i++) {
            FoodNutritiont *food = [[FoodNutritiont alloc]init];
            food.nutritionName = [NSString stringWithFormat:@"%@",[[nutritionList objectAtIndex:i] valueForKey:@"name"]];
            food.nutritionAmount = [NSString stringWithFormat:@"%@",[[nutritionList objectAtIndex:i] valueForKey:@"amount"]];
            food.unit = [NSString stringWithFormat:@"%@",[[nutritionList objectAtIndex:i] valueForKey:@"unit"]];
            [self.nutritionDetails addObject:food];
            [food release];
        }
    }
    [nutritionList release];
    
    [self.mTableView reloadData];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0) {
        return [self.foodDetails count];
    }
    if (section == 1) {
        return [self.nutritionDetails count];
    }
    return 0;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIImageView *headerImageView = [[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, TableViewHeiht)] autorelease];
    [headerImageView setImage:[UIImage imageNamed:@"cellheader.png"]];
    NSString *string = nil;
    if (section == 0) {
        string = @"材料";
    }
    if (section == 1) {
        string = @"提供营养";
    }
    UILabel *mLabel = [Instance addLableRect:CGRectMake(5, 0, 320, TableViewHeiht) text:string textsize:15];
    mLabel.textAlignment = UITextAlignmentCenter;
    [headerImageView addSubview:mLabel];
    [mLabel release];
    
    return headerImageView;
}
-(float)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return TableViewHeiht;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return TableViewHeiht+10;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        UIFont *font = [UIFont boldSystemFontOfSize:15];
        {
            UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(cell.contentView.frame.origin.x+10, cell.contentView.frame.origin.y, 200, cell.contentView.frame.size.height)];
            label.tag = 1000;
            label.backgroundColor = [UIColor clearColor];
            label.font = font;
            label.text = @"";
            [cell.contentView addSubview:label];
            [label release];
        }
        {
            UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(220, cell.contentView.frame.origin.y, 100, cell.contentView.frame.size.height)];
            label.tag = 1001;
            label.backgroundColor = [UIColor clearColor];
            label.font = font;
            label.text = @"";
            [cell.contentView addSubview:label];
            [label release];
        }
    }
    UILabel *labelName  = (UILabel *)[cell.contentView viewWithTag:1000];
    UILabel *labelAmount = (UILabel *)[cell.contentView viewWithTag:1001];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    FoodNutritiont *foodNutritiont = nil;
    if ([indexPath section] == 0) {
        foodNutritiont = [self.foodDetails objectAtIndex:[indexPath row]];

        labelName.text = foodNutritiont.foodName;
        labelAmount.text = foodNutritiont.foodAmount;
    }
    if ([indexPath section] == 1) {
        foodNutritiont = [self.nutritionDetails objectAtIndex:[indexPath row]];
        labelName.text = foodNutritiont.nutritionName;
        labelAmount.text = [NSString stringWithFormat:@"%@ %@",foodNutritiont.nutritionAmount,foodNutritiont.unit];
    }
    // Configure the cell...
    
    return cell;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
