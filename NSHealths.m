//
//  NSHealths.m
//  Health
//
//  Created by hq on 12-8-9.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "NSHealths.h"

@implementation NSHealths
@synthesize hID;
@synthesize hPublishTime,hPublishPerson,hTitle,hPublishPersonPhoto;
@synthesize isLoad;
@synthesize image;
-(void)dealloc
{
    [hPublishTime release]; hPublishTime = nil;
    [hPublishPerson release]; hPublishPerson = nil;
    [hTitle release]; hTitle = nil;
    [hPublishPersonPhoto release]; hPublishPersonPhoto = nil;
    [image release]; image = nil;
    [super dealloc];
}
@end


@implementation NSHealthsContext

@synthesize ReadCount,hcID;
@synthesize hcTitle,hcContent,hcAuthor,hcSource,hcKeyWord,hcPublishDateTime,hcUserName,hcPhoto;

-(void)dealloc
{
    [hcTitle release]; hcTitle = nil;
    [hcContent release]; hcContent = nil;
    [hcAuthor release]; hcAuthor = nil;
    [hcSource release]; hcSource = nil;
    [hcKeyWord release]; hcKeyWord = nil;
    [hcPublishDateTime release]; hcPublishDateTime = nil;
    [hcUserName release]; hcUserName = nil;
    [hcPhoto release]; hcPhoto = nil;
    [super dealloc];
}
@end