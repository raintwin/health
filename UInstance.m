//
//  GetUser.m
//  Health
//
//  Created by hq on 12-7-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "UInstance.h"

@implementation  UInstance
#pragma mark getdocument
+(NSString*)GetDocument
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    return documentDirectory;
}
+(NSString*)GetDocumentByString:(NSString*)string
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    documentDirectory = [documentDirectory stringByAppendingPathComponent:string];
    return documentDirectory;

}
+(NSString*)GetUseID
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];

    documentDirectory =  [documentDirectory stringByAppendingPathComponent:@"initlalize"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:documentDirectory]) {
        NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:documentDirectory];
        return [dic objectForKey:@"userID"];
    }
    else
        return nil;
}
+(NSString*)GetChildID
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    documentDirectory =  [documentDirectory stringByAppendingPathComponent:@"initlalize"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:documentDirectory]) {
        NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:documentDirectory];
        return [dic objectForKey:@"childID"];
    }
    else
        return nil;
}
+(int)GetUseType
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    documentDirectory =  [documentDirectory stringByAppendingPathComponent:@"initlalize"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:documentDirectory]) {
        NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:documentDirectory];
        return [[dic objectForKey:@"logintype"] intValue];
    }
    else
        return -1;

}
#pragma mark UINavigationItem 
+(void)setLeftImageBtnForNavigationItem:(UINavigationItem*)item Image:(NSString*) image target:(id) target selector:(SEL) selector
{
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:image].size.width, [UIImage imageNamed:image].size.height)];
    
    
    [btn setBackgroundImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    
    [btn addTarget: target action: selector forControlEvents: UIControlEventTouchUpInside];
    
    
    
    UIBarButtonItem* bi = [[UIBarButtonItem alloc] initWithCustomView: btn];
    [btn release];
    //UIImageView iv = [[UIImageView alloc] initWithImage:[]]
    
    item.leftBarButtonItem = bi;
    [bi release];	
    
}
+(void)setRightImageBtnForNavigationItem:(UINavigationItem*)item Image:(NSString*) image target:(id) target selector:(SEL) selector
{
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:image].size.width, [UIImage imageNamed:image].size.height)];
    
    
    [btn setBackgroundImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    
    [btn addTarget: target action: selector forControlEvents: UIControlEventTouchUpInside];
    
    
    
    UIBarButtonItem* bi = [[UIBarButtonItem alloc] initWithCustomView: btn];
    [btn release];
    //UIImageView iv = [[UIImageView alloc] initWithImage:[]]
    
    item.rightBarButtonItem = bi;
    [bi release];	
    
}
+(void)setRightImageAndNameForNavigationItem:(UINavigationItem*)item Image:(NSString*) image title:(NSString*)title target:(id)target selector:(SEL) selector
{
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:image].size.width, [UIImage imageNamed:image].size.height)];
    UIFont *font = [UIFont boldSystemFontOfSize:16];
    [btn setTitle:title forState:0];
    btn.titleLabel.font = font; 
    [btn setTitleColor:[UIColor whiteColor] forState:0];
    [btn setTitleShadowColor:[UIColor blackColor] forState:0];
    [btn setBackgroundImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    
    [btn addTarget: target action: selector forControlEvents: UIControlEventTouchUpInside];
    
    
    
    UIBarButtonItem* bi = [[UIBarButtonItem alloc] initWithCustomView: btn];
    [btn release];
    //UIImageView iv = [[UIImageView alloc] initWithImage:[]]
    
    item.rightBarButtonItem = bi;
    [bi release];	
    
}

+(void) setLeftImageAndNameForNavigationItem:(UINavigationItem*)item Image:(NSString*) image title:(NSString*)title target:(id)target selector:(SEL) selector;
{
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:image].size.width, [UIImage imageNamed:image].size.height)];
    UIFont *font = [UIFont boldSystemFontOfSize:16];
    [btn setTitle:title forState:0];
    btn.titleLabel.font = font; 
    [btn setTitleColor:[UIColor whiteColor] forState:0];
    [btn setTitleShadowColor:[UIColor blackColor] forState:0];
    [btn setBackgroundImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    
    [btn addTarget: target action: selector forControlEvents: UIControlEventTouchUpInside];
    
    
    
    UIBarButtonItem* bi = [[UIBarButtonItem alloc] initWithCustomView: btn];
    [btn release];
    //UIImageView iv = [[UIImageView alloc] initWithImage:[]]
    
    item.leftBarButtonItem = bi;
    [bi release];	
    
}

#pragma mark TextFileld
+(UITextField*)addTextFileld:(id)target originX:(float)x originY:(float)y width:(float)width height:(float)height
{
    UITextField *textfield = [[UITextField alloc] initWithFrame:CGRectMake(x,y, width, height)];
    [textfield setBorderStyle:UITextBorderStyleLine];
    textfield.placeholder = @"";
    textfield.text = @"";
    textfield.delegate = target;
    textfield.returnKeyType=UIReturnKeyDone;
    textfield.backgroundColor = [UIColor clearColor];
    return textfield;
}
+(UITextField*)addTextFileld:(id)trage frame:(CGRect)frame
{
    UITextField *textfield = [[UITextField alloc] initWithFrame:frame];
    [textfield setBorderStyle:UITextBorderStyleLine];
    textfield.placeholder = @"";
    textfield.text = @"";
    textfield.delegate = trage;
    textfield.returnKeyType=UIReturnKeyDone;
    textfield.backgroundColor = [UIColor clearColor];
    return textfield;
}

#pragma mark UITextView

+(UITextView*)addTextView:(id)target originX:(float)x originY:(float)y width:(float)width height:(float)height
{
    UITextView *textview = [[UITextView alloc]initWithFrame:CGRectMake(x, y, width, height)];
    [textview setBounds:CGRectMake(x, y, width, height)];
    textview.layer.borderColor = [UIColor grayColor].CGColor;
    textview.layer.borderWidth =1.0;
    textview.layer.cornerRadius =5.0;
    textview.text = @"";
    textview.delegate = target;
    textview.returnKeyType = UIReturnKeyDone;
    textview.backgroundColor = [UIColor whiteColor];
    return textview;
}

+(UITextView*)addTextView:(id)target frame:(CGRect)frame
{
    UITextView *textview = [[UITextView alloc]initWithFrame:frame];
    [textview setBounds:frame];
    textview.layer.borderColor = [UIColor grayColor].CGColor;
    textview.layer.borderWidth =1.0;
    textview.layer.cornerRadius =5.0;
    textview.text = @"";
    textview.delegate = target;
    textview.returnKeyType = UIReturnKeyDone;
    textview.backgroundColor = [UIColor whiteColor];
    return textview;

}

#pragma mark UILabel
+(UILabel*)addLablePoint:(CGPoint)point text:(NSString*)text textsize:(float)textsize
{
    UIFont *font = [UIFont systemFontOfSize:textsize];
    CGSize size = [text sizeWithFont:font];
    CGRect rect = CGRectMake(point.x, point.y, size.width, size.height);
    UILabel *label = [[UILabel alloc] initWithFrame:rect];
    label.text = text;
    label.font = font;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    return label;
}
+(UILabel*)addLableRect:(CGRect)rect  text:(NSString*)text textsize:(float)textsize
{
    UIFont *font = [UIFont systemFontOfSize:textsize];
    UILabel *label = [[UILabel alloc] initWithFrame:rect];
    label.text = text;
    label.font = font;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    return label;
}


#pragma mark button
+(UIButton *)addUIButton:(UIView *)fview Title:(NSString*)Title  rect:(CGRect)rect
{	
	UIButton *btn=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btn setFrame:rect];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn.backgroundColor = [UIColor clearColor];
    [btn setTitle:Title forState:UIControlStateNormal];
	[fview addSubview:btn];
	//[b addTarget:self action:@selector(m) forControlEvents:UIControlEventTouchUpInside];
	[btn release];
	return	btn;
}
@end



#import "JSON.h"
@implementation HealthHeaders
+(BOOL)DisposeRequest:(ASIHTTPRequest *)request
{
    NSData *responeseData = [request responseData];
    NSString *responseStr = [[NSString alloc] initWithData:responeseData encoding:NSUTF8StringEncoding];
    
    
    responseStr =   [responseStr stringByReplacingOccurrencesOfString: @"\r" withString:@""];
    responseStr =   [responseStr stringByReplacingOccurrencesOfString: @"\n" withString:@""];
    
    NSLog(@"---- responseStr %@",responseStr);
    NSMutableDictionary *mDict = [responseStr JSONValue];
    [responseStr release];
    
    int ResultCode = [[[mDict objectForKey:@"ResultStuates"] objectForKey:@"ResultCode"] intValue];
    NSLog(@"ResultCode %d",ResultCode);
    if (ResultCode == 0) {
        NSLog(@"发送正确，完成发送");
        return YES;
    }
    else if (ResultCode == 1000 )
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"用户名或密码错误" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        [SVProgressHUD dismiss];
        return NO;
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"网络错误" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        [SVProgressHUD dismiss];
        
        return NO;
    }
    
}
@end

