//
//  SingButtonView.m
//  Health
//
//  Created by hq on 12-6-25.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "SingButtonView.h"

@implementation SingButtonView
@synthesize Count;
@synthesize tagBtn;
@synthesize preImage;
@synthesize nextImage;
@synthesize pressMun;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        Count = 0;
        tagBtn = -1;
        pressMun = -1;
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
//-(void)setTagBtn:(int)tagBtnMun;
//{
//    self.tagBtn = tagBtnMun;
//}
-(void)setImage1:(NSString *)Image1  SetImage2:(NSString *)Image2 BtnTile:(NSMutableArray *)titles
{
//    float frameWidth;
    preImage = Image1;
    nextImage = Image2;
    float frameWidth = 0.0f;
    for (int i = 0; i<=[titles count]-1; i++) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(frameWidth , 0, self.frame.size.width, self.frame.size.height)];
        
        if ((tagBtn-1) == i) {
            [btn setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:Image1 ofType:@""]] forState:UIControlStateNormal];
        }
        else
        {
            [btn setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:Image2 ofType:@""]] forState:UIControlStateNormal];

        }
        frameWidth = btn.frame.origin.x + btn.frame.size.width;
    
        UIFont *font = [UIFont systemFontOfSize:self.frame.size.height];
        NSString *str = [titles objectAtIndex:i];
        CGSize size = [str sizeWithFont:font];
        UILabel *labelText  = [[UILabel alloc] initWithFrame:CGRectMake(frameWidth + 3, -1, size.width,size.height)];
        labelText.font = font;
        labelText.text = str;
        [labelText setBackgroundColor:[UIColor clearColor]];
        frameWidth = frameWidth + size.width + 15;
        btn.tag = i ;
        [btn addTarget:self action:@selector(ChageBtn:) forControlEvents:64];
        [self addSubview:btn];
        [self addSubview:labelText];
        [btn release];
        [labelText release];
    }
    
    CGRect rect;
    rect = self.frame;
    rect.size.width = frameWidth;
    [self setFrame:rect];
}
-(void)ChageBtn:sender
{
    for (id ctr in [self subviews]) {
        if ([ctr isKindOfClass:[UIButton class]]) {
            UIImage *image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:nextImage ofType:@""]];
            [ctr setBackgroundImage:image forState:UIControlStateNormal];
        }        
    }
    UIButton *tmpBtn = sender;
    [tmpBtn setBackgroundImage:[UIImage imageNamed:preImage] forState:UIControlStateNormal];
    pressMun = tmpBtn.tag;
}
-(void)setImage:(NSString *)imagePath
{
    float frameWidth;
    for (int i = 0; i<=(Count-1); i++) {
        UIImage *image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:imagePath ofType:@""]];
        UIImageView *img = [[UIImageView alloc]initWithImage:image];
        [img setFrame:CGRectMake((self.frame.size.width)*i , 0, self.frame.size.width, self.frame.size.height)];
        [self addSubview:img];
        frameWidth = img.frame.origin.x;
        frameWidth += img.frame.size.width;
        
    }
    CGRect rect;
    rect = self.frame;
    rect.size.width = frameWidth;
    [self setFrame:rect];
    
}
-(void)dealloc
{
    [preImage release];
    [nextImage release];
    [super dealloc];
}
-(int)retunSelectBtn
{
    return pressMun;
}
@end
