//
//  Dish.m
//  Health
//
//  Created by hq on 12-7-31.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "Dish.h"

@implementation Dish
@synthesize DishName,DishWeight;
@synthesize DishID;
-(void)dealloc
{
    [DishName release]; DishName = nil;
    [DishWeight release]; DishWeight = nil;
    [super dealloc];
}
@end
