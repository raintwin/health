//
//  NSHealths.h
//  Health
//
//  Created by hq on 12-8-9.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSHealths : NSObject

@property(nonatomic,assign)int hID;
@property(nonatomic,retain)NSString *hTitle,*hPublishTime,*hPublishPerson,*hPublishPersonPhoto;
@property(nonatomic,assign)BOOL isLoad;
@property(nonatomic,retain)UIImage *image;
@end



@interface NSHealthsContext : NSObject 


@property(nonatomic,assign)int ReadCount,hcID;
@property(nonatomic,retain)NSString *hcTitle,*hcContent,*hcAuthor,*hcSource,*hcKeyWord,*hcPublishDateTime,*hcUserName,*hcPhoto;

    
@end