//
//  ProvinceCity.m
//  Health
//
//  Created by hq on 12-9-13.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ProvinceCity.h"

@implementation ProvinceCity
@synthesize province,city;
@synthesize provinceID,cityID;
-(void)dealloc
{
    [province release]; province = nil;
    [city release]; city  = nil;
    [super dealloc];
}
@end
