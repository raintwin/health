//
//  FoodListViewController.h
//  Health
//
//  Created by hq on 12-6-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodListViewController : UITableViewController<HttpFormatRequestDeleagte>
{
    
}
@property(nonatomic,assign)int menuID;
@property(nonatomic,retain)NSMutableArray *mRecipeMenus;
@property(nonatomic,assign)int mValue;
@property(nonatomic,assign)BOOL mIsMakeRicepe;
@end
