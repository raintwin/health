//
//  RecipeMakeView.m
//  Health
//
//  Created by jiaodian on 12-10-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "RecipeMakeView.h"

@interface RecipeMakeView ()

@end

@implementation RecipeMakeView
@synthesize menuID;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"菜谱做法";
    }
    return self;
}
-(void)backNav
{

    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(backNav)];

    [self performSelector:@selector(GetRecipeMake) withObject:nil afterDelay:0.1f];
}
-(void)GetRecipeMake
{
    
    NSMutableDictionary*mDict = [[NSMutableDictionary alloc] init];
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/InHomeArrangeMeal/TPFetchOneMenuCookingMethod"];
    [mDict setObject:[NSNumber numberWithInt:menuID] forKey:@"menuID"];
    
    
    
    NSString *JSONString = [mDict JSONRepresentation];
    [mDict release];
    

    
    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate = self;
    [mHttpRequest CustomFormDataRequestDict:JSONString tag:0 url:urlstr];
    [self.view addSubview:mHttpRequest];
    [mHttpRequest release];

}
#pragma  mark requestFinished

-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{

    
    //    NSMutableArray *more = [[NSMutableArray alloc]init];
    NSString *CookingMethod = [[ResultContent JSONValue] valueForKey:@"cookingMethod"];
    
    NSLog(@"CookingMethod :%@",CookingMethod);
    UIWebView *webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 368)];
    [webview loadHTMLString:CookingMethod baseURL:nil];
    [self.view addSubview:webview];
    [webview release];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
