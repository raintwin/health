//
//  PhotoListViewController.h
//  Health
//
//  Created by hq on 12-7-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyScrollView.h"
#import "DownLoadTableImage.h"
//#import "WeiboViewController.h"




@interface PhotoListViewController : UIViewController<UIApplicationDelegate,ASIHTTPRequestDelegate,UIScrollViewDelegate,myscrollViewDelegate,UIActionSheetDelegate,UIAlertViewDelegate,HttpFormatRequestDeleagte>
{
    int mShareIndex;
    UITapGestureRecognizer *mTempRecognizer;


}

@property(nonatomic,assign)int downCount;

@property(nonatomic,assign)int ScrollerPages;
@property(nonatomic,assign)int mChildId;
@property(nonatomic,assign)BOOL isInhomeOrSchool;
@property(nonatomic,retain)UIScrollView *mPhotoScrollView;
@property(nonatomic,retain)NSMutableArray *mPhotos;
@property(nonatomic,retain)UIView *mFunctionView,*mCommentView;



@end
