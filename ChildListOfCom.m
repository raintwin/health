//
//  ChildListOfCom.m
//  Health
//
//  Created by hq on 12-6-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ChildListOfCom.h"
#import "CChild.h"
#import "DownLoadTableImage.h"


@implementation ChildListOfCom
@synthesize _chliddelgelagte;
@synthesize mChilds;
@synthesize mTable;
@synthesize mDownLoadImageDict;
@synthesize mComInfo;


#define tableHeight 40
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)CancelDownLoad
{
    for (int i = 0; i<[mChilds count]; i++) {
        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:i inSection:0];
        DownLoadTableImage *imageDownLoad = [mDownLoadImageDict objectForKey:indexpath];
        if (imageDownLoad != nil) {
            [imageDownLoad CancelDownloadImage];
        }
        
    }
    [mDownLoadImageDict removeAllObjects];
}
#pragma mark - View lifecycle
-(void)navBack
{

    [self CancelDownLoad];
    if ([mChilds count]>0) {
        [_chliddelgelagte SetPitchs:mChilds];
    }
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)PitchOn
{
    for (CCommont *obj  in mChilds) {
        obj.isCheck = YES;
    }
    [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"反选" target:self selector:@selector(CanclePit)];

    [mTable reloadData];
}
-(void)CanclePit
{
    for (CCommont *obj  in mChilds) {
        obj.isCheck = NO;
    }
    [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"全选" target:self selector:@selector(PitchOn)];
    [mTable reloadData];

}
#pragma mark requestFinished

-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    NSArray *array = [[[ResultContent JSONValue] valueForKey:@"childList"] retain];
    NSMutableArray *tmpArray = [[NSMutableArray alloc]initWithCapacity:[array count]];
    
    for (int i = 0; i < [array count]; i++) {
        CCommont *sif = [[CCommont alloc] init];
        sif.cID         = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childID"]] intValue];
        sif.cName   = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childName"]];
        sif.cPhotoUrl    = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"photo"]];
        
        sif.isLoad = NO;
        sif.image = nil;
        sif.isCheck = NO;
        sif.isExist = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"evaluated"]] boolValue];
        sif.mComId = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childName"]] intValue];
        
        
        if (sif.isExist) {
            sif.isCheck = NO;
            [tmpArray insertObject:sif atIndex:0];
        }
        [sif release];
    }
    
    for (int i = 0; i < [array count]; i++) {
        CCommont *sif = [[CCommont alloc] init];
        sif.cID         = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childID"]] intValue];
        sif.cName   = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childName"]];
        sif.cPhotoUrl    = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"photo"]];
        
        sif.isLoad = NO;
        sif.image = nil;
        sif.isCheck = NO;
        sif.isExist = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"evaluated"]] boolValue];
        sif.mComId = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childName"]] intValue];
        
        
        if (!sif.isExist) {
            sif.isCheck = YES;
            [tmpArray insertObject:sif atIndex:0];
        }
        [sif release];
    }
    
    self.mChilds = tmpArray;
    NSLog(@"mChilds count :%d",[mChilds count]);
    
    [tmpArray release];
    [array release];
    
    
    [mTable reloadData];
    

}
-(void)GetChildList
{
    
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/ChildRepresentation/TeacherFetchRepresentationChildListForSelect"];
    
    NSMutableDictionary *mDic = [[NSMutableDictionary alloc] init];
    
    [mDic setObject:[Instance GetUseID] forKey:@"ID"];
    [mDic setObject:[mComInfo objectForKey:@"date"] forKey:@"RepresentationDate"];
    [mDic setObject:[mComInfo objectForKey:@"type"] forKey:@"RepresentationType"];
    
    NSString *JSONString = [mDic JSONRepresentation];
    [mDic release];
    
    NSLog(@"JSONString :%@",JSONString);
    
    
    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate = self;
    [mHttpRequest CustomFormDataRequestDict:JSONString tag:0 url:urlstr];
    [self.view addSubview:mHttpRequest];
    [mHttpRequest release];
    

    
}






#pragma mark  load imageview 
-(void)StartDownLoadImage:(NSString*)url IndexPath:(NSIndexPath*)indexpath 
{
    DownLoadTableImage *imageDownLoad = [mDownLoadImageDict objectForKey:indexpath];
    if (imageDownLoad == nil) {
        imageDownLoad = [[DownLoadTableImage alloc] init];
        imageDownLoad.ImageURL = url;
        imageDownLoad.ImageHeight = 30;
        imageDownLoad.ImageWidth = 30;
        imageDownLoad.IndexPathTo = indexpath;
        imageDownLoad.Delegate =self;
        [mDownLoadImageDict setObject:imageDownLoad forKey:indexpath];
        [imageDownLoad StartDownloadImage];
        [imageDownLoad release];
    }
}
#pragma mark finish download image
-(void)FinishDownLoadImage:(NSIndexPath*)indexpath Image:(UIImage*)image IsFailed:(BOOL)isFaild
{
    DownLoadTableImage *imageDownLoad = [mDownLoadImageDict objectForKey:indexpath];
    if (imageDownLoad != nil) {
        UITableViewCell *cell = [self.mTable cellForRowAtIndexPath:indexpath];
        CCommont *obj = [mChilds objectAtIndex:[indexpath row]];
        
        if (isFaild) {
            obj.image = [UIImage imageNamed:@"acquiesce.jpg"];
            obj.isLoad = NO;
        }
        else
        {
            obj.image = image;
            obj.isLoad = YES;
        }
        
        cell.imageView.image = obj.image;
    }
}
- (void)loadImagesForOnscreenRows
{
    if ([mChilds count] > 0)
    {
        NSArray *visiblePaths = [self.mTable indexPathsForVisibleRows];
        for (NSIndexPath *indexPath in visiblePaths)
        {
            if ([indexPath row]<[mChilds count]) {
                CCommont *item = [mChilds objectAtIndex:indexPath.row];
                
                if (!item.isLoad ) // avoid the app icon download if the app already has an icon
                {
                    [self StartDownLoadImage:item.cPhotoUrl IndexPath:indexPath];
                }
                
            }
        }
    }
}
#pragma mark -
#pragma mark Deferred image loading (UIScrollViewDelegate)
#pragma mark -
// Load images for all onscreen rows when scrolling is finished
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	if (!decelerate)
	{
        [self loadImagesForOnscreenRows];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadImagesForOnscreenRows];
}
#pragma mark tableview 

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == mTable) {
        
        if ([mChilds count]<[indexPath row]) {
            return;
        }
        CCommont *sif = [mChilds objectAtIndex:[indexPath row]];
        
        
        if ([[tableView cellForRowAtIndexPath:indexPath] accessoryType] == UITableViewCellAccessoryCheckmark) {
            [[tableView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryNone];
            sif.isCheck = NO;
            
        }
        else
        {
            [[tableView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryCheckmark];
            sif.isCheck = YES;
            
        }
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return tableHeight;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [mChilds count];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    
    static NSString *cellName = @"cellstr";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    

    if (cell== nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellName];
        UIImage *image =[UIImage imageNamed:@"acquiesce.jpg"];
        cell.imageView.image = image;
        
        {
            UIImageView *markImg = [[UIImageView alloc]initWithFrame:CGRectMake(292, 9, 23, 23)];
            [markImg setImage:[UIImage imageNamed:@"tu_04.png"]];
            [cell.contentView addSubview:markImg];
            [markImg release];
        }
        {            
            UILabel *parentText = [[UILabel alloc]initWithFrame:CGRectMake(0 , 0, 320, tableHeight)];
            parentText.tag = 100001;
            parentText.font = [UIFont boldSystemFontOfSize:13];
            [parentText setBackgroundColor:[UIColor clearColor]];
            parentText.textColor = [UIColor blackColor];
            parentText.textAlignment = UITextAlignmentCenter;
            parentText.text = nil;
            [cell.contentView addSubview:parentText];
            [parentText release];
            
        }
        
        {
            UIImageView *markImg = [[UIImageView alloc]initWithFrame:CGRectMake(260, 12, 15, 15)];
            [markImg setImage:[UIImage imageNamed:@"已读.gif"]];
            markImg.tag = 1000011;
            [cell.contentView addSubview:markImg];
            [markImg release];
        }

    }
    


    
    NSInteger row = [indexPath row];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    [cell setAccessoryType:UITableViewCellAccessoryNone];


    CCommont *sif = [mChilds objectAtIndex:row];
    if (sif == NULL) {
        return cell;
    }
    NSString *childStr =  [NSString stringWithFormat:@"%@",sif.cName];
        
        // --------------------- load image
        {
            if (!sif.isLoad) {
                if (self.mTable.dragging == NO && self.mTable.decelerating == NO) {
                    [self StartDownLoadImage:sif.cPhotoUrl IndexPath:indexPath];
                }
            }
            else
            {
                cell.imageView.image = sif.image;
            }
        }
    
    {
        UILabel *label =(UILabel *) [cell.contentView viewWithTag:100001];
        label.text = childStr;
        label.textColor = [UIColor blackColor];
            
    }
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    UIImageView *imageview = (UIImageView *)[cell.contentView viewWithTag:1000011];
    [imageview setImage:nil];
    
    if (!sif.isExist) {
        if (imageview != nil) {
            imageview.image = [UIImage imageNamed:@"未读.gif"];
        }
    }
    else {
        imageview.image = [UIImage imageNamed:@"已读.gif"];

    }
    
    
    if (sif.isCheck) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else 
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
   return cell;
    
}
#pragma mark viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    [ViewTool addUIImageViewWithAutoSize:self.view imageName:@"viewblackgroup.jpg" type:@"" x:0 y:0 x1:0 y1:0];
    
    // Do any additional setup after loading the view from its nib.
    
    [Instance setLeftImageAndNameForNavigationItem:self.navigationItem Image:@"blankbutton.png" title:@"确定" target:self selector:@selector(navBack)];
    [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"全选" target:self selector:@selector(PitchOn)];
    
    
    
    NSMutableDictionary *mDictionary = [[NSMutableDictionary alloc]init];
    self.mDownLoadImageDict = mDictionary;
    [mDictionary release];
    
    NSLog(@"com mChilds count=%d",[mChilds count]);
    
    
    
    {
        UITableView *table = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,372)];
        table.delegate = self;
        table.dataSource = self;
        table.backgroundColor = [UIColor clearColor];
        self.mTable = table;
        [table release];
        
        [self.view addSubview:mTable];
    }


    [self performSelector:@selector(GetChildList) withObject:nil afterDelay:0.1f];
}
-(void)dealloc
{

    [mComInfo release];
    [mDownLoadImageDict release]; 
    [mChilds release]; 
    [mTable release]; 
    [super dealloc];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mDownLoadImageDict = nil;
    self.mTable = nil;
    self.mChilds = nil ;
    self.mComInfo = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
