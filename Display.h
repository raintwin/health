//
//  Display.h
//  Health
//
//  Created by hq on 12-8-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Display : NSObject
@property(nonatomic,retain)NSString  *childName,*childPhoto;
@property(nonatomic,assign)int childID,inSchool,inHome;
@property(nonatomic,retain)UIImage *image;

@property(nonatomic,assign)BOOL isLoad;



@end


@interface DisplayInfo : NSObject 


@property(nonatomic,assign)int mChildOfDisID,mDisType,mDiserID;
@property(nonatomic,retain)NSString *mDisComment,*mDisDate,*mDiserName;
@end