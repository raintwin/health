//
//  TodayMenusViewController.h
//  Health
//
//  Created by hq on 12-6-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Recipe.h"
@interface TodayMenusViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,HttpFormatRequestDeleagte>
{
    BOOL isDinner;
}
@property(nonatomic,retain)UITableView *mTable;
@property(nonatomic,retain)NSMutableArray *mRecipes;
@property(nonatomic,retain)NSMutableDictionary *mRecipeAnysisDict;
@property(nonatomic,retain)Recipe *mRecipeClass;
@property(nonatomic,assign)BOOL isToday;

@end
