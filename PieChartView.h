//
//  PieChartView.h
//  Health
//
//  Created by hq on 12-9-12.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PieChartView : UIView
- (id)initWithFrame:(CGRect)frame value:(NSMutableArray *)Values;

@end
