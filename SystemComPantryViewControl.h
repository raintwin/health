//
//  SystemComPantryViewControl.h
//  Health
//
//  Created by jiaodian on 12-12-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol SystemComPantryViewControlDelegate <NSObject>

-(void)SetNavBarHidden:(BOOL)isBOOL;

@end

@interface SystemComPantryViewControl : UIViewController<UITableViewDataSource,UITableViewDelegate,HttpFormatRequestDeleagte>


{
    int index;
    id<SystemComPantryViewControlDelegate>Delegate;
    int rescordSectionCount;
}

-(void)saveSysMealList;

@property(nonatomic,retain) NSMutableDictionary *requstRecordMealDict;


@property(nonatomic,assign) BOOL isRecordData;



@property(assign,nonatomic)id<SystemComPantryViewControlDelegate>Delegate;

@end
