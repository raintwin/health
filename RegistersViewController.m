//
//  RegistersViewController.m
//  Health
//
//  Created by hq on 12-7-5.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "RegistersViewController.h"
#import "ASIFormDataRequest.h"
#import "PeopleView.h"
#import "LoginCharacter.h"
#import "NewsAlertViewCotroller.h"
#define tablewidth 37.0f

#define mUserName   @"USERNAME"
#define mPassWord   @"PASSWORD"
#define mRegistNum  @"REGISTNUMBER"


@implementation RegistersViewController
@synthesize RegistInfo;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
//        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"img_03.png"]]];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)navBack
{

    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - View lifecycle
-(void)CompleteRigister
{

    UITableView *mTableView = (UITableView*)[self.view viewWithTag:100111];
    if ( mTableView == nil) {
        return;
    }
        //----------- 用户名
        {
            NSIndexPath *mIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            UITableViewCell *cell = [mTableView cellForRowAtIndexPath:mIndexPath];
            UITextField *textfiel =(UITextField *)[cell.contentView viewWithTag:10000];
            
            if (![textfiel.text isEqualToString:@""] && textfiel !=nil) {
                [RegistInfo setObject:textfiel.text forKey:mUserName];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入用户名" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
                [alert release];
                return;
            }
        }
        {
            NSIndexPath *mIndexPath = [NSIndexPath indexPathForRow:2 inSection:0];
            UITableViewCell *cell = [mTableView cellForRowAtIndexPath:mIndexPath];
            UITextField *textfiel =(UITextField *)[cell.contentView viewWithTag:10002];
            if (![textfiel.text isEqualToString:@""]) {
                [RegistInfo setObject:textfiel.text forKey:mRegistNum];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入验证码" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
                [alert release];
                return;
            }
            
        }
    [self performSelector:@selector(RegistRuqest:) withObject:RegistInfo];
        
        
    

}
-(void)AppServer
{
    NewsAlertViewCotroller *detailViewController = [[NewsAlertViewCotroller alloc] initWithNibName:@"NewsAlertViewCotroller" bundle:nil];
    detailViewController.title = @"服务协议";
    // ...
    // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    //BaiduMobStat* statTracker = [BaiduMobStat defaultStat];    [statTracker logEvent:@"regist" eventLabel:@"regist"];

    
    // Do any additional setup after loading the view from its nib.
//    [ViewTool addUIImageView:self.view imageName:@"img_03.png" type:@"" x:28 y:10 x1:262 y1:37];
    NSMutableDictionary *mDict = [[NSMutableDictionary alloc] init];
    self.RegistInfo = mDict;
    [mDict release];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"viewblackgroup.jpg"]]];

    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];
    [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"注册" target:self selector:@selector(CompleteRigister)];
    {
        UITableView *mytable = [[UITableView alloc]initWithFrame:self.view.frame style:UITableViewStyleGrouped ];
        mytable.delegate = self;
        mytable.dataSource = self;
        [mytable setBackgroundColor:[UIColor clearColor]];
        [mytable setScrollEnabled:NO];
        mytable .tag = 100111;
        [self.view addSubview:mytable];
        [mytable release];

    }   
    
    {
        UIButton *btn = [Instance addUIButtonByImage:self.view image:@"我已阅读.png" rect:CGRectMake(0, 0, 174, 13)];
        [btn setCenter:CGPointMake(160, 280)];
        [btn addTarget:self action:@selector(Versions) forControlEvents:64];
    }
    {
        
        NSString *titleStr = @"根据短信提示操作,即可成功注册";
        UILabel *title = [Instance addLableRect:CGRectMake(0,295, 320, 30) text:titleStr textsize:16];
        title.textColor = HEXCOLOR(0x1d1c1c);
        title.textAlignment = UITextAlignmentCenter;
        [self.view addSubview:title];
        [title release];
    }
}

-(void)Versions
{

    NewsAlertViewCotroller *detailViewController = [[NewsAlertViewCotroller alloc] initWithNibName:@"NewsAlertViewCotroller" bundle:nil];
    detailViewController.title = @"版权信息";
    // ...
    // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];

}


#pragma mark - 注册

-(void)RegistRuqest:(NSMutableDictionary*)mDict
{
    
    
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/UserManager/AuthUser"];
    
    NSMutableDictionary *mDic = [[NSMutableDictionary alloc] init];
    
    [mDic setObject:[mDict objectForKey:mUserName] forKey:@"mobile"];
    [mDic setObject:[mDict objectForKey:mRegistNum] forKey:@"authCode"];

    NSString *JSONString = [mDic JSONRepresentation];
    [mDic release];
    
    
    
    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate = self;
    [mHttpRequest CustomFormDataRequestDict:JSONString tag:10002 url:urlstr];
    [self.view addSubview:mHttpRequest];
    [mHttpRequest release];
    

    
}


#pragma mark - 获取验证码

-(void)GetRegistNum:(NSMutableDictionary*)mDict
{

    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/UserManager/Register"];
    
    NSMutableDictionary *mDic = [[NSMutableDictionary alloc] init];
    
    [mDic setObject:[mDict objectForKey:mUserName] forKey:@"mobile"];
    [mDic setObject:[mDict objectForKey:mPassWord] forKey:@"password"];
    
    NSString *JSONString = [mDic JSONRepresentation];
    [mDic release];
    
    
    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate = self;
    [mHttpRequest CustomFormDataRequestDict:JSONString tag:1000 url:urlstr];
    [self.view addSubview:mHttpRequest];
    [mHttpRequest release];
    


}

#pragma mark requestFinished 
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    

 }



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 4;
}
-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return tablewidth;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    // Configure the cell...
    {        
        NSArray *array1 = [[NSArray alloc] initWithObjects:@"用户名:", @"密  码:",@"验证码:",@"点击获取验证码",nil];
        cell.textLabel.text = [array1 objectAtIndex:[indexPath row]];
        [array1 release];
        if ([indexPath row] == [array1 count]-1) {
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
            [cell.textLabel setTextAlignment:UITextAlignmentCenter];
        }
    }

    
    {
        if ([indexPath row] == 3) {
            return cell;
        }
        NSArray *array2 = [[NSArray alloc] initWithObjects:@"请输入手机号码", @"请输入密码",@"请输入验证码",@"",nil];
        NSString *titleStr = [array2 objectAtIndex:[indexPath row]];
        UITextField *nameText  = [[UITextField alloc] initWithFrame:CGRectMake(70, (int)(tablewidth-20)/2, 200, 20)];
        [nameText setBorderStyle:UITextBorderStyleNone];
        nameText.tag = [indexPath row]+10000;
        nameText.clearButtonMode=UITextFieldViewModeWhileEditing;
        if ([indexPath row] == 1) {
            [nameText setSecureTextEntry:YES];
        }
        nameText.placeholder = titleStr;
        nameText.delegate = self;
        nameText.text = @"";
        nameText.returnKeyType=UIReturnKeyDone;
        [cell.contentView addSubview:nameText];
        [nameText release];
        
        [array2 release];

        
    }


    return cell;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag == 10000) {
        [RegistInfo setObject:textField.text forKey:mUserName];
    }
    if (textField.tag == 10001) {

        [RegistInfo setObject:textField.text forKey:mPassWord];

    }
    if (textField.tag == 10002) {

        [RegistInfo setObject:textField.text forKey:mRegistNum];

    }

    [textField resignFirstResponder];
    return NO;
}
-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text = @"";
    [textField resignFirstResponder];
    return NO;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    UITableView *mTableView = (UITableView*)[self.view viewWithTag:100111];
    if ( mTableView == nil) {
        return;
    }
    if ([indexPath row] == 3) {
        //----------- 用户名
        {
            NSIndexPath *mIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            UITableViewCell *cell = [mTableView cellForRowAtIndexPath:mIndexPath];
            UITextField *textfiel =(UITextField *)[cell.contentView viewWithTag:10000];

            if (![textfiel.text isEqualToString:@""] && textfiel !=nil) {
                [RegistInfo setObject:textfiel.text forKey:mUserName];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入用户名" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
                [alert release];
                return;
            }
        }
        {
            NSIndexPath *mIndexPath = [NSIndexPath indexPathForRow:1 inSection:0];
            UITableViewCell *cell = [mTableView cellForRowAtIndexPath:mIndexPath];
            UITextField *textfiel =(UITextField *)[cell.contentView viewWithTag:10001];
            if (![textfiel.text isEqualToString:@""]) {
                [RegistInfo setObject:textfiel.text forKey:mPassWord];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入用户密码" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
                [alert release];
                return;
            }

        }

        NSLog(@"取获验证码");

        [self performSelector:@selector(GetRegistNum:) withObject:RegistInfo];
        
    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

-(void)dealloc
{
    [RegistInfo release];
    [super dealloc];
    
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.RegistInfo = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
