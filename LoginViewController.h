//
//  LoginViewController.h
//  Health
//
//  Created by hq on 12-6-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BulidController.h"
#import "JsonInstance.h"
#import "PaymentViewController.h"
#import "DMAppDelegate.h"
//@protocol LoginViewDelegate <NSObject>
//
//-(void)InitTabBar;
//
//@end
@protocol  LoginViewControllerDelegate<NSObject>

-(void)GoLoginViewController;

@end
@interface LoginViewController : UIViewController<UIAlertViewDelegate,UITextFieldDelegate,HttpFormatRequestDeleagte>
{
    id<LoginViewControllerDelegate>delegate;

}
@property (retain,nonatomic) DMAppDelegate *rootDelegate;

@property(nonatomic,assign)id<LoginViewControllerDelegate>delegate;
@end
