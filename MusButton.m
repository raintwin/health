//
//  MusButton.m
//  Health
//
//  Created by hq on 12-9-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "MusButton.h"

@implementation MusButton
@synthesize BTNTAG;
@synthesize mRemarkBtns;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:[UIColor clearColor]];
        mCount = 0;
        BTNTAG = 0;
        NSMutableArray *array = [[NSMutableArray alloc]init];
        self.mRemarkBtns = array;
        [array release];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
-(void)setTitles:(NSMutableArray*)titles
{
    mCount = [titles count];
    int i = 0 ;
    UIImage *image = [UIImage imageNamed:@"tu_14.png"];

    for (NSString *str in titles) {
        UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(i*70, 0, image.size.width, image.size.height)];

        if (i == 0) [btn setBackgroundImage:image forState:0];
        else [btn setBackgroundImage:[UIImage imageNamed:@"tu_04.png"] forState:0];
        btn.tag = i;
        [btn addTarget:self action:@selector(Remark:) forControlEvents:64];
        [mRemarkBtns addObject:btn];
        [self addSubview:btn];
        [btn release];
        
        CGSize size = [[titles objectAtIndex:i] sizeWithFont:[UIFont systemFontOfSize:13]];
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(25+i*70, 2, size.width, size.height)];
        label.text = [titles objectAtIndex:i];
        label.font = [UIFont systemFontOfSize:13];
        [label setBackgroundColor:[UIColor clearColor]];
        label.textColor = [UIColor blackColor];
        [self addSubview:label];
        [label release];
        i++;
        
    }
    CGRect rect = self.frame;
    rect.size.width = i*60+60;
    [self setFrame:rect];
}
-(void)Remark:sender
{
    UIButton *tmpe = sender;
    for (UIButton *btn in mRemarkBtns) {
        [btn setBackgroundImage:[UIImage imageNamed:@"tu_04.png"] forState:0];
    }
    [tmpe setBackgroundImage:[UIImage imageNamed:@"tu_14.png"] forState:0];
    BTNTAG = tmpe.tag;
}
-(int)GetBtnValue
{
    return BTNTAG;
}
-(void)SetBtn:(int)tag
{
    UIButton *tmpe = [mRemarkBtns objectAtIndex:tag];
    for (UIButton *btn in mRemarkBtns) {
        [btn setBackgroundImage:[UIImage imageNamed:@"tu_04.png"] forState:0];
    }
    [tmpe setBackgroundImage:[UIImage imageNamed:@"tu_14.png"] forState:0];
    BTNTAG = tmpe.tag;

}
-(void)dealloc
{
    [mRemarkBtns release]; mRemarkBtns = nil;
    [super dealloc];
}
@end
