//
//  FoodInfoViewController.h
//  Health
//
//  Created by hq on 12-6-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodInfoViewController : UITableViewController<HttpFormatRequestDeleagte>
{
}
@property(nonatomic,assign)int foodNutrientDistributionID;

@property(nonatomic,retain)NSMutableDictionary *mFoodNutrientDetailInfo;

@end
