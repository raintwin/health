//
//  JsonFormat.h
//  Health
//
//  Created by hq on 12-7-25.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
@interface JsonFormat : NSObject<ASIHTTPRequestDelegate>

//-(void)JsonForNotitySend:(NSMutableArray*)ChildeID titleText:(NSString*)tileText contentText:(NSString*)contenText senderID:(NSString*)senderID trage:(id)trage;
-(void)JsonForNotitySend:(NSMutableDictionary*)mDict NSURL:(NSURL*)url trage:(id)trage;
-(void)JsonForPhotoSend:(NSMutableDictionary*)mDict photoarray:(NSMutableArray*)photos NSURL:(NSURL*)url trage:(id)trage;

@end
