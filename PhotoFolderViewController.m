//
//  PhotoFolderViewController.m
//  Health
//
//  Created by hq on 12-7-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "PhotoFolderViewController.h"
#import "PhotoListViewController.h"
#import "DownLoadTableImage.h"
#import "CChild.h"

#define tHeight  83.0f
#define tWidth 100.0f
#define ePageCount 1000
#define eDownLoadNum 16

@implementation PhotoFolderViewController
@synthesize photoArray;
@synthesize mChildID;
@synthesize isInhomeOrSchool;
@synthesize mDownLoadImageDict;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        mChildID = -1;
        mPageStart = -1;
        isInhomeOrSchool = NO;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)CancelDownLoad
{
    for (NSString *obj in mDownLoadImageDict) {
//        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:i inSection:0];
        DownLoadScrollImage *imageDownLoad = [mDownLoadImageDict objectForKey:obj];
        
        if (imageDownLoad != nil) {
            [imageDownLoad CancelDownloadImage];
        }
        
    }
    [mDownLoadImageDict removeAllObjects];
}

-(void)navigationBack
{
    [self CancelDownLoad];
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)GetChildPhoto
{
    
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/Photo/TPFetchChildPhotoRecordsOfOneBaby"];
    
    NSMutableDictionary *mDic = [[NSMutableDictionary alloc] init];
    mPageStart = 0;
    mPageStart = [photoArray count]/ePageCount;
    [mDic setObject:[NSNumber numberWithInt:mChildID] forKey:@"ID"];
    [mDic setObject:[NSNumber numberWithInt:ePageCount] forKey:@"PageCount"];
    [mDic setObject:[NSNumber numberWithInt:mPageStart] forKey:@"PageStart"];
    [mDic setObject:[NSNumber numberWithBool:isInhomeOrSchool] forKey:@"inSchoolOrInHome"];
    

    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate = self;
    [mHttpRequest CustomFormDataRequestDict:[mDic JSONRepresentation] tag:0 url:urlstr];
    [self.view addSubview:mHttpRequest];
    [mHttpRequest release];
    [mDic release];
    
}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    NSArray *array = [[[ResultContent JSONValue] valueForKey:@"childPhotoRecordsOfOneBaby"] retain];
    
    for (int i = 0; i < [array count]; i++) {
        ChildOfPhotoInfo *mChildObj = [[ChildOfPhotoInfo alloc] init];        
        mChildObj.childPhotoID = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childPhotoID"]] intValue];
        mChildObj.childPhotoComment = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childPhotoComment"]];
        mChildObj.publishDateTime = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"publishDateTime"]];
        mChildObj.publishPersonID = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"publishPersonID"]] intValue];
        mChildObj.publishPersonName = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"publishPersonName"]];
        
        mChildObj.picUrl = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"picUrl"]];
        mChildObj.smallPicUrl = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"smallPicUrl"]];
        
        mChildObj.smallphotoimage  = nil;
        mChildObj.bigphotoimage = nil;
        
        mChildObj.isSmallLoad = NO;
        mChildObj.isBigLoad = NO;
        
        [photoArray addObject:mChildObj];
        [mChildObj release];
    }
    [array release];
    
    if ([photoArray count] == 0 ) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"暂无照片" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    [self InitScrollView:photoArray];

}


-(void)InitScrollView:(NSMutableArray*)array
{
    [SVProgressHUD show];
    UIScrollView *scroll =(UIScrollView*) [self.view viewWithTag:1110111];
    if (scroll == nil) {
        return;
    }
    for (int i = 0; i< [array count]; i++) {
        
        ChildOfPhotoInfo *copi = [array objectAtIndex:i];
        UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(5+(i%4)*79, (i/4)*79, 75, 75)];
        [imageview setImage:[UIImage imageNamed:@"照片.png"]];
        imageview.tag = i;
        [scroll addSubview:imageview];
        [imageview release];
        
        if (i < eDownLoadNum) {
            [self StartDownLoadImage:copi.smallPicUrl tag:i];
        }
    }
    [scroll setContentSize:CGSizeMake(0,([array count]/eDownLoadNum +1) * 316)];
    
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navigationBack)];
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    {
        NSMutableDictionary *mDictionary = [[NSMutableDictionary alloc]init];
        self.mDownLoadImageDict = mDictionary;
        [mDictionary release];
    }    
    {
        NSMutableArray *array = [[NSMutableArray alloc]init];
        self.photoArray = array;
        [array release];
    }
    
    {
        UIScrollView *mScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 10, self.view.frame.size.width, 316)];
        mScrollView.delegate = self;
//        [mScrollView setBackgroundColor:[UIColor whiteColor]];
        mScrollView.tag = 1110111;
        [mScrollView setPagingEnabled:YES];
        mScrollView.showsVerticalScrollIndicator  = NO;
        mScrollView.showsHorizontalScrollIndicator = NO;
        [self.view addSubview:mScrollView];
        [mScrollView release];
        
    }

    [self performSelector:@selector(GetChildPhoto) withObject:nil afterDelay:0.1f];
}

#pragma mark 大图
-(void)GoBigPhotot:sender
{

    UIButton *tmp = sender;
    PhotoListViewController *detailViewController = [[PhotoListViewController alloc] initWithNibName:@"PhotoListViewController" bundle:nil];
    detailViewController.mChildId = mChildID;
    detailViewController.mPhotos = photoArray;
    detailViewController.isInhomeOrSchool = isInhomeOrSchool;
    detailViewController.ScrollerPages = tmp.tag;

    [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview:detailViewController.view];
    [detailViewController.view setFrame:[UIApplication sharedApplication].keyWindow.rootViewController.view.bounds];

    
    
//    [detailViewController release];

}

#pragma mark  load imageview 
-(void)StartDownLoadImage:(NSString*)url  tag:(int)index
{
    DownLoadScrollImage *imageDownLoad = [mDownLoadImageDict objectForKey:[NSNumber numberWithInt:index]];
    if (imageDownLoad == nil) {
        imageDownLoad = [[DownLoadScrollImage alloc] init];
        imageDownLoad.ImageWidth  = 100;
        imageDownLoad.ImageHeight = 100;
        imageDownLoad.tag = index;
        imageDownLoad.ImageURL = url;
        imageDownLoad.Delegate =self;
        [mDownLoadImageDict setObject:imageDownLoad forKey:[NSNumber numberWithInt:index]];
        [imageDownLoad StartDownloadImage];
        [imageDownLoad release];
    }
    
}
#pragma mark finish download image

-(void)FinishDownLoadImageOfScrooll:(int)index Image:(UIImage*)image IsFailed:(BOOL)isFaild
{
    [SVProgressHUD dismiss];
    DownLoadScrollImage *imageDownLoad = [mDownLoadImageDict objectForKey:[NSNumber numberWithInt:index]];
    if (imageDownLoad != nil) {
        ChildOfPhotoInfo *obj = [photoArray objectAtIndex:imageDownLoad.tag];
        
        if (isFaild) {
            obj.smallphotoimage = [UIImage imageNamed:@"acquiesce.jpg"];
            obj.isSmallLoad = NO;
        }
        else
        {
            if (image.size.width >100 || image.size.height>100) {
                CGSize itemSize = CGSizeMake(75, 75);
                UIGraphicsBeginImageContext(itemSize);
                CGRect imageRect = CGRectMake(0.0, 0.0, 75, 75);
                [image drawInRect:imageRect];
                image = UIGraphicsGetImageFromCurrentImageContext();
                [image retain];
                UIGraphicsEndImageContext();
                obj.smallphotoimage = image;
                [image release];
            }
            else
            { obj.smallphotoimage = image;}
            
            obj.isSmallLoad = YES;

            
        }
        
        UIScrollView *scorllview =  (UIScrollView *)[self.view viewWithTag:1110111];
        if (scorllview == nil) {
            return;
        }
        UIImageView *ImageView = (UIImageView *)[scorllview viewWithTag:index];
        if (ImageView == nil || obj.smallphotoimage == nil) {
            return;
        }
        {
            ImageView.image = obj.smallphotoimage;
            [ImageView setContentMode:UIViewContentModeScaleAspectFit];
            UIButton *btn = [[UIButton alloc]initWithFrame:ImageView.frame];
            btn.tag = index;
            [ImageView setBackgroundColor:[UIColor clearColor]];
            [scorllview addSubview:btn];
            [btn addTarget:self action:@selector(GoBigPhotot:) forControlEvents:64];
            [btn release];
        }
    }
}


-(void)loadImagesForScrollRows:(int)Rows
{
    int index = Rows*eDownLoadNum;
    for (int i = index; i<index+eDownLoadNum; i++) {
        if (i>=[photoArray count]) {
            return;
        }
        else
        {
            ChildOfPhotoInfo *pObj = [photoArray objectAtIndex:i];
            if (pObj.smallphotoimage == nil) {
                [self StartDownLoadImage:pObj.smallPicUrl tag:i];
            }
        }
    }
}
#pragma mark -
#pragma mark Deferred image loading (UIScrollViewDelegate)
#pragma mark -
// Load images for all onscreen rows when scrolling is finished
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    CGFloat pageWidth = scrollView.frame.size.height;
    // 根据当前的x坐标和页宽度计算出当前页数
	int currentPage = floor((scrollView.contentOffset.y - pageWidth / 2) / pageWidth) + 1;

	if (!decelerate)
	{
        NSLog(@"currentPage ing  %d",currentPage);
        [self loadImagesForScrollRows:currentPage];
//        [self performSelector:@selector(loadImagesForScrollRows) withObject:currentPage];
    }
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = scrollView.frame.size.height;
    // 根据当前的x坐标和页宽度计算出当前页数
	int currentPage = floor((scrollView.contentOffset.y - pageWidth / 2) / pageWidth) + 1;
    NSLog(@"currentPage  edn row %d",currentPage);
    [self loadImagesForScrollRows:currentPage];
}

//-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
//{
//    CGFloat pageWidth = scrollView.frame.size.height;
//    // 根据当前的x坐标和页宽度计算出当前页数
//	int currentPage = floor((scrollView.contentOffset.y - pageWidth / 2) / pageWidth) + 1;
//    NSLog(@"currentPage row %d",currentPage);
//
//}

//-(void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    CGFloat pageWidth = scrollView.frame.size.height;
//    // 根据当前的x坐标和页宽度计算出当前页数
//	int currentPage = floor((scrollView.contentOffset.y - pageWidth / 2) / pageWidth) + 1;
//    NSLog(@"scrollViewDidScroll row %d",currentPage);
//
//}
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    CGFloat pageWidth = scrollView.frame.size.width;
//    // 根据当前的x坐标和页宽度计算出当前页数
//	int currentPage = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
//
//    [self loadImagesForScrollRows:currentPage];
//    
//}

-(void)dealloc
{
    [mDownLoadImageDict release];
    [photoArray release]; 
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mDownLoadImageDict = nil;
    self.photoArray = nil;

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
