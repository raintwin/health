
//
//  FoodListViewController.m
//  Health
//
//  Created by hq on 12-6-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "FoodListViewController.h"
#import "Recipe.h"
#import "FoodInfoViewController.h"
#import "RecipeMakeView.h"
#import "JSON.h"
#define mTableHeight 46
#define mTableTextSize 14
@implementation FoodListViewController
@synthesize menuID;
@synthesize mRecipeMenus;
@synthesize mValue;
@synthesize mIsMakeRicepe;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
//        arrays  = [[NSMutableArray alloc] initWithObjects:@"热量",@"水分",@"脂肪",@"蛋白质",@"碳水化合物",@"钙", nil];
        mValue = -10;
        mIsMakeRicepe = NO;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)navBack
{

    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - View lifecycle
-(void)RecipeMake
{
    
    RecipeMakeView *detailViewController = [[RecipeMakeView alloc]initWithNibName:@"RecipeMakeView" bundle:nil];
    detailViewController.menuID = menuID;

    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    //BaiduMobStat* statTracker = [BaiduMobStat defaultStat];[statTracker logEvent:@"openFood" eventLabel:@"openFood"];

    
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];
    if (mIsMakeRicepe) {
        [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:FOURENSURE title:@"菜谱做法" target:self selector:@selector(RecipeMake)];

    }
    NSMutableArray *array = [[NSMutableArray alloc] init];
    self.mRecipeMenus = array;
    [array release];
    
    [self.tableView setScrollEnabled:NO];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];

//    [NSThread detachNewThreadSelector:@selector(GetRecipe) toTarget:self withObject:nil];
    [self performSelector:@selector(GetRecipe) withObject:nil afterDelay:0.1f];
}
-(void)GetRecipe
{
    
    NSMutableDictionary*mDict = [[NSMutableDictionary alloc] init];
    NSString *urlstr;
    if (mValue>=0) {
       urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/InHomeArrangeMeal/TPFetchOneMenuList"];
    }
    else
    {    
        urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/InSchoolArrangeMeal/TPFetchOneMenuList"];
    }
    
    [mDict setObject:[NSNumber numberWithInt:menuID] forKey:@"menuID"];
    
    
    
    NSString *JSONString = [mDict JSONRepresentation];
    [mDict release];
    
    
    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate = self;
    [mHttpRequest CustomFormDataRequestDict:JSONString tag:0 url:urlstr];
    [self.view addSubview:mHttpRequest];
    [mHttpRequest release];
}
#pragma  mark requestFinished

-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    
    //---------------
    NSMutableDictionary *RecipeDict = [[NSMutableDictionary alloc] init];
    [RecipeDict setObject:[NSNumber numberWithInt:[[[ResultContent JSONValue] valueForKey:@"weekArrangeMealBasicID"] intValue]] forKey:@"weekArrangeMealBasicID"];
    [RecipeDict setObject:[NSNumber numberWithInt:[[[ResultContent JSONValue] valueForKey:@"dayOfWeek"] intValue]] forKey:@"dayOfWeek"];
    
    [RecipeDict release];
    
    //----------------- 
    
    
    //    NSMutableArray *more = [[NSMutableArray alloc]init];
    NSArray *array = [[[ResultContent JSONValue] valueForKey:@"oneMenuList"] retain];
    
    for (int i = 0; i< [array count]; i++) {
        RecipeMenuList *recipeclass = [[RecipeMenuList alloc] init];
        recipeclass.foodKindName = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"foodKindName"]];
        recipeclass.foodName = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"foodName"]];
        recipeclass.foodPicUrl = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"foodPicUrl"]];
        
        recipeclass.foodNutrientDistributionID = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"foodNutrientDistributionID"]] intValue];
        recipeclass.foodWeight = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"foodWeight"]] intValue];
        
        [mRecipeMenus addObject:recipeclass];
        [recipeclass release];
    }
    
    [self.tableView reloadData];
    [array release];

}
-(void)dealloc
{
    [mRecipeMenus release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mRecipeMenus = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  mTableHeight;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [mRecipeMenus count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 46)];
        if ([indexPath row]%2 == 0)  [img setImage:[UIImage imageNamed:@"xia.jpg"]];        
        else  [img setImage:[UIImage imageNamed:@"shang.jpg"]];      
        [cell.contentView addSubview:img];
        [img release];

        
        {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 120, mTableHeight)];
            label.lineBreakMode = UILineBreakModeWordWrap;
            label.numberOfLines = 2;

            label.text = @"";
            label.tag = 100011;
            label.font = [UIFont systemFontOfSize:mTableTextSize];
            label.backgroundColor = [UIColor clearColor];
            label.textColor = [UIColor blackColor];
            [cell.contentView addSubview:label];
            [label release];
        }
        {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(120, 0, 170, mTableHeight)];
            label.text = @"";
            label.tag = 100022;
            label.font = [UIFont systemFontOfSize:mTableTextSize];
            label.backgroundColor = [UIColor clearColor];
            label.textColor = [UIColor blackColor];
            [cell.contentView addSubview:label];
            [label release];
        }
        {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(200, 0, 120, mTableHeight)];
            label.text = @"";
            label.tag = 100033;
            label.font = [UIFont systemFontOfSize:mTableTextSize];
            label.backgroundColor = [UIColor clearColor];
            label.textColor = [UIColor blackColor];
            [cell.contentView addSubview:label];
            [label release];
        }

    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    RecipeMenuList *recipeObj = [mRecipeMenus objectAtIndex:[indexPath row]];
    
    if ((UILabel *)[cell.contentView viewWithTag:100011]!=nil) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"  %@",recipeObj.foodName];
    }
    if (mValue<0) {
        if ((UILabel *)[cell.contentView viewWithTag:100022]!=nil) {
            ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"%d克",recipeObj.foodWeight];
        }
    }
    if ((UILabel *)[cell.contentView viewWithTag:100033]!=nil) {
        ((UILabel *)[cell.contentView viewWithTag:100033]).text = [NSString stringWithFormat:@"%@",recipeObj.foodKindName];
    }

    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    // Navigation logic may go here. Create and push another view controller.
    RecipeMenuList *recipeObj = [mRecipeMenus objectAtIndex:[indexPath row]];

     FoodInfoViewController *detailViewController = [[FoodInfoViewController alloc] initWithStyle:UITableViewStylePlain];
    detailViewController.foodNutrientDistributionID = recipeObj.foodNutrientDistributionID;
    detailViewController.title = recipeObj.foodName;
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     
}

@end
