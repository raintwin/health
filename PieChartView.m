//
//  PieChartView.m
//  Health
//
//  Created by hq on 12-9-12.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "PieChartView.h"
#import "PCPieChart.h"

@implementation PieChartView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (id)initWithFrame:(CGRect)frame value:(NSMutableArray *)Values;
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        [self setBackgroundColor:[UIColor colorWithWhite:1 alpha:1]];
        [self setBackgroundColor:[UIColor clearColor]];
        
        int height = frame.size.height/3*2.; // 220;
        int width = frame.size.width; //320;
        PCPieChart *pieChart = [[PCPieChart alloc] initWithFrame:CGRectMake(0,0,width,height)];
        [pieChart setShowArrow:YES];
        [pieChart setSameColorLabel:YES];
        [pieChart setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin];
        [pieChart setDiameter:width/2];
        [self addSubview:pieChart];
        [pieChart release];
        
        if ([[UIDevice currentDevice] userInterfaceIdiom]==UIUserInterfaceIdiomPad)
        {
            pieChart.titleFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:30];
            pieChart.percentageFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:50];
        }
        
//        NSString *sampleFile = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"sample_piechart_data.plist"];
//        NSDictionary *sampleInfo = [NSDictionary dictionaryWithContentsOfFile:sampleFile];
        NSMutableArray *components = [NSMutableArray array];
        int i = 0;
        for (NSMutableDictionary *mDict in Values) {
            if ([[mDict objectForKey:@"value"] intValue] == 0) {
//                continue;
            }
            PCPieComponent *component = [PCPieComponent pieComponentWithTitle:[mDict objectForKey:@"title"] value:[[mDict objectForKey:@"value"] floatValue]];
            [components addObject:component];
            if (i==0)
            {
                [component setColour:PCColorYellow];
            }
            else if (i==1)
            {
                [component setColour:PCColorGreen];
            }
            else if (i==2)
            {
                [component setColour:PCColorOrange];
            }
            else if (i==3)
            {
                [component setColour:PCColorRed];
            }
            else if (i==4)
            {
                [component setColour:PCColorBlue];
            }
            else if (i == 5)
            {
                [component setColour:PCColorOther1];
            }
            else if (i == 6)
            {
                [component setColour:PCColorOther2];
            }
            else if (i == 7)
            {
                [component setColour:PCColorOther3];
            }
            else if (i == 8)
            {
                [component setColour:PCColorOther4];
            }
            else if (i == 9)
            {
                [component setColour:PCColorDefault];
            }           
            i++;
        }
//        for (int i=0; i<[[sampleInfo objectForKey:@"data"] count]-1; i++)
//        {
//            NSDictionary *item = [[sampleInfo objectForKey:@"data"] objectAtIndex:i];
//            PCPieComponent *component = [PCPieComponent pieComponentWithTitle:[item objectForKey:@"title"] value:[[item objectForKey:@"value"] floatValue]];
//            [components addObject:component];
//            
//            if (i==0)
//            {
//                [component setColour:PCColorYellow];
//            }
//            else if (i==1)
//            {
//                [component setColour:PCColorGreen];
//            }
//            else if (i==2)
//            {
//                [component setColour:PCColorOrange];
//            }
//            else if (i==3)
//            {
//                [component setColour:PCColorRed];
//            }
//            else if (i==4)
//            {
//                [component setColour:PCColorBlue];
//            }
//        }
        [pieChart setComponents:components];
        
    }

    return self;
}
@end
