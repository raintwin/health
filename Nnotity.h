//
//  Nnotity.h
//  Health
//
//  Created by hq on 12-8-3.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Nnotity : NSObject
@property(nonatomic,retain)NSString *nPhoto,*nPublishTime,*nTitle,*nName;
@property(nonatomic,retain)UIImage *image;

@property(nonatomic,assign)int nID;

@property(nonatomic,assign)BOOL isLoad;
@end
