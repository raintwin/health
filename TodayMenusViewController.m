//
//  TodayMenusViewController.m
//  Health
//
//  Created by hq on 12-6-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "TodayMenusViewController.h"
#import "FoodListViewController.h"
#import "AnalysisViewController.h"
#import "FoodMenuAnalysisController.h"
#import "Recipe.h"
#define mTableHeight  30.0f
@implementation TodayMenusViewController
@synthesize mTable;
@synthesize mRecipes;
@synthesize isToday;
@synthesize mRecipeAnysisDict;
@synthesize mRecipeClass;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)navBack
{

    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark 食谱分析
-(void)AnysisRecipe
{

    
    NSLog(@"mRecipeAnysisDict :%@",[mRecipeAnysisDict JSONRepresentation]);
    FoodMenuAnalysisController *detailViewController = [[FoodMenuAnalysisController alloc] initWithStyle:UITableViewStylePlain];
    detailViewController.title = @"食谱分析";
    detailViewController.mAnysisRecipeDict = mRecipeAnysisDict;
    detailViewController.isDinner = isDinner;
    // ...
    // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    self.mRecipes = array;
    [array release];

    UIImageView *img = [ViewTool addUIImageView:self.view imageName:@"biao_03.png" type:@"" x:0 y:0 x1:103 y1:84];
    img.tag = 80808;
    [img setCenter:CGPointMake(160, 200)];
    isDinner = NO;

    [self performSelector:@selector(GetTodayMenu)];
}
-(void)GetTodayMenu
{
    
    NSMutableDictionary*mDict = [[NSMutableDictionary alloc] init];
    
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/InSchoolArrangeMeal/TPFetchOneDayArrangeMealMenu"];

    [mDict setObject:[Instance GetUseID] forKey:@"ID"];
    [mDict setObject:[NSNumber numberWithBool:TRUE] forKey:@"todayOrOther"];
    if (isToday) {
        [mDict setObject:[NSNumber numberWithBool:TRUE] forKey:@"todayOrOther"];
        [mDict setObject:@"0" forKey:@"whichDay"];
    }
    else
    {
        [mDict setObject:[NSNumber numberWithBool:FALSE] forKey:@"todayOrOther"];
        [mDict setObject:mRecipeClass.dateForFindCondition forKey:@"whichDay"];
    }
    
    
    
    NSString *JSONString = [mDict JSONRepresentation];
    [mDict release];
    
    
    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate = self;
    [mHttpRequest CustomFormDataRequestDict:JSONString tag:0 url:urlstr];
    [self.view addSubview:mHttpRequest];
    [mHttpRequest release];

}

#pragma  mark requestFinished

-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    
    
    //---------------
    NSMutableDictionary *RecipeDict = [[NSMutableDictionary alloc] init];
    [RecipeDict setObject:[NSNumber numberWithInt:[[[ResultContent JSONValue] valueForKey:@"weekArrangeMealBasicID"] intValue]] forKey:@"weekArrangeMealBasicID"];
    [RecipeDict setObject:[[ResultContent JSONValue] valueForKey:@"dayOfWeek"] forKey:@"dayOfWeek"];
    self.mRecipeAnysisDict = RecipeDict;
    [RecipeDict release];
    
    NSLog(@"mRecipeAnysisDict :%@",[mRecipeAnysisDict JSONRepresentation]);
    
    BOOL isTodayOrOther = [[[ResultContent JSONValue] valueForKey:@"todayOrOther"]  boolValue];
    if (isTodayOrOther) {
        self.title = @"今日食谱";
    }
    else
    { self.title = [NSString stringWithFormat:@"%@食谱",mRecipeClass.dateForFindCondition];}
    
    //    [mDict setObject:mRecipeClass.dateForFindCondition forKey:@"whichDay"];
    
    //----------------- 
    
    
    NSArray *array = [[[ResultContent JSONValue] valueForKey:@"oneDayMenuList"] retain];
    if ([array count] == 0) {
        [ViewTool addUIImageView:self.view imageName:@"wan.png" type:@"" x:0 y:0 x1:320 y1:372];
        return;
    }
    else
    {
        if ((UIImageView*)[self.view viewWithTag:80808]!=nil) {
            [((UIImageView*)[self.view viewWithTag:80808]) removeFromSuperview];
        }
        [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:FOURENSURE title:@"食谱分析" target:self selector:@selector(AnysisRecipe)];
        CGRect frame = [[UIScreen mainScreen] bounds];
        frame.origin.y = 20;
        frame.size.height = frame.size.height -20-50-44;

        UITableView *table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0,320, frame.size.height)];
        table.dataSource = self;
        table.delegate = self;
        [table setBackgroundColor:[UIColor clearColor]];
        [table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        
        self.mTable = table;
        [table release];
        [self.view addSubview:mTable];
        
    }
    for (int i = 0; i< [array count]; i++) {
        NSMutableDictionary *RecipeClassesDict = [[NSMutableDictionary alloc] init];
        
        RecipeClasses *recipeclass = [[RecipeClasses alloc] init];
        recipeclass.arrangeMealOrNot = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"dateForFindCondition"]] boolValue]; 
        
        recipeclass.mealName = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"mealName"]];
        recipeclass.mealTime = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"mealTime"]];
        recipeclass.mealType = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"mealType"]] intValue];
        if (recipeclass.mealType == 23) {
            isDinner = YES;
        }
        //        NSString *str = [NSString stringWithFormat:@"%@",[[[array objectAtIndex:i] valueForKey:@"mealMenuList"] JSONRepresentation]];
        
        
        NSArray *subarray = [[[[[array objectAtIndex:i] valueForKey:@"mealMenuList"] JSONRepresentation] JSONValue] retain];
        
        // ----------餐次名称 ID 重量
        NSMutableArray *RecipeInfos =[[NSMutableArray alloc] init];
        for (int sub = 0; sub<[subarray count]; sub++) {
            RecipeClassesName *recipeObj = [[RecipeClassesName alloc] init];
            recipeObj.menuID = [[NSString stringWithFormat:@"%@",[[subarray objectAtIndex:sub] valueForKey:@"menuID"]] intValue];
            recipeObj.menuWeight = [[NSString stringWithFormat:@"%@",[[subarray objectAtIndex:sub] valueForKey:@"menuWeight"]] intValue];
            recipeObj.menuName = [NSString stringWithFormat:@"%@",[[subarray objectAtIndex:sub] valueForKey:@"menuName"]];
            recipeObj.menuPicUrl = [NSString stringWithFormat:@"%@",[[subarray objectAtIndex:sub] valueForKey:@"menuPicUrl"]];
            [RecipeInfos addObject:recipeObj];
            [recipeObj release];
            NSLog(@"recipeObj.menuName :%@",recipeObj.menuName);
            
        }
        [RecipeClassesDict setObject:recipeclass forKey:@"RecipeClasses"];
        [RecipeClassesDict setObject:RecipeInfos forKey:@"RecipeInfos"];
        
        
        [recipeclass release];
        [subarray release];
        [RecipeInfos release];
        
        [mRecipes addObject:RecipeClassesDict];
        [RecipeClassesDict release];
    }
    
    [mTable reloadData];
    [array release];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [mRecipes count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{    
    NSMutableArray *array = [[mRecipes objectAtIndex:section] objectForKey:@"RecipeInfos"];
    return [array count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return mTableHeight;
}
-(float)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45.0f;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section  // custom view for header. will be adjusted to default or specified header height
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 45)];
    [view setUserInteractionEnabled:YES];
    [view setBackgroundColor:[UIColor clearColor]];
    
    [ViewTool addUIImageView:view imageName:@"xia.jpg" type:@"" x:0 y:0 x1:320 y1:45];
    
    RecipeClasses *recipeObj = [[mRecipes objectAtIndex:section] objectForKey:@"RecipeClasses"];
    [ViewTool addUIImageView:view imageName:[NSString stringWithFormat:@"meal_%d.png",recipeObj.mealType] type:@"" x:10 y:10 x1:19 y1:19];

    {
        NSString *str = recipeObj.mealName;
        UILabel *labelText = [[UILabel alloc] initWithFrame:CGRectMake(0,0, 320, 45)];
        labelText.font = [UIFont boldSystemFontOfSize:18];
        labelText.text = [NSString stringWithFormat:@"%@  ",str];
        labelText.textAlignment = UITextAlignmentRight;
        [labelText setBackgroundColor:[UIColor clearColor]];
        [view addSubview:labelText];
        [labelText release];     
    }
    
    {
        UILabel *labelText = [[UILabel alloc] initWithFrame:CGRectMake(40,0, 100, 45)];
        labelText.font = [UIFont systemFontOfSize:15];
        labelText.text = recipeObj.mealTime;
        labelText.textAlignment = UITextAlignmentLeft;
        [labelText setBackgroundColor:[UIColor clearColor]];
        [view addSubview:labelText];
        [labelText release];     
    }

    
    return [view autorelease];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.textLabel.text = @"";
        
        
        {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, mTableHeight)];
            label.text = @"";
            label.tag = 100011;
            label.font = [UIFont systemFontOfSize:16];
            label.backgroundColor = [UIColor clearColor];
            label.textColor = [UIColor blackColor];
            [cell.contentView addSubview:label];
            [label release];
        }
        {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(150, 0, 170, mTableHeight)];
            label.text = @"";
            label.tag = 100022;
            label.font = [UIFont systemFontOfSize:16];
            label.backgroundColor = [UIColor clearColor];
            label.textColor = [UIColor blackColor];
            [cell.contentView addSubview:label];
            [label release];
        }
    }    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    NSMutableArray *array = [[mRecipes objectAtIndex:[indexPath section]] objectForKey:@"RecipeInfos"];
    
    RecipeClassesName *recipeObj = [array objectAtIndex:[indexPath row]];
    if ((UILabel *)[cell.contentView viewWithTag:100011]!=nil) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"  %@",recipeObj.menuName];
    }
    if ((UILabel *)[cell.contentView viewWithTag:100022]!=nil) {
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"%d克",recipeObj.menuWeight];
    }
 
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSMutableArray *array = [[mRecipes objectAtIndex:[indexPath section]] objectForKey:@"RecipeInfos"];
    
    RecipeClassesName *recipeObj = [array objectAtIndex:[indexPath row]];
    FoodListViewController *detailViewController = [[FoodListViewController alloc] initWithStyle:UITableViewStylePlain];
    detailViewController.title = recipeObj.menuName;
    detailViewController.menuID = recipeObj.menuID;
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];

}

#pragma mark - Table view delegate

-(void)dealloc
{
    [mRecipeClass release];

    [mRecipes release]; 
    [mTable release]; 
    [mRecipeAnysisDict release];
    [super dealloc];
}
- (void)viewDidUnload
{
    self.mRecipeClass = nil;
    self.mRecipeAnysisDict = nil;
    self.mRecipes = nil;
    self.mTable = nil;
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
