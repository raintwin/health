//
//  Display.m
//  Health
//
//  Created by hq on 12-8-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "Display.h"

@implementation Display
@synthesize childName,childPhoto;
@synthesize childID,inSchool,inHome;
@synthesize isLoad;
@synthesize image;
-(void)dealloc
{
    [image release]; image = nil;
    [childName release];childName = nil;
    [childPhoto release]; childPhoto = nil;
    [super dealloc];
}
@end

@implementation DisplayInfo

@synthesize mChildOfDisID,mDisType,mDiserID;
@synthesize mDisComment,mDisDate,mDiserName;

-(void)dealloc
{
    [mDisDate release]; mDisDate  = nil;
    [mDisComment release]; mDisComment = nil;
    [mDiserName release]; mDiserName = nil;
    [super dealloc];
}
@end