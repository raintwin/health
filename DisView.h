//
//  DisView.h
//  Health
//
//  Created by hq on 12-8-15.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DisView : UIView

@property(nonatomic,assign)int btnTag;
@property(nonatomic,retain)NSMutableArray *btns;
-(void)SetRefresh:(NSString *)imgstr;
@end



@interface DisViewImage : UIView 
-(void)SetRefresh:(NSString *)imgstr;
-(void)SetDisNumber:(int)tag;
@end