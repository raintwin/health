//
//  PaperForChildList.m
//  Health
//
//  Created by hq on 12-6-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "PaperForChildList.h"
#import "CChild.h"
#import "DownLoadTableImage.h"
#import "PaperViewController.h"
#import "LoadingTable.h"
@implementation PaperForChildList
@synthesize mChilds;
@synthesize mTable;
@synthesize mDownLoadImageDict;

#define tableHeiht 40
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        isSuccess = YES;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)CancelDownLoad
{
    for (int i = 0; i<[mChilds count]; i++) {
        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:i inSection:0];
        DownLoadTableImage *imageDownLoad = [mDownLoadImageDict objectForKey:indexpath];
        if (imageDownLoad != nil) {
            [imageDownLoad CancelDownloadImage];
        }
        
    }
    [mDownLoadImageDict removeAllObjects];
}
#pragma mark - View lifecycle
-(void)navBack
{

    [self CancelDownLoad];

    [self.navigationController popViewControllerAnimated:YES];
}
-(void)RefreshMessage
{

    [self CancelDownLoad];
    [mChilds removeAllObjects];
    [mTable reloadData];
    [self performSelector:@selector(GetChildPaper)];

}
- (void)viewDidLoad
{
    [super viewDidLoad];
//    [Instance baiduApi:@"paper" eventlabel:@"paper"];

    //BaiduMobStat* statTracker = [BaiduMobStat defaultStat];    [statTracker logEvent:@"paper" eventLabel:@"paper"];

    
//    [ViewTool addUIImageViewWithAutoSize:self.view imageName:@"viewblackgroup.jpg" type:@"" x:0 y:0 x1:0 y1:0];
    
    // Do any additional setup after loading the view from its nib.
//    
//    [Instance setRightImageBtnForNavigationItem:self.navigationItem Image:@"img_07.png" target:self selector:@selector(RefreshMessage)];
    
    [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"刷新" target:self selector:@selector(RefreshMessage)];

    
    NSMutableDictionary *mDictionary = [[NSMutableDictionary alloc]init];
    self.mDownLoadImageDict = mDictionary;
    [mDictionary release];
    
    {
        UITableView *table = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,372)];
        table.delegate = self;
        table.dataSource = self;
        table.backgroundColor = [UIColor clearColor];
        self.mTable = table;
        [table release];
        
        [self.view addSubview:mTable];
    }
    [self performSelector:@selector(GetChildPaper)];
}
-(void)GetChildPaper
{
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/LeaveMessage/TeacherFetchChildListAndNotReadCount"];
    
    NSMutableDictionary *mDic = [[NSMutableDictionary alloc] init];
    
    [mDic setObject:[Instance GetUseID] forKey:@"ID"];
    
    NSString *JSONString = [mDic JSONRepresentation];
    [mDic release];
    
    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate = self;
    [mHttpRequest CustomFormDataRequestDict:JSONString tag:0 url:urlstr];
    [self.view addSubview:mHttpRequest];
    [mHttpRequest release];
    
}


#pragma mark requestFinished 
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag{
    NSArray *array = [[[ResultContent JSONValue] valueForKey:@"childList"] retain];
    
    
    NSMutableArray *tmpArray = [[NSMutableArray alloc]init];
    for (int i = 0; i < [array count]; i++) {
        ChildForPaper *nobj = [[ChildForPaper alloc] init];
        nobj.childID = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childID"]] intValue];
        nobj.childName = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childName"]]; 
        nobj.photo = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"photo"]];
        
        nobj.notReadCount = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"notReadCount"]] intValue];
        
        nobj.isLoad = NO;
        nobj.image = nil;
        [tmpArray addObject:nobj];
        [nobj release];
    }
    
    self.mChilds = tmpArray;
    [tmpArray release];
    [array release];
    
    [mTable reloadData];
    
    
}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag{}



#pragma mark  load imageview 
-(void)StartDownLoadImage:(NSString*)url IndexPath:(NSIndexPath*)indexpath 
{
    DownLoadTableImage *imageDownLoad = [mDownLoadImageDict objectForKey:indexpath];
    if (imageDownLoad == nil) {
        imageDownLoad = [[DownLoadTableImage alloc] init];
        imageDownLoad.ImageURL = url;
        imageDownLoad.ImageHeight = 30;
        imageDownLoad.ImageWidth = 30;
        imageDownLoad.IndexPathTo = indexpath;
        imageDownLoad.Delegate =self;
        [mDownLoadImageDict setObject:imageDownLoad forKey:indexpath];
        [imageDownLoad StartDownloadImage];
        [imageDownLoad release];
    }
}
#pragma mark finish download image
-(void)FinishDownLoadImage:(NSIndexPath*)indexpath Image:(UIImage*)image IsFailed:(BOOL)isFaild
{
    DownLoadTableImage *imageDownLoad = [mDownLoadImageDict objectForKey:indexpath];
    if (imageDownLoad != nil) {
        UITableViewCell *cell = [self.mTable cellForRowAtIndexPath:indexpath];
        ChildForPaper *obj = [mChilds objectAtIndex:[indexpath row]];
        
        if (isFaild) {
            obj.image = [UIImage imageNamed:@"acquiesce.jpg"];
            obj.isLoad = NO;
        }
        else
        {
            obj.image = image;
            obj.isLoad = YES;
        }
        
        cell.imageView.image =  obj.image ;
    }
}
- (void)loadImagesForOnscreenRows
{
    if ([mChilds count] > 0)
    {
        NSArray *visiblePaths = [self.mTable indexPathsForVisibleRows];
        for (NSIndexPath *indexPath in visiblePaths)
        {
            if ([indexPath row]<[mChilds count]) {
                ChildForPaper *item = [mChilds objectAtIndex:indexPath.row];
                
                if (!item.isLoad ) // avoid the app icon download if the app already has an icon
                {
                    [self StartDownLoadImage:item.photo IndexPath:indexPath];
                }
                
            }
        }
    }
}
#pragma mark -
#pragma mark Deferred image loading (UIScrollViewDelegate)
#pragma mark -
// Load images for all onscreen rows when scrolling is finished
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	if (!decelerate)
	{
        [self loadImagesForOnscreenRows];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadImagesForOnscreenRows];
}
#pragma mark tableview 

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    if ([indexPath row] > [mChilds count]) {
        return;
    }
    ChildForPaper *obj = [mChilds objectAtIndex:[indexPath row]];
    PaperViewController *pvCtr = [[PaperViewController alloc] initWithNibName:@"PaperViewController" bundle:nil];
    pvCtr.title = @"留 言";
    pvCtr.childid = obj.childID;
    pvCtr.notReadCount = obj.notReadCount;
    if (obj.notReadCount == 0) {
        pvCtr.notReadCount = 5;
    }
    [self.navigationController pushViewController:pvCtr animated:YES];
    [pvCtr release];

    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return tableHeiht;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [mChilds count];	
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    
    static NSString *cellName = @"cellstr";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    
    
    if (cell== nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellName];
        
        {
            UIImage *image = [UIImage imageNamed:@"acquiesce.jpg"];
            
            CGSize itemSize = CGSizeMake(30, 30);
            UIGraphicsBeginImageContext(itemSize);
            CGRect imageRect = CGRectMake(0.0, 0.0, 30, 30);
            [image drawInRect:imageRect];
            [image release];
            image = UIGraphicsGetImageFromCurrentImageContext();
            [image retain];
            UIGraphicsEndImageContext();
            cell.imageView.image = image;
            
            [image release];
        }
        
        {
            UILabel *parentText = [[UILabel alloc]initWithFrame:CGRectMake(50 ,0 , 200,tableHeiht)];
            parentText.font = [UIFont boldSystemFontOfSize:15];
            [parentText setBackgroundColor:[UIColor clearColor]];
            parentText.textColor = [UIColor blackColor];
            parentText.text = @"";
            parentText.tag = 200001;
            [cell.contentView addSubview:parentText];
            [parentText release];
        }

    }
    
    
    NSInteger row = [indexPath row];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if ([mChilds count] == 0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"读取幼儿列表失败" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            return cell;
        }
        ChildForPaper *sif = [mChilds objectAtIndex:row];
        // --------------------- load image
        {
            if (!sif.isLoad) {
                if (self.mTable.dragging == NO && self.mTable.decelerating == NO) {
                    [self StartDownLoadImage:sif.photo IndexPath:indexPath];
                }
            }
            else
            {
                cell.imageView.image = sif.image;
            }
        }
        {
            NSString *str  = [NSString stringWithFormat:@"%@  未读留言:%d",sif.childName,sif.notReadCount];
            UILabel *parentText = (UILabel *)[cell.contentView viewWithTag:200001];
            parentText.text = str;
        }
        

    
    return cell;
    
}

-(void)dealloc
{

    [mDownLoadImageDict release]; 
    [mChilds release]; 
    [mTable release]; 
    [super dealloc];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mDownLoadImageDict = nil;
    self.mTable = nil;
    self.mChilds = nil ;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
