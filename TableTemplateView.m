//
//  TableTemplateView.m
//  Health
//
//  Created by jiaodian on 12-11-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "TableTemplateView.h"
#import "HttpFormatRequest.h"

#define DETextPull      @"下拉可以刷新..."
#define DETextRelease   @"松开即可刷新..."
#define DETextLoading   @"加载中..."

#define ePageCount 10
@interface TableTemplateView()
@property (nonatomic,retain)HttpFormatRequest *httpRequest;
@end

@implementation TableTemplateView

@synthesize TableHeightForRow;
@synthesize requestDatas;
@synthesize isLoading,isLoadOver;
@synthesize UrlString;
@synthesize DataDict;
@synthesize delegate;
@synthesize mTableView;
@synthesize isLoadImage;
@synthesize commitEditing;

@synthesize httpRequest;

@synthesize isCase;
@synthesize mKeyDict;
-(void)initData;
{
    {
        NSMutableArray *array = [[NSMutableArray alloc]init];
        self.requestDatas = array;
        [array release];
    }
    {
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        self.mKeyDict = mDict;
        [mDict release];
    }
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        isLoading = NO;
        isLoadOver = YES;
        isLoadImage = NO;
        isCase = NO;
        commitEditing = NO;
        mPageStart = 0;
        TableHeightForRow = 0;
        

        [self initData];
        
        self.httpRequest = [[HttpFormatRequest alloc]init];
        self.httpRequest.Delegate = self;
        [self addSubview:self.httpRequest];
        
        
        UITableView *table = [[UITableView alloc]initWithFrame:self.frame];
        table.delegate = self;
        table.dataSource = self;
        self.mTableView = table;
        [self addSubview:mTableView];
        [table release];
    }
    return self;
}
-(void)setCharacter
{
    if (!isCase) {
        [DataDict setObject:[NSNumber numberWithInt:mPageStart] forKey:@"PageStart"];
        [DataDict setObject:[NSNumber numberWithInt:ePageCount] forKey:@"PageCount"]; 
    }
    else {
        [DataDict setObject:[NSNumber numberWithInt:mPageStart] forKey:@"pageStart"];
        [DataDict setObject:[NSNumber numberWithInt:ePageCount] forKey:@"pageCount"]; 

    }
}
-(void)GetDataForHttp
{


    mPageStart = [requestDatas count]/ePageCount;
    [self setCharacter];
    [self.httpRequest CustomFormDataRequestDict:[DataDict JSONRepresentation] tag:0 url:UrlString];
}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{
    [self StopLoading];
    [delegate ReturnHttpData:ResultContent container:requestDatas tag:tag isFinish:NO];

}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    if ([ResultContent length] == 0 ) {
        [self.mTableView reloadData];
        return;
    }
    [delegate ReturnHttpData:ResultContent container:requestDatas tag:tag isFinish:YES];
    [self StopLoading];
    [self initPresentation];
    [self.mTableView reloadData];
};
-(void)initPresentation
{
    if (isLoadOver) {
        return;
    }
    {
        
        CGRect rect = self.mTableView.frame;
        UIView *mView = [[UIView alloc]initWithFrame:CGRectMake(0, rect.origin.y+[requestDatas count]*TableHeightForRow, rect.size.width, TableHeightForRow)];
        mView.tag = 10001;
        [mView setBackgroundColor:[UIColor clearColor]];
        UILabel *mLabel = [Instance addLable:CGRectMake(0, 0, mView.frame.size.width, mView.frame.size.height) tag:10011 size:15 string:DETextPull];
        [mLabel setTextAlignment:UITextAlignmentCenter];
        
        UIActivityIndicatorView  *mActivity = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
        [mActivity setCenter:CGPointMake(230, TableHeightForRow/2)];
        [mActivity setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
        mActivity.tag = 10021;
        [mActivity stopAnimating];
        mActivity.hidesWhenStopped = YES;
        [mView addSubview:mActivity];
        [mActivity release];
        
        [mView addSubview:mLabel];
        [self.mTableView addSubview:mView];
        [mLabel release];
        [mView release];
    }
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (isLoading || isLoadOver) {
        return;
    }
    UIView *mView = (UIView *)[self.mTableView viewWithTag:10001];
    [mView setHidden:NO];
    UILabel *mLabel = (UILabel *)[mView viewWithTag:10011];
    UIActivityIndicatorView *mActivity = (UIActivityIndicatorView*)[mView viewWithTag:10021];
    [mActivity stopAnimating];
    if (scrollView.contentOffset.y + self.mTableView.frame.size.height >= ([requestDatas count]+1)*TableHeightForRow+20 ) {
        mLabel.text = DETextRelease;
    }
    else {
        mLabel.text = DETextPull;
    }
    isLoading = NO;
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    
    if (!decelerate && isLoadImage)
	{
        [delegate TableDownLoadCellImage];
    }
    
    if (!isLoadOver) {
        UIView *mView = (UIView *)[self.mTableView viewWithTag:10001];
        UIActivityIndicatorView *mActivity = (UIActivityIndicatorView*)[mView viewWithTag:10021];
        if (scrollView.contentOffset.y + self.mTableView.frame.size.height >= ([requestDatas count]+1)*TableHeightForRow+20 ) {
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.3];
            [mActivity startAnimating];
            [UIView commitAnimations];
            [self startLoading];
            isLoading = YES;
            [self refreshData];
        }
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (isLoadImage) {
        [delegate TableDownLoadCellImage];
    }
}
-(void)refreshData
{
    [self performSelector:@selector(GetDataForHttp) withObject:nil afterDelay:0.1f];
}
- (void)startLoading {
    
    UIView *mView = (UIView *)[self.mTableView viewWithTag:10001];
    UILabel *mLabel = (UILabel *)[mView viewWithTag:10011];
    UIActivityIndicatorView *mActivity = (UIActivityIndicatorView*)[mView viewWithTag:10021];
    
    
    // Show the header
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    self.mTableView.contentInset = UIEdgeInsetsMake(0, 0, TableHeightForRow, 0);
    mLabel.text = DETextLoading;
    [mActivity startAnimating];
    [UIView commitAnimations];
}
- (void)StopLoading {
    
    // Show the header
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDidStopSelector:@selector(stopLoadingComplete:finished:context:)];
    self.mTableView.contentInset = UIEdgeInsetsZero;
    UIEdgeInsets tableContentInset = self.mTableView.contentInset;
    tableContentInset.top = 0.0;
    [UIView commitAnimations];
    UIView *mView = (UIView *)[self.mTableView viewWithTag:10001];
    [mView removeFromSuperview];
}
- (void)stopLoadingComplete:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
    // Reset the header
    if (isLoadOver) {
        UIView *mView = (UIView *)[self.mTableView viewWithTag:10001];
        UIActivityIndicatorView *mActivity = (UIActivityIndicatorView*)[mView viewWithTag:10021];
        mView.hidden = YES;
        [mActivity stopAnimating];
    }
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [requestDatas count];
}
-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TableHeightForRow;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell== nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.textLabel.text = @"";
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    
    // Configure the cell...
    [delegate SetTableData:cell indexpath:indexPath datas:requestDatas];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [delegate TableDidSelect:indexPath Datas:requestDatas];
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return commitEditing;
}

-(void)dealloc
{

    [UrlString release];     UrlString = nil;
    [requestDatas release];    requestDatas = nil;
    [mTableView release];    mTableView = nil;
    [DataDict release];       DataDict = nil;
    [self.httpRequest release]; self.httpRequest = nil;
    [super dealloc];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
