//
//  ModPasswordViewViewController.m
//  Health
//
//  Created by hq on 12-7-5.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ModPasswordViewViewController.h"
#import "JSON.h"
#import "LoginSelectView.h"
#import "PeopleView.h"
#import "LoginCharacter.h"
#import "NewsAlertViewCotroller.h"
#define tablewidth 37.0f

#define mUserName   @"ID"
#define mPassWord   @"password"
#define mConPassWord  @"confirmPassword"


@implementation ModPasswordViewViewController
@synthesize mInfo;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"img_03.png"]]];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)navBack
{

    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    //BaiduMobStat* statTracker = [BaiduMobStat defaultStat];    [statTracker logEvent:@"regist" eventLabel:@"regist"];
    
    
    // Do any additional setup after loading the view from its nib.
    //    [ViewTool addUIImageView:self.view imageName:@"img_03.png" type:@"" x:28 y:10 x1:262 y1:37];
    NSMutableDictionary *mDict = [[NSMutableDictionary alloc] init];
    self.mInfo = mDict;
    [mDict release];
    

    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];
    [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"修改" target:self selector:@selector(CompletePassword)];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"viewblackgroup.jpg"]]];
    
    {
        UITableView *mytable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, 320, [Instance getScreenHeight]) style:UITableViewStyleGrouped];
        mytable.delegate = self;
        mytable.dataSource = self;
        [mytable setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"viewblackgroup.jpg"]]];
        [mytable setScrollEnabled:NO];
        mytable .tag = 100111;
        [self.view addSubview:mytable];
        [mytable release];
        
    }   
}

-(void)CompletePassword
{

    UITableView *mTableView = (UITableView*)[self.view viewWithTag:100111];
    if ( mTableView == nil) {
        return;
    }
    //----------- 密码
    NSIndexPath *mIndexPath1 = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewCell *cell1 = [mTableView cellForRowAtIndexPath:mIndexPath1];
    UITextField *textfiel1 =(UITextField *)[cell1.contentView viewWithTag:10000];
    
    NSIndexPath *mIndexPath2 = [NSIndexPath indexPathForRow:1 inSection:0];
    UITableViewCell *cell2 = [mTableView cellForRowAtIndexPath:mIndexPath2];
    UITextField *textfiel2 =(UITextField *)[cell2.contentView viewWithTag:10001];
    
    if ([textfiel1.text length]==0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入密码" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        [textfiel1 becomeFirstResponder];
        return;
    }
    
    if ([textfiel2.text length] == 0) {
        [textfiel2 becomeFirstResponder];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入确认密码" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];

        return;
    }
    if (![textfiel2.text isEqualToString:textfiel1.text]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"两次密码不相同" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        return;
    }
    [mInfo setObject:textfiel1.text forKey:mPassWord];
    [mInfo setObject:textfiel2.text forKey:mConPassWord];
    [mInfo setObject:[Instance GetUseID] forKey:mUserName];
    
    [self performSelector:@selector(ModPassword)];
    
    
}

#pragma mark - 注册

-(void)ModPassword
{
    
    
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/UserManager/ChangeUserPassword"];
    
    NSMutableDictionary *mDic = [[NSMutableDictionary alloc] init];
        
    NSString *JSONString = [mInfo JSONRepresentation];
    [mDic release];
    
    
    
    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate = self;
    [mHttpRequest CustomFormDataRequestDict:JSONString tag:0 url:urlstr];
    [self.view addSubview:mHttpRequest];
    [mHttpRequest release];
    
    
}
#pragma mark requestFinished 
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag{
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 2;
}
-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return tablewidth;
}

-(UITextField*)CellAddTextFiled:(NSString*)Text tag:(int)tag
{
    UITextField *TextField  = [[UITextField alloc] initWithFrame:CGRectMake(90, 5, 180, tablewidth-10)];
    [TextField setBorderStyle:UITextBorderStyleBezel];
    TextField.tag = tag;
    TextField.clearButtonMode=UITextFieldViewModeWhileEditing;
    
    TextField.delegate = self;
    TextField.text = Text;
    TextField.returnKeyType=UIReturnKeyDone;
    return TextField;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    // Configure the cell...
    NSArray *array1 = [[NSArray alloc] initWithObjects:@"密码:",@"确认密码:",nil];
    cell.textLabel.text = [array1 objectAtIndex:[indexPath row]];
    [array1 release];

    if ([indexPath row] == 0) {
        UITextField *TextField   = [self CellAddTextFiled:@"" tag:[indexPath row]+10000];
        [TextField  setSecureTextEntry:YES];
        TextField.placeholder = @"密码";
        [cell.contentView addSubview:TextField ];
        [TextField  release];
    }   
    if ([indexPath row] == 1) {
        UITextField *TextField   = [self CellAddTextFiled:@"" tag:[indexPath row]+10000];
        [TextField  setSecureTextEntry:YES];
        TextField.placeholder = @"确认密码";
        [cell.contentView addSubview:TextField ];
        [TextField  release];
    }   
    return cell;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag == 10000) {
        [mInfo setObject:textField.text forKey:mPassWord];
    }
    if (textField.tag == 10001) {
        [mInfo setObject:textField.text forKey:mConPassWord];
    }
    [textField resignFirstResponder];
    return NO;
}
-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text = @"";
    [textField resignFirstResponder];
    return NO;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}
-(void)dealloc
{
    [mInfo release];
    [super dealloc];
    
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mInfo = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
