//
//  SendPhotoViewController.m
//  Health
//
//  Created by hq on 12-7-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "SendPhotoViewController.h"
#import "ChildListViewController.h"
#import "Instance.h"
#import "JsonInstance.h"
#import "CChild.h"

@interface SendPhotoViewController ()

@property(nonatomic,retain) UIImageView *photoImageView;
@property(nonatomic,retain) UITextField *describeTextFileld;
@end


@implementation SendPhotoViewController

@synthesize photoImageView;
@synthesize describeTextFileld;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    NSLog(@"MemoryWarning---");
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)navBack
{

    [self delTmpeImage];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:@"backbutton.png" target:self selector:@selector(navBack)];


//    [UIView setAnimationTransition:<#(UIViewAnimationTransition)#> forView:<#(UIView *)#> cache:<#(BOOL)#>];
    [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"发送" target:self selector:@selector(savePhoto)];

    self.photoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 100, 100)];
    self.photoImageView.image = nil;
    
    [self.view addSubview:self.photoImageView];

    self.describeTextFileld = [[UITextField alloc]initWithFrame:CGRectMake(110, 5, 210, 100)];
    self.describeTextFileld .font = [UIFont systemFontOfSize:15];
    [self.describeTextFileld  becomeFirstResponder];
    self.describeTextFileld .returnKeyType = UIReturnKeyNext;
    self.describeTextFileld .placeholder = @"描述";
    [self.view addSubview:self.describeTextFileld ];

    UIButton *btn = [Instance addUIButtonByImage:self.view image:@"图片.png" rect:CGRectMake(10+50 , 110 , 26, 26)];
    [btn addTarget:self action:@selector(SelectPhotoCtr) forControlEvents:64];

    
    [self delTmpeImage];
    [self selectPhotoCtr];
}
#pragma mark select photo
-(void)selectPhotoCtr
{

    UIActionSheet *pickerSheet = [[UIActionSheet alloc]initWithTitle:@"请选择图片" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照",@"相册",nil ];
    pickerSheet.delegate = self;
    pickerSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;//设置样式
    [pickerSheet showInView:self.view];
    [pickerSheet release];

}
#pragma mark 照机 相册
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image= [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    }
    int  ImageWidth  = image.size.width /3;
    int  ImageHeight = image.size.height/3;
    
    UIImage *theImage = [self imageWithImageSimple:image scaledToSize:CGSizeMake(ImageWidth, ImageHeight)];
    [theImage retain];
    [self saveImage:theImage WithName:[NSString stringWithFormat:@"tmpeImage.png"]];
    [[UIApplication sharedApplication].keyWindow.rootViewController dismissModalViewControllerAnimated:YES];
    [picker release];
}
-(UIImage*)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize
{
    // Create a graphics image context
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
- (void)saveImage:(UIImage *)tempImage WithName:(NSString *)imageName
{
    NSData* imageData = UIImagePNGRepresentation(tempImage);
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    NSString* fullPathToFile = [documentsDirectory stringByAppendingPathComponent:imageName];
    [imageData writeToFile:fullPathToFile atomically:NO];
    [self.photoImageView setImage:[UIImage imageWithContentsOfFile:[Instance GetDocumentByString:@"tmpeImage.png"]]];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex;  // after animation
{
    return;
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.editing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [[UIApplication sharedApplication].keyWindow.rootViewController presentModalViewController:picker animated:YES];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"打开镜头错误" message:@""delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
    }
    if (buttonIndex == 1) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.editing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [[UIApplication sharedApplication].keyWindow.rootViewController presentModalViewController:picker animated:YES];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"连接到图片库错误" message:@""delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
    }
    return;
}

-(NSString *)getPhotoFile
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];    
    NSString *fullPathFile = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"tmpeImage.png"]];
    NSLog(@"fullPathFile :%@",fullPathFile);
    
    return fullPathFile;
}
-(BOOL)judgePhotoIsFile
{    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];    
    NSString *fullPathFile = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"tmpeImage.png"]];
    NSFileManager *mFiles = [NSFileManager defaultManager];
    if ([mFiles fileExistsAtPath:fullPathFile]) {
        return NO;
    }
    return YES;
}

#pragma mark delegateFunction

-(void)delTmpeImage
{
    
    NSFileManager *file = [NSFileManager defaultManager];
    if ([file fileExistsAtPath:[Instance GetDocumentByString:@"tmpeImage.png"]]) {
        [file removeItemAtPath:[Instance GetDocumentByString:@"tmpeImage.png"] error:nil];
    }
}

#pragma mark send the photo

-(void)savePhoto
{

    [self performSelector:@selector(savePhotoThread)];
}
-(void)savePhotoThread
{
//    if ([self judgePhotoIsFile]) {
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"请选择图片" message:@""delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
//        [alert show];
//        [alert release];  
//        return;
//    }
    
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/Photo/SendPhoto"];
    NSMutableDictionary*mDict = [[NSMutableDictionary alloc] init];
    
    
    
    if ([Instance getCharacter]) {
        [mDict setObject:[NSNumber numberWithBool:FALSE] forKey:@"inSchoolOrInHome"];
    }
    else
    {
        [mDict setObject:[NSNumber numberWithBool:TRUE] forKey:@"inSchoolOrInHome"];
    }
    if ([self.describeTextFileld.text length]>0) {
        [mDict setObject:self.describeTextFileld.text forKey:@"description"];
    }
    [mDict setObject:[Instance GetUseID] forKey:@"publishPersonID"];

    
    NSString *JSONString = [mDict JSONRepresentation];
    
    
    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate = self;
    [mHttpRequest CustomFormDataRequestSendFile:JSONString FilePath:[self getPhotoFile] tag:FILESEND url:urlstr forkey:@"photoUrl"];
    [self.view addSubview:mHttpRequest];
    [mHttpRequest release];
    [mDict release];
    
}
#pragma mark requestFinished 

-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"网络错误" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];

}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    UIAlertView *alerView = [[UIAlertView alloc]initWithTitle:nil message:@"发送成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    alerView.delegate = self;
    [alerView show];
    [alerView release];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self navBack];
    }
}

#pragma mark 响应输入
-(BOOL)textFieldShouldReturn:(UITextField* )textField
{
	[textField resignFirstResponder];
	return YES;
}


- (BOOL)textFieldShouldClear:(UITextField *)textField
{
	textField.text = @"";
	[textField resignFirstResponder];
	return NO;
}

#pragma mark - View lifecycle
-(void)dealloc
{
    [self.photoImageView release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.photoImageView = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
