//
//  GetUser.m
//  Health
//
//  Created by hq on 12-7-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "Instance.h"
#import "Recipe.h"

#import "DownLoadTableImage.h"

#define selecto(x) @selector(x)

@implementation  Instance
#pragma mark getdocument
+(BOOL)IsGetDocument:(NSString*)File
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    documentDirectory =  [documentDirectory stringByAppendingPathComponent:File];
    if ([[NSFileManager defaultManager] fileExistsAtPath:documentDirectory]) {
        return YES;
    }

    return NO;

}
+(NSString*)GetDocument
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    return documentDirectory;
}
+(NSString*)GetDocumentByString:(NSString*)string
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    documentDirectory = [documentDirectory stringByAppendingPathComponent:string];
    return documentDirectory;
}
+(NSString*)GetUseID
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"defaults user ID:%@",[[defaults objectForKey:NSLocalizedString(@"HT_UserID", nil)] stringValue]);
    NSString *useId = [[defaults objectForKey:NSLocalizedString(@"HT_UserID", nil)] stringValue];
    return [useId length] >0? useId:@"nil";

}
+(NSString*)GetUsePhotoUrl;
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:@"photo"];
    
}
+(NSString*)GetChildID
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [[defaults objectForKey:NSLocalizedString(@"HT_UserID", nil)] stringValue];
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentDirectory = [paths objectAtIndex:0];
//    
//    documentDirectory =  [documentDirectory stringByAppendingPathComponent:@"initlalize"];
//    if ([[NSFileManager defaultManager] fileExistsAtPath:documentDirectory]) {
//        NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:documentDirectory];
////        NSLog(@"dic :%@",[dic JSONRepresentation]);
//        return [dic objectForKey:@"childID"];
//    }   
//    else
//        return nil;
}
+(NSString*)GetChildName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    documentDirectory =  [documentDirectory stringByAppendingPathComponent:@"initlalize"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:documentDirectory]) {
        NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:documentDirectory];
        return [dic objectForKey:@"childName"];
    }
    else
        return nil;
}
+(NSString*)GetChildAge
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    documentDirectory =  [documentDirectory stringByAppendingPathComponent:@"initlalize"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:documentDirectory]) {
        NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:documentDirectory];
        return [NSString stringWithFormat:@"%d 岁",[[dic objectForKey:@"childAge"] intValue]];
        
    }
    else
        return nil;
}
+(int)GetChildAgeOFINT
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    documentDirectory =  [documentDirectory stringByAppendingPathComponent:@"initlalize"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:documentDirectory]) {
        NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:documentDirectory];
        return [[dic objectForKey:@"childAge"] intValue];
        
    }
    else
        return 0;
}
+(NSString*)GetChildSchoolAndClass
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    NSString *SchoolName = nil;
    NSString *ClassName = nil;

    documentDirectory =  [documentDirectory stringByAppendingPathComponent:@"initlalize"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:documentDirectory]) {
        NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:documentDirectory];
        if ([[dic objectForKey:@"childSchoolID"] intValue]== 0) {
            SchoolName = @"暂无幼儿园";
        }
        else
            SchoolName = [dic objectForKey:@"childSchoolName"];
        
        
        if ([[dic objectForKey:@"childClassID"] intValue]== 0) {
            ClassName = @"暂无班级";
        }
        else
            ClassName = [dic objectForKey:@"childClassName"];

        return [NSString stringWithFormat:@"%@  %@",SchoolName,ClassName];
    }
    else
        return nil;
}

+(BOOL)GetUseType
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [[defaults objectForKey:NSLocalizedString(@"HT_UarentOrTeacher", nil)] boolValue];

}
+(NSString*)GetIconFile
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    NSString *filePng = [documentDirectory stringByAppendingPathComponent:@"icon.png"];
    NSString *fileJpg = [documentDirectory stringByAppendingPathComponent:@"icon.jpg"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePng]) 
    {return filePng;}
    if ([[NSFileManager defaultManager] fileExistsAtPath:fileJpg]) 
    {return fileJpg; }
    return nil;

}
+(NSString*)GetProvince
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    documentDirectory =  [documentDirectory stringByAppendingPathComponent:@"initlalize"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:documentDirectory]) {
        NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:documentDirectory];
        return [dic objectForKey:@"provinceName"];
    }   
    else
        return nil;
}
+(NSString*)GetProvinceID
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    documentDirectory =  [documentDirectory stringByAppendingPathComponent:@"initlalize"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:documentDirectory]) {
        NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:documentDirectory];
        return [dic objectForKey:@"provinceID"];
    }   
    else
        return nil;
}
+(NSString*)GetCity
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    documentDirectory =  [documentDirectory stringByAppendingPathComponent:@"initlalize"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:documentDirectory]) {
        NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:documentDirectory];
        return [dic objectForKey:@"cityName"];
    }   
    else
        return nil;
}
+(NSString*)GetCityID
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    documentDirectory =  [documentDirectory stringByAppendingPathComponent:@"initlalize"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:documentDirectory]) {
        NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:documentDirectory];
        return [dic objectForKey:@"cityID"];
    }   
    else
        return nil;
}
+(NSNumber*)GetChildGender
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    documentDirectory =  [documentDirectory stringByAppendingPathComponent:@"initlalize"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:documentDirectory]) {
        NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:documentDirectory];
        if ([[dic objectForKey:@"childGender"] boolValue]) {
            return [NSNumber numberWithInt:1];
        }
        else
            return [NSNumber numberWithInt:2];
    }   
    else
        return [NSNumber numberWithInt:0];;
}

+(float)getScreenHeight;
{
    CGRect frame = [[UIScreen mainScreen] bounds];
    frame.origin.y = 20;
    frame.size.height = frame.size.height -20-50-44;
    return frame.size.height;
}

//在父亲视图fview加入UIButton
+(UIButton *)addUIButton:(UIView *)fview imageName:(NSString*)iname  type:(NSString *)itype x:(float)x y:(float)y x1:(float)x1 y1:(float)y1
{	
	UIButton *b=[[UIButton alloc] initWithFrame:CGRectMake(x, y, x1, y1)];
    [b setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:iname ofType:itype]] forState:UIControlStateNormal];			
	[fview addSubview:b];
	//[b addTarget:self action:@selector(m) forControlEvents:UIControlEventTouchUpInside];
	[b release];
	return	b;
}


#pragma mark 获取日期
+(NSString*)GetSystemDate
{
    NSString* date;
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    date = [formatter stringFromDate:[NSDate date]];
//    NSLog(@"日期:%@",date);
    [formatter release];
    return date;
}

//////------------------ 
#pragma mark UINavigationItem 
+(void)setLeftImageBtnForNavigationItem:(UINavigationItem*)item Image:(NSString*) image target:(id) target selector:(SEL) selector
{
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:image].size.width, [UIImage imageNamed:image].size.height)];
    
    
    [btn setBackgroundImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    
    [btn addTarget: target action: selector forControlEvents: UIControlEventTouchUpInside];
    
    
    
    UIBarButtonItem* bi = [[UIBarButtonItem alloc] initWithCustomView: btn];
    [btn release];
    //UIImageView iv = [[UIImageView alloc] initWithImage:[]]
    
    item.leftBarButtonItem = bi;
    [bi release];	
    
}
+(void)setRightImageBtnForNavigationItem:(UINavigationItem*)item Image:(NSString*) image target:(id) target selector:(SEL) selector
{
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:image].size.width, [UIImage imageNamed:image].size.height)];
    
    
    [btn setBackgroundImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    
    [btn addTarget: target action: selector forControlEvents: UIControlEventTouchUpInside];
    
    
    
    UIBarButtonItem* bi = [[UIBarButtonItem alloc] initWithCustomView: btn];
    [btn release];
    //UIImageView iv = [[UIImageView alloc] initWithImage:[]]
    
    item.rightBarButtonItem = bi;
    
    [bi release];	
    
}
+(void)setRightImageAndNameForNavigationItem:(UINavigationItem*)item Image:(NSString*) image title:(NSString*)title target:(id)target selector:(SEL) selector
{
    
    UIImage *imageBtn = [UIImage imageNamed:image];
    UIImage *backgroundButtonImage = [UIImage imageNamed:@"blankbutton.png"];
    backgroundButtonImage = [backgroundButtonImage stretchableImageWithLeftCapWidth:backgroundButtonImage.size.width/2
                                                                       topCapHeight:backgroundButtonImage.size.height/2];
    
    CGRect buttonRect = CGRectMake(0, 0, imageBtn.size.width, 30);
    
    UIButton *closeButton = [[UIButton alloc] initWithFrame:buttonRect];
    [closeButton setBackgroundImage:backgroundButtonImage forState:UIControlStateNormal];
    [closeButton setTitle:title forState:UIControlStateNormal];
    closeButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    closeButton.titleEdgeInsets = UIEdgeInsetsMake(4, 0, 0, 0);
    [closeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [closeButton addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *closeItem = [[UIBarButtonItem alloc] initWithCustomView:closeButton];
    item.rightBarButtonItem = closeItem;
    
    [closeButton release];
    [closeItem release];

    
//    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:image].size.width, [UIImage imageNamed:image].size.height)];
//    UIFont *font = [UIFont boldSystemFontOfSize:16];
//    [btn setTitle:title forState:0];
//    btn.titleLabel.font = font; 
//    [btn setTitleColor:[UIColor whiteColor] forState:0];
//    [btn setTitleShadowColor:[UIColor blackColor] forState:0];
//    [btn setBackgroundImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
//    
//    [btn addTarget: target action: selector forControlEvents: UIControlEventTouchUpInside];
//     UIBarButtonItem* bi = [[UIBarButtonItem alloc] initWithCustomView: btn];
//    [btn release];
//    
//    item.rightBarButtonItem = bi;
//    [bi release];	
//    
}
+(void)setNavigationItemRightTwo:(UINavigationItem*)item buttons:(NSMutableArray*)ButtonArray target:(id)target
selector:(SEL) selector
{
    NSMutableArray *array = [[NSMutableArray alloc]init];
    for (int i = 0; i<[ButtonArray count]; i++) {
        NSMutableDictionary *mDict = [ButtonArray objectAtIndex:i];
        UIImage *mImage = [UIImage imageNamed:[mDict objectForKey:@"imageName"]];
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, mImage.size.width, mImage.size.height)];
        UIFont *font = [UIFont boldSystemFontOfSize:16];
        [btn setTitle:[mDict objectForKey:@"title"] forState:0];
        btn.titleLabel.font = font;
        [btn setTitleColor:[UIColor whiteColor] forState:0];
        [btn setTitleShadowColor:[UIColor blackColor] forState:0];
        [btn setBackgroundImage:mImage forState:UIControlStateNormal];
        
        btn.tag = i;
        [btn addTarget: target action:selector forControlEvents: UIControlEventTouchUpInside];
        UIBarButtonItem* bi = [[UIBarButtonItem alloc] initWithCustomView: btn];
        [btn release];
        
        [array addObject:bi];
        [bi release];
    }
    
    item.rightBarButtonItems = array;
    [array release];

}
+(void) setLeftImageAndNameForNavigationItem:(UINavigationItem*)item Image:(NSString*) image title:(NSString*)title target:(id)target selector:(SEL) selector;
{
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [UIImage imageNamed:image].size.width, [UIImage imageNamed:image].size.height)];
    UIFont *font = [UIFont boldSystemFontOfSize:16];
    [btn setTitle:title forState:0];
    btn.titleLabel.font = font; 
    [btn setTitleColor:[UIColor whiteColor] forState:0];
    [btn setTitleShadowColor:[UIColor blackColor] forState:0];
    [btn setBackgroundImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    
    [btn addTarget: target action: selector forControlEvents: UIControlEventTouchUpInside];
    
    
    
    UIBarButtonItem* bi = [[UIBarButtonItem alloc] initWithCustomView: btn];
    [btn release];
    //UIImageView iv = [[UIImageView alloc] initWithImage:[]]
    
    item.leftBarButtonItem = bi;
    [bi release];	
    
}

#pragma mark TextFileld
+(UITextField*)addTextFileld:(id)target originX:(float)x originY:(float)y width:(float)width height:(float)height
{
    UITextField *textfield = [[UITextField alloc] initWithFrame:CGRectMake(x,y, width, height)];
    [textfield setBorderStyle:UITextBorderStyleLine];
    textfield.placeholder = @"";
    textfield.text = @"";
    textfield.delegate = target;
    textfield.returnKeyType=UIReturnKeyDone;
    textfield.backgroundColor = [UIColor clearColor];
    return textfield;
}
+(UITextField*)addTextFileld:(id)trage frame:(CGRect)frame
{
    UITextField *textfield = [[UITextField alloc] initWithFrame:frame];
    [textfield setBorderStyle:UITextBorderStyleLine];
    textfield.placeholder = @"";
    textfield.text = @"";
    textfield.delegate = trage;
    textfield.returnKeyType=UIReturnKeyDone;
    textfield.backgroundColor = [UIColor clearColor];
    return textfield;
}
+(UITextField*)addTextFileldForView:(UIView*)SelfView frame:(CGRect)frame 
{
    UITextField *textfield = [[UITextField alloc] initWithFrame:frame];
    textfield.textColor = [UIColor blackColor];
    [textfield setBorderStyle:UITextBorderStyleLine];
    textfield.placeholder = @"";
    textfield.text = @"";
    textfield.returnKeyType=UIReturnKeyNext;
    textfield.backgroundColor = [UIColor clearColor];
    [SelfView addSubview:textfield];
    [textfield release];
    return textfield;
}
#pragma mark UITextView

+(UITextView*)addTextView:(id)target originX:(float)x originY:(float)y width:(float)width height:(float)height
{
    UITextView *textview = [[UITextView alloc]initWithFrame:CGRectMake(x, y, width, height)];
    [textview setBounds:CGRectMake(x, y, width, height)];
    textview.layer.borderColor = [UIColor grayColor].CGColor;
    textview.layer.borderWidth =1.0;
    textview.layer.cornerRadius =5.0;
    textview.text = @"";
    textview.delegate = target;
    textview.returnKeyType = UIReturnKeyDone;
    textview.backgroundColor = [UIColor whiteColor];
    return textview;
}

+(UITextView*)addTextView:(id)target frame:(CGRect)frame
{
    UITextView *textview = [[UITextView alloc]initWithFrame:frame];
    [textview setBounds:frame];
    textview.layer.borderColor = [UIColor grayColor].CGColor;
    textview.layer.borderWidth =1.0;
    textview.layer.cornerRadius =5.0;
    textview.text = @"";
    textview.delegate = target;
    textview.returnKeyType = UIReturnKeyDone;
    textview.backgroundColor = [UIColor whiteColor];
    return textview;

}

#pragma mark UILabel
+(UILabel*)addLablePoint:(CGPoint)point text:(NSString*)text textsize:(float)textsize
{
    UIFont *font = [UIFont systemFontOfSize:textsize];
    CGSize size = [text sizeWithFont:font];
    CGRect rect = CGRectMake(point.x, point.y, size.width, size.height);
    UILabel *label = [[UILabel alloc] initWithFrame:rect];
    label.text = text;
    label.font = font;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    return label;
}
+(UILabel*)addLableRect:(CGRect)rect  text:(NSString*)text textsize:(float)textsize
{
    UIFont *font = [UIFont boldSystemFontOfSize:textsize];
    UILabel *label = [[UILabel alloc] initWithFrame:rect];
    label.text = text;
    label.font = font;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    return label;
}
+(UILabel*)addLable:(CGRect)rect tag:(int)tag size:(int)size string:(NSString*)text
{
    UILabel *label = [[UILabel alloc] initWithFrame:rect];
    label.text = text;
    label.tag = tag;
    label.font = [UIFont systemFontOfSize:size];
    
    label.textAlignment = UITextAlignmentLeft;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    return label;
}

#pragma mark button
+(UIButton *)addUIButton:(UIView *)fview Title:(NSString*)Title  rect:(CGRect)rect
{	
	UIButton *btn=[[UIButton alloc] initWithFrame:rect];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn.backgroundColor = [UIColor clearColor];
    [btn setTitle:Title forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:10];
	[fview addSubview:btn];
	//[b addTarget:self action:@selector(m) forControlEvents:UIControlEventTouchUpInside];
	[btn release];
	return	btn;
}
+(UIButton *)addUIButtonOfImage:(NSString *)image Title:(NSString*)Title  rect:(CGRect)rect
{	
    UIButton *btn=[[UIButton alloc] initWithFrame:rect];
    [btn setBackgroundImage:[UIImage imageNamed:image] forState:0];
    
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn setTitle:Title forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont boldSystemFontOfSize:11];
    return	btn;
}
+(UIButton *)addUIButtonByImage:(UIView *)fview image:(NSString*)imagename  rect:(CGRect)rect
{	
	UIButton *btn=[[UIButton alloc] initWithFrame:rect];
    [btn setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:imagename ofType:@""] ] forState:0];
	[fview addSubview:btn];
	//[b addTarget:self action:@selector(m) forControlEvents:UIControlEventTouchUpInside];
	[btn release];
	return	btn;
}


+(void)baiduApi:(NSString*)logevent eventlabel:(NSString*)eventlabel
{
    
    //BaiduMobStat* statTracker = [BaiduMobStat defaultStat]; [statTracker logEvent:logevent eventLabel:eventlabel];
     
}
#pragma mark ShowGlobalsingleRecognizer
+(void)ShowGlobalsingleRecognizer
{
    
}

+(void)DissGlobalsingleRecognizer
{
    
}

#pragma mark 记录是否已经登录
+(BOOL)isNotSignIn
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [[defaults objectForKey:NSLocalizedString(@"PS_Sign", nil)] boolValue];
}
+(BOOL)getCharacter
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [[defaults objectForKey:@"uarentOrTeacher"] boolValue];
}
#pragma mark 记录是否已经登录
+(NSString *)getSignInPrompt
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *signinPrompt = [defaults objectForKey:NSLocalizedString(@"PS_Prompt", nil)];
    if ([signinPrompt length]>0) {
        return signinPrompt;
    }
    return nil;
    //    return NO;
}
#pragma mark 记录登录状态。
+(void)signIn:(BOOL)is
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    documentDirectory =  [documentDirectory stringByAppendingPathComponent:NSLocalizedString(@"PS_Sign", nil)];
    NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
    [mDict setObject:[NSNumber numberWithBool:is] forKey:NSLocalizedString(@"PS_Sign", nil)];
    [mDict writeToFile:documentDirectory atomically:YES];
    [mDict release];
}



+(int)getMealType
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [[defaults objectForKey:@"mealType"] intValue];
 
}
@end




@implementation HealthHeaders
+(BOOL)DisposeRequest:(ASIHTTPRequest *)request
{
    NSData *responeseData = [request responseData];
    NSString *responseStr = [[NSString alloc] initWithData:responeseData encoding:NSUTF8StringEncoding];
    
    
    responseStr =   [responseStr stringByReplacingOccurrencesOfString: @"\r" withString:@""];
    responseStr =   [responseStr stringByReplacingOccurrencesOfString: @"\n" withString:@""];
    
    NSLog(@"---- responseStr %@",responseStr);
    NSMutableDictionary *mDict = [responseStr JSONValue];
    [responseStr release];
    
    int ResultCode = [[[mDict objectForKey:@"ResultStuates"] objectForKey:@"ResultCode"] intValue];
    NSLog(@"ResultCode %d",ResultCode);
    if (ResultCode == 0) {
        NSLog(@"发送正确，完成发送");
        return YES;
    }
    else if (ResultCode == 1000 )
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"用户名或密码错误" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        [SVProgressHUD dismiss];
        return NO;
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"网络错误" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        [SVProgressHUD dismiss];
        
        return NO;
    }
    
}
+(void)FailedDisposeRequest:(ASIHTTPRequest *)request
{
    NSData *data = [request responseData];
    NSString *responseStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"requestFailed -- :%@",responseStr);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"网络错误，发送失败" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
    
    [responseStr release];
    [SVProgressHUD dismiss];

}
+(void)SVProgressView:(UIView *)mview;
{
    [SVProgressHUD show];
    UIView *view = [[UIView alloc] initWithFrame:[mview bounds]];
    view.tag = 111100;
    [view setUserInteractionEnabled:YES];
    [view setBackgroundColor:[UIColor clearColor]];
    [mview addSubview:view];
    [view release];
}
+(void)SVProgressDiss:(UIView *)mview;
{
    [SVProgressHUD dismiss];
    UIView *view = (UIView *)[mview viewWithTag:111100];
    if (view != nil) {
        [view removeFromSuperview];
    }
    
}

+(void)CancelDownLoad:(NSMutableArray*)array  mdictDownLoad:(NSMutableDictionary*)mDownLoadImageDict 
{
    for (int i = 0; i<[array count]; i++) {
        //        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:i inSection:0];
        DownLoadTableImage *imageDownLoad = [mDownLoadImageDict objectForKey:[NSNumber numberWithInt:i]];
        if (imageDownLoad != nil) {
            [imageDownLoad CancelDownloadImage];
        }
        
    }
    [mDownLoadImageDict removeAllObjects];
}

@end

