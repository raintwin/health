//
//  HBTEventDetailHttp.h
//  Health
//
//  Created by dinghao on 13-4-15.
//
//

#import <Foundation/Foundation.h>

@interface HBTEventDetailHttp : NSObject
+(void)DictAddDataString:(NSUserDefaults *)defaults Key:(NSString*)KeyString Value:(NSString*)ValueString;
+(void)eventSigninDetail:(NSMutableDictionary *)valueData;
+ (BOOL)accountsPastAlert;
+(NSString *)getTimeOutMsg;

@end
