//
//  CustomTabBar.h
//  Health
//
//  Created by dinghao on 13-4-3.
//
//

#import <UIKit/UIKit.h>

@interface CustomTabBar : UITabBarController


- (void)hideRealTabBar;
- (void)customTabBar;
- (void)selectedTab:(UIButton *)button;

@end
