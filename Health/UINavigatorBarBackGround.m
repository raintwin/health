//
//  UINavigatorBarBackGround.m
//  WeatherClient
//
//  Created by  hq on 12-5-11.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "UINavigatorBarBackGround.h"


@implementation UINavigationBar (BackGroundImage)
-(UIImage*)  barBackground
{
    return [UIImage imageNamed:@"navback.png"];  
}


- (void)didMoveToSuperview  
{  
    //iOS5 only  
    if ([self respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)])  
    {  
        [self setBackgroundImage:[self barBackground] forBarMetrics:0];  
    }  
}  
//    if([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)]){
//        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"img_44" ofType:@"jpg" ] ] forBarMetrics:UIBarMetricsDefault];
//        NSLog(@"%@",@"device is 5.0");
//    }

//this doesn't work on iOS5 but is needed for iOS4 and earlier  
- (void)drawRect:(CGRect)rect  
{  
    //draw image  
    [[self barBackground] drawInRect:rect];  
}
@end

