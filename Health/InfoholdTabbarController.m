//
//  InfoholdTabbarController.m
//  tabbarBackground
//
//  Created by infohold mac1 on 11-8-8.
//  Copyright 2011 infohold. All rights reserved.
//

#import "InfoholdTabbarController.h"
@implementation InfoholdTabBarItem
@synthesize CustomHighlightedImage;

-(void)dealloc{
	[super dealloc];
	CustomHighlightedImage=nil;
	[CustomHighlightedImage release];
}

-(UIImage *)selectedImage{
	return self.CustomHighlightedImage;
}

@end



@implementation InfoholdTabbarController
@synthesize TabbarBackgroundImage;
-(void)viewDidLoad{
	CGRect frame = CGRectMake(0.0, 0.0, 80, 50);
	
	UIImageView *imgView=[[UIImageView alloc] initWithFrame:frame];
//	imgView.autoresizingMask=UIViewAutoresizingFlexibleWidth;
//    imgView.contentMode = UIViewContentModeScaleToFill;
    

	[imgView setImage:self.TabbarBackgroundImage];
	/*
	 UIView *v = [[UIView alloc] initWithFrame:frame];
	 [v setBackgroundColor:[[UIColor alloc] initWithRed:1.0 
	 green:0.0 
	 blue:0.0 
	 alpha:1.0]]; 
	 
	 
	 v.autoresizingMask=UIViewAutoresizingFlexibleWidth;
	 [myTabBar insertSubview:v atIndex:0];
	 [v release];  
	 */
	[self.tabBar insertSubview:imgView atIndex:0];
	[imgView release];
	
	
}

@end
