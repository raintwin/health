 //
//  DMAppDelegate.m
//  Health
//
//  Created by hq on 12-6-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "DMAppDelegate.h"
#import "DMViewController.h"
#import "BulidController.h"
#import "LoginViewController.h"
#import "BulidController.h"
#import "Instance.h"


#import "CustomTabBar.h"
#import "LeveyTabBarController.h"
#import "BulidController.h"
#import "IndexsViewController.h"
#import "FunctionViewController.h"
#import "PaperForChildList.h"
#import "SetingViewController.h"

#import "AlixPay.h"
#import "AlixPayResult.h"
#import "DataVerifier.h"
#import <sys/utsname.h>

#import "SetingViewController.h"
#import "PaperViewController.h"
#import "PhotoViewController.h"
//#import "BaiduMobStat.h"

@interface DMAppDelegate ()

- (BOOL)isSingleTask;
- (void)parseURL:(NSURL *)url application:(UIApplication *)application;

@end

@implementation DMAppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController;
@synthesize leveyTabBarController;

- (BOOL)isSingleTask{
	struct utsname name;
	uname(&name);
	float version = [[UIDevice currentDevice].systemVersion floatValue];//判定系统版本。
	if (version < 4.0 || strstr(name.machine, "iPod1,1") != 0 || strstr(name.machine, "iPod2,1") != 0) {
		return YES;
	}
	else {
		return NO;
	}
}

#pragma mark -
#pragma mark Application lifecycle

- (void)dealloc
{

    [_window release];
    [_viewController release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound|UIRemoteNotificationTypeAlert];
//    [[UIApplication sharedApplication]setStatusBarStyle:NO animated:NO];
    [application setStatusBarStyle:UIStatusBarStyleBlackOpaque];
    
    
    FunctionViewController *mainViewController = [[FunctionViewController alloc] init];
    
    PhotoViewController *searchViewController = [[PhotoViewController alloc]init];
    
//    PaperViewController *myselfViewController = [[PaperViewController alloc]init];
    
    SetingViewController *settingViewController = [[SetingViewController alloc]init];
    UIViewController *myselfViewController;
    if ([Instance getCharacter]) {
        myselfViewController = [[PaperViewController alloc] initWithNibName:@"PaperViewController" bundle:nil];
        myselfViewController.title    = @"留言";
    }
    else
    {
        myselfViewController = [[PaperForChildList alloc] initWithNibName:@"PaperForChildList" bundle:nil];
        myselfViewController.title    = @"留言";
    }

    
    
    //隐藏tabbar所留下的黑边（试着注释后你会知道这个的作用）
    
    mainViewController.hidesBottomBarWhenPushed = true;

    searchViewController.hidesBottomBarWhenPushed = true;

     myselfViewController.hidesBottomBarWhenPushed = true;

     settingViewController.hidesBottomBarWhenPushed = true;
    

    
    NSLog(@"mainViewController.view = %@.",NSStringFromCGRect(mainViewController.view.frame));
    
    //创建导航
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:mainViewController ];
    
    UINavigationController *nav1 = [[ UINavigationController alloc] initWithRootViewController:searchViewController];
    
    UINavigationController *nav2 = [[UINavigationController alloc] initWithRootViewController:myselfViewController];
    
    UINavigationController *nav3 = [[UINavigationController alloc]initWithRootViewController:settingViewController];
    

    
    //创建数组
    
    NSMutableArray *controllers = [[NSMutableArray alloc]init];
    
    [controllers addObject:nav];
    
    [controllers addObject:nav1];
    
    [controllers addObject:nav2];
    
    [controllers addObject:nav3];
    
    
    
    //创建tabbar
    
    tabBarController = [[CustomTabBar alloc] init];
    
    tabBarController.viewControllers = controllers;
    tabBarController.delegate = self;
    tabBarController.selectedIndex = 0;
    
    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    self.viewController = [[[DMViewController alloc] initWithNibName:@"DMViewController" bundle:nil] autorelease];

    [self.window addSubview:tabBarController.view];
    [self.window makeKeyAndVisible];

    
    NSLog(@"%@",[Instance GetDocument]);

    /*
	 *单任务handleURL处理
	 */
	if ([self isSingleTask]) {
		NSURL *url = [launchOptions objectForKey:@"UIApplicationLaunchOptionsURLKey"];
		
		if (nil != url) {
			[self parseURL:url application:application];
		}
	}


    return YES;
}
#pragma mark 记录登录状态。

-(void)SendUDID:(NSString *)deviceToken
{


    if ([deviceToken length] == 0) {
        return;
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:deviceToken forKey:NSLocalizedString(@"PS_deviceSerialID", nil)];
    [defaults synchronize];
}
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_3_0)
{
	NSLog(@"didRegister start:%@", deviceToken);
    
    
    
    NSMutableString* strDeviceToken = [[NSMutableString alloc] initWithFormat:@"%@", deviceToken];
    
    [strDeviceToken replaceOccurrencesOfString:@" " withString:@"" options:0 range:NSMakeRange(0, strDeviceToken.length)];
    [strDeviceToken replaceOccurrencesOfString:@"<" withString:@"" options:0 range:NSMakeRange(0, strDeviceToken.length)];
    [strDeviceToken replaceOccurrencesOfString:@">" withString:@"" options:0 range:NSMakeRange(0, strDeviceToken.length)];

    
	NSLog(@"didRegister start:%@", strDeviceToken);

    [self SendUDID:strDeviceToken];
    [strDeviceToken release];

}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_3_0);
{
    NSLog(@"didRegister fail  %@",error);

}

-(NSString*)DocumentFilePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    [documentDirectory stringByAppendingPathComponent:@"initlalize"];
    return [documentDirectory stringByAppendingPathComponent:@"initlalize"];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
	
	[self parseURL:url application:application];
	return YES;
}


- (void)parseURL:(NSURL *)url application:(UIApplication *)application {
	AlixPay *alixpay = [AlixPay shared];
	AlixPayResult *result = [alixpay handleOpenURL:url];
	if (result) {
		//是否支付成功
		if (9000 == result.statusCode) {
			/*
			 *用公钥验证签名
			 */
            
            NSString *path = [[NSBundle mainBundle] bundlePath];
            NSString *finalPath = [path stringByAppendingPathComponent: @"Info.plist"];
            NSMutableDictionary * dict = [ [ NSMutableDictionary alloc ]initWithContentsOfFile:finalPath];
            
            NSLog(@"privatekey=%@",[dict objectForKey:@"RSA private key"]);
//            id<DataVerifier> verifier = CreateRSADataVerifier([[NSBundle mainBundle] objectForInfoDictionaryKey:@"RSA public key"]);

			id<DataVerifier> verifier = CreateRSADataVerifier([dict objectForKey:@"RSA public key"]);
            NSLog(@"public key:%@",[dict objectForKey:@"RSA public key"]);
			if ([verifier verifyString:result.resultString withSign:result.signString]) {
				UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"提示"
																	 message:result.statusMessage 
																	delegate:nil
														   cancelButtonTitle:@"确定"
														   otherButtonTitles:nil];
				[alertView show];
				[alertView release];
                NSLog(@"result.statusMessage T= %@",result.statusMessage);

			}//验签错误
			else {
				UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"提示"
																	 message:@"签名错误"
																	delegate:nil
														   cancelButtonTitle:@"确定"
														   otherButtonTitles:nil];
				[alertView show];
				[alertView release];
			}
		}
		//如果支付失败,可以通过result.statusCode查询错误码
		else {
			UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"提示"
																 message:result.statusMessage
																delegate:nil
													   cancelButtonTitle:@"确定"
													   otherButtonTitles:nil];
			[alertView show];
			[alertView release];
            NSLog(@"result.statusMessage f= %@",result.statusMessage);
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            if ([defaults objectForKey:@"isTimeOut"]) {
                [defaults setObject:[NSNumber numberWithBool:NO] forKey:NSLocalizedString(@"PS_Sign", nil)];
                [defaults synchronize];
            }

		}
		
	}
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
    NSLog(@"放弃激活");    


}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    NSLog(@"进入后台");

    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
//     */
//    NSLog(@"响应");
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    if ([defaults objectForKey:NSLocalizedString(@"PS_Sign", nil)]) {
//        [self.viewController defaultSignIn];
//    }

}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */

}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    //    return !(interfaceOrientation == UIDeviceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
//    if ( interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown ) {
//        return NO;
//    }
//    else {
//        return YES;
//    } 
//}
//
//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
//    return UIInterfaceOrientationPortrait;
//}

@end
