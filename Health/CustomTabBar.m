//
//  CustomTabBar.m
//  Health
//
//  Created by dinghao on 13-4-3.
//
//

#import "CustomTabBar.h"

@interface CustomTabBar ()
@property (nonatomic,assign) int currentSelectedIndex;
@property (nonatomic,retain) NSMutableArray *buttons;

@end

@implementation CustomTabBar


@synthesize currentSelectedIndex;
@synthesize buttons;

- (void)viewDidAppear:(BOOL)animated{
    
    [self hideRealTabBar];
    [self customTabBar];
}

- (void)hideRealTabBar{
    for(UIView *view in self.view.subviews){
        if([view isKindOfClass:[UITabBar class]]){
            view.hidden = YES;
            break;
        }
    }
}

- (void)customTabBar{
    NSArray *array = [[NSArray alloc]initWithObjects:@"0322new_05.jpg",@"0322new_06.jpg",@"0322new_07.jpg",@"0322new_08.jpg", nil];
    
    //创建按钮
    int viewCount = self.viewControllers.count > 5 ? 5 : self.viewControllers.count;
    self.buttons = [NSMutableArray arrayWithCapacity:viewCount];
    double _width = 320 / viewCount;
    double _height = self.tabBar.frame.size.height;
    NSLog(@"self.tabBar.frame.size.height = %f",self.tabBar.frame.size.height);
    for (int i = 0; i < viewCount; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(i*_width,self.tabBar.frame.origin.y, _width, _height);
        [btn setBackgroundImage:[UIImage imageNamed:[array objectAtIndex:i]] forState:0];
        [btn addTarget:self action:@selector(selectedTab:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = i;
        [self.buttons addObject:btn];
        [self.view  addSubview:btn];
        [btn release];
    }
    [array release];


    [self selectedTab:[self.buttons objectAtIndex:0]];
    
}

- (void)selectedTab:(UIButton *)button{
    if (self.currentSelectedIndex == button.tag) {
        return;
    }
    self.currentSelectedIndex = button.tag;
    self.selectedIndex = self.currentSelectedIndex;
    [self performSelector:@selector(slideTabBg:) withObject:button];
}
-(void)setButtonImage
{

    NSArray *array = [[NSArray alloc]initWithObjects:@"0322new_05.jpg",@"0322new_06.jpg",@"0322new_07.jpg",@"0322new_08.jpg", nil];
    for (UIButton *btn in self.buttons) {
        [btn setBackgroundImage:[UIImage imageNamed:[array objectAtIndex:btn.tag]] forState:0];
    }
}
- (void)slideTabBg:(UIButton *)btn{

    [self setButtonImage];
    NSArray *array = [[NSArray alloc]initWithObjects:@"0322new_o05.jpg",@"0322new_o06.jpg",@"0322new_o07.jpg",@"0322new_o08.jpg", nil];

    [btn setBackgroundImage:[UIImage imageNamed:[array objectAtIndex:btn.tag]] forState:0];
    [array release];
    
}

- (void) dealloc{
    [self.buttons release];
    [super dealloc];
}
@end
