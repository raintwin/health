//
//  BulidController.m
//  Health
//
//  Created by hq on 12-6-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "BulidController.h"
#import "IndexsViewController.h"
#import "FunctionViewController.h"
#import "PaperListViewController.h"
#import "SetingViewController.h"
#import "LoadingTable.h"
#import "UINavigatorBarBackGround.h"
#import "InfoholdTabbarController.h"
#import "PaperForChildList.h"
#import "PaperViewController.h"
#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "ChildPhotoMainView.h"
#import "PhotoViewController.h"
@implementation BulidController


-(void)GetPaperNotNum
{
    [[UIApplication sharedApplication].keyWindow removeGestureRecognizer:GlobalsingleRecognizer];
    GlobalsingleRecognizer = nil;
    GlobalsingleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapFrom)];
    GlobalsingleRecognizer.numberOfTapsRequired = 1; // 单击
    GlobalsingleRecognizer.numberOfTouchesRequired = 1; //手指数
    [[UIApplication sharedApplication].keyWindow addGestureRecognizer:GlobalsingleRecognizer];
    [GlobalsingleRecognizer release];
    
    NSString *urlstr;
    NSMutableDictionary *mDic = [[NSMutableDictionary alloc] init];

    if ([Instance GetUseType] == LOGIN_TEACH) {
        urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/LeaveMessage/TeacherFetchChildListAndNotReadCount"];
        [mDic setObject:[Instance GetUseID] forKey:@"ID"];

    }
    if ([Instance GetUseType] == LOGIN_BIND_P || [Instance GetUseType] == LOGIN_NOBIND_P) {
        urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/LeaveMessage/ParentFetchNotReadCount"];
        [mDic setObject:[Instance GetUseID] forKey:@"ID"];
        [mDic setObject:[Instance GetChildID] forKey:@"childID"];

    }
    NotNumber = 0;
    NSURL *url = url = [NSURL URLWithString:urlstr];
    
    NSString *JSONString = [mDic JSONRepresentation];
    [mDic release];
    NSLog(@"JSONString :%@",JSONString);
    [SVProgressHUD show];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setDelegate:self];
    [request setTimeOutSeconds:20];
    [request setPostValue:JSONString forKey:@"requestContent"];
    [request startSynchronous];
    
    


}
-(void)handleSingleTapFrom
{
    NSLog(@"login");
}
#pragma mark requestFinished 
- (void)requestFinished:(ASIHTTPRequest *)request {
    
    [[UIApplication sharedApplication].keyWindow removeGestureRecognizer:GlobalsingleRecognizer];
    GlobalsingleRecognizer = nil;

    
    if (![HealthHeaders DisposeRequest:request]) {
        return;
    }    
    NSData *responeseData = [request responseData];
    NSString *responseStr = [[NSString alloc] initWithData:responeseData encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *mDict = [responseStr JSONValue];
    NSString *ResultContent = [[mDict objectForKey:@"ResultContent"] JSONRepresentation];
    NSLog(@"ResultContent :%@",ResultContent);
    [responseStr release];
    
    if ([Instance GetUseType] == LOGIN_TEACH) {
        NSArray *array = [[[ResultContent JSONValue] valueForKey:@"childList"] retain];
        for (int i = 0; i < [array count]; i++) {
            NotNumber += [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"notReadCount"]] intValue];
        }
        [array release];
        [SVProgressHUD dismiss];
        return ;
    }
    if ([Instance GetUseType] == LOGIN_BIND_P) {
        NotNumber =[[[ResultContent JSONValue] valueForKey:@"notReadCount"] intValue];
        [SVProgressHUD dismiss];
        return;
    }
    

    
    
}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    NotNumber = 0;
    [SVProgressHUD dismiss];
    
}
#pragma mark 首页
-(void)BuildeIndex:(NSMutableArray *)ma
{
    
    InfoholdTabBarItem *MytabBarItem=[[InfoholdTabBarItem alloc] initWithTitle:@"首页" image:[UIImage imageNamed:@"nav_37.png"] tag:0];
	MytabBarItem.CustomHighlightedImage=[UIImage imageNamed:@"nav_28.png"];

    
    IndexsViewController *iv = [[IndexsViewController alloc] initWithNibName:@"IndexsViewController" bundle:nil];
    iv.title = @"首页";
    UIViewController *vc = [[UINavigationController alloc]initWithRootViewController:iv];

	iv.tabBarItem=MytabBarItem;
    [MytabBarItem release];
    
    [ma addObject:vc];
    [iv release];
    [vc release];
    
    
}
#pragma mark 功能
-(void)BuildeFunction:(NSMutableArray *)ma
{
    FunctionViewController *iv = [[FunctionViewController alloc] initWithNibName:@"FunctionViewController" bundle:nil];
    iv.title = @"功能";

    UIViewController *vc = [[UINavigationController alloc]initWithRootViewController:iv];
    
    InfoholdTabBarItem *MytabBarItem=[[InfoholdTabBarItem alloc] initWithTitle:nil image:[UIImage imageNamed:@"bottom_1.jpg"] tag:0];
	MytabBarItem.CustomHighlightedImage=[UIImage imageNamed:@"bottomover_1.jpg"];
	iv.tabBarItem=MytabBarItem;
    [MytabBarItem release];
    
    [ma addObject:vc];
    [iv release];
    [vc release];
}
#pragma mark 相册
-(void)BuildePhoto:(NSMutableArray *)ma
{
    PhotoViewController *iv = [[PhotoViewController alloc]initWithNibName:@"PhotoViewController" bundle:nil];
    iv.title = @"相册";
    
    UIViewController *vc = [[UINavigationController alloc]initWithRootViewController:iv];
    
    InfoholdTabBarItem *MytabBarItem=[[InfoholdTabBarItem alloc] initWithTitle:nil image:[UIImage imageNamed:@"bottom_2.jpg"] tag:1];
	MytabBarItem.CustomHighlightedImage=[UIImage imageNamed:@"bottomover_2.png"];
	iv.tabBarItem=MytabBarItem;
    [MytabBarItem release];
    
    [ma addObject:vc];
    [iv release];
    [vc release];
}
#pragma mark 留言
-(void)BuildePaper:(NSMutableArray *)ma
{

//    if ([Instance GetUseType] == LOGIN_TEACH) {
//        PaperForChildList *iv = [[PaperForChildList alloc] initWithNibName:@"PaperForChildList" bundle:nil];
//        iv.title = @"留言";
//        UIViewController *vc = [[UINavigationController alloc]initWithRootViewController:iv];
//        InfoholdTabBarItem *MytabBarItem=[[InfoholdTabBarItem alloc] initWithTitle:@"留言" image:[UIImage imageNamed:@"nav_32.png"] tag:2];
//        MytabBarItem.CustomHighlightedImage=[UIImage imageNamed:@"nav_64.png"];
//        if (NotNumber>0) {
//            MytabBarItem.badgeValue = [NSString stringWithFormat:@"%d",NotNumber];  ;
//
//        }
//        iv.tabBarItem=MytabBarItem;
//        [MytabBarItem release];
//        
//        [ma addObject:vc];
//        [iv release];
//        [vc release];
//
//    }
//    if ([Instance GetUseType] == LOGIN_BIND_P) {
        PaperViewController *iv = [[PaperViewController alloc] initWithNibName:@"PaperViewController" bundle:nil];
        iv.title = @"留言";
        iv.childid = [[Instance GetChildID] intValue];
        iv.notReadCount = 5;
        UIViewController *vc = [[UINavigationController alloc]initWithRootViewController:iv];
        InfoholdTabBarItem *MytabBarItem=[[InfoholdTabBarItem alloc] initWithTitle:nil image:[UIImage imageNamed:@"bottom_3.jpg"] tag:2];
        MytabBarItem.CustomHighlightedImage=[UIImage imageNamed:@"bottomover_3.jpg"];
//        if (NotNumber>0) {
//            MytabBarItem.badgeValue = [NSString stringWithFormat:@"%d",NotNumber];  ;
////            iv.notReadCount = NotNumber;
////
//        }

        iv.tabBarItem=MytabBarItem;
        [MytabBarItem release];
        
        [ma addObject:vc];
        [iv release];
        [vc release];
//    }
}
#pragma mark 设置
-(void)BuildeSeting:(NSMutableArray *)ma
{
    SetingViewController *iv = [[SetingViewController alloc] initWithNibName:@"SetingViewController" bundle:nil];
    UIViewController *vc = [[UINavigationController alloc]initWithRootViewController:iv];
    InfoholdTabBarItem *MytabBarItem=[[InfoholdTabBarItem alloc] initWithTitle:nil image:[UIImage imageNamed:@"bottom_4.jpg"] tag:3];
	MytabBarItem.CustomHighlightedImage=[UIImage imageNamed:@"bottomover_4.jpg"];
	iv.tabBarItem=MytabBarItem;
    [MytabBarItem release];
    [ma addObject:vc];
    [iv release];
    [vc release];
    
}
-(UITabBarController *)BuildsController
{
//    [self performSelectorOnMainThread:@selector(GetPaperNotNum) withObject:nil waitUntilDone:YES];
//    if ([Instance GetUseType] != LOGIN_NULL) {
//        [self GetPaperNotNum];
//
//    }
    UITabBarController* tbcCtr = [[UITabBarController alloc] init];
    
    NSMutableArray *ma = [[NSMutableArray alloc]init];
//    if ([Instance GetUseType] == LOGIN_BIND_P) {
//        
//        
//        
//        [self BuildeIndex:ma];
//    }
//    [self BuildeIndex:ma];

    
    [self BuildeFunction:ma];
    [self BuildePhoto:ma];
    [self BuildePaper:ma];
    [self BuildeSeting:ma];
    
    
    tbcCtr.viewControllers = ma;
    [ma release];
    return tbcCtr;
}
-(void)dealloc
{
//    [funiv release];
    [super dealloc];
}
@end
