//
//  DMViewController.h
//  Health
//
//  Created by hq on 12-6-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DMAppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import <sqlite3.h>

#import "LoginViewController.h"
#import "LeveyTabBarController.h"
#import "PaymentViewController.h"
@class PhotoListViewController;

@interface DMViewController : UIViewController<HttpFormatRequestDeleagte,UIAlertViewDelegate>
{
    
    BOOL complete;
    NSTimer *timer;
    int timeIndex;
}
@property (retain,nonatomic) DMAppDelegate *myDelegate;

-(void)GoLoginViewController;

-(void)setButtonsendSubviewToBack;
-(void)setButtonBringSubviewToFront;
- (void)goPaymentViewController;
-(void)defaultSignIn;

@end
