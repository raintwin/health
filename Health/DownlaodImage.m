//
//  DownlaodImage.m
//  Health
//
//  Created by hq on 12-7-23.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "DownlaodImage.h"

@implementation DownlaodImage
@synthesize mImageURL;// = StrImageURL;
@synthesize mConnection;// = objConnection;
@synthesize mImageDate;// = ImageDate;
@synthesize mImageWidth;// = ImageWidth;
@synthesize mImageHeight;// = ImageHeight;
@synthesize mImageView;// = ImageView;
-(id)init
{
    if (self = [super init]) {
        ImageHeight = 60;
        ImageWidth = 90;
        self.mImageURL = @"http://travel.lingnantiandi.com/lnip/Public/Uploads/yhhd/1342410329.jpg";
    }
    return self;
}

-(void) dealloc
{
	if (self.mConnection) 
	{
		[self.mConnection cancel];
	}
	self.mImageDate = nil;
	self.mConnection = nil;
	
	[StrImageURL release];
    
	
	
    
    
	
	[super dealloc];
}

-(BOOL) CancelDownloadImage
{
	if (self.mConnection) 
	{
		[self.mConnection cancel];
	}
	self.mImageDate = nil;
	self.mConnection = nil;
	
	return YES;
}

#pragma mark NSURLConnectionDelegate

-(BOOL)StartDownloadImage
{
    self.mImageDate = [NSMutableData data];
    
//    objConnection = [[NSURLConnection alloc] initWithRequest:
//                     [NSURLRequest requestWithURL:[NSURL URLWithString:self.mImageURL] cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:30] delegate:self];
    mConnection = [[NSURLConnection alloc] initWithRequest:
                     [NSURLRequest requestWithURL:
                      [NSURL URLWithString:self.mImageURL] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30] delegate:self];

    NSLog(@"load image from Url:%@",self.mImageURL);
    return YES;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.mImageDate appendData:data];
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    self.mImageDate = nil;
    self.mConnection = nil;
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    UIImage *image = [[UIImage alloc] initWithData:self.mImageDate];
    self.mImageDate = nil;
    self.mConnection = nil;
    
    if (image.size.width == 0) {
        
    }
    else
    {
//        if (image.size.width !=ImageWidth && image.size.height != ImageHeight ) {
            CGSize itemSize = CGSizeMake(image.size.width, image.size.height);
            UIGraphicsBeginImageContext(itemSize);
            CGRect imageRect = CGRectMake(0, 0, itemSize.width, itemSize.height);
            [image drawInRect:imageRect];
            [image release];
            image = UIGraphicsGetImageFromCurrentImageContext();
            [image retain];
            UIGraphicsEndImageContext();
//        }
        self.mImageView.image = image;
    }
}
@end
