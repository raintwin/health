//
//  DMViewController.m
//  Health
//
//  Created by hq on 12-6-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "DMViewController.h"
#import "LoginViewController.h"
#import "Instance.h"
#import "HBTEventDetailHttp.h"
#import "JSON.h"
#import "CustomTabBar.h"

#import "IndexsViewController.h"
#import "FunctionViewController.h"
#import "PaperViewController.h"
#import "SetingViewController.h"
#import "PhotoViewController.h"
#import "PaperForChildList.h"

@interface DMViewController ()

@property(nonatomic,retain)NSMutableArray *viewControllers;
@property(nonatomic,retain)UIButton *button1;
@property(nonatomic,retain)UIButton *button2;
@property(nonatomic,retain)UIButton *button3;
@property(nonatomic,retain)UIButton *button4;
@property(nonatomic,retain) NSMutableDictionary *mSignInDict;

@end


@implementation DMViewController
@synthesize mSignInDict;
@synthesize viewControllers;

@synthesize button1;
@synthesize button2;
@synthesize button3;
@synthesize button4;

@synthesize myDelegate;
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

	// Do any additional setup after loading the view, typically from a nib.
    self.viewControllers = [[NSMutableArray alloc]init];
    self.mSignInDict = [[NSMutableDictionary alloc]init];
        
    CGRect frame = [[UIScreen mainScreen] bounds];
    frame.origin.y = 20;
    frame.size.height = frame.size.height -20;
    [self.view setFrame:frame];
    NSLog(@"farma-- :%@",NSStringFromCGRect(self.view.frame));

    timeIndex = 0;
    [self initViewController];
    NSLog(@"self.view.bounds.size.height :%f",self.view.frame.size.height);
    self.button1 = [ViewTool addUIButton:self.view imageName:@"bottomover_1.jpg" type:@"" x:0 y:self.view.bounds.size.height-50 x1:80 y1:50];
    self.button1.tag = 1;
    [self.button1 addTarget:self action:@selector(functonView:) forControlEvents:64];

    self.button2 = [ViewTool addUIButton:self.view imageName:@"bottom_2.jpg" type:@"" x:80 y:self.view.bounds.size.height-50 x1:80 y1:50];
    self.button2.tag = 2;
    [self.button2 addTarget:self action:@selector(functonView:) forControlEvents:64];

    self.button3 = [ViewTool addUIButton:self.view imageName:@"bottom_3.jpg" type:@"" x:160 y:self.view.bounds.size.height-50 x1:80 y1:50];
    self.button3.tag = 3;
    [self.button3 addTarget:self action:@selector(functonView:) forControlEvents:64];

    self.button4 = [ViewTool addUIButton:self.view imageName:@"bottom_4.jpg" type:@"" x:240 y:self.view.bounds.size.height-50 x1:80 y1:50];
    self.button4.tag = 4;
    [self.button4 addTarget:self action:@selector(functonView:) forControlEvents:64];
    

    [self performSelector:@selector(defaultSignIn) withObject:nil afterDelay:0.0f];
    complete = NO;
    
}
-(void)setButtonImage
{
    [self.button1 setImage:[UIImage imageNamed:@"bottom_1.jpg"] forState:0];
    [self.button2 setImage:[UIImage imageNamed:@"bottom_2.jpg"] forState:0];
    [self.button3 setImage:[UIImage imageNamed:@"bottom_3.jpg"] forState:0];
    [self.button4 setImage:[UIImage imageNamed:@"bottom_4.jpg"] forState:0];
}
-(void)setButtonBringSubviewToFront
{

    [self.view bringSubviewToFront:self.button1];
    [self.view bringSubviewToFront:self.button2];
    [self.view bringSubviewToFront:self.button3];
    [self.view bringSubviewToFront:self.button4];
}
-(void)setButtonsendSubviewToBack
{

    [self.view sendSubviewToBack:self.button1];
    [self.view sendSubviewToBack:self.button2];
    [self.view sendSubviewToBack:self.button3];
    [self.view sendSubviewToBack:self.button4];
}
-(void)resetViewController
{
    for (UINavigationController *navigation in self.viewControllers) {
        [navigation.view removeFromSuperview];
    }
}
-(void)initViewController
{
    [self resetViewController];
    [self.button1 setImage:[UIImage imageNamed:@"bottomover_1.jpg"] forState:0];
    UIViewController *viewcontroller = nil;
    UINavigationController *navigationController = nil;
    viewcontroller = [[FunctionViewController alloc] initWithNibName:@"FunctionViewController" bundle:nil];
    navigationController = [[UINavigationController alloc] initWithRootViewController:viewcontroller];
    viewcontroller.title    = @"功能";
    [navigationController.view setFrame:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, self.view.bounds.size.height - 50)];
    [self.view addSubview:navigationController.view];
    [self.viewControllers addObject:navigationController];
    [viewcontroller release];
    [navigationController release];
    
    [self setButtonBringSubviewToFront];

}
-(void)functonView:sender
{

    UIButton *btn = sender;
    
    if (![Instance isNotSignIn] && btn.tag !=1 && btn.tag != 4) {
        [self loadSignIn];
        return;
    }
    
    UIViewController *viewcontroller = nil;
    UINavigationController *navigationController = nil;
    [self setButtonImage];
    [self resetViewController];
    int height = 50;
    switch (btn.tag) {
        case 1:
            [self.button1 setImage:[UIImage imageNamed:@"bottomover_1.jpg"] forState:0];

            viewcontroller = [[FunctionViewController alloc] initWithNibName:@"FunctionViewController" bundle:nil];
            navigationController = [[UINavigationController alloc] initWithRootViewController:viewcontroller];
            viewcontroller.title    = @"功能";
            break;
        case 2:
            [self.button2 setImage:[UIImage imageNamed:@"bottomover_2.jpg"] forState:0];
            viewcontroller = [[PhotoViewController alloc] initWithNibName:@"PhotoViewController" bundle:nil];
            navigationController = [[UINavigationController alloc] initWithRootViewController:viewcontroller];
            viewcontroller.title    = @"相册";
            height = 0;
            break;

        case 3:
            [self.button3 setImage:[UIImage imageNamed:@"bottomover_3.jpg"] forState:0];

            if ([Instance getCharacter]) {
                viewcontroller = [[PaperViewController alloc] initWithNibName:@"PaperViewController" bundle:nil];
                navigationController = [[UINavigationController alloc] initWithRootViewController:viewcontroller];
                viewcontroller.title    = @"留言";
            }
            else
            {
                viewcontroller = [[PaperForChildList alloc] initWithNibName:@"PaperForChildList" bundle:nil];
                navigationController = [[UINavigationController alloc] initWithRootViewController:viewcontroller];
                viewcontroller.title    = @"留言";
            }
            [navigationController.navigationItem setHidesBackButton:YES];

            
            break;

        case 4:
            [self.button4 setImage:[UIImage imageNamed:@"bottomover_4.jpg"] forState:0];
            viewcontroller = [[SetingViewController alloc] initWithNibName:@"SetingViewController" bundle:nil];
            navigationController = [[UINavigationController alloc] initWithRootViewController:viewcontroller];
            viewcontroller.title    = @"设置";
            break;

        default:
            break;
    }
    [navigationController.view setFrame:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, self.view.bounds.size.height - height)];
    [self.view addSubview:navigationController.view];
    [self.viewControllers addObject:navigationController];
    [viewcontroller release];
    [navigationController release];
    
    [self setButtonBringSubviewToFront];
}

-(void)GoLoginViewController
{
    [self setButtonBringSubviewToFront];
    NSString *filePath = [Instance GetDocumentByString:[NSString stringWithFormat:@"%@.png",[Instance GetUseID]]];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[Instance GetUsePhotoUrl]]];
    [request setDownloadDestinationPath:filePath];
    [request setDelegate:self];
    [request setTimeOutSeconds:20];
    [request startAsynchronous];
    request.tag = FILEDOWNID;

    [self initViewController];
}

-(void)defaultSignIn
{
    [self initSignDict];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *UDID = [defaults objectForKey:NSLocalizedString(@"PS_deviceSerialID", nil)];
    
    if ([UDID length] >0) {
        [mSignInDict setObject:UDID forKey:NSLocalizedString(@"HT_DeviceSerialID", nil)];
    }
    [mSignInDict setObject:NSLocalizedString(@"PORT_SigIN", nil) forKey:NSLocalizedString(@"PORT_ReqType", nil)];
    [mSignInDict setObject:[NSNumber numberWithInt:1] forKey:NSLocalizedString(@"HT_MoblieType", nil)];

    
    NSString *useName = [defaults objectForKey:NSLocalizedString(@"HT_UserNo", nil)];
    NSString *usePassWord = [defaults objectForKey:NSLocalizedString(@"HT_Password", nil)];
    
    if ([useName length] > 0) {
        [mSignInDict setObject:useName forKey:NSLocalizedString(@"HT_UserNo", nil)];
    }
    if ([usePassWord length] > 0) {
        [mSignInDict setObject:usePassWord forKey:NSLocalizedString(@"HT_Password", nil)];
    }
    NSLog(@"mSignInDict _%@",mSignInDict);
    [self httpSignIn:[mSignInDict JSONRepresentation]];
}
-(void)httpSignIn:(NSString *)JSONString
{
    NSLog(@"JSONString - %@",JSONString);
    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate =self;
    [mHttpRequest CustomFormDataRequestDict:JSONString tag:HTTPSIGN url:WEBSERVERURL];
    [self.view addSubview:mHttpRequest];
    [mHttpRequest release];

}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    
    if (tag == 1) {
        NSLog(@"ResultContent :%@",ResultContent);
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:ResultContent forKey:NSLocalizedString(@"PS_Prompt", nil)];
        [defaults setObject:[NSNumber numberWithBool:NO] forKey:NSLocalizedString(@"PS_Sign", nil)];
        [defaults synchronize];
        [self GoLoginViewController];
        return;
    }

    

    NSDictionary *dict = [ResultContent JSONValue];
    
    if ([dict objectForKey:@"isTimeOut"]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:[dict objectForKey:@"timeOutMsg"] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"马上付支", nil];
        alert.delegate = self;
        alert.tag = 100;
        [alert show];
        [alert release];
        [[NSUserDefaults standardUserDefaults] setObject:[dict objectForKey:@"userID"] forKey:@"userID"];

        return;
    }
    else if ([[dict objectForKey:@"timeOutMsg"] length] > 0 && ![dict objectForKey:@"isTimeOut"]) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:[dict objectForKey:@"timeOutMsg"] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"马上付支", nil];
        alert.delegate = self;
        alert.tag = 101;
        [alert show];
        [alert release];
    }
    

    [HBTEventDetailHttp eventSigninDetail:[ResultContent JSONValue]];
    [self GoLoginViewController];
    return;

}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        NSLog(@"支付宝");
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:NSLocalizedString(@"PS_Sign", nil)];
        
        [self goPaymentViewController];
    }
    if (buttonIndex == 0 && alertView.tag == 100) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[NSNumber numberWithBool:NO] forKey:NSLocalizedString(@"PS_Sign", nil)];
        [defaults synchronize];
        [self GoLoginViewController];

    }

}
- (void)goPaymentViewController
{
    PaymentViewController *paymentViewController = [[PaymentViewController alloc]initWithNibName:@"PaymentViewController" bundle:nil];
    [self.view addSubview:paymentViewController.view];
}
- (void)requestFinished:(ASIHTTPRequest *)request
{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    
//    
//    [defaults setObject:[NSNumber numberWithBool:NO] forKey:NSLocalizedString(@"PS_Sign", nil)];
    NSLog(@"fin??");
}
- (void)requestFailed:(ASIHTTPRequest *)request
{}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"网络错误，重请登录" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alertView show];
    [alertView release];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithBool:NO] forKey:NSLocalizedString(@"PS_Sign", nil)];
    [defaults synchronize];
    [self GoLoginViewController];

}

-(void)initSignDict
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *UDID = [defaults objectForKey:NSLocalizedString(@"PS_deviceSerialID", nil)];
    
    if ([UDID length] >0) {
        [mSignInDict setObject:UDID forKey:NSLocalizedString(@"HT_DeviceSerialID", nil)];
    }
    [mSignInDict setObject:NSLocalizedString(@"PORT_SigIN", nil) forKey:NSLocalizedString(@"PORT_ReqType", nil)];
    [mSignInDict setObject:[NSNumber numberWithInt:1] forKey:NSLocalizedString(@"HT_MoblieType", nil)];

}
-(void)loadSignIn
{
    LoginViewController *mProductMianView = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
    CGRect frame = [UIApplication sharedApplication].keyWindow.bounds;
    [mProductMianView.view setFrame:frame];
    
    NSLog(@"[UIApplication sharedApplication].keyWindow.bounds :%@",NSStringFromCGRect(frame));
    [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview:mProductMianView.view];
    mProductMianView.delegate = (id)[UIApplication sharedApplication].keyWindow.rootViewController.self;
    CATransition *animation = [CATransition animation];
    animation.delegate = self;
    animation.duration = 0.3f;
    animation.timingFunction = UIViewAnimationCurveEaseInOut;
    animation.fillMode = kCAFillModeForwards;
    animation.type = kCATransitionPush;
    animation.subtype = kCATransitionFade;
    [[UIApplication sharedApplication].keyWindow.rootViewController.view.layer addAnimation:animation forKey:@"animation"];
    
}
-(void)dealloc
{
    [mSignInDict release];
//    [tabBarController release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mSignInDict = nil;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //    return !(interfaceOrientation == UIDeviceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
    if ( interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown ) {
        return YES;
    }
    else {
        return NO;
    } 
}

@end
