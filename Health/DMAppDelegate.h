//
//  DMAppDelegate.h
//  Health
//
//  Created by hq on 12-6-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTabBar.h"
@class DMViewController;
@class LeveyTabBarController;
@interface DMAppDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate>
{
    UITabBarController* tabBarController;
    
    LeveyTabBarController *leveyTabBarController;

}


-(NSString*)DocumentFilePath;

@property (nonatomic, retain) IBOutlet LeveyTabBarController *leveyTabBarController;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) DMViewController *viewController;


@end
