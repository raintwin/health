//
//  SetingViewController.m
//  Health
//
//  Created by hq on 12-6-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "SetingViewController.h"
#import "CommonSetViewController.h"
#import "NewsAlertViewCotroller.h"
#import "PeopleView.h"
#import "LoginViewController.h"
#import "RegistersViewController.h"
#import "ModPasswordViewViewController.h"




#define kWBAlertViewLogOutTag 100
#define kWBAlertViewLogInTag  101
@implementation SetingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"设置";
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)dealloc
{
//    [weiBoEngine setDelegate:nil];
//    [weiBoEngine release], weiBoEngine = nil;

    [super dealloc];
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    CGRect frame = [[UIScreen mainScreen] bounds];
    frame.origin.y = 20;
    frame.size.height = frame.size.height -20-50-44;

    
    {
        UITableView *mytable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, frame.size.height) style:UITableViewStyleGrouped];
        mytable.delegate = self;
        mytable.dataSource = self;
        [mytable setScrollEnabled:NO];
        [mytable setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:mytable];
        [mytable release];

    }

}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if ([Instance GetUseType] == LOGIN_NULL) {
        return 3;
    }
    if ([Instance GetUseType] != LOGIN_NULL) {
        return 4;
    }
    return 0;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }

    
    NSArray *arrays = [[NSArray alloc] initWithObjects:@"个人信息",@"修改密码",@"版权信息",@"注销登录",nil ];
    cell.textLabel.text =[arrays objectAtIndex:[indexPath section]];;
    [arrays release];

    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    [tableView deselectRowAtIndexPath:indexPath animated:NO];

    if (([indexPath section] == 0 || [indexPath section] ==1) && ![Instance isNotSignIn]) {
        LoginViewController *mProductMianView = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
        CGRect frame = [UIApplication sharedApplication].keyWindow.bounds;
        [mProductMianView.view setFrame:frame];
        
        NSLog(@"[UIApplication sharedApplication].keyWindow.bounds :%@",NSStringFromCGRect(frame));
        [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview:mProductMianView.view];
        mProductMianView.delegate = (id)[UIApplication sharedApplication].keyWindow.rootViewController.self;
        CATransition *animation = [CATransition animation];
        animation.delegate = self;
        animation.duration = 0.3f;
        animation.timingFunction = UIViewAnimationCurveEaseInOut;
        animation.fillMode = kCAFillModeForwards;
        animation.type = kCATransitionPush;
        animation.subtype = kCATransitionFade;
        [[UIApplication sharedApplication].keyWindow.rootViewController.view.layer addAnimation:animation forKey:@"animation"];

        return;
    }
    if ([indexPath section] == 0) {
        PeopleView *ctr = [[PeopleView alloc] initWithNibName:@"PeopleView" bundle:nil];
        ctr.title = @"个人信息";
        [self.navigationController pushViewController:ctr animated:YES];
        [ctr release];
    }
    if ([indexPath section] == 1) {
        ModPasswordViewViewController *detailViewController = [[ModPasswordViewViewController alloc] initWithNibName:@"ModPasswordViewViewController" bundle:nil];
        detailViewController.title = @"修改密码";
        [self.navigationController pushViewController:detailViewController animated:YES];
        [detailViewController release];
    }
    
    if ([indexPath section] == 2) {
        NewsAlertViewCotroller *detailViewController = [[NewsAlertViewCotroller alloc] initWithNibName:@"NewsAlertViewCotroller" bundle:nil];
        detailViewController.title = @"版权信息";
        // ...
        // Pass the selected object to the new view controller.
        [self.navigationController pushViewController:detailViewController animated:YES];
        [detailViewController release];
    }
    if ([indexPath section] == 3) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:[NSNumber numberWithBool:NO] forKey:NSLocalizedString(@"PS_Sign", nil)];
        [defaults synchronize];
        
        LoginViewController *mProductMianView = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
        CGRect frame = [UIApplication sharedApplication].keyWindow.bounds;
        [mProductMianView.view setFrame:frame];
        
        [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview:mProductMianView.view];
        mProductMianView.delegate = (id)[UIApplication sharedApplication].keyWindow.rootViewController.self;
        CATransition *animation = [CATransition animation];
        animation.delegate = self;
        animation.duration = 0.5f;
        animation.timingFunction = UIViewAnimationCurveEaseInOut;
        animation.fillMode = kCAFillModeForwards;
        animation.type = kCATransitionPush;
        animation.subtype = kCATransitionFade;
        [[UIApplication sharedApplication].keyWindow.rootViewController.view.layer addAnimation:animation forKey:@"animation"];    }

    
}



#pragma mark - WBEngineDelegate Methods

-(void)ExitLogin
{
    
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
