//
//  HistroyFoodMenuController.m
//  Health
//
//  Created by hq on 12-6-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "HistroyFoodMenuController.h"
#import "JSON.h"
#import "Recipe.h"
#import "TodayMenusViewController.h"
#define TABLAHIGHT 40
//#define TABLAMOREHEGIHT 52

#define ePageCount 10

#define DETextPull      @"下拉可以刷新..."
#define DETextRelease   @"松开即可刷新..."
#define DETextLoading   @"加载中..."

@implementation HistroyFoodMenuController
@synthesize mTableTemplateView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)navBack
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    if (buttonIndex == 0) {
        //        [self.navigationController popViewControllerAnimated:YES];
    }
}


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    //    [Instance baiduApi:@"looknotify" eventlabel:@"sendnotify"];
    
    
    //BaiduMobStat* statTracker = [BaiduMobStat defaultStat];[statTracker logEvent:@"looknotify" eventLabel:@"looknotify"];
    
    
    
    
    [ViewTool addUIImageViewWithAutoSize:self.view imageName:@"viewblackgroup.jpg" type:@"" x:0 y:0 x1:0 y1:0];
    
    UIImageView *img = [ViewTool addUIImageView:self.view imageName:@"biao_03.png" type:@"" x:0 y:0 x1:103 y1:84];
    img.tag = 1020101;
    [img setCenter:CGPointMake(160, 200)];
    
    NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
    [mDict setObject:[Instance GetUseID] forKey:@"ID"];

    
    // Do any additional setup after loading the view from its nib.
    {
        TableTemplateView *table = [[TableTemplateView alloc]initWithFrame:CGRectMake(0, 0, 320, [Instance getScreenHeight])];
        [table setTableHeightForRow:TABLAHIGHT];
        [table setUrlString:[NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/InSchoolArrangeMeal/TPFetchArrangeMealMenuList"]];
        table.DataDict = mDict;
        table.delegate = self;
        table.isCase = YES;

        self.mTableTemplateView = table;
        [self.view addSubview:mTableTemplateView];
        [mDict release];
        [table release];
        [mTableTemplateView GetDataForHttp];
    }    
    
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];    
    
    
    
}
-(void)ReturnHttpData:(NSString*)ResultContent container:(NSMutableArray*)arrays tag:(int)tag isFinish:(BOOL)isFinish;
{
    NSLog(@"ResultContent--:%@",ResultContent);
    
    NSArray *array = [[[ResultContent JSONValue] valueForKey:@"menuList"] retain];
    
    if ([array count]<ePageCount) {
        mTableTemplateView.isLoadOver = YES;
    }
    else
        mTableTemplateView.isLoadOver = NO;
    
    
    for (int i = 0; i< [array count]; i++) {
        Recipe *nobj = [[Recipe alloc] init];
        nobj.dateForFindCondition = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"dateForFindCondition"]]; 
        nobj.dateForRepresentation = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"dateForRepresentation"]];
        nobj.dayOfWeek = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"dayOfWeek"]];
        [arrays addObject:nobj];
        [nobj release];
    }
    [array release];
    if ([arrays count] == 0) {
        if ((UIImageView *)[self.view viewWithTag:1020101]!=nil) {
            [self.view bringSubviewToFront:((UIImageView *)[self.view viewWithTag:1020101])];
            [mTableTemplateView removeFromSuperview];
        }
    }
    [mTableTemplateView setRequestDatas:arrays];
}
-(void)SetTableData:(UITableViewCell*)cell indexpath:(NSIndexPath*)indexPath datas:(NSMutableArray *)mDatas;
{
    
    
    
    if ((UIImageView *)[cell.contentView viewWithTag:10110011]!=nil) {
        [((UIImageView *)[cell.contentView viewWithTag:10110011]) removeFromSuperview];
    }
    NSInteger row = [indexPath row];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.imageView.image = nil;
    
    Recipe *obj = [mDatas objectAtIndex:row];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    cell.textLabel.text = obj.dateForRepresentation;
    cell.textLabel.textAlignment = UITextAlignmentLeft;
    cell.imageView.image = [UIImage imageNamed:@"历史食谱列表小图标.png"];
}
-(void)TableDidSelect:(NSIndexPath*)indexPath Datas:(NSMutableArray*)mDatas;
{
        Recipe *obj = [mDatas objectAtIndex:[indexPath row]];
        TodayMenusViewController *nifCtr = [[TodayMenusViewController alloc] initWithNibName:@"TodayMenusViewController" bundle:nil];
        nifCtr.title = @"食谱";
        nifCtr.isToday = NO;
        nifCtr.mRecipeClass = obj;
        NSLog(@"dateForFindCondition :%@",obj.dateForFindCondition);
        [self.navigationController pushViewController:nifCtr animated:YES];
        [nifCtr release];
}
-(void)TableDownLoadCellImage
{}
-(void)dealloc
{
    [mTableTemplateView release]; 
    
    [super dealloc];
}
- (void)viewDidUnload
{
    self.mTableTemplateView = nil;

    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

