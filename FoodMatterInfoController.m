
//
//  FoodMatterInfoController.m
//  Health
//
//  Created by hq on 12-6-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "FoodMatterInfoController.h"
#import "PieChartView.h"

#define mTableHeight 40
#define mTableTextSize 12
#define mTablePieHeight 400

@implementation FoodMatterInfoController
@synthesize mAnalysisResultDict;
@synthesize mViewTag;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)navBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];

    
    [self.tableView setScrollEnabled:YES];
    
}
-(void)dealloc
{
    [mAnalysisResultDict release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mAnalysisResultDict = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#define FoodViewTag     1001
#define HitFoodViewTag  1002
#define HitNutViewTag   1003
#define ProteinViewTag  1004
#define FatViewTag      1005
#define RatioViewTag    1006
#define NutViewTag      1007

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    return  mTableHeight;

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //Begin 食物类别计量页面所需要的指标          1001
    return 81-6;
//    return 96;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    for (UIView *sub in [cell.contentView subviews]) {
        [sub removeFromSuperview];
    }
    
    {
        UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 46)];
        [img setImage:[UIImage imageNamed:[indexPath row]%2 == 0 ? @"xia.jpg":@"shang.jpg"]];  
        img.tag = 121211;
        [cell.contentView addSubview:img];
        [img release];
        
    }
    
    {
        UILabel *label = [Instance addLable:CGRectMake(5, 0, 160, mTableHeight) tag:100011 size:mTableTextSize string:@""];
        [cell.contentView addSubview:label];
        [label release];
    }
    {
        UILabel *label = [Instance addLable:CGRectMake(160, 0, 300, mTableHeight) tag:100022 size:mTableTextSize string:@""];
        [cell.contentView addSubview:label];
        [label release];
    }
    //Begin 营养素分析结果页面所需要的指标

    int mIndexRow = [indexPath row];

    ///-------------  1
    if (mIndexRow == 0) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).font = [UIFont boldSystemFontOfSize:mTableTextSize+3];
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = @"能量"; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131

    }
    if (mIndexRow == 1) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"能量全日标准%0.2f千卡",[[mAnalysisResultDict objectForKey:@"energyFullDayStandard"] floatValue]]; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"能量在园标准%0.2f千卡",[[mAnalysisResultDict objectForKey:@"energyInSchoolStandard"] floatValue]];//131

    }
    if (mIndexRow == 2) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"能量在园实给:%0.2f千卡",[[mAnalysisResultDict objectForKey:@"energyInSchoolSupply"] floatValue]];//132
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"能量在园供比 :%0.2f%%",[[mAnalysisResultDict objectForKey:@"energyInSchoolPercentage"] floatValue]];//133
    }
    if (mIndexRow == 3) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"能量评价:%@",[mAnalysisResultDict objectForKey:@"energyEvaluationValue"]];//134
        ((UILabel *)[cell.contentView viewWithTag:100011]).numberOfLines = 2;
        [((UILabel *)[cell.contentView viewWithTag:100011]) setFrame:CGRectMake(5, 0, 320, mTableHeight)];

        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131

    }
    if (mIndexRow == 4) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).font = [UIFont boldSystemFontOfSize:mTableTextSize+3];
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = @"碳水物"; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131

    }
//    if (mIndexRow == 5) {
//        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"碳水物供热全日标准:%0.2f克",[[mAnalysisResultDict objectForKey:@"carbohydrateEnergyFullDayStandard"] floatValue]];//135
//        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"碳水物供热在园标准:%0.2f克",[[mAnalysisResultDict objectForKey:@"carbohydrateEnergyInSchoolStandard"] floatValue]];//136
//
//    }
    mIndexRow = mIndexRow +1;
    if (mIndexRow == 6) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"碳水物供热在园实给:%0.2f克",[[mAnalysisResultDict objectForKey:@"carbohydrateEnergyInSchoolSupply"] floatValue]];//137
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"碳水物供热在园供比 :%0.2f%%",[[mAnalysisResultDict objectForKey:@"carbohydrateEnergyInSchoolPercentage"] floatValue]];//138

    }
    if (mIndexRow == 7) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"碳水物供热评价:%@",[mAnalysisResultDict objectForKey:@"carbohydrateEnergyEvaluationValue"]];//139
        ((UILabel *)[cell.contentView viewWithTag:100011]).numberOfLines = 2;
        ((UILabel *)[cell.contentView viewWithTag:100011]).frame = CGRectMake(5, 0, 320, mTableHeight);
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131

    }
    if (mIndexRow == 8) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = @"脂肪"; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
        ((UILabel *)[cell.contentView viewWithTag:100011]).font = [UIFont boldSystemFontOfSize:mTableTextSize+3];
    }
    mIndexRow = mIndexRow +1;

//    if (mIndexRow == 9) {
//        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"脂肪供热全日标准:%@克",[mAnalysisResultDict objectForKey:@"fatEnergyFullDayStandard"]]; //140
//        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"脂肪供热在园标准:%@克",[mAnalysisResultDict objectForKey:@"fatEnergyInSchoolStandard"]];//141
//
//    }
    ///------------- 2
    if (mIndexRow == 10) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"脂肪供热在园实给:%0.2f克",[[mAnalysisResultDict objectForKey:@"fatEnergyInSchoolSupply"] floatValue]];//142
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"脂肪供热在园供比:%0.2f%%",[[mAnalysisResultDict objectForKey:@"fatEnergyInSchoolPercentage"] floatValue]];//143

    }
    if (mIndexRow == 11) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"脂肪供热评价:%@",[mAnalysisResultDict objectForKey:@"fatEnergyEvaluationValue"]];//144
        ((UILabel *)[cell.contentView viewWithTag:100011]).numberOfLines = 2;
        ((UILabel *)[cell.contentView viewWithTag:100011]).frame = CGRectMake(5, 0, 320, mTableHeight);
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131

    }
    if (mIndexRow == 12) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = @"蛋白质"; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
        ((UILabel *)[cell.contentView viewWithTag:100011]).font = [UIFont boldSystemFontOfSize:mTableTextSize+3];

    }
    if (mIndexRow == 13) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"蛋白质供热全日标准:%@",[mAnalysisResultDict objectForKey:@"porteinEnergyFullDayStandard"]];//145
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"蛋白质供热在园标准:%@",[mAnalysisResultDict objectForKey:@"porteinEnergyInSchoolStandard"]];//146
    }
    if (mIndexRow == 14) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"蛋白质供热在园实给:%0.2f",[[mAnalysisResultDict objectForKey:@"porteinEnergyInSchoolSupply"] floatValue]];//147
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"蛋白质供热在园供比:%0.2f%%",[[mAnalysisResultDict objectForKey:@"porteinEnergyInSchoolPercentage"] floatValue]];//148

    }
    if (mIndexRow == 15) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"蛋白质供热评价:%@",[mAnalysisResultDict objectForKey:@"porteinEnergyEvaluationValue"]];//149
        ((UILabel *)[cell.contentView viewWithTag:100011]).numberOfLines = 2;
        ((UILabel *)[cell.contentView viewWithTag:100011]).frame = CGRectMake(5, 0, 320, mTableHeight);
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131

    }
    if (mIndexRow == 16) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = @"蛋白质总量"; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
        ((UILabel *)[cell.contentView viewWithTag:100011]).font = [UIFont boldSystemFontOfSize:mTableTextSize+3];

    }
    if (mIndexRow == 17) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"蛋白质总量全日标准:%@克",[mAnalysisResultDict objectForKey:@"porteinFullDayStandard"]]; //150
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"蛋白质总量在园标准:%@克",[mAnalysisResultDict objectForKey:@"porteinInSchoolStandard"]];//151

    }
    if (mIndexRow == 18) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"蛋白质总量在园实给:%0.2f克",[[mAnalysisResultDict objectForKey:@"porteinInSchoolSupply"] floatValue]];//152
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"蛋白质总量在园供比:%0.2f%%",[[mAnalysisResultDict objectForKey:@"porteinInSchoolPercentage"] floatValue]];//153

    }
    if (mIndexRow == 19) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"蛋白质总量评价:%@",[mAnalysisResultDict objectForKey:@"porteinEvaluationValue"]];//154
        ((UILabel *)[cell.contentView viewWithTag:100011]).numberOfLines = 2;
        ((UILabel *)[cell.contentView viewWithTag:100011]).frame = CGRectMake(5, 0, 320, mTableHeight);
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131

    }
    
    ///------------- 3
    if (mIndexRow == 20) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = @"动物蛋白"; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
        ((UILabel *)[cell.contentView viewWithTag:100011]).font = [UIFont boldSystemFontOfSize:mTableTextSize+3];

    }
    mIndexRow = mIndexRow +1;
//    if (mIndexRow == 21) {
//        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"动物蛋白全日标准:%@克",[mAnalysisResultDict objectForKey:@"animalPorteinFullDayStandard"]];//155
//        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"动物蛋白在园标准:%@克",[mAnalysisResultDict objectForKey:@"animalPorteinInSchoolStandard"]];//156
//
//    }
    if (mIndexRow == 22) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"动物蛋白在园实给:%0.2f克",[[mAnalysisResultDict objectForKey:@"animalPorteinInSchoolSupply"] floatValue]];//157
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"动物蛋白在园供比:%0.2f%%",[[mAnalysisResultDict objectForKey:@"animalPorteinInSchoolPercentage"] floatValue]];//158

    }
//    if (mIndexRow == 23) {
//        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"动物蛋白评价:%@",[mAnalysisResultDict objectForKey:@"animalPorteinEvaluationValue"]];//159
//        ((UILabel *)[cell.contentView viewWithTag:100011]).numberOfLines = 2;
//        ((UILabel *)[cell.contentView viewWithTag:100011]).frame = CGRectMake(5, 0, 320, mTableHeight);
//        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
//
//    }
    mIndexRow = mIndexRow +1;

    if (mIndexRow == 24) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = @"动豆蛋白"; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
        ((UILabel *)[cell.contentView viewWithTag:100011]).font = [UIFont boldSystemFontOfSize:mTableTextSize+3];

    }
//    if (mIndexRow == 25) {
//        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"动豆蛋白全日标准:%@克",[mAnalysisResultDict objectForKey:@"animalAndSoyBeanPorteinFullDayStandard"]]; //160
//        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"动豆蛋白在园标准:%@克",[mAnalysisResultDict objectForKey:@"animalAndSoyBeanPorteinInSchoolStandard"]];//161
//
//    }
    mIndexRow = mIndexRow +1;

    if (mIndexRow == 26) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"动豆蛋白在园实给:%0.2f",[[mAnalysisResultDict objectForKey:@"animalAndSoyBeanPorteinInSchoolSupply"] floatValue]];//162
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"动豆蛋白在园供比:%0.2f%%",[[mAnalysisResultDict objectForKey:@"animalAndSoyBeanPorteinInSchoolPercentage"] floatValue]];//163

    }
    mIndexRow = mIndexRow +1;

//    if (mIndexRow == 27) {
//        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"动豆蛋白评价:%@",[mAnalysisResultDict objectForKey:@"animalAndSoyBeanPorteinEvaluationValue"]];//164
//        ((UILabel *)[cell.contentView viewWithTag:100011]).numberOfLines = 2;
//        ((UILabel *)[cell.contentView viewWithTag:100011]).frame = CGRectMake(5, 0, 320, mTableHeight);
//        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
//
//    }
    if (mIndexRow == 28) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = @"钙"; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
        ((UILabel *)[cell.contentView viewWithTag:100011]).font = [UIFont boldSystemFontOfSize:mTableTextSize+3];

    }
    if (mIndexRow == 29) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"钙全日标准:%@毫克",[mAnalysisResultDict objectForKey:@"calciumFullDayStandard"]];//165
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"钙在园标准:%@毫克",[mAnalysisResultDict objectForKey:@"calciumInSchoolStandard"]];//166

    } 
    ///-------------  4
    if (mIndexRow == 30) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"钙在园实给:%0.2f毫克",[[mAnalysisResultDict objectForKey:@"calciumInSchoolSupply"] floatValue]];//167
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"钙在园供比:%0.2f%%",[[mAnalysisResultDict objectForKey:@"calciumInSchoolPercentage"] floatValue]];//168

    }
    if (mIndexRow == 31) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"钙评价:%@",[mAnalysisResultDict objectForKey:@"calciumEvaluationValue"]];//169
        ((UILabel *)[cell.contentView viewWithTag:100011]).numberOfLines = 2;
        ((UILabel *)[cell.contentView viewWithTag:100011]).frame = CGRectMake(5, 0, 320, mTableHeight);
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131

    }
    if (mIndexRow == 32) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = @"维生素A"; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
        ((UILabel *)[cell.contentView viewWithTag:100011]).font = [UIFont boldSystemFontOfSize:mTableTextSize+3];

    }
    if (mIndexRow == 33) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"维生素A全日标准:%@微克",[mAnalysisResultDict objectForKey:@"vitAFullDayStandard"]]; //170
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"维生素A在园标准:%@微克",[mAnalysisResultDict objectForKey:@"vitAInSchoolStandard"]];//171

    }
    if (mIndexRow == 34) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"维生素A在园实给:%0.2f微克",[[mAnalysisResultDict objectForKey:@"vitAInSchoolSupply"] floatValue]];//172
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"维生素A在园供比:%0.2f%%",[[mAnalysisResultDict objectForKey:@"vitAInSchoolPercentage"] floatValue]];//173

    }
    if (mIndexRow == 35) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"维生素A评价:%@",[mAnalysisResultDict objectForKey:@"vitAEvaluationValue"]];//174
        ((UILabel *)[cell.contentView viewWithTag:100011]).numberOfLines = 2;
        ((UILabel *)[cell.contentView viewWithTag:100011]).frame = CGRectMake(5, 0, 320, mTableHeight);
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131

    }
    if (mIndexRow == 36) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = @"硫胺素(维生素B1)"; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
        ((UILabel *)[cell.contentView viewWithTag:100011]).font = [UIFont boldSystemFontOfSize:mTableTextSize+3];

    }
    if (mIndexRow == 37) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"硫胺素全日标准:%@毫克",[mAnalysisResultDict objectForKey:@"vitB1FullDayStandard"]];//175
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"硫胺素在园标准:%@毫克",[mAnalysisResultDict objectForKey:@"vitB1InSchoolStandard"]];//176

    }
    if (mIndexRow == 38) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"硫胺素在园实给:%0.2f毫克",[[mAnalysisResultDict objectForKey:@"vitB1InSchoolSupply"] floatValue]];//177
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"硫胺素在园供比:%0.2f%%",[[mAnalysisResultDict objectForKey:@"vitB1InSchoolPercentage"] floatValue]];//178

    }
    if (mIndexRow == 39) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"硫胺素评价:%@",[mAnalysisResultDict objectForKey:@"vitB1EvaluationValue"]];//179
        ((UILabel *)[cell.contentView viewWithTag:100011]).numberOfLines = 2;
        ((UILabel *)[cell.contentView viewWithTag:100011]).frame = CGRectMake(5, 0, 320, mTableHeight);
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131

    }
    
    ///-------------  5
    if (mIndexRow == 40) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = @"核黄素(维生素B2)"; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
        ((UILabel *)[cell.contentView viewWithTag:100011]).font = [UIFont boldSystemFontOfSize:mTableTextSize+3];

    }
    if (mIndexRow == 41) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"核黄素全日标准:%@毫克",[mAnalysisResultDict objectForKey:@"vitB2FullDayStandard"]]; //180
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"核黄素)在园标准:%@毫克",[mAnalysisResultDict objectForKey:@"vitB2InSchoolStandard"]];//181

    }
    if (mIndexRow == 42) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"核黄素)在园实给:%0.2f毫克",[[mAnalysisResultDict objectForKey:@"vitB2InSchoolSupply"] floatValue]];//182
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"核黄素在园供比:%0.2f%%",[[mAnalysisResultDict objectForKey:@"vitB2InSchoolPercentage"] floatValue]];//183

    }
    if (mIndexRow == 43) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"核黄素评价:%@",[mAnalysisResultDict objectForKey:@"vitB2EvaluationValue"]];//184
        ((UILabel *)[cell.contentView viewWithTag:100011]).numberOfLines = 2;
        ((UILabel *)[cell.contentView viewWithTag:100011]).frame = CGRectMake(5, 0, 320, mTableHeight);
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131

    }
    if (mIndexRow == 44) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = @"维生素C"; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
        ((UILabel *)[cell.contentView viewWithTag:100011]).font = [UIFont boldSystemFontOfSize:mTableTextSize+3];

    }
    if (mIndexRow == 45) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"维生素C全日标准:%@毫克",[mAnalysisResultDict objectForKey:@"vitCFullDayStandard"]];//185
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"维生素C在园标准:%@毫克",[mAnalysisResultDict objectForKey:@"vitCInSchoolStandard"]];//186

    }
    if (mIndexRow == 46) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"维生素C在园实给:%0.2f毫克",[[mAnalysisResultDict objectForKey:@"vitCInSchoolSupply"] floatValue]];//187
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"维生素C在园供比:%0.2f%%",[[mAnalysisResultDict objectForKey:@"vitCInSchoolPercentage"] floatValue]];//188

    }
    if (mIndexRow == 47) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"维生素C评价:%@",[mAnalysisResultDict objectForKey:@"vitCEvaluationValue"]];//189
        ((UILabel *)[cell.contentView viewWithTag:100011]).numberOfLines = 2;
        ((UILabel *)[cell.contentView viewWithTag:100011]).frame = CGRectMake(5, 0, 320, mTableHeight);
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131

    }
    if (mIndexRow == 48) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = @"维生素E"; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
        ((UILabel *)[cell.contentView viewWithTag:100011]).font = [UIFont boldSystemFontOfSize:mTableTextSize+3];

    }
    if (mIndexRow == 49) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"维生素E全日标准:%@毫克",[mAnalysisResultDict objectForKey:@"vitEFullDayStandard"]]; //190
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"维生素E在园标准:%@毫克",[mAnalysisResultDict objectForKey:@"vitEInSchoolStandard"]];//191

    }
    ///-------------  6
    if (mIndexRow == 50) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"维生素E在园实给:%0.2f毫克",[[mAnalysisResultDict objectForKey:@"vitEInSchoolSupply"] floatValue]];//192
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"维生素E在园供比:%0.2f%%",[[mAnalysisResultDict objectForKey:@"vitEInSchoolPercentage"] floatValue]];//193

    }
    if (mIndexRow == 51) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"维生素E评价:%@",[mAnalysisResultDict objectForKey:@"vitEEvaluationValue"]];//194
        ((UILabel *)[cell.contentView viewWithTag:100011]).numberOfLines = 2;
        ((UILabel *)[cell.contentView viewWithTag:100011]).frame = CGRectMake(5, 0, 320, mTableHeight);
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131

    }
    if (mIndexRow == 52) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = @"尼克酸(维生素PP,维生素B5)"; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
        ((UILabel *)[cell.contentView viewWithTag:100011]).font = [UIFont boldSystemFontOfSize:mTableTextSize+3];
        ((UILabel *)[cell.contentView viewWithTag:100011]).frame = CGRectMake(5, 0, 320, mTableHeight);

    }
    if (mIndexRow == 53) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"尼克酸全日标准:%@毫克",[mAnalysisResultDict objectForKey:@"vitPPFullDayStandard"]];//195
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"尼克酸(在园标准:%@毫克",[mAnalysisResultDict objectForKey:@"vitPPInSchoolStandard"]];//196

    }
    if (mIndexRow == 54) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"尼克酸在园实给:%0.2f毫克",[[mAnalysisResultDict objectForKey:@"vitPPInSchoolSupply"] floatValue]];//197
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"尼克酸在园供比:%0.2f%%",[[mAnalysisResultDict objectForKey:@"vitPPInSchoolPercentage"] floatValue]];//198

    }
    if (mIndexRow == 55) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"尼克酸评价:%@",[mAnalysisResultDict objectForKey:@"vitPPEvaluationValue"]];//199
        ((UILabel *)[cell.contentView viewWithTag:100011]).numberOfLines = 2;
        ((UILabel *)[cell.contentView viewWithTag:100011]).frame = CGRectMake(5, 0, 320, mTableHeight);
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131

    }
    if (mIndexRow == 56) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = @"钾(K)"; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
        ((UILabel *)[cell.contentView viewWithTag:100011]).font = [UIFont boldSystemFontOfSize:mTableTextSize+3];

    }
    if (mIndexRow == 57) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"钾全日标准:%@毫克",[mAnalysisResultDict objectForKey:@"potassiumFullDayStandard"]]; //200
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"钾在园标准:%@毫克",[mAnalysisResultDict objectForKey:@"potassiumInSchoolStandard"]];//201

    }
    if (mIndexRow == 58) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"钾在园实给:%0.2f毫克",[[mAnalysisResultDict objectForKey:@"potassiumInSchoolSupply"] floatValue]];//202
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"钾在园供比:%0.2f%%",[[mAnalysisResultDict objectForKey:@"potassiumInSchoolPercentage"] floatValue]];//203

    }
    if (mIndexRow == 59) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"钾评价:%@",[mAnalysisResultDict objectForKey:@"potassiumEvaluationValue"]];//204
        ((UILabel *)[cell.contentView viewWithTag:100011]).numberOfLines = 2;
        ((UILabel *)[cell.contentView viewWithTag:100011]).frame = CGRectMake(5, 0, 320, mTableHeight);
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131

    }
    ///-------------  7
    if (mIndexRow == 60) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = @"镁(Mg)"; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
        ((UILabel *)[cell.contentView viewWithTag:100011]).font = [UIFont boldSystemFontOfSize:mTableTextSize+3];

    }
    if (mIndexRow == 61) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"镁全日标准:%@毫克",[mAnalysisResultDict objectForKey:@"magnesiumFullDayStandard"]];//205
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"镁在园标准:%@毫克",[mAnalysisResultDict objectForKey:@"magnesiumInSchoolStandard"]];//206

    }
    if (mIndexRow == 62) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"镁在园实给:%0.2f毫克",[[mAnalysisResultDict objectForKey:@"magnesiumInSchoolSupply"] floatValue]];//207
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"镁在园供比:%0.2f%%",[[mAnalysisResultDict objectForKey:@"magnesiumInSchoolPercentage"] floatValue]];//208

    }
    if (mIndexRow == 63) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"镁评价:%@",[mAnalysisResultDict objectForKey:@"magnesiumEvaluationValue"]];//209
        ((UILabel *)[cell.contentView viewWithTag:100011]).numberOfLines = 2;
        ((UILabel *)[cell.contentView viewWithTag:100011]).frame = CGRectMake(5, 0, 320, mTableHeight);
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131

    }
    if (mIndexRow == 64) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = @"铁(Fe)"; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
        ((UILabel *)[cell.contentView viewWithTag:100011]).font = [UIFont boldSystemFontOfSize:mTableTextSize+3];

    }
    if (mIndexRow == 65) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"铁全日标准:%@毫克",[mAnalysisResultDict objectForKey:@"ironFullDayStandard"]]; //210
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"铁在园标准:%@毫克",[mAnalysisResultDict objectForKey:@"ironInSchoolStandard"]];//211

    }
    if (mIndexRow == 66) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"铁在园实给:%0.2f毫克",[[mAnalysisResultDict objectForKey:@"ironInSchoolSupply"] floatValue]];//212
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"铁在园供比:%0.2f%%",[[mAnalysisResultDict objectForKey:@"ironInSchoolPercentage"] floatValue]];//213

    }
    if (mIndexRow == 67) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"铁评价:%@",[mAnalysisResultDict objectForKey:@"ironEvaluationValue"]];//214
        ((UILabel *)[cell.contentView viewWithTag:100011]).numberOfLines = 2;
        ((UILabel *)[cell.contentView viewWithTag:100011]).frame = CGRectMake(5, 0, 320, mTableHeight);
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131

    }
    if (mIndexRow == 68) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = @"锌(Zn)"; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
        ((UILabel *)[cell.contentView viewWithTag:100011]).font = [UIFont boldSystemFontOfSize:mTableTextSize+3];

    }
    if (mIndexRow == 69) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"锌全日标准:%@毫克",[mAnalysisResultDict objectForKey:@"zincFullDayStandard"]];//215
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"锌在园标准:%@毫克",[mAnalysisResultDict objectForKey:@"zincInSchoolStandard"]];//216

    }
    ///------------- 8
    if (mIndexRow == 70) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"锌在园实给:%0.2f毫克",[[mAnalysisResultDict objectForKey:@"zincInSchoolSupply"] floatValue]];//217
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"锌在园供比:%0.2f%%",[[mAnalysisResultDict objectForKey:@"zincInSchoolPercentage"] floatValue]];//218

    }
    if (mIndexRow == 71) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"锌评价:%@",[mAnalysisResultDict objectForKey:@"zincEvaluationValue"]];//219
        ((UILabel *)[cell.contentView viewWithTag:100011]).numberOfLines = 2;
        ((UILabel *)[cell.contentView viewWithTag:100011]).frame = CGRectMake(5, 0, 320, mTableHeight);
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131

    }
    if (mIndexRow == 72) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = @"磷(P)"; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
        ((UILabel *)[cell.contentView viewWithTag:100011]).font = [UIFont boldSystemFontOfSize:mTableTextSize+3];

    }
    if (mIndexRow == 73) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"磷全日标准:%@毫克",[mAnalysisResultDict objectForKey:@"phosphorusFullDayStandard"]]; //220
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"磷在园标准:%@毫克",[mAnalysisResultDict objectForKey:@"phosphorusInSchoolStandard"]];//221

    }
    if (mIndexRow == 74) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"磷在园实给:%0.2f毫克",[[mAnalysisResultDict objectForKey:@"phosphorusInSchoolSupply"] floatValue]];//222
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"磷在园供比:%0.2f%%",[[mAnalysisResultDict objectForKey:@"phosphorusInSchoolPercentage"] floatValue]];//223

    }
    if (mIndexRow == 75) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"磷评价:%@",[mAnalysisResultDict objectForKey:@"phosphorusEvaluationValue"]];//224
        ((UILabel *)[cell.contentView viewWithTag:100011]).numberOfLines = 2;
        ((UILabel *)[cell.contentView viewWithTag:100011]).frame = CGRectMake(5, 0, 320, mTableHeight);
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131

    }
    if (mIndexRow == 76) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = @"硒(Se)"; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
        ((UILabel *)[cell.contentView viewWithTag:100011]).font = [UIFont boldSystemFontOfSize:mTableTextSize+3];

    }
    if (mIndexRow == 77) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"硒(全日标准:%@微克",[mAnalysisResultDict objectForKey:@"seleniumFullDayStandard"]];//225
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"硒在园标准:%@微克",[mAnalysisResultDict objectForKey:@"seleniumInSchoolStandard"]];//226

    }
    if (mIndexRow == 78) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"硒在园实给:%0.2f微克",[[mAnalysisResultDict objectForKey:@"seleniumInSchoolSupply"] floatValue]];//227
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"硒在园供比:%0.2f%%",[[mAnalysisResultDict objectForKey:@"seleniumInSchoolPercentage"] floatValue]];//228

    }
    if (mIndexRow == 79) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"硒评价:%@",[mAnalysisResultDict objectForKey:@"seleniumEvaluationValue"]];//229
        ((UILabel *)[cell.contentView viewWithTag:100011]).numberOfLines = 2;
        ((UILabel *)[cell.contentView viewWithTag:100011]).frame = CGRectMake(5, 0, 320, mTableHeight);
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131

    }
    ///-------------  9
    if (mIndexRow >= 80) {
        return cell;
    }
    if (mIndexRow == 80) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = @"钙磷"; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
        ((UILabel *)[cell.contentView viewWithTag:100011]).font = [UIFont boldSystemFontOfSize:mTableTextSize+3];

    }
    if (mIndexRow == 81) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"钙磷全日标准:%@毫克",[mAnalysisResultDict objectForKey:@"calciumPhosphorusRatioFullDayStandard"]]; //230
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"钙磷在园标准:%@毫克",[mAnalysisResultDict objectForKey:@"calciumPhosphorusRatioInSchoolStandard"]];//231

    }
    if (mIndexRow == 82) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"钙磷在园实给:%0.2f毫克",[[mAnalysisResultDict objectForKey:@"calciumPhosphorusRatioInSchoolSupply"] floatValue]];//232
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"钙磷在园供比:%0.2f%%",[[mAnalysisResultDict objectForKey:@"calciumPhosphorusRatioInSchoolPercentage"] floatValue]];//233

    }
    if (mIndexRow == 83) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"钙磷评价:%@",[mAnalysisResultDict objectForKey:@"calciumPhosphorusRatioEvaluationValue"]];//234
        ((UILabel *)[cell.contentView viewWithTag:100011]).numberOfLines = 2;
        ((UILabel *)[cell.contentView viewWithTag:100011]).frame = CGRectMake(5, 0, 320, mTableHeight);
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131

    }
    if (mIndexRow == 84) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = @"胡萝卜素"; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
        ((UILabel *)[cell.contentView viewWithTag:100011]).font = [UIFont boldSystemFontOfSize:mTableTextSize+3];

    }
    if (mIndexRow == 85) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"胡萝卜素全日标准:%@微克",[mAnalysisResultDict objectForKey:@"caroteneFullDayStandard"]];//235
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"胡萝卜素在园标准:%@微克",[mAnalysisResultDict objectForKey:@"caroteneInSchoolStandard"]];//236

    }
    if (mIndexRow == 86) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"胡萝卜素在园实给:%0.2f微克",[[mAnalysisResultDict objectForKey:@"caroteneInSchoolSupply"] floatValue]];//237
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"胡萝卜素在园供比:%0.2f%%",[[mAnalysisResultDict objectForKey:@"caroteneInSchoolPercentage"] floatValue]];//238

    }
    if (mIndexRow == 87) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"胡萝卜素评价:%@",[mAnalysisResultDict objectForKey:@"caroteneEvaluationValue"]];//239
        ((UILabel *)[cell.contentView viewWithTag:100011]).numberOfLines = 2;
        ((UILabel *)[cell.contentView viewWithTag:100011]).frame = CGRectMake(5, 0, 320, mTableHeight);
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131

    }
    if (mIndexRow == 88) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = @"膳食纤维"; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
        ((UILabel *)[cell.contentView viewWithTag:100011]).font = [UIFont boldSystemFontOfSize:mTableTextSize+3];

    }
    if (mIndexRow == 89) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"膳食纤维全日标准:%@毫克",[mAnalysisResultDict objectForKey:@"fiberFullDayStandard"]]; //240
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"膳食纤维在园标准:%@毫克",[mAnalysisResultDict objectForKey:@"fiberInSchoolStandard"]];//241
    }
    ///-------------  10
    if (mIndexRow == 90) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"膳食纤维在园实给:%0.2f毫克",[[mAnalysisResultDict objectForKey:@"fiberInSchoolSupply"] floatValue]];//242
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"膳食纤维在园供比:%0.2f%%",[[mAnalysisResultDict objectForKey:@"fiberInSchoolPercentage"] floatValue]];//243
    }
    if (mIndexRow == 91) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"膳食纤维评价:%@",[mAnalysisResultDict objectForKey:@"fiberEvaluationValue"]];//244
        ((UILabel *)[cell.contentView viewWithTag:100011]).numberOfLines = 2;
        ((UILabel *)[cell.contentView viewWithTag:100011]).frame = CGRectMake(5, 0, 320, mTableHeight);
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
    }
    if (mIndexRow == 92) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = @"胆固醇"; //130
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131
        ((UILabel *)[cell.contentView viewWithTag:100011]).font = [UIFont boldSystemFontOfSize:mTableTextSize+3];

    }
    if (mIndexRow == 93) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"胆固醇全日标准:%@毫克",[mAnalysisResultDict objectForKey:@"cholesterolFullDayStandard"]];//245
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"胆固醇在园标准:%@毫克",[mAnalysisResultDict objectForKey:@"cholesterolInSchoolStandard"]];//246

    }
    if (mIndexRow == 94) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"胆固醇在园实给:%0.2f毫克",[[mAnalysisResultDict objectForKey:@"cholesterolInSchoolSupply"] floatValue]];//247
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"胆固醇在园供比:%0.2f%%",[[mAnalysisResultDict objectForKey:@"cholesterolInSchoolPercentage"] floatValue]];//248

    }
    if (mIndexRow == 95) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"胆固醇评价:%@",[mAnalysisResultDict objectForKey:@"cholesterolEvaluationValue"]];//249
        ((UILabel *)[cell.contentView viewWithTag:100011]).numberOfLines = 2;
        ((UILabel *)[cell.contentView viewWithTag:100011]).frame = CGRectMake(5, 0, 320, mTableHeight);
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//131

    }

    return cell;
}

@end
