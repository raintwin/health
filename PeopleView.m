//
//  PeopleView.m
//  Health
//
//  Created by hq on 12-8-31.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "PeopleView.h"






#define TextHeigth 26
#define TextWidth 150


@interface  PeopleView()
@property(nonatomic,retain)UITableView *mTableView;
@property(nonatomic,retain)HttpFormatRequest *httpFormatRequest;
@property(nonatomic,retain)NSMutableDictionary *datailDict;
@property(nonatomic,retain)UIImage *iconImage;
@end
@implementation PeopleView


@synthesize datailDict;
@synthesize httpFormatRequest;
@synthesize mTableView;
@synthesize iconImage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)navBack
{

    [self.navigationController popViewControllerAnimated:YES];
}

-(void)dealloc
{
    [self.datailDict release];
    [self.mTableView release];
    [self.httpFormatRequest release];
    
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
    self.datailDict  = nil;
    self.mTableView = nil;
    self.httpFormatRequest = nil;
    
}



#pragma mark 获取本地图片
-(NSString *)GetIconFile:(NSMutableDictionary *)mDict
{
    NSString *file = [Instance GetDocumentByString:[NSString stringWithFormat:@"%@.jpg",[mDict objectForKey:@"ID"]]];
    return file;
}




#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:HEXCOLOR(0xe7e7e7)];
    
    self.iconImage = [[UIImage alloc]init];
    self.iconImage = [UIImage imageWithContentsOfFile:[Instance GetDocumentByString:[NSString stringWithFormat:@"%@.png",[Instance GetUseID]]]];
    self.mTableView = [[UITableView alloc]initWithFrame:self.view.frame style:UITableViewStyleGrouped];
    self.mTableView.delegate = self;
    self.mTableView.dataSource = self;
    self.mTableView.scrollEnabled = NO;

    [self.view addSubview:self.mTableView];
    

    self.datailDict = [[NSMutableDictionary alloc] init];
    [self.datailDict setObject:[Instance GetUseID] forKey:@"ID"];
    
    self.httpFormatRequest = [[HttpFormatRequest alloc]init];
    self.httpFormatRequest.Delegate = self;
    [self.view addSubview:self.httpFormatRequest];
    

    
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];
    
}

#pragma mark 记录图片
-(void)WriteImage:(UIImage *)image FileName:(NSString*)FileStr
{
    if (image == nil) {
        return;
    }
    float ImageWidth = image.size.width;
    float ImageHeight = image.size.height;
    if (image.size.width >100 || image.size.height >100) {
        ImageWidth = image.size.width  *0.3;
        ImageHeight = image.size.height *0.3;
    }
    
    CGSize itemSize = CGSizeMake(ImageWidth, ImageHeight);
    UIGraphicsBeginImageContext(itemSize);
    CGRect imageRect1 = CGRectMake(0.0, 0.0, (int)ImageWidth, (int)ImageHeight);
    [image drawInRect:imageRect1];
    image = UIGraphicsGetImageFromCurrentImageContext();
    [image retain];
    UIGraphicsEndImageContext();
    NSData *imageData = UIImagePNGRepresentation(image);
    [imageData writeToFile:[Instance GetDocumentByString:FileStr] atomically:YES];
    NSLog(@"file ok-- %@",[Instance GetDocumentByString:FileStr]);
    [image release];
    
}

#pragma mark 保存老师个人信息
-(void)saveIcon
{
    [self.datailDict setObject:[NSNumber numberWithInt:[[Instance GetUseID] intValue]] forKey:@"userId"];
    [self.datailDict setObject:NSLocalizedString(@"PORT_GetIcon", nil) forKey:NSLocalizedString(@"PORT_ReqType", nil)];

    [self.httpFormatRequest CustomFormDataRequestSendFile:[self.datailDict JSONRepresentation] FilePath:[Instance GetDocumentByString:[NSString stringWithFormat:@"%@.png",[Instance GetUseID]]] tag:FILESEND url:WEBSERVERURL forkey:@"photoUrl"];
    self.iconImage = [UIImage imageWithContentsOfFile:[Instance GetDocumentByString:[NSString stringWithFormat:@"%@.png",[Instance GetUseID]]]];
}



#pragma mark tableView
-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath section] == 0) {
        return 50;
    }
    return 40;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.

    return 2;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0) {
        return 1;
    }
    if (section == 1) {
        return 5;
    }
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        UILabel *mLabel = [Instance addLableRect:CGRectMake(10, 0, 300, cell.frame.size.height) text:@"" textsize:15];
        mLabel.tag = 100000;
        [mLabel setTextAlignment:UITextAlignmentLeft];
        [cell.contentView addSubview:mLabel];
        [mLabel release];
        


        
        UIImageView *mImageView = [[UIImageView alloc] initWithFrame:CGRectMake(220, 5, 40, 40)];
        mImageView.tag = 100001;
        mImageView.image = nil;
        [cell.contentView addSubview:mImageView];
        [mImageView release];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    UILabel *mLabel = (UILabel*)[cell.contentView viewWithTag:100000];
    UIImageView *mImageView = (UIImageView *)[cell.contentView viewWithTag:100001];
    mLabel.text = nil;
    mImageView.image = nil;
    if ([indexPath section] == 0) {
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        mLabel.text = @"头像";
        [mImageView setImage:self.iconImage];
    }
    else if ([indexPath section] == 1)
    {
        switch ([indexPath row]) {
            case 0:
                mLabel.text = [NSString stringWithFormat:@"姓名:  %@",[defaults objectForKey:@"childName"]];
                break;
            case 1:
                mLabel.text = [NSString stringWithFormat:@"学校:  %@",[defaults objectForKey:@"childSchoolName"]];
                break;
            case 2:
                mLabel.text = [NSString stringWithFormat:@"班级:  %@",[defaults objectForKey:@"childClassName"]];
                break;
            case 3:
                mLabel.text = [NSString stringWithFormat:@"性别:  %@",[defaults objectForKey:@"childGender"]];
                break;
            case 4:
                mLabel.text = [NSString stringWithFormat:@"生日:  %@",[defaults objectForKey:@"childBirthday"]];
                break;
            default:
                break;
        }
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if ([indexPath section] == 0 && [indexPath row] == 0) {
        [self getOpenImagePicker];
    }

}

#pragma mark requestFinished 

-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{
    
}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    NSString *PhotoUrl = [[ResultContent JSONValue] objectForKey:@"imgUrl"];
    NSLog(@"PhotoUrl :%@",PhotoUrl);
    [self.mTableView reloadData];
    
}
-(void)downFileOfIcon:(NSString *)url
{
    NSString *path = [Instance GetDocumentByString:[NSString stringWithFormat:@"%@.png",[Instance GetUseID]]];
    [self.httpFormatRequest CustomFormDataRequestDownFile:path tag:FILEDOWNID url:url];
}





#pragma mark 选择图片
-(void)getOpenImagePicker
{
    UIActionSheet *pickerSheet = [[UIActionSheet alloc]initWithTitle:@"请选择图片" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照",@"相册",nil ];
    pickerSheet.delegate = self;
    pickerSheet.tag = 10000001;
    [pickerSheet showInView:[UIApplication sharedApplication].keyWindow.rootViewController.view];

    [pickerSheet release];
    
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 10000001) {
        if (buttonIndex == 0) {
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                picker.editing = YES;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [[UIApplication sharedApplication].keyWindow.rootViewController presentModalViewController:picker animated:YES];
            }
            else {  
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"打开镜头错误" message:@""delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];  
                [alert show];  
                [alert release];  
            } 
        }
        if (buttonIndex == 1) {
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                picker.editing = YES;
                picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                [[UIApplication sharedApplication].keyWindow.rootViewController presentModalViewController:picker animated:YES];
            }
            else {  
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"连接到图片库错误" message:@""delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [alert show];
                [alert release];  
            }  
        }
        return;
    }

}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image= [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    }
    int  ImageWidth  = image.size.width /12;
    int  ImageHeight = image.size.height/12;
    UIImage *theImage = [self imageWithImageSimple:image scaledToSize:CGSizeMake(ImageWidth, ImageHeight)];
//    UIImage *midImage = [self imageWithImageSimple:image scaledToSize:CGSizeMake(210.0, 210.0)];
//    UIImage *bigImage = [self imageWithImageSimple:image scaledToSize:CGSizeMake(440.0, 440.0)];
    [theImage retain];
//    [midImage retain];
//    [bigImage retain];
    [self saveImage:theImage WithName:[NSString stringWithFormat:@"%@.png",[Instance GetUseID]]];
    
//    [self saveImage:midImage WithName:@"iconImageMid.png"];
//    [self saveImage:bigImage WithName:@"iconImageBig.png"];
    [[UIApplication sharedApplication].keyWindow.rootViewController dismissModalViewControllerAnimated:YES];
    [picker release];
}
-(UIImage*)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize
{
    // Create a graphics image context
    UIGraphicsBeginImageContext(newSize);
    
    // Tell the old image to draw in this new context, with the desired
    // new size
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    // Get the new image from the context
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    // End the context
    UIGraphicsEndImageContext();
    // Return the new image.
    return newImage;
}
- (void)saveImage:(UIImage *)tempImage WithName:(NSString *)imageName
{
    NSData* imageData = UIImagePNGRepresentation(tempImage);
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentsDirectory = [paths objectAtIndex:0];
    // Now we get the full path to the file
    NSString* fullPathToFile = [documentsDirectory stringByAppendingPathComponent:imageName];
    // and then we write it out
    [imageData writeToFile:fullPathToFile atomically:NO];
    [self performSelector:@selector(saveIcon) withObject:nil];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex;  // after animation
{
    return;
}

#pragma mark 选择生日
-(void)SelectDateOfChilde
{

    NSString *title = UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation) ? @"\n\n\n\n\n\n\n\n\n" : @"\n\n\n\n\n\n\n\n\n\n\n\n" ;
    
    UIActionSheet *shareSheet = [[UIActionSheet alloc] initWithTitle:title delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"确定",nil];
    shareSheet.delegate = self;
    shareSheet.tag = 101010;
    UIDatePicker *datePicker = [[[UIDatePicker alloc] init] autorelease];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.tag = 101;
    
    [shareSheet addSubview:datePicker];
    [datePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    [shareSheet showFromToolbar:self.navigationController.toolbar];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
