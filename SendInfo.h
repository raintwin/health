//
//  SendInfo.h
//  Health
//
//  Created by hq on 12-7-26.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SendInfo : NSObject

@property(nonatomic,retain) NSString *fName,*cName,*cPhotoUrl;
@property(nonatomic,assign) int cID;

@end
