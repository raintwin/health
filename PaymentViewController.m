//
//  PaymentViewController.m
//  Health
//
//  Created by dinghao on 13-8-11.
//
//

#import "PaymentViewController.h"
#import "AlixPayOrder.h"
#import "AlixPayResult.h"
#import "AlixPay.h"
#import "DataSigner.h"

@interface PaymentViewController ()
@property (nonatomic,retain)NSMutableDictionary *alipayDict;
@end

@implementation PaymentViewController

@synthesize alipayDict;
@synthesize userContent;
@synthesize alipayDate;
@synthesize alipayMoney;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    self.alipayDict = [[NSMutableDictionary alloc]init];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"userID"] == nil) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"操作失败，请与客服联系" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        return;
    }
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"] forKey:@"userId"];
    [dict setObject:NSLocalizedString(@"PORT_ALIPAY", nil) forKey:NSLocalizedString(@"PORT_ReqType", nil)];

    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate =self;
    [mHttpRequest CustomFormDataRequestDict:[dict JSONRepresentation] tag:ALIPAYTAG url:WEBSERVERURL];
    [self.view addSubview:mHttpRequest];
    [dict release];
    [mHttpRequest release];

    [self.userContent setEditable:NO];
}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    NSLog(@"ResultContent = %@",ResultContent);
    self.alipayDict = [ResultContent JSONValue];
    
    self.userContent.text = [self.alipayDict objectForKey:@"content"];
    self.alipayDate.text = [NSString stringWithFormat:@"%@ 个月",[self.alipayDict objectForKey:@"freTime"]];
    self.alipayMoney.text = [NSString stringWithFormat:@"%@ 元",[self.alipayDict objectForKey:@"totalCharge"]];
    
//    [[NSBundle mainBundle] setValue:[self.alipayDict objectForKey:@"RSA_PUBLIC"] forKey:@"RSA public key"];
//     objectForInfoDictionaryKey
    
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSString *finalPath = [path stringByAppendingPathComponent: @"Info.plist"];
    NSLog(@"finalPath -%@",finalPath);
    NSMutableDictionary * dict = [ [ NSMutableDictionary alloc ]initWithContentsOfFile:finalPath];
    [dict setObject:[self.alipayDict objectForKey:@"RSA_PRIVATE"] forKey:@"RSA private key"];
    [dict setObject:[self.alipayDict objectForKey:@"RSA_PUBLIC"] forKey:@"RSA public key"];

    [dict writeToFile:finalPath atomically:YES];
    

}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"网络错误，重请登录" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alertView show];
    [alertView release];
}
- (IBAction)alipayMethod:(id)sender
{
	/*
	 *点击获取prodcut实例并初始化订单信息
	 */
//	Product *product = [_products objectAtIndex:indexPath.row];
	
	/*
	 *商户的唯一的parnter和seller。
	 *本demo将parnter和seller信息存于（AlixPayDemo-Info.plist）中,外部商户可以考虑存于服务端或本地其他地方。
	 *签约后，支付宝会为每个商户分配一个唯一的 parnter 和 seller。
	 */
	//如果partner和seller数据存于其他位置,请改写下面两行代码
	NSString *partner = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"Partner"];
    NSString *seller = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"Seller"];
	
    NSLog(@"partner=%@",partner);
    NSLog(@"seller=%@",seller);

	//partner和seller获取失败,提示
	if ([partner length] == 0 || [seller length] == 0)
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
														message:@"缺少partner或者seller。"
													   delegate:self
											  cancelButtonTitle:@"确定"
											  otherButtonTitles:nil];
		[alert show];
		[alert release];
		return;
	}
	
	/*
	 *生成订单信息及签名
	 *由于demo的局限性，本demo中的公私钥存放在AlixPayDemo-Info.plist中,外部商户可以存放在服务端或本地其他地方。
	 */
	//将商品信息赋予AlixPayOrder的成员变量
	AlixPayOrder *order = [[AlixPayOrder alloc] init];
	order.partner = partner;
	order.seller = seller;
	order.tradeNO = [self.alipayDict objectForKey:@"serialNo"]; //订单ID（由商家自行制定）
	order.productName = [self.alipayDict objectForKey:@"productName"]; //商品标题
	order.productDescription = [self.alipayDict objectForKey:@"content"]; //商品描述
	order.amount = [NSString stringWithFormat:@"%.2f",[[self.alipayDict objectForKey:@"totalCharge"] floatValue]]; //商品价格
	order.notifyURL =  [self.alipayDict objectForKey:@"url"]; //回调URL
	
	//应用注册scheme,在AlixPayDemo-Info.plist定义URL types,用于安全支付成功后重新唤起商户应用
	NSString *appScheme = @"Health";
	
	//将商品信息拼接成字符串
	NSString *orderSpec = [order description];
	NSLog(@"orderSpec = %@",orderSpec);
	
	//获取私钥并将商户信息签名,外部商户可以根据情况存放私钥和签名,只需要遵循RSA签名规范,并将签名字符串base64编码和UrlEncode
	id<DataSigner> signer = CreateRSADataSigner([self.alipayDict objectForKey:@"RSA_PRIVATE"]);
	NSString *signedString = [signer signString:orderSpec];
	
	//将签名成功字符串格式化为订单字符串,请严格按照该格式
	NSString *orderString = nil;
	if (signedString != nil) {
		orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
                       orderSpec, signedString, @"RSA"];
        
        //获取安全支付单例并调用安全支付接口
        AlixPay * alixpay = [AlixPay shared];
        int ret = [alixpay pay:orderString applicationScheme:appScheme];
        
        if (ret == kSPErrorAlipayClientNotInstalled) {
            UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"提示"
                                                                 message:@"您还没有安装支付宝快捷支付，请先安装。"
                                                                delegate:self
                                                       cancelButtonTitle:@"确定"
                                                       otherButtonTitles:nil];
            [alertView setTag:123];
            [alertView show];
            [alertView release];
        }
        else if (ret == kSPErrorSignError) {
            NSLog(@"签名错误！");
        }
	}
    [self.view removeFromSuperview];
    [self release];
}
- (IBAction)alipayCancels:(id)sender
{
    [self.view removeFromSuperview];
    [self release];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
    self.alipayMoney = nil;
    self.alipayDate = nil;
    
}
-(void)dealloc
{
    [super dealloc];
//    [self.alipayMoney release];
//    [self.alipayDate release];
    

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
