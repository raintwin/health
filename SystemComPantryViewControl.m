//
//  SystemComPantryViewControl.m
//  Health
//
//  Created by jiaodian on 12-12-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>

#import "SystemComPantryViewControl.h"
#import "FoodListViewController.h"
#import "SysDetialMealViewController.h"
@interface SystemComPantryViewControl ()
@property(retain,nonatomic)NSMutableDictionary *sysDetialMealsDict;
@property(retain,nonatomic)NSMutableArray *mRecords;
@property(nonatomic,retain) NSMutableDictionary *requstSaveMealDict;

@property(nonatomic,retain) NSMutableArray *recordCounts;
@property(retain,nonatomic)UITableView *mTableView;
@property(nonatomic,retain)UIButton *leftButton,*rightButton;

@property(retain,nonatomic)HttpFormatRequest *httpRequest;


@end

#define TableViewHeiht 30

#define SavaData 2200

@implementation SystemComPantryViewControl
@synthesize mTableView;
@synthesize mRecords;
@synthesize httpRequest;
@synthesize Delegate;
@synthesize sysDetialMealsDict;
@synthesize requstSaveMealDict;
@synthesize isRecordData;
@synthesize recordCounts;
@synthesize requstRecordMealDict;
@synthesize leftButton,rightButton;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        index = 0;
        isRecordData = NO;
    }
    return self;
}
#pragma mark - View lifecycle
-(void)backNav
{
//    [Delegate SetNavBarHidden:YES];
//    [self.navigationController popViewControllerAnimated:YES];
    
    CATransition *animation = [CATransition animation];
    animation.duration = 0.4f;
    animation.timingFunction = UIViewAnimationCurveEaseInOut;
    animation.fillMode = kCAFillModeForwards;
    animation.type = kCATransitionPush;
    animation.subtype = kCATransitionFromLeft;
    [self.view.superview.layer addAnimation:animation forKey:@"animation"];
    [self.view removeFromSuperview];
    [self release];

}

-(void)dealloc
{
    [self.mTableView  release];
    [self.mRecords  release];
    [self.httpRequest release];
    [self.sysDetialMealsDict release];
    [self.requstSaveMealDict release];

    [self.recordCounts release];
    [requstRecordMealDict release];
    [self.leftButton release];
    [self.rightButton release];


    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mTableView = nil;
    self.mRecords = nil;
    
    self.httpRequest = nil;
    self.sysDetialMealsDict = nil;
    self.requstSaveMealDict = nil;
    self.requstRecordMealDict = nil;
    self.recordCounts = nil;
    self.leftButton = nil;
    self.rightButton = nil;
}

#pragma mark viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.requstSaveMealDict = [[NSMutableDictionary alloc]init];
    self.recordCounts = [[NSMutableArray alloc]init];
    self.mRecords = [[NSMutableArray alloc]init];
    rescordSectionCount = 0;
    
    {
        CGRect rect = self.view.frame;
        if (isRecordData)
        {        [ViewTool addUIImageView:self.view imageName:@"navback.png" type:@"" x:0 y:0 x1:320 y1:44];
            [[ViewTool addUIButton:self.view imageName:@"backbutton.png" type:@"" x:5 y:7 x1:45 y1:30] addTarget:self action:@selector(backNav) forControlEvents:64];
            
            UILabel *titleLabel = [ViewTool addUILableBold:self.view x:0 y:0 x1:320 y1:44 fontSize:18 lableText:@"配餐"];
            titleLabel.textAlignment = UITextAlignmentCenter;
            titleLabel.textColor = [UIColor whiteColor];
            rect.origin.y = rect.origin.y +44;
            rect.size.height = [Instance getScreenHeight]+50;
        }
        else
        {
            
            rect.size.height = [Instance getScreenHeight]-30+50;
        }
        self.mTableView  = [[UITableView alloc]initWithFrame:rect style:UITableViewStylePlain];
        self.mTableView .delegate = self;
        self.mTableView .dataSource = self;
        
        [self.view addSubview:self.mTableView];
    }
    {
        self.sysDetialMealsDict = [[NSMutableDictionary alloc]init];
        
        [self.sysDetialMealsDict setObject:NSLocalizedString(@"PORT_SysMeal", nil) forKey:NSLocalizedString(@"PORT_ReqType", nil)];
    }
    
    {
        self.httpRequest = [[HttpFormatRequest alloc]init];
        self.httpRequest.Delegate = self;
        [self.view addSubview:self.httpRequest];
    }
    if (isRecordData) {
        [self.httpRequest CustomFormDataRequestDict:[requstRecordMealDict JSONRepresentation] tag:0 url:WEBSERVERURL];
    }
    else
    {
        if ([[self getResultConten] length]>0) {
            [self setResultContent:[self getResultConten]];
            index = 1;
            [mRecords addObject:[self getResultConten]];
        }
        else
        {
            [self requestMealData];
            index = 0;
        }
        UISwipeGestureRecognizer *oneFingerSwipeRight =[[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(oneFingerSwipeRight)] autorelease];
        [oneFingerSwipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
        [mTableView addGestureRecognizer:oneFingerSwipeRight];
        UISwipeGestureRecognizer *oneFingerSwipeLeft =[[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(oneFingerSwipeLeft)] autorelease];
        [oneFingerSwipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
        [mTableView addGestureRecognizer:oneFingerSwipeLeft];
        
        self.leftButton = [[UIButton alloc]initWithFrame:CGRectMake(-10, 100, 60, 130)];
        [self.leftButton setImage:[UIImage imageNamed:@"leftButton"] forState:0];
        [self.leftButton addTarget:self action:@selector(oneFingerSwipeLeft) forControlEvents:64];
        self.leftButton.alpha = 0.8f;
        [self.view addSubview:self.leftButton];
        
        self.rightButton = [[UIButton alloc]initWithFrame:CGRectMake(270, 100, 60, 130)];
        [self.rightButton setImage:[UIImage imageNamed:@"rightButton"] forState:0];
        [self.rightButton addTarget:self action:@selector(oneFingerSwipeRight) forControlEvents:64];
        self.rightButton.alpha = 0.8f;
        [self.view addSubview:self.rightButton];

    }
    
   

}
#pragma mark 保存食谱
-(void)saveSysMealList
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *ResultContent = [defaults objectForKey:@"ResultContent"];
    [self.requstSaveMealDict setObject:NSLocalizedString(@"PORT_SaveMealList", nil) forKey:NSLocalizedString(@"PORT_ReqType", nil)];

    [self.requstSaveMealDict setObject:[NSNumber numberWithInt:[[Instance GetUseID] intValue]] forKey:@"userId"];
    [self.requstSaveMealDict setObject:[[ResultContent JSONValue] valueForKey:@"mealA"] forKey:@"mealA"];
    [self.requstSaveMealDict setObject:[[ResultContent JSONValue] valueForKey:@"mealB"] forKey:@"mealB"];
    [self.requstSaveMealDict setObject:[[ResultContent JSONValue] valueForKey:@"mealC"] forKey:@"mealC"];
    [self.requstSaveMealDict setObject:[[ResultContent JSONValue] valueForKey:@"mealD"] forKey:@"mealD"];
    [self.requstSaveMealDict setObject:[[ResultContent JSONValue] valueForKey:@"mealE"] forKey:@"mealE"];

    
    [self.httpRequest CustomFormDataRequestDict:[self.requstSaveMealDict JSONRepresentation] tag:SavaData url:WEBSERVERURL];

}
typedef enum  {
	BreakfastType = 18,
    BreakfastDotType,
    LunchType,
    LunchDotType,
    DinnerType,
} mealType;

-(NSString*)SetTableTitle:(int)flag
{
    NSString *TitleText = @"";
    switch (flag) {
        case BreakfastType:
            TitleText = @"早餐";
            break;
        case BreakfastDotType:
            TitleText = @"早点";
            break;
        case LunchType:
            TitleText = @"午餐";
            break;
        case LunchDotType:
            TitleText = @"午点";
            break;
        case DinnerType:
            TitleText = @"晚餐";
            break;           
        default:
            break;
    }
return TitleText;
}

#pragma mark 向右滑
- (void)oneFingerSwipeRight
{
    index++;
    if (index >=[self.mRecords count]) {
        index = [self.mRecords count];
        [self.httpRequest CustomFormDataRequestDict:[self.sysDetialMealsDict JSONRepresentation] tag:0 url:WEBSERVERURL];
    }
    else
    {
        [SVProgressHUD show];
        [self recodeResultConten:[self.mRecords objectAtIndex:index]];
        [self setResultContent:[self.mRecords objectAtIndex:index]];
        [SVProgressHUD dismiss];
    }
    NSLog(@"index %d",index);
}
#pragma mark 向左滑
- (void)oneFingerSwipeLeft
{
    index--;
    
    if (index < 0) {
        index = 0;
        return;
    }
   else {
       [SVProgressHUD show];
       [self recodeResultConten:[self.mRecords objectAtIndex:index]];
       [self setResultContent:[self.mRecords objectAtIndex:index]];
       [SVProgressHUD dismiss];
    }
    NSLog(@"index %d",index);

}
-(void)requestMealData
{
   [self.httpRequest CustomFormDataRequestDict:[self.sysDetialMealsDict JSONRepresentation] tag:0 url:WEBSERVERURL];
}
#pragma mark request 
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{
    if (tag == SavaData) {
        return;
    }
}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    if (tag == SavaData) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"保存成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
        [alertView release];
        
        return;
    }
    if (isRecordData) {
        [self setResultContent:ResultContent];
        return;
    }
    [self.mRecords addObject:ResultContent];
    [self recodeResultConten:ResultContent];
    [self setResultContent:ResultContent];
    index = [self.mRecords count]-1;

    NSLog(@"index count :%d",index);
}
-(void)recodeResultConten:(NSString *)ResultContent
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:ResultContent forKey:@"ResultContent"];
    [defaults synchronize];
}
-(NSString *)getResultConten
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *ResultContent = [defaults objectForKey:@"ResultContent"];
    NSLog(@"NSMutableDictionary :%@",ResultContent);

    return ResultContent;
}
-(void)setResultContent:(NSString *)ResultContent
{
    [self.recordCounts removeAllObjects];
    NSArray *mealA = [[[ResultContent JSONValue] valueForKey:@"mealA"] retain];
    NSArray *mealB = [[[ResultContent JSONValue] valueForKey:@"mealB"] retain];

    NSArray *mealC = [[[ResultContent JSONValue] valueForKey:@"mealC"] retain];
    NSArray *mealD = [[[ResultContent JSONValue] valueForKey:@"mealD"] retain];
    NSArray *mealE = [[[ResultContent JSONValue] valueForKey:@"mealE"] retain];

    [self setContent:mealA tag:BreakfastType];
    [self setContent:mealB tag:BreakfastDotType];
    [self setContent:mealC tag:LunchType];
    [self setContent:mealD tag:LunchDotType];
    [self setContent:mealE tag:DinnerType];

    
    [self.mTableView  reloadData];
}
-(void)setContent:(NSArray *)dataAray tag:(int)type
{
    if ([dataAray count] > 0) {
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        NSMutableArray      *mealDatas = [[NSMutableArray alloc]init];

        for (int i = 0; i<[dataAray count]; i++) {
            sysMeal *meal = [[sysMeal alloc]init];
            meal.ID = [[NSString stringWithFormat:@"%@",[[dataAray objectAtIndex:i] valueForKey:@"id"]] intValue];
            meal.name = [NSString stringWithFormat:@"%@",[[dataAray objectAtIndex:i] valueForKey:@"name"]];
            meal.uType = [NSString stringWithFormat:@"%@",[[dataAray objectAtIndex:i] valueForKey:@"uType"]];
            [mealDatas addObject:meal];
            [meal release];
        }
        [mDict setObject:mealDatas forKey:@"lists"];
        [mDict setObject:[NSNumber numberWithInt:type] forKey:@"type"];
        [self.recordCounts addObject:mDict];
        [mealDatas release];
        [mDict release];
    }
    [dataAray release];
    
}
#pragma mark scroller
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.rightButton.alpha == 0.8f) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2f];
        self.rightButton.alpha = 0.0f;
        self.leftButton.alpha = 0.0f;
        [UIView commitAnimations];
    }
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (self.rightButton.alpha != 0.8f) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2f];
        self.rightButton.alpha = 0.8f;
        self.leftButton.alpha = 0.8f;
        [UIView commitAnimations];
    }
}
#pragma  mark tableview
-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TableViewHeiht;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.textLabel.text = @"";
        cell.imageView.image = nil;

    }
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.imageView.image = nil;
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;

    NSMutableDictionary *mealDict = [self.recordCounts objectAtIndex:[indexPath section]];
    
    NSMutableArray *mealDatas = [mealDict objectForKey:@"lists"];
    if ([mealDatas count] == 0 ) {
        return cell;
    }

    sysMeal *sysmeal = (sysMeal *)[mealDatas objectAtIndex:[indexPath row]];
    cell.textLabel.text = sysmeal.name;
    return cell;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSMutableDictionary *mDict = [self.recordCounts objectAtIndex:section];
    
    NSMutableArray *datas = [mDict objectForKey:@"lists"];

    return [datas count];
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIImageView *headerImageView = [[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 20)] autorelease];
    [headerImageView setImage:[UIImage imageNamed:@"iphon.jpg"]];
    
    NSMutableDictionary *mDict = [self.recordCounts objectAtIndex:section];

    
    UILabel *mLabel = [Instance addLableRect:CGRectMake(5, 0, 100, TableViewHeiht) text:[self SetTableTitle:[[mDict objectForKey:@"type"] intValue]] textsize:15];
    mLabel.textAlignment = UITextAlignmentCenter;
    [headerImageView addSubview:mLabel];
    [mLabel release];
    
    return headerImageView;
}
-(float)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return TableViewHeiht;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.recordCounts count];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
//    NSMutableDictionary *mealDict = [self.recordCounts objectAtIndex:[indexPath section]];
//    
//    NSMutableArray *mealDatas = [mealDict objectForKey:@"lists"];
//
//    
//    sysMeal *sysmeal = (sysMeal *)[mealDatas objectAtIndex:[indexPath row]];
    SysDetialMealViewController *detailViewController = [[SysDetialMealViewController alloc]initWithNibName:@"SysDetialMealViewController" bundle:nil];
//    [detailViewController.view setFrame:[UIApplication sharedApplication].keyWindow.rootViewController.view.bounds];

    [self.navigationController pushViewController:detailViewController animated:YES];
    
//    [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview:detailViewController.view];
//    detailViewController.sysmeal = sysmeal;
//
//    CATransition *animation = [CATransition animation];
//    animation.delegate = self;
//    animation.duration = 0.3f;
//    animation.timingFunction = UIViewAnimationCurveEaseInOut;
//    animation.fillMode = kCAFillModeForwards;
//    animation.type = kCATransitionPush;
//    animation.subtype = kCATransitionFromRight;
//    [[UIApplication sharedApplication].keyWindow.rootViewController.view.layer addAnimation:animation forKey:@"animation"];

    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
