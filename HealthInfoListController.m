//
//  HealthInfoListController.m
//  Health
//
//  Created by hq on 12-6-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "HealthInfoListController.h"
#import "SendNotifyViewController.h"
#import "InFoViewController.h"
#import "JSON.h"
#import "Nnotity.h"
#import "DownLoadTableImage.h"
#import "NSHealths.h"

#define TABLAHIGHT 80
#define TABLAMOREHEGIHT 52

#define ePageCount 10

#define KUserID      @"UserID"

#define KPageCount   @"PageCount"
#define KPageStart   @"PageStart"


#define KID                  @"ID"
#define KPhoto               @"publishPersonPhoto"
#define KPublishTime         @"publishTime"
#define KTitle               @"title"
#define KPublishPersonName   @"publishPersonName"


@interface HealthInfoListController()
@property(nonatomic,retain)NSMutableDictionary *mDownLoadImageDict;
@property(nonatomic,retain)TableTemplateView *mTableTemplateView;
@end

@implementation HealthInfoListController

@synthesize mDownLoadImageDict;
@synthesize mTableTemplateView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)CancelDownLoad
{
    for (int i = 0; i<[mTableTemplateView.requestDatas count]; i++) {
        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:i inSection:0];
        DownLoadTableImage *imageDownLoad = [mDownLoadImageDict objectForKey:indexpath];
        if (imageDownLoad != nil) {
            [imageDownLoad CancelDownloadImage];
        }
        
    }
    [mDownLoadImageDict removeAllObjects];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    if (buttonIndex == 0) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


#pragma mark - View lifecycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.mDownLoadImageDict = [[NSMutableDictionary alloc]init];

    
    CGRect frame = [[UIScreen mainScreen] bounds];
    frame.size.height = frame.size.height-CONTENTHEIGHT;

    {
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        TableTemplateView *table = [[TableTemplateView alloc]initWithFrame:CGRectMake(0, 0, 320, frame.size.height)];
        [table setTableHeightForRow:TABLAHIGHT];
        [table setUrlString:[NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/HealthInfo/FetchHealthInfoList"]];
        table.DataDict = mDict;
        table.delegate = self;
        table.isLoadImage = YES;
        self.mTableTemplateView = table;
        [table release];

        [self.view addSubview:mTableTemplateView];
        [mDict release];
        [mTableTemplateView GetDataForHttp];
    }    
}



-(void)ReturnHttpData:(NSString*)ResultContent container:(NSMutableArray*)arrays tag:(int)tag isFinish:(BOOL)isFinish;
{
    NSLog(@"ResultContent--:%@",ResultContent);
    
    NSArray *array = [[[ResultContent JSONValue] valueForKey:@"healthInfoList"] retain];
    
    mTableTemplateView.isLoadOver = YES;

    
    
    for (int i = 0; i< [array count]; i++) {
        NSHealths *hobj = [[NSHealths alloc] init];
        hobj.hID                 = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:KID]] intValue];
        hobj.hTitle              = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:KTitle]];
        hobj.hPublishPerson      = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:KPublishPersonName]];
        hobj.hPublishPersonPhoto = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:KPhoto]];
        hobj.hPublishTime        = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:KPublishTime]];
        hobj.image = [UIImage imageNamed:@"acquiesce.jpg"];
        hobj.isLoad = NO;
        [arrays addObject:hobj];
        [hobj release];
    }
    [array release];
    [mTableTemplateView setRequestDatas:arrays];
    
    if ([arrays count] == 0 && isFinish == NO) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"加载失败" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alertView.delegate = self;
        [alertView show];
        [alertView release];
    }
}
-(void)SetTableData:(UITableViewCell*)cell indexpath:(NSIndexPath*)indexPath datas:(NSMutableArray *)mDatas;
{
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.imageView.image = nil;
    

    NSHealths *items = [mDatas objectAtIndex:[indexPath row]];


    cell.imageView.image = [UIImage imageNamed:@"information.jpeg"];
    cell.textLabel.text = items.hTitle;
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
    cell.detailTextLabel.text = items.hPublishTime;
}
-(void)TableDidSelect:(NSIndexPath*)indexPath Datas:(NSMutableArray*)mDatas;
{
    NSHealths *obj = [mDatas objectAtIndex:[indexPath row]];
    InFoViewController *nifCtr = [[InFoViewController alloc] initWithNibName:@"InFoViewController" bundle:nil];
    nifCtr.USERID = obj.hID;
    nifCtr.CONTEXTTYPE = CONTEXT_HEALTH;
    [self.navigationController pushViewController:nifCtr animated:YES];
    [nifCtr release];
}

#pragma mark  load imageview 
-(void)StartDownLoadImage:(NSString*)url IndexPath:(NSIndexPath*)indexpath 
{
//    DownLoadTableImage *imageDownLoad = [mDownLoadImageDict objectForKey:indexpath];
//    if (imageDownLoad == nil) {
//        imageDownLoad = [[DownLoadTableImage alloc] init];
//        imageDownLoad.ImageURL = url;
//        imageDownLoad.ImageHeight = 65;
//        imageDownLoad.ImageWidth = 65;
//        imageDownLoad.IndexPathTo = indexpath;
//        imageDownLoad.Delegate =self;
//        [mDownLoadImageDict setObject:imageDownLoad forKey:indexpath];
//        [imageDownLoad StartDownloadImage];
//        [imageDownLoad release];
//    }
}
#pragma mark finish download image
-(void)FinishDownLoadImage:(NSIndexPath*)indexpath Image:(UIImage*)image IsFailed:(BOOL)isFaild
{
//    DownLoadTableImage *imageDownLoad = [mDownLoadImageDict objectForKey:indexpath];
//    
//    if (imageDownLoad != nil) {
//        UITableViewCell *cell = [mTableTemplateView.mTableView cellForRowAtIndexPath:indexpath];
//        NSHealths *obj = [mTableTemplateView.requestDatas objectAtIndex:[indexpath row]];
//        if (isFaild) {
//            obj.image = [UIImage imageNamed:@"not_found_img.png"];
//            obj.isLoad = NO;
//        }
//        else
//        {
//            obj.image = image;
//            obj.isLoad = YES;
//        }
//        cell.imageView.image = obj.image;
//
//    }
}
- (void)loadImagesForOnscreenRows
{
        NSArray *visiblePaths = [mTableTemplateView.mTableView indexPathsForVisibleRows];
        for (NSIndexPath *indexPath in visiblePaths)
        {
            if ([indexPath row]<[mTableTemplateView.requestDatas count]) {
                NSHealths *item = [mTableTemplateView.requestDatas objectAtIndex:indexPath.row];
                
                if (!item.isLoad ) // avoid the app icon download if the app already has an icon
                {
                    [self StartDownLoadImage:item.hPublishPersonPhoto IndexPath:indexPath];
                }
                
            }
        }
}
-(void)TableDownLoadCellImage
{
    [self loadImagesForOnscreenRows];
}
-(void)dealloc
{
    [self.mDownLoadImageDict release];
    
    [self.mTableTemplateView release];
    
    [super dealloc];
}
- (void)viewDidUnload
{



    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.mDownLoadImageDict = nil;
    self.mTableTemplateView = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
