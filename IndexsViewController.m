//
//  IndexsViewController.m
//  Health
//
//  Created by hq on 12-7-4.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "IndexsViewController.h"
#import "ChildDisListsViewController.h"
#import "UINavigatorBarBackGround.h"
#import "PeopleView.h"
#import "ChildOneListView.h"

#define tableHeight 40
//@implementation UINavigationBar (Customized)
//
//- (void)drawRect:(CGRect)rect {
//    UIImage *image = [[UIImage imageNamed:@"img_44.png"] retain];
//    [image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
//    NSLog(@"%@",@"device is < 5.0");
//    [image release];
//}
//
//@end 

@implementation IndexsViewController
@synthesize arrays;
@synthesize mTableView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        NSMutableArray *arrary = [[NSMutableArray alloc] initWithObjects:@"在家表现",@"在园表现", nil];
        self.arrays = arrary;
        [arrary release];
        self.title = @"首页";
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)setRelodateView{
    NSLog(@"刷新");
    [mTableView reloadData];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
//    self.navigationController.title = @"首页";
    [self.navigationItem setHidesBackButton: YES];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"img_04.png"]]];
    // Do any additional setup after loading the view from its nib.

    UITableView  *mytable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 368) style:UITableViewStylePlain];
    mytable.delegate = self;
    mytable.dataSource = self;
    [mytable setBackgroundColor:[UIColor clearColor]];
    [mytable setScrollEnabled:NO];
    self.mTableView = mytable;
    [mytable release];
    [self.view addSubview:mTableView];
    
//    [[UITabBar appearance] setSelectedImageTintColor:[UIColor redColor]];

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath row] == 0)   return 118.0f;
    else  return tableHeight;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [arrays count]+1;
}
-(NSString*)DocumentFilePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    return documentDirectory;
}


-(NSString*)IconFilePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    NSString *filePng = [documentDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",[Instance GetChildID]]];
    NSString *fileJpg = [documentDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",[Instance GetChildID]]];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePng]) 
    {return filePng;}
    if ([[NSFileManager defaultManager] fileExistsAtPath:fileJpg]) 
    {return fileJpg; }
    return @"";
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    for (UIView *subview in [cell.contentView subviews]) {
        [subview removeFromSuperview];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if ([indexPath row] == 0) {
        {
            
            UIImageView*  imageIcon = [[UIImageView alloc]initWithFrame:CGRectMake(20, 17, 95, 95)];
            [imageIcon setImage:[UIImage imageWithContentsOfFile:[Instance GetIconFile]]];   
            [imageIcon setContentMode:UIViewContentModeScaleAspectFit];
            [cell.contentView addSubview:imageIcon];
            [imageIcon release];

            
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:[self IconFilePath]]) {
                [imageIcon setImage:[UIImage imageWithContentsOfFile:[self IconFilePath]]];
            }
            else
                [imageIcon setImage:[UIImage imageNamed:@"iconuse.png"]];

            
        }        
        
        {
            {
                [Instance GetChildName];
                NSString *str = [Instance GetChildName];        
                CGSize size = [str sizeWithFont:[UIFont boldSystemFontOfSize:16] ];
                UILabel *labelText = [[UILabel alloc] initWithFrame:CGRectMake(130, 20, size.width, size.height)];
                labelText.font = [UIFont boldSystemFontOfSize:15];
                labelText.text = str;
                [labelText setBackgroundColor:[UIColor clearColor]];
                [cell.contentView addSubview:labelText];
                [labelText release];
            }
           {
                NSString *str = [Instance GetChildAge];        
                CGSize size = [str sizeWithFont:[UIFont systemFontOfSize:16] ];
                UILabel *labelText = [[UILabel alloc] initWithFrame:CGRectMake(130, 50, size.width, size.height)];
                labelText.font = [UIFont systemFontOfSize:15];
                labelText.text = str;
                [labelText setBackgroundColor:[UIColor clearColor]];
                [cell.contentView addSubview:labelText];
                [labelText release];
            }
            {
                NSString *str = [Instance GetChildSchoolAndClass];        
                CGSize size = [str sizeWithFont:[UIFont boldSystemFontOfSize:16] ];
                UILabel *labelText = [[UILabel alloc] initWithFrame:CGRectMake(130, 80, size.width, size.height)];
                labelText.font = [UIFont systemFontOfSize:15];
                labelText.text = str;
                [labelText setBackgroundColor:[UIColor clearColor]];
                [cell.contentView addSubview:labelText];
                [labelText release];
            }

        }
    }
    // Configure the cell...
    else {
        UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 46)];
        [img setImage:[UIImage imageNamed:@"xia.jpg"]];        
        [cell.contentView addSubview:img];
        [img release];

        {
                UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(12, (tableHeight - 24)/2, 25, 24)];
                [img setImage:[UIImage imageNamed:@"234234.png"]];        
                [cell.contentView addSubview:img];
                [img release];

            NSString *str = [arrays objectAtIndex:[indexPath row]-1];        
            CGSize size = [str sizeWithFont:[UIFont systemFontOfSize:16] ];
            UILabel *labelText = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, size.width, tableHeight)];
            labelText.font = [UIFont boldSystemFontOfSize:16];
            labelText.text = str;
            [labelText setBackgroundColor:[UIColor clearColor]];
            [cell.contentView addSubview:labelText];
            [labelText release];
            
        }
    }
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    


    if ([indexPath row] == 0) {
        return;
    }
    if ([Instance GetUseType] == LOGIN_BIND_P) {
        int row = [indexPath row]-1;
        ChildOneListView *nifCtr = [[ChildOneListView alloc] initWithNibName:@"ChildOneListView" bundle:nil];
        nifCtr.title = @"个人列表";
        //    nifCtr.mDisChildId = obj.childID;
        if (row == 0) { nifCtr.TYPE = INHOME; nifCtr.title = @"在家";}
        if (row == 1) { nifCtr.TYPE = INSCHOOL; nifCtr.title = @"在园";};
        nifCtr.mChildID = [[Instance GetChildID] intValue];
        [self.navigationController pushViewController:nifCtr animated:YES];
        [nifCtr release];
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    }

}
-(void)SetPesonal
{
    PeopleView *detailViewController = [[PeopleView alloc] initWithNibName:@"PeopleView" bundle:nil];
//    detailViewController._indexDelegate = self;
    detailViewController.title = @"个人信息完善";
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];
    


}
-(void)dealloc
{
    [mTableView release];
    [arrays release];
    
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.arrays = nil;
    self.mTableView = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
