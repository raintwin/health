//
//  InFoViewController.m
//  Health
//
//  Created by hq on 12-6-25.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "InFoViewController.h"
#import "JSON.h"
#import "ASIFormDataRequest.h"
#import "Nnotity.h"
#import "NSHealths.h"


#define ERIGHTICON         @"but_01.png"
#define KNofiticationID    @"NotificationID"
#define KReceiver          @"receiver"
#define KContent             @"content"
#define KPublishDateTime     @"publishDateTime"
#define KTitle               @"title"

#define KCID                 @"ID"
#define LUserName            @"userName"
#define KPhoto               @"photo"
@implementation InFoViewController

@synthesize USERID;
@synthesize CONTEXTTYPE;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        USERID = -1;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)navBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)UIViewAnimations:(float)x
{
    UIView *view = (UIView *)[self.view viewWithTag:10003];
    CGRect rect = view.frame;
    rect.origin.x = x;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3f];
    [view setFrame:rect];
    [UIView commitAnimations];
}
#pragma mark - View lifecycle
-(void)LookUp
{
    [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"退回" target:self selector:@selector(BackUp)];
    [self UIViewAnimations:220.0f ];

}
-(void)BackUp
{
    [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:FOURENSURE title:@"接收人" target:self selector:@selector(LookUp)];

    [self UIViewAnimations:320.0f];

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];
    
    [self performSelector:@selector(GetMianThread)withObject:nil afterDelay:0.1f];

    CGRect frame = [[UIScreen mainScreen] bounds];
    frame.origin.y = 20;
    frame.size.height = frame.size.height -20-50-44;
    {

        
        UIWebView *webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, frame.size.height)];
        webview.tag = 20001;
        [self.view addSubview:webview];
        [webview release];
        
        
        UILabel *mDateText = [[UILabel alloc] initWithFrame:CGRectMake(0, frame.size.height-20, 320, 20)];
        mDateText.tag = 10002;
        [mDateText setBackgroundColor:[UIColor clearColor]];
        mDateText.textAlignment = UITextAlignmentRight;
        mDateText.text = @"";
        [self.view addSubview:mDateText];
        [mDateText release];
    }
    // ------------------ 通知
    if (![Instance getCharacter] && CONTEXTTYPE == CONTEXT_NOTITY) {
        [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:FOURENSURE title:@"接收人" target:self selector:@selector(LookUp)];
        
        {
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(320, 0, 100, frame.size.height)];
            [view setBackgroundColor:[UIColor whiteColor]];
            view.tag = 10003;
            [self.view addSubview:view];
            [view release];
        }
    }
    
    //------------------- 健康资讯


}
#pragma mark 设 通知内容
-(void)SetNotifiation:(NSString*)context datetext:(NSString*)date
{
    UIWebView *mWebView = (UIWebView *)[self.view viewWithTag:20001];
    [mWebView loadHTMLString:context baseURL:nil];
    UILabel *mDateText = (UILabel *)[self.view viewWithTag:10002];
    mDateText.text = date;

}
-(void)SetHealth:(NSHealthsContext*)obj
{
//    if ([obj.hcAuthor length]>0) {
//        NSString *Author = [NSString stringWithFormat:@"作者: %@",obj.hcAuthor];
//        UILabel *mAuthor = [Instance addLableRect:CGRectMake(0,350,320,20) text:Author textsize:15];
//        [self.view addSubview:mAuthor];
//        [mAuthor release];
//
//    }
    
//    if ([obj.hcSource length]>0) {
//        NSString *sSource = [NSString stringWithFormat:@"出自: %@",obj.hcSource];
//        UILabel *mDateText = (UILabel *)[self.view viewWithTag:10002];
//        mDateText.text = sSource;
//    }
    
    CGRect frame = [[UIScreen mainScreen] bounds];
    frame.origin.y = 20;
    frame.size.height = frame.size.height -20-50-44-20;

    UIWebView *mWebView = (UIWebView *)[self.view viewWithTag:20001];

    [mWebView setFrame:CGRectMake(0, 20, 320, frame.size.height)];
    [mWebView loadHTMLString:obj.hcContent baseURL:nil];
    
    NSString *str = [NSString stringWithFormat:@"%@ 发布于 %@ ",obj.hcUserName,obj.hcPublishDateTime];
    UILabel *mPublish = [Instance addLableRect:CGRectMake(0, 0, 320, 20) text:str textsize:15];
    [self.view addSubview:mPublish];
    [mPublish release];

}
#pragma mark 表现内容
-(void)SetContextNotify
{
    NSString *urlstr;

    if ([Instance getCharacter]) {
        urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/Notification/ParentFetchNotificationInfo"];
    }
    else
        urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/Notification/TeacherFetchNotificationInfo"];

    
    NSMutableDictionary *mDict = [[NSMutableDictionary alloc] init];
    [mDict setObject:[NSNumber numberWithInt:USERID] forKey:KNofiticationID];
    NSString *JSONString = [mDict JSONRepresentation];
    [mDict release];
    
    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate = self;
    [self.view addSubview:mHttpRequest];
    [mHttpRequest CustomFormDataRequestDict:JSONString tag:0 url:urlstr];
    [mHttpRequest release];


}
-(void)SetContextHealth
{
    
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/HealthInfo/FetchHealthInfoDetail"];
    

    NSMutableDictionary *mDict = [[NSMutableDictionary alloc] init]; 
    [mDict setObject:[NSNumber numberWithInt:USERID] forKey:@"ID"];
    NSString *JSONString = [mDict JSONRepresentation];    
    [mDict release];
    
    
    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate = self;
    [self.view addSubview:mHttpRequest];
    [mHttpRequest CustomFormDataRequestDict:JSONString tag:0 url:urlstr];
    [mHttpRequest release];
}

#pragma mark thread
-(void)GetMianThread{
    
    
    if (CONTEXTTYPE == CONTEXT_HEALTH) {
        [self performSelector:@selector(SetContextHealth)];
    }
    if (CONTEXTTYPE == CONTEXT_NOTITY) {
        [self performSelector:@selector(SetContextNotify)];

    }
}
-(void)SetChildName:(NSMutableArray*)array
{   
    UIView *view = (UIView *)[self.view viewWithTag:10003];
    UIScrollView *scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    [scroll setShowsHorizontalScrollIndicator:NO];
    [scroll setShowsVerticalScrollIndicator:NO];
    [scroll setBackgroundColor:[UIColor clearColor]];
    [view addSubview:scroll];
    
    for (int i = 0; i<[array count]; i++) {
        Nnotity *obj = [array objectAtIndex:i];
        UIFont *font = [UIFont systemFontOfSize:16];
        CGSize size = [obj.nName sizeWithFont:font];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, (size.height+10) *i, size.width, size.height)];
        label.text = obj.nName;
        label.textColor = [UIColor blackColor];
        label.backgroundColor = [UIColor clearColor];
        label.font = font;
        [scroll addSubview:label];
        [label release];
        [scroll setContentSize:CGSizeMake(0, (size.height+10 )*i)];
    }
    [scroll release];

}
#pragma mark Request
-(void)RequestNotify:(NSString*)ResultContent
{
    NSString *content = [[ResultContent JSONValue] objectForKey:@"content"];
    NSString *publishDateTime = [[ResultContent JSONValue] objectForKey:@"publishDateTime"];
    
    self.title = [[ResultContent JSONValue] objectForKey:@"title"];
    [self SetNotifiation:content datetext:publishDateTime];
    
    
    NSArray *array = [[[ResultContent JSONValue] valueForKey:KReceiver] retain];
    NSMutableArray *more = [[NSMutableArray alloc]init];
    
    for (int i = 0; i< [array count]; i++) {
        Nnotity *nobj = [[Nnotity alloc] init];
        nobj.nID = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:KCID]] intValue];
        nobj.nPhoto = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:KPhoto]]; 
        
        nobj.nName = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:LUserName]];
        nobj.isLoad = NO;
        nobj.image = nil;
        
        [more addObject:nobj];
        [nobj release];

        
    }
    [self performSelector:@selector(SetChildName:) withObject:more];
    [more release];
    [array release];

}
#define KResultContent @"ResultContent"
-(void)RequestHealth:(NSString*)ResultContent
{
    NSHealthsContext *obj = [[NSHealthsContext alloc] init];


    
    obj.hcContent = [[ResultContent JSONValue] objectForKey:@"content"];
    obj.hcPublishDateTime = [[ResultContent JSONValue] objectForKey:@"publishDateTime"];
    obj.hcTitle = [[ResultContent JSONValue] objectForKey:@"title"];
    obj.hcSource = [[ResultContent JSONValue] objectForKey:@"source"];
    obj.hcAuthor = [[ResultContent JSONValue] objectForKey:@"author"];
    

    obj.hcID = [[[[ResultContent JSONValue] objectForKey:@"publishPerson"] objectForKey:@"ID"] intValue];
    obj.hcUserName = [[[ResultContent JSONValue] objectForKey:@"publishPerson"] objectForKey:@"userName"];
    obj.hcPhoto = [[[ResultContent JSONValue] objectForKey:@"publishPerson"] objectForKey:@"photo"];

    obj.hcKeyWord = nil;
    
    self.title = obj.hcTitle;

    [self performSelector:@selector(SetHealth:) withObject:obj];
    
    [obj release];
}
#pragma mark finish 
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    if (CONTEXT_HEALTH == CONTEXTTYPE) {
        [self RequestHealth:ResultContent];
    }
    if (CONTEXT_NOTITY == CONTEXTTYPE) {
        [self RequestNotify:ResultContent];
    }
}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{
}
-(void)dealloc
{
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
