//
//  IndexsViewController.h
//  Health
//
//  Created by hq on 12-7-4.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface IndexsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
}
-(void)setRelodateView;

-(NSString*)IconFilePath;
-(NSString*)DocumentFilePath;

@property(nonatomic,retain)NSMutableArray *arrays;
@property(nonatomic,retain)UITableView *mTableView;
@end
