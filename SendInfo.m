//
//  SendInfo.m
//  Health
//
//  Created by hq on 12-7-26.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "SendInfo.h"

@implementation SendInfo
@synthesize cID;
@synthesize fName,cName,cPhotoUrl;
-(void)dealloc
{
    [fName release]; fName = nil;
    [cName release]; cName = nil;
    [cPhotoUrl release]; cPhotoUrl = nil;
}
@end
