//
//  ChildGrowListController.m
//  Health
//
//  Created by hq on 12-6-25.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ChildGrowListController.h"
#import "InFoViewController.h"
#import "SingButtonView.h"

@implementation ChildGrowListController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)navBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];


    UITableView *mytable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 370)];
    mytable.dataSource = self;
    mytable.delegate = self;
    [self.view addSubview:mytable];
    [mytable release];

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    InFoViewController  *chlCtr = [[InFoViewController alloc] initWithNibName:@"InFoViewController" bundle:nil];
    chlCtr.title = @"某某成长记录详细";
    [self.navigationController pushViewController:chlCtr animated:YES];
    [chlCtr release];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return tableWidth;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * cellName = @"ChildListViewController";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier: cellName] autorelease];
    }
    cell.textLabel.text = @"类别 表现";

    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    SingButtonView *singbtn = [[SingButtonView alloc] initWithFrame:CGRectMake(100, 17, 28, 28)];
    singbtn.Count = 5;
    [singbtn setImage:@"B点.png"];
    
    [cell addSubview:singbtn];
    [singbtn release];
    return cell;
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
