//
//  ChildDisListsViewController.m
//  Health
//
//  Created by hq on 12-7-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ChildDisListsViewController.h"
#import "ChildDisViewController.h"
#import "ChlidDisHistroyController.h"
#import "Instance.h"
#import "ChildOneListView.h"
@implementation ChildDisListsViewController
@synthesize mDateDict;
@synthesize mHttpFormatRequestView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        mType = -1;
        mSelectRow = -1;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
#pragma mark navigationButton
-(void)HistroyCom
{
    
}
-(void)navBack
{

    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark 判断日期是否已经 评价
-(void)JudgeDate:(NSString*)date row:(int)tag
{


    NSMutableDictionary*mDict = [[NSMutableDictionary alloc] init];
    
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/ChildRepresentation/JudgeParentWroteChildRepresentationForOneBabyAndTypeInSomeday"];    
    [mDict setObject:[Instance GetChildID] forKey:@"ID"];
    [mDict setObject:date forKey:@"RepresentationDate"];
    [mDict setObject:[NSNumber numberWithInt:[self GetEvaluateType:tag]] forKey:@"RepresentationType"];
    
    NSString *JSONString = [mDict JSONRepresentation];
    [mDict release];
    
//    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
//    mHttpRequest.Delegate = self;
//    [self.view addSubview:mHttpRequest];
//    [mHttpRequest CustomFormDataRequestDict:JSONString tag:0 url:urlstr];
//    [mHttpRequest release];

    [mHttpFormatRequestView CustomFormDataRequestDict:JSONString tag:0 url:urlstr];
}
-(void)JudgeDateOfDict:(NSMutableDictionary*)mTmpeDict;
{
    NSString *date = [mTmpeDict objectForKey:@"date"];
    int tag = [[mTmpeDict objectForKey:@"type"] intValue];
    
    NSMutableDictionary*mDict = [[NSMutableDictionary alloc] init];
    
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/ChildRepresentation/JudgeParentWroteChildRepresentationForOneBabyAndTypeInSomeday"];
    
    [mDict setObject:[Instance GetUseID] forKey:@"ID"];
    [mDict setObject:date forKey:@"RepresentationDate"];
    [mDict setObject:[NSNumber numberWithInt:[self GetEvaluateType:tag]] forKey:@"RepresentationType"];
    
    
    NSString *JSONString = [mDict JSONRepresentation];
    [mDict release];
    

    [mHttpFormatRequestView CustomFormDataRequestDict:JSONString tag:0 url:urlstr];
    [mTmpeDict release];
}
#pragma  mark requestFinished
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag{

    int representationID = [[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] objectForKey:@"representationID"]] intValue];
    
    
    if (representationID != -1 && mSelectRow != -1 ) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"该日期已评价，是否重新评价" delegate:self cancelButtonTitle:@"否" otherButtonTitles:@"是", nil];
        alert.delegate = self;
        [alert show];
        [alert release];
        
    }
    else
    {
        [self GoChildDisViewController:mSelectRow]; 
    }
    
}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{    
}
#pragma mark 选择是否重评
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        return;
    }
    if (buttonIndex == 1) {
        [self GoChildDisViewController:mSelectRow];
    }
}
#pragma mark 确定日期
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    NSString *str = [mDateDict objectForKey:@"date"];
    NSLog(@"str--   :%@",str);
    mSelectRow = actionSheet.tag;
    
    if ([Instance GetUseType] == LOGIN_BIND_P) {
        [self JudgeDate:str row:mSelectRow];
        return;
    }
    [self GoChildDisViewController:mSelectRow];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];
    {
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc] init];
        self.mDateDict = mDict;
        [mDict release];

    }
    {
        HttpFormatRequest *mHttp = [[HttpFormatRequest alloc]init];
        mHttp.Delegate = self;
        self.mHttpFormatRequestView = mHttp;
        [mHttp release];
        
        [self.view addSubview:mHttpFormatRequestView];
        
    }
    {
        UITableView *mytable = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStyleGrouped];
        mytable.dataSource = self;
        mytable.delegate = self;
        mytable.backgroundColor = [UIColor clearColor];
        [mytable setScrollEnabled:NO];
        [self.view addSubview:mytable];
        [mytable release];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 40;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    int count = [arrays count];
    if (section == 0) {
        return 5;
    }
    else
        return 2;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView              // Default is 1 if not implemented
{
    return 2;
}

-(int)GetEvaluateType:(int)num
{
    int type = -1;	
    switch (num) {
        case 0:
            type = EvaluateTypeDay;
            break;

        case 1:
            type = EvaluateTypeWeek;
            break;
        case 2:
            type = EvaluateTypeDay;
            
            break;
        case 3:
            type = EvaluateTypeWeek;
            break;
        case 4:
            type = EvaluateTer;
            break;
            
        default:
            break;
    }
    return type;
}

#pragma mark 评价界面
-(void)GoChildDisViewController:(int)row
{

    
    NSArray *dis = [[NSArray alloc]initWithObjects:@"今天评价",@"本周评价",@"日评价",@"周评价",@"期末评价", nil ];
    ChildDisViewController *ctr = [[ChildDisViewController alloc] initWithNibName:@"ChildDisViewController" bundle:nil];
    if (![Instance getCharacter]) {
        ctr.isAnd = YES;

    }
    ctr.isText = YES;
    ctr.isDis = YES;
    ctr.EvaluateType = [self GetEvaluateType:row];
    ctr.title = [dis objectAtIndex:row];
    [dis release];

    ctr.mDateDict = mDateDict;
    [self.navigationController pushViewController:ctr animated:YES];
    [ctr release];
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{


    [tableView deselectRowAtIndexPath:indexPath  animated:NO];
    
    NSInteger section = [indexPath section];
    NSInteger row = [indexPath row];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *now = [dateFormatter stringFromDate:[NSDate date]];
    [dateFormatter release];

    [mDateDict setObject:now forKey:@"date"];
    NSString *str = [mDateDict objectForKey:@"date"];
    
    if (section == 0) {
        if (row == 0 || row == 1) {
            mSelectRow = row;
            if ([Instance getCharacter]) {
                NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
                [mDict setObject:str forKey:@"date"];
                [mDict setObject:[NSNumber numberWithInt:[self GetEvaluateType:row]] forKey:@"type"];
                [self performSelector:@selector(JudgeDateOfDict:) withObject:mDict afterDelay:0.1f];
            }
            else  {
                [self GoChildDisViewController:[self GetEvaluateType:row]];
            }
            return;
        }
        UIActionSheet *shareSheet = [[UIActionSheet alloc]initWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n\n" delegate:self cancelButtonTitle:@"确定" destructiveButtonTitle:nil otherButtonTitles:nil, nil];
        shareSheet.delegate = self;
        shareSheet.tag = row;
        
        UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        datePicker.datePickerMode = UIDatePickerModeDate;
        datePicker.tag = row;
        [shareSheet addSubview:datePicker];
        [datePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
        [shareSheet showFromToolbar:self.navigationController.toolbar];
        [datePicker release];
        
    }
    if (section == 1) {
        NSArray *his = [[NSArray alloc]initWithObjects:@"在家表现记录",@"在园表现记录", nil ];

        if (![Instance getCharacter]) {
            ChlidDisHistroyController *ctr = [[ChlidDisHistroyController alloc] initWithNibName:@"ChlidDisHistroyController" bundle:nil];
            ctr.title = [his objectAtIndex:row];
            if (row == 0)  ctr.TYPE = INHOME;
            if (row == 1)  ctr.TYPE = INSCHOOL;
            [self.navigationController pushViewController:ctr animated:YES];
            [ctr release];
            [his release];
            return;
        }
        else {
            ChildOneListView *nifCtr = [[ChildOneListView alloc] initWithNibName:@"ChildOneListView" bundle:nil];
            nifCtr.title = @"个人列表";
            //    nifCtr.mDisChildId = obj.childID;
            if (row == 0)  nifCtr.TYPE = INHOME;
            if (row == 1)  nifCtr.TYPE = INSCHOOL;
            nifCtr.mChildID = [[Instance GetUseID] intValue];
            [self.navigationController pushViewController:nifCtr animated:YES];
            [nifCtr release];
            [his release];

            [tableView deselectRowAtIndexPath:indexPath animated:NO];
        }
    }
}
-(void)dateChanged:(id)sender{    
  
    UIDatePicker *control = (UIDatePicker*)sender;    
    NSDate* _date = control.date;  
    
//    NSDateComponents *comps = [[NSDateComponents alloc]init];

    /*添加你自己响应代码*/    
    // ---  日期
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *now = [dateFormatter stringFromDate:_date];


    
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSString *nowTimer = [dateFormatter stringFromDate:[NSDate date]];
    
    [dateFormatter release];

    //--- 时间

    now = [NSString stringWithFormat:@"%@ %@",now,nowTimer];
    [mDateDict setObject:now forKey:@"date"];

    

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * cellName = @"PaperListViewController";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier: cellName] autorelease];
    }
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;    

    NSArray *dis = [[NSArray alloc]initWithObjects:@"今天评价",@"本周评价",@"日评价",@"周评价",@"学期末评价", nil ];
    
    NSArray *his = [[NSArray alloc]initWithObjects:@"在家表现记录",@"在园表现记录", nil ];
    NSInteger section = [indexPath section];
    if (section == 0) {
        cell.textLabel.text = [dis objectAtIndex:[indexPath row]];

    }
    if (section == 1) {
        cell.textLabel.text = [his objectAtIndex:[indexPath row]];

    }
    [dis release];
    [his release];
    return cell;
    
}

-(void)dealloc
{
    [mDateDict release];
    [mHttpFormatRequestView release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mDateDict = nil;
    self.mHttpFormatRequestView = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
