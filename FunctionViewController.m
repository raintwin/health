//
//  FunctionViewController.m
//  Health
//
//  Created by hq on 12-6-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "FunctionViewController.h"
#import "SendNotifyViewController.h"
#import "ChildDisViewController.h"
#import "ChlidDisHistroyController.h"
#import "HealthInfoListController.h"
#import "TodayMenusViewController.h"
#import "SingButtonView.h"
#import "Instance.h"
#import "HistroyFoodMenuController.h"
#import "NotifyListViewController.h"
#import "ChildPhotoMainView.h"
#import "PhotoListViewController.h"
#import "ChildDisListsViewController.h"
#import "SeekFoodView.h"
#import "Instance.h"
#import "PantrytMianViewController.h"
#import "PantryListViewController.h"
#import "LoginViewController.h"


#define ChildSignIn     TRUE
#define TeatherSignIn   FALSE


@implementation FunctionViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
     }
    return self;
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


-(void)SetButtonTitle:(UIButton*)btn Title:(NSString*)title
{
    UIFont *font  = [UIFont systemFontOfSize:13];
    CGSize size = [title sizeWithFont:font];
    int xPoint = btn.frame.size.width /2 - size.width/2;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(xPoint, btn.frame.size.height+5,size.width, size.height)];
    label.backgroundColor = [UIColor clearColor];
    label.font = font;
    label.text = title;
    label.textAlignment = UITextAlignmentCenter;
    label.textColor = [UIColor blackColor];
    [btn addSubview:label];
    [title release];
}
typedef  enum
{
    RecipeViewControllertag = 0,
    RecipeHistroyViewControllertag,
    PantryViewControllertag,
    PantryHistroyViewControllertag,
    NotifyListViewControllertag,
    HealthInfoListControllertag,
    ChildDisListsViewControllertag,
}viewControllerId;

-(void)loadSignIn
{
    LoginViewController *mProductMianView = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
    CGRect frame = [UIApplication sharedApplication].keyWindow.bounds;
    frame.origin.y = 20;
    [mProductMianView.view setFrame:frame];
    [[UIApplication sharedApplication].keyWindow addSubview:mProductMianView.view];
    mProductMianView.delegate = (id)[UIApplication sharedApplication].keyWindow.rootViewController.self;
    CATransition *animation = [CATransition animation];
    animation.delegate = self;
    animation.duration = 0.5f;
    animation.timingFunction = UIViewAnimationCurveEaseInOut;
    animation.fillMode = kCAFillModeForwards;
    animation.type = kCATransitionPush;
    animation.subtype = kCATransitionFade;
    [[UIApplication sharedApplication].keyWindow.rootViewController.view.layer addAnimation:animation forKey:@"animation"];

}
-(void)GoViewController:sender
{
    UIButton *button = sender;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
     [[defaults objectForKey:NSLocalizedString(@"PS_Sign", nil)] boolValue];


    if (![[defaults objectForKey:NSLocalizedString(@"PS_Sign", nil)] boolValue] && button.tag != HealthInfoListControllertag) {
        [self loadSignIn];        
        return;
    }

    
    UIViewController *viewController = nil;
    TodayMenusViewController *todayMenusViewController = nil;
    switch (button.tag) {
        case RecipeViewControllertag:
            todayMenusViewController  = [[TodayMenusViewController alloc]initWithNibName:@"TodayMenusViewController" bundle:nil];
            todayMenusViewController.title = @"今日食谱";
            todayMenusViewController.isToday  = YES;
            viewController = todayMenusViewController;
            break;
        case RecipeHistroyViewControllertag:
            viewController  = [[HistroyFoodMenuController alloc]initWithNibName:@"HistroyFoodMenuController" bundle:nil];
            viewController.title = @"历史食谱";
            break;
        case PantryViewControllertag:

            viewController  = [[PantrytMianViewController alloc]initWithNibName:@"PantrytMianViewController" bundle:nil];

            break;
        case PantryHistroyViewControllertag:
            viewController  = [[PantryListViewController alloc]initWithNibName:@"PantryListViewController" bundle:nil];
            viewController.title = @"配餐记录";

            break;
        case NotifyListViewControllertag:
            viewController = [[NotifyListViewController alloc] initWithNibName:@"NotifyListViewController" bundle:nil];
            viewController.title = @"通知";
            break;
        case HealthInfoListControllertag:
            viewController  = [[HealthInfoListController alloc]initWithNibName:@"HealthInfoListController" bundle:nil];
            viewController.title = @"资讯";
            break;
        case ChildDisListsViewControllertag:
            viewController  = [[ChildDisListsViewController alloc]initWithNibName:@"ChildDisListsViewController" bundle:nil];
            viewController.title = @"幼儿评价";
            break;
        default:
            break;
    }
    [self.navigationController pushViewController:viewController animated:YES];
    [viewController release];

}
#pragma mark 加载已绑定家长
-(void)initFunctionButton
{
    int i = 0;
    NSString *f_menu1 = @"G-幼儿评价.png";
    NSString *f_menu3 = @"E-通知.png";
    NSString *f_menu4 = @"A-今日食谱.png";
    NSString *f_menu5 = @"B-历史食谱.png";
    NSString *f_menu6 = @"E-自助配餐.png";
    NSString *f_menu7 = @"D-历史配餐.png";

    if (![Instance isNotSignIn]) {
        f_menu1 = @"G-幼儿评价-灰.png";
        f_menu3 = @"E-通知-灰.png";
        f_menu4 = @"A-今日食谱-灰.png";
        f_menu5 = @"B-历史食谱-灰.png";
        f_menu6 = @"E-自助配餐-灰.png";
        f_menu7 = @"D-历史配餐-灰.png";

    }
    {
        UIButton *btn = [ViewTool addUIButton:self.view imageName:@"F-资讯.png" type:@"" x:33 + (58+40)*(i%3)  y:20 + (58 +40)*(i/3) x1:57 y1:58];
        btn.tag = HealthInfoListControllertag;
        [self SetButtonTitle:btn Title:@"资讯"];
        [btn addTarget:self action:@selector(GoViewController:) forControlEvents:64];
        [self.view addSubview:btn];
        i++;
    }
    {
        UIButton *btn = [ViewTool addUIButton:self.view imageName:f_menu6 type:@"" x:33 + (58+40)*(i%3)  y:20 + (58 +40)*(i/3) x1:57 y1:58];
        btn.tag = PantryViewControllertag;
        [self SetButtonTitle:btn Title:@"自助配餐"];
        [btn addTarget:self action:@selector(GoViewController:) forControlEvents:64];
        i++;
    }
    {
        UIButton *btn = [ViewTool addUIButton:self.view imageName:f_menu7 type:@"" x:33 + (58+40)*(i%3)  y:20 + (58 +40)*(i/3) x1:57 y1:58];
        btn.tag = PantryHistroyViewControllertag;
        [self SetButtonTitle:btn Title:@"历史自助配餐"];
        [btn addTarget:self action:@selector(GoViewController:) forControlEvents:64];
        i++;
    }

    {
        UIButton *btn = [ViewTool addUIButton:self.view imageName:f_menu4 type:@"" x:33 + (58+40)*(i%3)  y:20 + (58 +40)*(i/3) x1:57 y1:58];
        btn.tag = RecipeViewControllertag;
        [self SetButtonTitle:btn Title:@"今日食谱"];
        [btn addTarget:self action:@selector(GoViewController:) forControlEvents:64];
        i++;
    }
    {
        UIButton *btn = [ViewTool addUIButton:self.view imageName:f_menu5 type:@"" x:33 + (58+40)*(i%3)  y:20 + (58 +40)*(i/3) x1:57 y1:58];
        btn.tag = RecipeHistroyViewControllertag;
        [self SetButtonTitle:btn Title:@"历史食谱"];
        [btn addTarget:self action:@selector(GoViewController:) forControlEvents:64];
        i++;
    }
    {
        UIButton *btn = [ViewTool addUIButton:self.view imageName:f_menu3 type:@"" x:33 + (58+40)*(i%3)  y:20 + (58 +40)*(i/3) x1:57 y1:58];
        btn.tag = NotifyListViewControllertag;
        [self SetButtonTitle:btn Title:@"通知"];
        [btn addTarget:self action:@selector(GoViewController:) forControlEvents:64];
        i++;
    }

    {
        UIButton *btn = [ViewTool addUIButton:self.view imageName:f_menu1 type:@"" x:33 + (58+40)*(i%3)  y:20 + (58 +40)*(i/3) x1:57 y1:58];
        btn.tag = ChildDisListsViewControllertag;
        [self SetButtonTitle:btn Title:@"幼儿评价"];
        [btn addTarget:self action:@selector(GoViewController:) forControlEvents:64];
        i++;
    }

}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [ViewTool addUIImageViewWithAutoSize:self.view imageName:@"viewblackgroup.jpg" type:@"" x:0 y:0 x1:0 y1:0];
    if (![Instance isNotSignIn]) {
        [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"登录" target:self selector:@selector(loadSignIn)];
    }
    [self initFunctionButton];
    NSLog(@"self.view.frame = %@",NSStringFromCGRect(self.view.frame));
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
