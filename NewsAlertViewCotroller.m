//
//  NewsAlertViewCotroller.m
//  Health
//
//  Created by hq on 12-7-10.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "NewsAlertViewCotroller.h"

@interface NewsAlertViewCotroller ()
@property(nonatomic,retain) UITableView *tableViews;

@end


@implementation NewsAlertViewCotroller
@synthesize tableViews;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)navBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)awakeFromNib
{
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];


    self.tableViews = [[UITableView alloc]initWithFrame:CGRectMake(0, 0,self.view.frame.size.width, [Instance getScreenHeight]) style:UITableViewStyleGrouped];
    self.tableViews.delegate = self;
    self.tableViews.dataSource = self;
    [self.tableViews setScrollEnabled:NO];
    [self.tableViews setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.tableViews];
}
-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            UILabel *label = [Instance addLable:CGRectMake(10, 0, 300, 40) tag:100011 size:15 string:@""];
            [cell.contentView addSubview:label];
            [label release];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    UILabel *label = ( UILabel * )[cell.contentView viewWithTag:100011];
    label.text = @"";
    if ([indexPath row] == 0) {
        label.text = @"当前版本:   V2.0.4";
    }
    if ([indexPath row] == 1) {
        label.text = @"客服热线:  400-108-1101";
    }    if ([indexPath row] == 2) {
        label.text = @"客服邮箱:  dinghao1101@126.com";
    }    if ([indexPath row] == 3) {
        label.text = @"    开发商:  广州鼎昊信息科技有限公司";
    }
    return cell;
}

-(void)dealloc
{
    [super dealloc];
    [self.tableViews  release];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.tableViews = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
