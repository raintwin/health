//
//  OptionalResultViewController.h
//  Health
//
//  Created by dinghao on 13-4-22.
//
//

#import <UIKit/UIKit.h>
#import "SysDetialMealViewController.h"
#import "ViewController.h"
@protocol OptionalResultDelegate <NSObject>

-(void)addMeals:(sysMeal*)meal;

@end

@interface OptionalResultViewController : ViewController<HttpFormatRequestDeleagte,UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate>
{
    int indexPage;
    id<OptionalResultDelegate>delegate;
    BOOL hasNext;

}
@property(nonatomic,assign)id<OptionalResultDelegate>delegate;

@end
