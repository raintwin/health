//
//  RecommendDinnerView.m
//  Health
//
//  Created by hq on 12-9-15.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "RecommendDinnerView.h"
#import "Recipe.h"
#import "FoodListViewController.h"
#import "FoodInfoViewController.h"

#define mTableHeight 30.0f
#define mTableHeaderHeight 40.0f
@implementation RecommendDinnerView

@synthesize mRecommendFoodTable,mFoodTable,mRecommendRecipeTable;
@synthesize mFoods,mRecommendFoods,mRecommendRecipes;
@synthesize mAnalysisResultDict,mConditionDict;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        mealID = 0;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)dealloc
{
    [mFoods release];
    [mRecommendFoods release];
    [mRecommendRecipes release];
    
    [mConditionDict release];
    [mAnalysisResultDict release];
    
    [mRecommendFoodTable release];
    [mFoodTable release];
    [mRecommendRecipeTable release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mFoods = nil;
    self.mRecommendFoods = nil;
    self.mRecommendRecipes = nil;
    
    self.mConditionDict = nil;
    self.mAnalysisResultDict = nil;
    
    self.mRecommendRecipeTable = nil;
    self.mFoodTable = nil;
    self.mRecommendFoodTable = nil;
}

#pragma mark - View lifecycle
-(void)navBack
{

    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark 更多食物
-(void)FrushRecommendFood
{
    
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/InHomeArrangeMeal/FetchOneMealRecommandFoodList"];
    NSMutableArray *mTemparray = [[NSMutableArray alloc]init];
    for (FoodResult *objClass  in mFoods) {
        NSMutableDictionary *mTempDict = [[NSMutableDictionary alloc]init];
        [mTempDict setObject:[NSNumber numberWithInt:objClass.foodNutrientDistributionID] forKey:@"foodNutrientDistributionID"];
        [mTempDict setObject:[NSNumber numberWithInt:objClass.foodKindIDBySSBT] forKey:@"foodKindIDBySSBT"];
        [mTemparray addObject:mTempDict];
        [mTempDict release];
    }
    [mConditionDict setObject:mTemparray forKey:@"selectedFoodList"];
    
    [mConditionDict setObject:[NSNumber numberWithInt:mealID] forKey:@"mealID"];
    [mConditionDict removeObjectForKey:@"selectedAndRecommandFoodList"];
    
    [mTemparray release];
    [mConditionDict writeToFile:[Instance GetDocumentByString:@"FOODLIST"] atomically:YES];
    
    NSString *JSONString = [mConditionDict JSONRepresentation];
    
    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate = self;
    [mHttpRequest CustomFormDataRequestDict:JSONString tag:10001 url:urlstr];
    [self.view addSubview:mHttpRequest];
    [mHttpRequest release];
}
-(void)MoreFood
{
    //BaiduMobStat* statTracker = [BaiduMobStat defaultStat];    [statTracker logEvent:@"frushfood" eventLabel:@"frushfood"];

    [self performSelector:@selector(FrushRecommendFood)];
}
#pragma mark 更多推荐
-(void)FrushRecommendRecipe
{
    
    
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/InHomeArrangeMeal/FetchOneMealRecommandMenuList"];
    NSMutableArray *mTemparray = [[NSMutableArray alloc]init];
    for (FoodResult *objClass  in mFoods) {
        NSMutableDictionary *mTempDict = [[NSMutableDictionary alloc]init];
        [mTempDict setObject:[NSNumber numberWithInt:objClass.foodNutrientDistributionID] forKey:@"foodNutrientDistributionID"];
        [mTempDict setObject:[NSNumber numberWithInt:objClass.foodKindIDBySSBT] forKey:@"foodKindIDBySSBT"];
        [mTemparray addObject:mTempDict];
        [mTempDict release];
    }
    for (FoodResult *objClass  in mRecommendFoods) {
        NSMutableDictionary *mTempDict = [[NSMutableDictionary alloc]init];
        [mTempDict setObject:[NSNumber numberWithInt:objClass.foodNutrientDistributionID] forKey:@"foodNutrientDistributionID"];
        [mTempDict setObject:[NSNumber numberWithInt:objClass.foodKindIDBySSBT] forKey:@"foodKindIDBySSBT"];
        [mTemparray addObject:mTempDict];
        [mTempDict release];
    }
    
    [mConditionDict setObject:mTemparray forKey:@"selectedAndRecommandFoodList"];
    [mConditionDict removeObjectForKey:@"selectedFoodList"];
    
    [mTemparray release];
        
    NSString *JSONString = [mConditionDict JSONRepresentation];
    
    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate = self;
    [mHttpRequest CustomFormDataRequestDict:JSONString tag:10002 url:urlstr];
    [self.view addSubview:mHttpRequest];
    [mHttpRequest release];
}
-(void)MoreRecomment
{
    //BaiduMobStat* statTracker = [BaiduMobStat defaultStat];[statTracker logEvent:@"frushrecipe" eventLabel:@"frushrecipe"];

    [self performSelector:@selector(FrushRecommendRecipe)];
}
#pragma  mark requestFinished
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag{
    if (tag == 10001) {
        [mRecommendFoods removeAllObjects];
        NSArray *array = [[[ResultContent JSONValue] valueForKey:@"foodList"] retain];
        
        for (int i = 0; i< [array count]; i++) {
            FoodResult *foodresult = [[FoodResult alloc]init];
            foodresult.foodNutrientDistributionID = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"foodNutrientDistributionID"]] intValue];
            foodresult.foodName = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"foodName"]];
            foodresult.foodKindNameBySSBT = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"foodKindNameBySSBT"]];
            foodresult.foodKindIDBySSBT = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"foodKindIDBySSBT"]] intValue];
            foodresult.foodPicUrl = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"foodPicUrl"]];
            foodresult.isMark = NO;
            [mRecommendFoods addObject:foodresult];
            [foodresult release];
        }
        [mRecommendFoodTable reloadData];
        [array release];
        
        NSArray *array2 = [[[ResultContent JSONValue] valueForKey:@"menuList"] retain];
        [mRecommendRecipes removeAllObjects];
        for (int i = 0; i< [array2 count]; i++) {
            recommendRecipe *recomrecipe = [[recommendRecipe alloc]init];
            recomrecipe.menuID = [[NSString stringWithFormat:@"%@",[[array2 objectAtIndex:i] valueForKey:@"menuID"]] intValue];
            recomrecipe.menuName = [NSString stringWithFormat:@"%@",[[array2 objectAtIndex:i] valueForKey:@"menuName"]];
            recomrecipe.menuPicUrl = [NSString stringWithFormat:@"%@",[[array2 objectAtIndex:i] valueForKey:@"menuPicUrl"]];
            [mRecommendRecipes addObject:recomrecipe];
            [recomrecipe release];
        }
        [array2 release];
        [mRecommendRecipeTable reloadData];
        
    }
    if (tag == 10002) {
        
        NSArray *array2 = [[[ResultContent JSONValue] valueForKey:@"menuList"] retain];
        [mRecommendRecipes removeAllObjects];
        for (int i = 0; i< [array2 count]; i++) {
            recommendRecipe *recomrecipe = [[recommendRecipe alloc]init];
            recomrecipe.menuID = [[NSString stringWithFormat:@"%@",[[array2 objectAtIndex:i] valueForKey:@"menuID"]] intValue];
            recomrecipe.menuName = [NSString stringWithFormat:@"%@",[[array2 objectAtIndex:i] valueForKey:@"menuName"]];
            recomrecipe.menuPicUrl = [NSString stringWithFormat:@"%@",[[array2 objectAtIndex:i] valueForKey:@"menuPicUrl"]];
            [mRecommendRecipes addObject:recomrecipe];
            [recomrecipe release];
        }
        [array2 release];
        [mRecommendRecipeTable reloadData];
    }
    
}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{}

-(void)InitDataArray
{
    {
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        self.mConditionDict = mDict;
        [mDict release];
    }
    {
        NSMutableArray *array = [[NSMutableArray alloc]init];
        self.mFoods = array;
        [array release];
    }
    {
        NSMutableArray *array = [[NSMutableArray alloc]init];
        self.mRecommendFoods = array;
        [array release];
    }
    {
        NSMutableArray *array = [[NSMutableArray alloc]init];
        self.mRecommendRecipes = array;
        [array release];
    }
}
#pragma mark viewDidLoad
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //BaiduMobStat* statTracker = [BaiduMobStat defaultStat];[statTracker logEvent:@"commentDinner" eventLabel:@"commentDinner"];

    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];
    mealID = 5;
    [self InitDataArray];

    
    [mConditionDict setObject:[NSNumber numberWithInt:mealID] forKey:@"mealID"];
    [mConditionDict setObject:[NSNumber numberWithInt:[Instance GetChildAgeOFINT]] forKey:@"ageID"];
    [mConditionDict setObject:[Instance GetChildGender] forKey:@"genderID"];
    [mConditionDict setObject:[Instance GetProvinceID] forKey:@"provinceID"];
    [mConditionDict setObject:[Instance GetCityID] forKey:@"cityID"];

    
    
    NSLog(@"mConditionDict -- :%@",[mConditionDict JSONRepresentation]);
    {
        UIImageView *titleImageview  = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
        [titleImageview setImage:nil];
//        [titleImageview setBackgroundColor:HEXCOLOR(0xfe9327)];
        [titleImageview setImage:[UIImage imageNamed:@"IMG_1489BG.png"]];

        [titleImageview setUserInteractionEnabled:YES];
        UILabel *label = [Instance addLable:titleImageview.frame tag:0 size:13 string:@"  在园供给"];
        label.font = [UIFont boldSystemFontOfSize:16];

        
        [titleImageview addSubview:label];
        [self.view addSubview:titleImageview];
        
        [label release];
        [titleImageview release];
    }    
    
    
    {
        UIImageView *titleImageview  = [[UIImageView alloc] initWithFrame:CGRectMake(0, 120, 320, 40)];
        [titleImageview setImage:nil];
        [titleImageview setImage:[UIImage imageNamed:@"IMG_1489BG.png"]];

        [titleImageview setUserInteractionEnabled:YES];
        
        UILabel *label = [Instance addLable:CGRectMake(0, 0, 320, 40) tag:0 size:13 string:@"  食物推荐"];
        label.font = [UIFont boldSystemFontOfSize:16];
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(260, 6, 51, 28)];
        [btn setBackgroundImage:[UIImage imageNamed:TWOENSURE] forState:0];
        [btn setTitle:@"更多" forState:0];
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        [btn addTarget:self action:@selector(MoreFood) forControlEvents:64];
        
        
        [titleImageview addSubview:btn];
        [titleImageview addSubview:label];
        [self.view addSubview:titleImageview];
        
        [btn release];
        [label release];
        [titleImageview release];
    }  
    {
        UIImageView *titleImageview  = [[UIImageView alloc] initWithFrame:CGRectMake(0, 240, 320, 40)];
        [titleImageview setImage:nil];
        [titleImageview setImage:[UIImage imageNamed:@"IMG_1489BG.png"]];

        [titleImageview setUserInteractionEnabled:YES];
        
        UILabel *label = [Instance addLable:CGRectMake(0, 0, 320, 40) tag:0 size:13 string:@"  菜谱推荐"];
        label.font = [UIFont boldSystemFontOfSize:16];

        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(260, 6, 51, 28)];
        [btn setBackgroundImage:[UIImage imageNamed:TWOENSURE] forState:0];
        [btn setTitle:@"更多" forState:0];
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        [btn addTarget:self action:@selector(MoreRecomment) forControlEvents:64];
        
        
        [titleImageview addSubview:btn];
        [titleImageview addSubview:label];
        [self.view addSubview:titleImageview];
        
        [btn release];
        [label release];
        [titleImageview release];
    }  
    
    
    {
        UITableView *table = [[UITableView alloc] initWithFrame:CGRectMake(0, 40,320, mTableHeight*2)];
        table.dataSource = self;
        table.delegate = self;
        [table setBackgroundColor:[UIColor clearColor]];
        self.mFoodTable = table;
        [table release];
        [self.view addSubview:mFoodTable];
    }
    {
        UITableView *table = [[UITableView alloc] initWithFrame:CGRectMake(0, 160,320, 80)];
        table.dataSource = self;
        table.delegate = self;
        [table setBackgroundColor:[UIColor clearColor]]; 
        self.mRecommendFoodTable = table;
        
        [table release];
        [self.view addSubview:mRecommendFoodTable];
    }  
    {
        UITableView *table = [[UITableView alloc] initWithFrame:CGRectMake(0, 280,320, 90)];
        table.dataSource = self;
        table.delegate = self;
        [table setBackgroundColor:[UIColor clearColor]];   
        self.mRecommendRecipeTable = table;
        [table release];
        [self.view addSubview:mRecommendRecipeTable];
    }
    
    [self performSelector:@selector(FrushRecommendFood)];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == mFoodTable) return mTableHeight*2;

    return  mTableHeight;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (tableView == mFoodTable) {
        return 1;
    }
    if (tableView == mRecommendFoodTable) {
        return [mRecommendFoods count];
    }
    if (tableView == mRecommendRecipeTable) {
        return [mRecommendRecipes count];
    }
    return 0;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.textLabel.text = nil;
    }
    if (tableView == mFoodTable) {
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        {
            NSString *str = [NSString stringWithFormat:@"热量 %0.2f 千卡",[[mAnalysisResultDict objectForKey:@"energyInSchoolSupply"] floatValue]];
            UILabel *label = [Instance addLableRect:CGRectMake(10,0, 150, mTableHeight) text:str textsize:15];
            [cell.contentView addSubview:label];
            [label release];
        }
        {
            NSString *str = [NSString stringWithFormat:@"碳水化合物 %0.2f 克",[[mAnalysisResultDict objectForKey:@"carbohydrateEnergyInSchoolSupply"] floatValue]];
            UILabel *label = [Instance addLableRect:CGRectMake(160,0, 150, mTableHeight) text:str textsize:15];
            [cell.contentView addSubview:label];
            [label release];
        }
        {
            NSString *str = [NSString stringWithFormat:@"脂肪 %0.2f 克",[[mAnalysisResultDict objectForKey:@"fatEnergyInSchoolSupply"] floatValue]];
            UILabel *label = [Instance addLableRect:CGRectMake(10,mTableHeight, 150, mTableHeight) text:str textsize:15];
            [cell.contentView addSubview:label];
            [label release];
        }     
        {
            NSString *str = [NSString stringWithFormat:@"蛋白质 %0.2f 克",[[mAnalysisResultDict objectForKey:@"porteinEnergyInSchoolSupply"] floatValue]];
            UILabel *label = [Instance addLableRect:CGRectMake(160,mTableHeight, 150, mTableHeight) text:str textsize:15];
            [cell.contentView addSubview:label];
            [label release];
        }
        
    }
    if (tableView == mRecommendFoodTable) {
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        FoodResult *FoodClass = [mRecommendFoods objectAtIndex:[indexPath row]];
        cell.textLabel.text = FoodClass.foodName;
        
    }
    if (tableView == mRecommendRecipeTable) {
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        recommendRecipe *FoodClass = [mRecommendRecipes objectAtIndex:[indexPath row]];
        cell.textLabel.text = FoodClass.menuName;
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (tableView == mRecommendFoodTable) {
        FoodResult *recipeObj = [mRecommendFoods objectAtIndex:[indexPath row]];
        
        FoodInfoViewController *detailViewController = [[FoodInfoViewController alloc] initWithStyle:UITableViewStylePlain];
        detailViewController.foodNutrientDistributionID = recipeObj.foodNutrientDistributionID;
        detailViewController.title = recipeObj.foodName;
        // ...
        // Pass the selected object to the new view controller.
        [self.navigationController pushViewController:detailViewController animated:YES];
        [detailViewController release];
        
        
    }
    if (tableView == mRecommendRecipeTable) {
        recommendRecipe *FoodClass = [mRecommendRecipes objectAtIndex:[indexPath row]];
        
        FoodListViewController *detailViewController = [[FoodListViewController alloc] initWithStyle:UITableViewStylePlain];
        detailViewController.title = FoodClass.menuName;
        detailViewController.menuID = FoodClass.menuID;
        detailViewController.mValue = 100;
        // ...
        // Pass the selected object to the new view controller.
        [self.navigationController pushViewController:detailViewController animated:YES];
        [detailViewController release];
        
        
        
    }
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
