//
//  SendPhotoViewController.h
//  Health
//
//  Created by hq on 12-7-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChildListViewController.h"
@interface SendPhotoViewController : UIViewController<UIAlertViewDelegate,UITextFieldDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,HttpFormatRequestDeleagte>

@end
