    //
//  YDTImageDownloader.m
//  YDTClient
//
//  Created by  hq on 11-10-27.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DownLoadTableImage.h"


@implementation DownLoadTableImage

@synthesize ImageURL = strImageURL;

@synthesize IndexPathTo = objIndexPathTo,Delegate = objDelegate;

@synthesize ImageData = objImageData;
@synthesize Connection = objConnection;

@synthesize ImageWidth = nImageWidth, ImageHeight = nImageHeight;
@synthesize tag;

#pragma mark -
#pragma mark override function
#pragma mark -

-(id) init
{
	if (self = [super init]) 
	{
		nImageWidth = nImageHeight = 30;
	}
	
	return self;
}
-(void) dealloc
{
	if (self.Connection) 
	{
		[self.Connection cancel];
	}
	[self.Connection release];
	[self.ImageData release];
	
	[self.ImageURL release];
    
	
	[self.IndexPathTo release];
	
    
    
	
	[super dealloc];
}


#pragma mark interface function to outside
#pragma mark -
-(BOOL) StartDownloadImage
{
    
    self.ImageData = [NSMutableData data];
    
    self.Connection = [[NSURLConnection alloc] initWithRequest:
					   [NSURLRequest requestWithURL:
						[NSURL URLWithString:self.ImageURL] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30] delegate:self];
    
    [self.Connection release];
	
	return YES;
}
-(BOOL) CancelDownloadImage
{
	if (self.Connection) 
	{
		[self.Connection cancel];
	}
	self.ImageData = nil;
	self.Connection = nil;
    
	self.ImageURL = nil;
    self.IndexPathTo = nil;
    
	return YES;
}






#pragma mark Download support (NSURLConnectionDelegate)
#pragma mark -
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.ImageData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
//	self.ImageData = nil;
//	self.Connection = nil;
	
    [self.Delegate FinishDownLoadImage:self.IndexPathTo Image:nil IsFailed:YES];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // Set appIcon and clear temporary data/image
    UIImage *image = [[UIImage alloc] initWithData:self.ImageData];
    
	self.ImageData = nil;
	self.Connection = nil;
	
	
	if (image.size.width == 0)
	{
        [self.Delegate FinishDownLoadImage:self.IndexPathTo Image:nil IsFailed:YES];
	}
	else 
	{
		if (image.size.width != nImageWidth && image.size.height != nImageHeight)
		{
			CGSize itemSize = CGSizeMake(nImageWidth, nImageHeight);
			UIGraphicsBeginImageContext(itemSize);
			CGRect imageRect = CGRectMake(0.0, 0.0, nImageWidth, nImageWidth);
			[image drawInRect:imageRect];
			[image release];
			image = UIGraphicsGetImageFromCurrentImageContext();
			[image retain];
			UIGraphicsEndImageContext();
		}	
        [self.Delegate FinishDownLoadImage:self.IndexPathTo Image:image  IsFailed:NO];
	}
	
	[image release]; 
    
}

@end




///---------------------------
///---------------------------
///---------------------------

//
//  YDTImageDownloader.m
//  YDTClient
//
//  Created by  hq on 11-10-27.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#pragma mark scrollview image

@implementation DownLoadScrollImage

@synthesize ImageURL = strImageURL;

@synthesize Delegate = objDelegate;

@synthesize ImageData = objImageData;
@synthesize Connection = objConnection;

@synthesize ImageWidth = nImageWidth, ImageHeight = nImageHeight;
@synthesize tag;

#pragma mark -
#pragma mark override function
#pragma mark -

-(id) init
{
	if (self = [super init]) 
	{
		nImageWidth = nImageHeight = 30;
	}
	
	return self;
}
-(void) dealloc
{
	if (self.Connection) 
	{
		[self.Connection cancel];
	}
	[self.Connection release];
	[self.ImageData release];
	
	[self.ImageURL release];
    
	
	
    
    
	
	[super dealloc];
}


#pragma mark interface function to outside
#pragma mark -
-(BOOL) StartDownloadImage
{
    
    self.ImageData = [NSMutableData data];
    
    self.Connection = [[NSURLConnection alloc] initWithRequest:
					   [NSURLRequest requestWithURL:
						[NSURL URLWithString:self.ImageURL] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30] delegate:self];
    
    [self.Connection release];
	
	return YES;
}
-(BOOL) CancelDownloadImage
{
	if (self.Connection) 
	{
		[self.Connection cancel];
	}
	self.ImageData = nil;
	self.Connection = nil;
    
	self.ImageURL = nil;
    
	return YES;
}






#pragma mark Download support (NSURLConnectionDelegate)
#pragma mark -
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.ImageData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    //	self.ImageData = nil;
    //	self.Connection = nil;
	NSLog(@"tag = %d",tag);
    [self.Delegate FinishDownLoadImageOfScrooll:tag Image:nil IsFailed:YES];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // Set appIcon and clear temporary data/image
    UIImage *image = [[UIImage alloc] initWithData:self.ImageData];
    
	self.ImageData = nil;
	self.Connection = nil;
	
	
	if (image.size.width == 0)
	{
        [self.Delegate FinishDownLoadImageOfScrooll:tag Image:nil IsFailed:YES];
	}
	else 
	{
        
			CGSize itemSize = CGSizeMake(image.size.width, image.size.height);
			UIGraphicsBeginImageContext(itemSize);
			CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
			[image drawInRect:imageRect];
			[image release];
			image = UIGraphicsGetImageFromCurrentImageContext();
			[image retain];
			UIGraphicsEndImageContext();
        [self.Delegate FinishDownLoadImageOfScrooll:tag Image:image IsFailed:NO];
	}
	
	[image release]; 
    
}

@end

///---------------------------
///---------------------------
///---------------------------

//
//  YDTImageDownloader.m
//  YDTClient
//
//  Created by  hq on 11-10-27.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#pragma mark scrollview image downiamge

@implementation DownLoadImageView

@synthesize ImageURL = strImageURL;

@synthesize Delegate = objDelegate;

@synthesize ImageData = objImageData;
@synthesize Connection = objConnection;

@synthesize ImageWidth = nImageWidth, ImageHeight = nImageHeight;
@synthesize tag;
//@synthesize photoName;
//@synthesize ImageView;

@synthesize obj;
#pragma mark -
#pragma mark override function
#pragma mark -

-(id) init
{
	if (self = [super init]) 
	{
		nImageWidth = nImageHeight = 30;
	}
	
	return self;
}
-(void) dealloc
{
	if (self.Connection) 
	{
		[self.Connection cancel];
	}
	[self.Connection release];
	[self.ImageData release];
	
	[self.ImageURL release];
    
	
    
    /// --------------- 
    
//	[ImageView release];
//    [photoName release];
    
    [obj release];
	
	[super dealloc];
}


#pragma mark interface function to outside
#pragma mark -
-(BOOL) StartDownloadImage
{
    
    self.ImageData = [NSMutableData data];
    
    self.Connection = [[NSURLConnection alloc] initWithRequest:
					   [NSURLRequest requestWithURL:
						[NSURL URLWithString:self.ImageURL] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30] delegate:self];
    
    [self.Connection release];
	
	return YES;
}
-(BOOL) CancelDownloadImage
{
	if (self.Connection) 
	{
		[self.Connection cancel];
	}
	self.ImageData = nil;
	self.Connection = nil;
    
	self.ImageURL = nil;
    
//    self.photoName = nil;
    self.obj = nil;
	return YES;
}






#pragma mark Download support (NSURLConnectionDelegate)
#pragma mark -
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.ImageData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    //	self.ImageData = nil;
    //	self.Connection = nil;
//    [self.Delegate FinishDownLoadImageOfScrooll:tag Image:nil IsFailed:YES];
//    if (ImageView != nil) {
//        ImageView.image = [UIImage imageNamed:@"acquiesce.jpg"];
//    }
//    obj.image = [UIImage imageNamed:@"acquiesce.jpg"];
    
    
    
    UIImage *image = [UIImage imageNamed:@"iconuse.png"];
    CGSize itemSize = CGSizeMake(nImageWidth, nImageHeight);
    UIGraphicsBeginImageContext(itemSize);
    CGRect imageRect = CGRectMake(0.0, 0.0, nImageWidth, nImageHeight);
    [image drawInRect:imageRect];
    [image release];
    image = UIGraphicsGetImageFromCurrentImageContext();
    [image retain];
    UIGraphicsEndImageContext();
    
    NSData *data = UIImagePNGRepresentation(image);
    
    NSString *file = [Instance GetDocumentByString:[NSString stringWithFormat:@"%d.png",obj.publishPersonID]];
    [data writeToFile:file atomically:YES];

    
    obj.isLoad = YES;
    [self.Delegate FinishDownLoadImageOfScrooll];
    [image release]; 

}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // Set appIcon and clear temporary data/image
    UIImage *image = [[UIImage alloc] initWithData:self.ImageData];
    
	self.ImageData = nil;
	self.Connection = nil;
	
	
	if (image.size.width == 0)
	{
        obj.image = [UIImage imageNamed:@"acquiesce.jpg"];
	}
	else 
	{
        
        CGSize itemSize = CGSizeMake(nImageWidth, nImageHeight);
        UIGraphicsBeginImageContext(itemSize);
        CGRect imageRect = CGRectMake(0.0, 0.0, nImageWidth, nImageHeight);
        [image drawInRect:imageRect];
        [image release];
        image = UIGraphicsGetImageFromCurrentImageContext();
        [image retain];
        UIGraphicsEndImageContext();
        
        NSData *data = UIImagePNGRepresentation(image);
        
        NSString *file = [Instance GetDocumentByString:[NSString stringWithFormat:@"%d.png",obj.publishPersonID]];
        NSLog(@"photo file :%@",file);
        [data writeToFile:file atomically:YES];
        
        obj.isLoad = YES;

        [self.Delegate FinishDownLoadImageOfScrooll];
	}
	
	[image release]; 
    
}

@end