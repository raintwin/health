//
//  ChatViewCell.h
//  Health
//
//  Created by dinghao on 13-5-9.
//
//

#import <UIKit/UIKit.h>

@interface ChatViewCell : UITableViewCell
{
    IBOutlet UILabel *nameLabel;
}
@property (retain, nonatomic) IBOutlet UILabel *nameLabel;

@end
