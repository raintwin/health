//
//  shareWeiBoViewController.m
//  Health
//
//  Created by dinghao on 13-5-27.
//
//

#import "shareWeiBoViewController.h"
#import "SHKItem.h"
#import "SHKActionSheet.h"


@interface shareWeiBoViewController ()

@end

@implementation shareWeiBoViewController
@synthesize imageView;
@synthesize shareInfo;

-(void)dealloc
{
    [super dealloc];
    [self.imageView release];
    [self.shareInfo release];
    
}
-(void)viewDidUnload
{
    [super viewDidUnload];
    self.imageView = nil;
    self.shareInfo = nil;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization


    }
    return self;
}
-(NSString*)filePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    documentDirectory =  [documentDirectory stringByAppendingPathComponent:@"big"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:documentDirectory]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:documentDirectory withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return documentDirectory;
}
-(NSString*)setFileFormat:(NSString *)String
{
    NSArray *list=[String componentsSeparatedByString:@"/"];
    
    return [list objectAtIndex:[list count]-1];
    
    
}
-(NSString *)documentFilePath:(NSString *)String
{
    NSString *document = [self filePath];
    return [document stringByAppendingPathComponent:[self setFileFormat:String]];
}
-(void)navBack
{
    self.navigationController.toolbarHidden = YES;
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.toolbarItems = [NSArray arrayWithObjects:
                         [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] autorelease],
                         [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(share)] autorelease],
                         [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] autorelease],
                         nil
                         ];
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];
    
    self.imageView = [[UIImageView alloc]initWithImage:[UIImage imageWithContentsOfFile:[self documentFilePath:shareInfo.picUrl]]];
	self.imageView.frame = CGRectMake(0,0,self.view.bounds.size.width,[Instance getScreenHeight]);
    NSLog(@"self.imageView.frame :%@",NSStringFromCGRect(self.imageView.frame));
	[self.imageView setContentMode:UIViewContentModeScaleAspectFit];
	[self.view addSubview:self.imageView];
	
    self.navigationController.toolbarHidden = NO;
}
- (void)share
{
	SHKItem *item = [SHKItem image:imageView.image title:@"说说"];
	SHKActionSheet *actionSheet = [SHKActionSheet actionSheetForItem:item];
	
	[actionSheet showFromToolbar:self.navigationController.toolbar];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
