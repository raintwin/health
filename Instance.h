//
//  GetUser.h
//  Health
//
//  Created by hq on 12-7-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
#import "Recipe.h"

@interface Instance : NSObject
#pragma mark UINavigationItem 
+(void)setRightImageBtnForNavigationItem:(UINavigationItem*)item Image:(NSString*) image target:(id) target selector:(SEL) selector;
+(void)setLeftImageBtnForNavigationItem:(UINavigationItem*)item Image:(NSString*) image target:(id) target selector:(SEL) selector;

+(void)setRightImageAndNameForNavigationItem:(UINavigationItem*)item Image:(NSString*) image title:(NSString*)title target:(id)target selector:(SEL) selector;

+(void)setNavigationItemRightTwo:(UINavigationItem*)item buttons:(NSMutableArray*)ButtonArray target:(id)target
                        selector:(SEL) selector;
+(void) setLeftImageAndNameForNavigationItem:(UINavigationItem*)item Image:(NSString*) image title:(NSString*)title target:(id)target selector:(SEL) selector;


#pragma mark TextFileld
+(UITextField*)addTextFileld:(id)target originX:(float)x originY:(float)y width:(float)width height:(float)height;
+(UITextField*)addTextFileld:(id)trage frame:(CGRect)frame;

+(UITextField*)addTextFileldForView:(UIView*)SelfView frame:(CGRect)frame;


+(UITextView*)addTextView:(id)target originX:(float)x originY:(float)y width:(float)width height:(float)height;
+(UITextView*)addTextView:(id)target frame:(CGRect)frame;

#pragma mark UILabel
+(UILabel*)addLablePoint:(CGPoint)point text:(NSString*)text textsize:(float)textsize;
+(UILabel*)addLableRect:(CGRect)rect  text:(NSString*)text textsize:(float)textsize;
+(UILabel*)addLable:(CGRect)rect tag:(int)tag size:(int)size string:(NSString*)text;
#pragma mark button
+(UIButton *)addUIButton:(UIView *)fview Title:(NSString*)Title  rect:(CGRect)rect;
+(UIButton *)addUIButtonByImage:(UIView *)fview image:(NSString*)imagename  rect:(CGRect)rect;
+(UIButton *)addUIButtonOfImage:(NSString *)image Title:(NSString*)Title  rect:(CGRect)rect;
+(void)baiduApi:(NSString*)logevent eventlabel:(NSString*)eventlabel;
#pragma mark getdocument

+(BOOL)IsGetDocument:(NSString*)File;
+(NSString*)GetDocument;
+(NSString*)GetDocumentByString:(NSString*)string;

+(NSString*)GetUseID;
+(NSString*)GetChildID;
+(NSString*)GetChildName;
+(NSString*)GetChildAge;
+(BOOL)getCharacter;
+(int)GetChildAgeOFINT;
+(NSString *)getSignInPrompt;

+(NSString*)GetChildSchoolAndClass;
+(NSString*)GetIconFile;
+(NSString*)GetProvince;
+(NSString*)GetProvinceID;
+(NSString*)GetCity;
+(NSString*)GetCityID;
+(NSNumber*)GetChildGender;
+(NSString*)GetSystemDate;
+(float)getScreenHeight;

+(BOOL)GetUseType;
+(NSString*)GetUsePhotoUrl;

//-------GlobalsingleRecognizer
+(void)ShowGlobalsingleRecognizer;
+(void)DissGlobalsingleRecognizer;

+(BOOL)isNotExistIDofDateTable:(NSString*)Date;
+(void)SaveDate:(NSString*)SystemDate;


+(int)GetDataID:(NSString*)Dat;


+(void)SaveMeal:(int)FatherID MealTypeID:(int)mealId;
+(BOOL)isNotExistIDofMealTable:(int)FatherID mealTypeId:(int)mealID;
+(int)GetMealID:(int)DateID;

+(BOOL)SaveFood:(int)FatherID  FoodType:(int)type food:(FoodResult*)FoodClass;

+(void)DeleFood:(int)FatherID FoodType:(int)Type;


+(BOOL)SaveRecommenRecipe:(int)FatherID  food:(recommendRecipe*)FoodClass;
+(void)DeleRecommenRecipe:(int)FatherID;
+(void)DeleRecommenRecipeOne:(int)FatherID mealID:(int)mealID;

+(void)QuestFoodTable:(NSMutableArray *)array SQL:(NSString*)StrSQL;
+(void)QuestRecommentRecipe:(NSMutableArray *)array SQL:(NSString*)StrSQL;


+(BOOL)isNotExistComMealData:(int)FatherId;

//------------------- 记录是否已经登录
+(BOOL)isNotSignIn;
+(void)signIn:(BOOL)is;

@end



@interface HealthHeaders : NSObject 

+(BOOL)DisposeRequest:(ASIHTTPRequest *)request;
+(void)FailedDisposeRequest:(ASIHTTPRequest *)request;
+(void)SVProgressView:(UIView *)mview;
+(void)SVProgressDiss:(UIView *)mview;
+(void)CancelDownLoad:(NSMutableArray*)array  mdictDownLoad:(NSMutableDictionary*)mDownLoadImageDict;
@end