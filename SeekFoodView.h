//
//  SeekFoodView.h
//  Health
//
//  Created by hq on 12-9-15.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Province.h"
#import "City.h"
#import "ConditionView.h"
#import "MusButton.h"


@protocol SeekFoodViewDelegate <NSObject>

-(void)ReturnQuestResult:(NSMutableArray*)array condition:(NSMutableDictionary*)mDict;

@end

@interface SeekFoodView : UIViewController<HttpFormatRequestDeleagte,UITextFieldDelegate,ProvinceDelegate,CityDelegate,ConditionDelegate,UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>
{
    MusButton *mBtnsView;
    int mPageStart;
    
    BOOL isMore;

    
    id<SeekFoodViewDelegate>delegate;
}


-(void)ReturnResult:(NSMutableArray*)array;



@property(nonatomic,assign)int mealID;
@property(nonatomic,retain)UITextField *mTmepTextField,*mFoodTextField;
@property(nonatomic,retain)NSMutableDictionary *mConditionDict;
@property(nonatomic,assign)id<SeekFoodViewDelegate>delegate;

@property(nonatomic,retain)NSMutableArray *mFoods;
@property(nonatomic,retain)NSMutableArray *mResults;
@property(nonatomic,retain)UITableView *mSeekFoodResultTable;
@end


