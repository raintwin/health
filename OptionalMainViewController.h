//
//  OptionalMainViewController.h
//  Health
//
//  Created by dinghao on 13-4-22.
//
//

#import <UIKit/UIKit.h>

@interface OptionalMainViewController : UIViewController

@property(retain,nonatomic) UIViewController *viewControllerDelegate;

@end
