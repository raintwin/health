//
//  OptionalResultViewController.m
//  Health
//
//  Created by dinghao on 13-4-22.
//
//

#import "OptionalResultViewController.h"

#define TableViewHeiht 35
#define RequreCount 300
@interface OptionalResultViewController ()
@property(retain,nonatomic)HttpFormatRequest *mHttpRequest;

@property(retain,nonatomic)NSMutableDictionary *mConditionDict;
@property (nonatomic, retain) UITableView *tableview;
@property(nonatomic,retain)NSMutableArray *mMeals;
@property(nonatomic,retain)UITextField *conditionLabel;
@property(nonatomic,retain)UIButton *searchButton;
@end

@implementation OptionalResultViewController
@synthesize delegate;
@synthesize mHttpRequest;
@synthesize mConditionDict;
@synthesize tableview;
@synthesize mMeals;
@synthesize conditionLabel;
@synthesize searchButton;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    
    }
    return self;
}

-(void)dealloc
{
    [self.mHttpRequest release];
    [self.mConditionDict release];
    [self.tableview release];
    [self.mMeals release];
    [self.conditionLabel release];
    [self.searchButton release];

    [super dealloc];
}
-(void)viewDidUnload
{
    [super viewDidUnload];
    self.mHttpRequest = nil;
    self.mConditionDict = nil;
    self.tableview = nil;
    self.mMeals = nil;
    self.conditionLabel = nil;
    self.searchButton = nil;

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.mMeals = [[NSMutableArray alloc]init];
    indexPage = 1;
    hasNext = NO;
    {
        [ViewTool addUIImageView:self.view imageName:@"mealbackgroup.jpg" type:@"" x:0 y:0 x1:320 y1:416];
        [ViewTool addUIImageView:self.view imageName:@"search.jpg" type:@"" x:27 y:30 x1:266 y1:327];
        
        

    }

    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    ;

    self.mConditionDict = [[NSMutableDictionary alloc]init];
    [self.mConditionDict setObject:@"" forKey:@"name"];
    [self.mConditionDict setObject:[NSNumber numberWithInt:[[defaults objectForKey:@"mealType"] intValue]] forKey:@"mealId"];
    [self.mConditionDict setObject:[NSNumber numberWithInt:indexPage] forKey:@"page"];
    [self.mConditionDict setObject:[NSNumber numberWithInt:RequreCount] forKey:@"everyPage"];

    
    [self.mConditionDict setObject:@"394" forKey:NSLocalizedString(@"PORT_ReqType", nil)];
    [self.mConditionDict setObject:[NSNumber numberWithInt:1] forKey:NSLocalizedString(@"HT_MoblieType", nil)];

    self.mHttpRequest = [[HttpFormatRequest alloc]init];
    self.mHttpRequest.Delegate = self;
    [self.view addSubview:self.mHttpRequest];
    
    [mHttpRequest CustomFormDataRequestDict:[self.mConditionDict JSONRepresentation] tag:0 url:WEBSERVERURL];

    
    self.tableview = [[UITableView alloc] initWithFrame:CGRectMake(35, 86, 250, 245) style:UITableViewStylePlain];
    self.tableview.backgroundColor = [UIColor clearColor];
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
	self.tableview.showsVerticalScrollIndicator = YES;
	
	[self.view addSubview:self.tableview];

    self.conditionLabel = [[UITextField alloc]initWithFrame:CGRectMake(70, 46, 150, 20)];
    self.conditionLabel.delegate = self;
    self.conditionLabel.text = @"";
    self.conditionLabel.placeholder = @"菜谱名称/食品名称";
    self.conditionLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.conditionLabel];
    
    self.searchButton = [[UIButton alloc]initWithFrame:CGRectMake(235, 46, 27, 21)];
    [self.searchButton setHighlighted:YES];
    [self.searchButton setBackgroundColor:[UIColor clearColor]];
    [self.searchButton addTarget:self action:@selector(searchMeal) forControlEvents:64];
    [self.view addSubview:self.searchButton];
}
-(void)searchMeal
{
    
    [self.mConditionDict setObject:[NSNumber numberWithInt:1] forKey:@"page"];
    [self.mConditionDict setObject:self.conditionLabel.text forKey:@"name"];
    [mHttpRequest CustomFormDataRequestDict:[self.mConditionDict JSONRepresentation] tag:0 url:WEBSERVERURL];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.conditionLabel resignFirstResponder];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.

    return [mMeals count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return TableViewHeiht;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    for (UIView *view in [cell.contentView subviews]) {
        [view removeFromSuperview];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];

    sysMeal *meal = [self.mMeals objectAtIndex:[indexPath row]];
    cell.textLabel.text = [NSString stringWithFormat:@"   %@",meal.name];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
    {
        UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(200, 3, 26, 28)];
        button.tag = [indexPath row];
        [button setBackgroundImage:[UIImage imageNamed:@"addButton"] forState:0];
        [button addTarget:self action:@selector(confirmMeal:) forControlEvents:64];
        [cell.contentView addSubview:button];
        [button release];
    }
    // Configure the cell...
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([indexPath row] >= [self.mMeals count]) {
        return ;
    }
    sysMeal *sysmeal = (sysMeal *)[self.mMeals objectAtIndex:[indexPath row]];
    SysDetialMealViewController *detailViewController = [[SysDetialMealViewController alloc]initWithNibName:@"SysDetialMealViewController" bundle:nil];
    [detailViewController.view setFrame:self.view.frame];
    detailViewController.sysmeal = sysmeal;
    detailViewController.title = sysmeal.name;
    [self.navigationController pushViewController:detailViewController animated:YES];
    


}
-(void)confirmMeal:sender
{
    UIButton *button = sender;
    sysMeal *meal = [self.mMeals objectAtIndex:button.tag];
    [delegate addMeals:meal];
    [self backNav];
}
#pragma mark HttpRequest
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{
}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    [self.mMeals removeAllObjects];
    NSArray *array = [[[ResultContent JSONValue] valueForKey:@"menuList"] retain];
    if ([array count] > 0) {
        for (int i = 0; i<[array count]; i++) {
            sysMeal *meal = [[sysMeal alloc]init];
            meal.ID = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"id"]] intValue];
            meal.name = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"name"]];
            meal.uType = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"uType"]];
            [self.mMeals addObject:meal];
            [meal release];
        }
    }
    [array release];
    hasNext = [[[ResultContent JSONValue] valueForKey:@"hasNext"] boolValue];
    [self.tableview reloadData];
    NSLog(@"count :%d",[mMeals count]);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
