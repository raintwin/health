//
//  PaperListViewController.m
//  Health
//
//  Created by hq on 12-6-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "PaperListViewController.h"
#import "PaperViewController.h"
@implementation PaperListViewController

#define tableWidths 82
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
//        // Custom initialization
//        {
//            UITabBarItem *item = [[UITabBarItem alloc]initWithTitle:@"留言" image:[UIImage imageNamed:@"nav_32.png"] tag:3];
//            self.tabBarItem = item;
//            [item release];
//            
//        }

    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)navBack
{
   [self.navigationController popViewControllerAnimated:YES];
}
-(void)RefreshView
{
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [ViewTool addUIImageViewWithAutoSize:self.view imageName:@"viewblackgroup.jpg" type:@"" x:0 y:0 x1:0 y1:0];

    // Do any additional setup after loading the view from its nib.
//    if([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)]){
//        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"img_44" ofType:@"jpg" ] ] forBarMetrics:UIBarMetricsDefault];
//        NSLog(@"%@",@"device is 5.0");
//    }

    
    {
        UIButton *Refresh = [[UIButton alloc]initWithFrame:CGRectMake(0, 0,34, 28)];
        [Refresh addTarget:self action:@selector(RefreshView) forControlEvents:64];
        [Refresh setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"img_07.png" ofType:@""] ] forState:0];
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc]initWithCustomView:Refresh];
        self.navigationItem.rightBarButtonItem = backItem;
        [Refresh release];
        [backItem release];
        
    }
    {
        UIButton *back = [[UIButton alloc]initWithFrame:CGRectMake(0, 0,58, 29)];
        [back addTarget:self action:@selector(navBack) forControlEvents:64];
        [back setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"backbutton.png" ofType:@""] ] forState:0];
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc]initWithCustomView:back];
        self.navigationItem.leftBarButtonItem = backItem;
        [back release];
        [backItem release];
        
    }

    
    
    {
        UITableView *mytable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,372)];
        mytable.delegate = self;
        mytable.dataSource = self;
        mytable.backgroundColor = [UIColor clearColor];
        [self.view addSubview:mytable];
        arrays = [[NSMutableArray alloc]init];
        
        for (int i = 0 ; i<10; i++) {
//            [arrays addObject:[NSString stringWithFormat:@"cell -  %li",i]];
        }

    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"dd");

    PaperViewController *pvCtr = [[PaperViewController alloc] initWithNibName:@"PaperViewController" bundle:nil];
    pvCtr.title = @"留 言";
    [self.navigationController pushViewController:pvCtr animated:YES];
    [pvCtr release];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return tableWidths;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    int count = [arrays count];
	return [arrays count];	
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 
    static NSString * cellName = @"PaperListViewController";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier: cellName] autorelease];
    }
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.imageView.image = [UIImage imageNamed:@"tou_50.png"];

    
    NSString *nameStr = @"昵称 :";
    NSString *timeStr = @"时间 :";
        

        
        {
            CGSize rect1 = [nameStr sizeWithFont:[UIFont boldSystemFontOfSize:13]];
            
            UILabel *parentText = [[UILabel alloc]initWithFrame:CGRectMake(70 , 11, rect1.width, rect1.height)];
            parentText.font = [UIFont boldSystemFontOfSize:13];
            [parentText setBackgroundColor:[UIColor clearColor]];
//            [parentText setHighlightedTextColor:[UIColor whiteColor]];

            parentText.text = nameStr;
            [cell.contentView addSubview:parentText];
            [parentText release];
            
            
            CGSize rect2 = [timeStr sizeWithFont:[UIFont boldSystemFontOfSize:12]];
            UILabel *childText = [[UILabel alloc]initWithFrame:CGRectMake(75 , parentText.frame.origin.y + parentText.frame.size.height + 3 , rect2.width, rect2.height)];
            childText.tag = [indexPath row];
            [childText setBackgroundColor:[UIColor clearColor]];
            childText.font = [UIFont boldSystemFontOfSize:12];
            childText.text = timeStr;
            [cell.contentView addSubview:childText];
            [childText release];
            
        }

        {
            UIImageView *lineImg = [[UIImageView alloc] initWithFrame:CGRectMake(19, 80, 282, 1)];
            [lineImg setImage:[UIImage imageNamed:@"xian_03.png"]];
            [cell.contentView addSubview:lineImg];
            [lineImg release];
            
        }
        
 
    return cell;

}
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{  
    //  [self.view addSubview:viewController.view];  
//    tabBarController.selectedViewController = viewController;
    viewController.tabBarItem.badgeValue = [NSString stringWithFormat:@"%d",80];  
    //  viewController.tabBarItem.title = @"aaa";  
}  -(void)dealloc
{

   
   [arrays release];
   arrays = nil;

   [super dealloc];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
