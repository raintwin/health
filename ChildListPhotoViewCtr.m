//
//  ChildListPhotoViewCtr.m
//  Health
//
//  Created by jiaodian on 12-11-22.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ChildListPhotoViewCtr.h"
#import "PhotoFolderViewController.h"
#import "JSON.h"
#import "DownLoadTableImage.h"
#import "CChild.h"
#define ePageCount 10
#define TABLAHIGHT 50
@interface ChildListPhotoViewCtr ()

@end

@implementation ChildListPhotoViewCtr
@synthesize OnHome;
@synthesize isInhomeOrSchool;
@synthesize mDownLoadImageDict;
@synthesize mTableTemplateView;- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        OnHome = NO;
        isInhomeOrSchool = NO;

    }
    return self;
}
-(void)CancelDownLoad
{
    for (int i = 0; i<[mTableTemplateView.requestDatas count]; i++) {
        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:i inSection:0];
        DownLoadTableImage *imageDownLoad = [mDownLoadImageDict objectForKey:indexpath];
        if (imageDownLoad != nil) {
            [imageDownLoad CancelDownloadImage];
        }
    }
    [mDownLoadImageDict removeAllObjects];
}
-(void)navBack
{
    [self CancelDownLoad];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    NSMutableDictionary *mDictionary = [[NSMutableDictionary alloc]init];
    self.mDownLoadImageDict = mDictionary;
    [mDictionary release];
    
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];
//    [[UIDevice currentDevice] systemName]
    {
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        [mDict setObject:[Instance GetUseID] forKey:@"ID"];
        [mDict setObject:[NSNumber numberWithBool:isInhomeOrSchool] forKey:@"inSchoolOrInHome"];
        
        TableTemplateView *table = [[TableTemplateView alloc]initWithFrame:CGRectMake(0, 0, 320, 372)];
        [table setTableHeightForRow:TABLAHIGHT];
        [table setUrlString:[NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/Photo/TeacherFetchChildPhotoRecordsOfEveryBaby"]];
        table.DataDict = mDict;
        table.delegate = self;
        table.isLoadImage = YES;
        self.mTableTemplateView = table;
//        mTableTemplateView.isCase = YES;
        [table release];
        
        [self.view addSubview:mTableTemplateView];
        [mDict release];
        [mTableTemplateView GetDataForHttp];
    } 


    
}
-(void)ReturnHttpData:(NSString*)ResultContent container:(NSMutableArray*)arrays tag:(int)tag isFinish:(BOOL)isFinish;
{
    NSLog(@"ResultContent--:%@",ResultContent);
    
    NSArray *array = [[[ResultContent JSONValue] valueForKey:@"childPhotoRecordsOfEveryBody"] retain];
    
    mTableTemplateView.isLoadOver = YES;
    for (int i = 0; i < [array count]; i++) {
        ChildOfPhoto *mChildObj = [[ChildOfPhoto alloc] init];
        
        mChildObj.cID = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childID"]] intValue];
        mChildObj.cName = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childName"]];
        mChildObj.cPhotoUrl = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childPhoto"]];
        mChildObj.mRecorder = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childPhotoRecords"]] intValue];
        mChildObj.image = nil;
        mChildObj.isLoad = NO;
        [arrays addObject:mChildObj];
        [mChildObj release];
    }    
    [array release];
    
    [mTableTemplateView setRequestDatas:arrays];
    
    //-----
    if ([arrays count] == 0 && isFinish == NO) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"加载失败" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alertView.delegate = self;
        [alertView show];
        [alertView release];
    }
    if ([arrays count] == 0 && isFinish == YES) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"暂无幼儿" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alertView.delegate = self;
        [alertView show];
        [alertView release];
    }
}
-(void)SetTableData:(UITableViewCell*)cell indexpath:(NSIndexPath*)indexPath datas:(NSMutableArray *)mDatas;
{
    NSInteger row = [indexPath row];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    
    ChildOfPhoto *sif = [mDatas objectAtIndex:row];
    NSString *childStr =  [NSString stringWithFormat:@"%@   图片记录数:%d",sif.cName ,sif.mRecorder] ;
    cell.textLabel.font = [UIFont boldSystemFontOfSize:15];
    cell.textLabel.text = @"";
    
    UIImage *image =[UIImage imageNamed:@"acquiesce.jpg"];
    
    cell.imageView.image = image;
    // --------------------- load image
    {
        if (!sif.isLoad) {
            if (mTableTemplateView.mTableView.dragging == NO && mTableTemplateView.mTableView.decelerating == NO) {
                if ([sif.cPhotoUrl length] != 0) {
                    [self StartDownLoadImage:sif.cPhotoUrl IndexPath:indexPath];
                }
                else
                {
                    sif.image = [UIImage imageNamed:@"acquiesce.jpg"];
                    sif.isLoad = YES;
                }
            }
        }
        else
        {
            cell.imageView.image = sif.image;
        }
    }
    cell.textLabel.text = childStr;
    
    
}

-(void)TableDidSelect:(NSIndexPath*)indexPath Datas:(NSMutableArray*)mDatas;
{
    ChildOfPhoto *cop = [mTableTemplateView.requestDatas objectAtIndex:[indexPath row]];
    
    PhotoFolderViewController *detailViewController = [[PhotoFolderViewController alloc] initWithNibName:@"PhotoFolderViewController" bundle:nil];
    detailViewController.mChildID = cop.cID;
    detailViewController.isInhomeOrSchool = isInhomeOrSchool;
    
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];
}

#pragma mark  load imageview 
-(void)StartDownLoadImage:(NSString*)url IndexPath:(NSIndexPath*)indexpath 
{
    DownLoadTableImage *imageDownLoad = [mDownLoadImageDict objectForKey:indexpath];
    if (imageDownLoad == nil) {
        imageDownLoad = [[DownLoadTableImage alloc] init];
        imageDownLoad.ImageURL = url;
        imageDownLoad.ImageHeight = 40;
        imageDownLoad.ImageWidth = 40;
        imageDownLoad.IndexPathTo = indexpath;
        imageDownLoad.Delegate =self;
        [mDownLoadImageDict setObject:imageDownLoad forKey:indexpath];
        [imageDownLoad StartDownloadImage];
        [imageDownLoad release];
    }
}
#pragma mark finish download image
-(void)FinishDownLoadImage:(NSIndexPath*)indexpath Image:(UIImage*)image IsFailed:(BOOL)isFaild
{
    DownLoadTableImage *imageDownLoad = [mDownLoadImageDict objectForKey:indexpath];
    
    if (imageDownLoad != nil) {
        UITableViewCell *cell = [mTableTemplateView.mTableView cellForRowAtIndexPath:indexpath];
        ChildOfPhoto *obj = [mTableTemplateView.requestDatas objectAtIndex:[indexpath row]];
        if (isFaild) {
            obj.image = [UIImage imageNamed:@"acquiesce.jpg"];
            obj.isLoad = NO;
        }
        else
        {
            obj.image = image;
            obj.isLoad = YES;
        }
        cell.imageView.image =  obj.image ;
    }
}
- (void)loadImagesForOnscreenRows
{
    NSArray *visiblePaths = [mTableTemplateView.mTableView indexPathsForVisibleRows];
    for (NSIndexPath *indexPath in visiblePaths)
    {
        if ([indexPath row]<[mTableTemplateView.requestDatas count]) {
            ChildOfPhoto *item = [mTableTemplateView.requestDatas objectAtIndex:indexPath.row];
            
            if (!item.isLoad ) // avoid the app icon download if the app already has an icon
            {
                [self StartDownLoadImage:item.cPhotoUrl IndexPath:indexPath];
            }
            
        }
    }
}
-(void)TableDownLoadCellImage
{
    [self loadImagesForOnscreenRows];
}
-(void)dealloc
{
    [mDownLoadImageDict release];
    [mTableTemplateView release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mDownLoadImageDict = nil;
    self.mTableTemplateView = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
