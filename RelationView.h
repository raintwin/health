//
//  RelationView.h
//  Health
//
//  Created by jiaodian on 12-10-12.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol RelationViewDelegate <NSObject>

-(void)SetRelation:(NSString*)Relation gender:(int)gender;

@end

@interface RelationView : UITableViewController
{
    id<RelationViewDelegate>deleget;
}
@property(nonatomic,assign)id<RelationViewDelegate>deleget;

@end
