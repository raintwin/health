//
//  OptionalMealViewController.h
//  Health
//
//  Created by dinghao on 13-4-22.
//
//

#import <UIKit/UIKit.h>
#import "OptionalResultViewController.h"
#import "ViewController.h"
@interface OptionalMealViewController : ViewController<HttpFormatRequestDeleagte,UITableViewDelegate, UITableViewDataSource,OptionalResultDelegate>

-(void)addMeals:(sysMeal*)meal;

@end
