//
//  DownLoadTableImage.h
//  Health
//
//  Created by hq on 12-8-7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CChild.h"
@protocol DownLoadImageDelegate

-(void)FinishDownLoadImage:(NSIndexPath*)indexpath Image:(UIImage*)image IsFailed:(BOOL)isFaild;
@end

@interface DownLoadTableImage : NSObject 
{
	int        nImageWidth;
	int		   nImageHeight;
	NSString* strImageURL;
    
	
	NSIndexPath *objIndexPathTo;
    id<DownLoadImageDelegate> objDelegate;
    
    NSMutableData *objImageData;
    NSURLConnection *objConnection;
}


@property (nonatomic,assign) int ImageWidth;
@property (nonatomic,assign) int ImageHeight;

@property (nonatomic, retain) NSString* ImageURL;


@property (nonatomic, retain) NSIndexPath* IndexPathTo;
@property (nonatomic, assign) id<DownLoadImageDelegate> Delegate;

@property (nonatomic,retain) NSMutableData* ImageData;
@property (nonatomic,retain) NSURLConnection* Connection;

@property(nonatomic,assign)int tag;

-(BOOL) StartDownloadImage;
-(BOOL) CancelDownloadImage;

@end

/// ------------ 


#pragma mark 下载图片

@protocol DownLoadImageOfScrollDelegate

-(void)FinishDownLoadImageOfScrooll:(int)index Image:(UIImage*)image IsFailed:(BOOL)isFaild;
@end

@interface DownLoadScrollImage : NSObject 
{
	int        nImageWidth;
	int		   nImageHeight;
	NSString* strImageURL;
    
	
    id<DownLoadImageOfScrollDelegate> objDelegate;
    
    NSMutableData *objImageData;
    NSURLConnection *objConnection;
}


@property (nonatomic,assign) int ImageWidth;
@property (nonatomic,assign) int ImageHeight;

@property (nonatomic, retain) NSString* ImageURL;


@property (nonatomic, assign) id<DownLoadImageOfScrollDelegate> Delegate;

@property (nonatomic,retain) NSMutableData* ImageData;
@property (nonatomic,retain) NSURLConnection* Connection;

@property(nonatomic,assign)int tag;

-(BOOL) StartDownloadImage;
-(BOOL) CancelDownloadImage;

@end


/// ----------------- 


#pragma Down Image of peper
@protocol DownImageIconFileDelegate

-(void)FinishDownLoadImageOfScrooll;

@end
@interface DownLoadImageView : NSObject 
{
	int        nImageWidth;
	int		   nImageHeight;
	NSString* strImageURL;
    
    id<DownImageIconFileDelegate> objDelegate;

    
    NSMutableData *objImageData;
    NSURLConnection *objConnection;
}


@property (nonatomic,assign) int ImageWidth;
@property (nonatomic,assign) int ImageHeight;

@property (nonatomic, retain) NSString* ImageURL;



@property (nonatomic,retain) NSMutableData* ImageData;
@property (nonatomic,retain) NSURLConnection* Connection;

@property(nonatomic,retain)PaperInfo *obj;
//@property (nonatomic,retain)UIImageView *ImageView;
//@property (nonatomic,retain)NSString *photoName;
@property (nonatomic, assign) id<DownImageIconFileDelegate> Delegate;


@property(nonatomic,assign)int tag;

-(BOOL) StartDownloadImage;
-(BOOL) CancelDownloadImage;

@end

