//
//  RegistersViewController.h
//  Health
//
//  Created by hq on 12-7-5.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistersViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,HttpFormatRequestDeleagte>
{
}

@property(nonatomic,retain)NSMutableDictionary *RegistInfo;
@end
