//
//  FoodMenuAnalysisController.h
//  Health
//
//  Created by hq on 12-6-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodMenuAnalysisController : UITableViewController<HttpFormatRequestDeleagte>
{
    BOOL mIsDownOver;
}
-(BOOL)isFloat:(NSString*)string;
@property(nonatomic,assign)BOOL isDinner;
@property(nonatomic,retain)NSMutableDictionary *mAnysisRecipeDict,*mAnalysisResultDict;
@end
