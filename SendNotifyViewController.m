//
//  SendNotifyViewController.m
//  Health
//
//  Created by hq on 12-6-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "SendNotifyViewController.h"
#import "ChildListViewController.h"
#import "Reachability.h"
#import "JsonInstance.h"
#import "JSON.h"
#import "SVProgressHUD.h"
#import "CChild.h"
#import "UIPlaceHolderTextView.h"

#define EPersonIDsOfNotice @"PersonIDsOfNotice"

#define IViewTag    100022
#define SCHILD      1000301
#define STITLE      1000302
#define SCONTENT    1000303


@implementation SendNotifyViewController
@synthesize mChilds,mChildID;
@synthesize mCNameText,mNotifyTitle;
@synthesize mDisText;
@synthesize isAnd,isPhoto,isText,isTitle,isUsers;
@synthesize _Delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_01.png"]]];
        OnSuccess = YES;
        isPhoto = isText = isTitle = isAnd = isUsers = NO;
        
    }
    return self;
}
// ----------------

#pragma mark ChildListViewController  选择小孩
-(void)SlectChild
{
    ChildListViewController *nlvcCtr  = [[ChildListViewController alloc]initWithNibName:@"ChildListViewController" bundle:nil];
    nlvcCtr._chliddelgelagte = self;
    nlvcCtr.title = @"幼儿姓名";
    
    if (mChilds!=nil) {
        nlvcCtr.mChilds = mChilds;  
    }    
    [self.navigationController pushViewController:nlvcCtr animated:YES];
    [nlvcCtr release];
    
}
#pragma mark delegateFunction 
-(void)SetPitchs:(NSMutableArray *)childs
{
    self.mChilds = childs;
}
-(void)SetnotityTitle
{

    [self.view bringSubviewToFront:[self.view viewWithTag:IViewTag]];
    [self.view bringSubviewToFront:mNotifyTitle];
    [mNotifyTitle becomeFirstResponder];
}
-(void)SetNotifyContent
{
    [self.view bringSubviewToFront:[self.view viewWithTag:IViewTag]];
    [self.view bringSubviewToFront:mDisText];
    [mDisText becomeFirstResponder];
}
#define IconWidth 20
// ----------------
-(void)InitTools
{
    
    UIView *tempview = (UIView *)[self.view viewWithTag:IViewTag];
    if (tempview != nil) {
        return;
    }
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, IconWidth+10)];
    view.tag = IViewTag;
    [self.view addSubview:view];
    [view release];
    int i = 0;
    


    if (isAnd) {
        UIButton *btn = [Instance addUIButtonByImage:view image:@"人物.png" rect:CGRectMake(10 + i*55, 3, IconWidth, IconWidth)];
        [btn addTarget:self action:@selector(SlectChild) forControlEvents:64];
        i++;
    }
    if (isTitle) {
        UIButton *btn = [Instance addUIButtonByImage:view image:@"标题.png" rect:CGRectMake(10 + i*55, 3, IconWidth, IconWidth)];
        [btn addTarget:self action:@selector(SetnotityTitle) forControlEvents:64];
        i++;
    }
    if (isText) {
        UIButton *btn = [Instance addUIButtonByImage:view image:@"内容.png" rect:CGRectMake(10 + i*55, 3, IconWidth, IconWidth)];
        [btn addTarget:self action:@selector(SetNotifyContent) forControlEvents:64];
        i++;
    }

}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)navBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)dealloc
{
    [mChildID release]; 
    [mChilds release]; 
    [mCNameText release];     
    [mDisText release]; 
    [mNotifyTitle release]; 
    [super dealloc];
}
- (void)viewDidUnload
{
    
    self.mChildID = nil;
    self.mChilds = nil;
    self.mCNameText = nil;
    self.mNotifyTitle = nil;
    self.mDisText = nil;

    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:@"backbutton.png" target:self selector:@selector(navBack)];
    [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"发送" target:self selector:@selector(SaveNotifyInfo)];

    UIFont *font = [UIFont systemFontOfSize:16];



    {
        UIPlaceHolderTextView *textview = [[UIPlaceHolderTextView alloc]initWithFrame:CGRectMake(0, 0, 320, 170)];
        textview.placeholder = @"内容......";
        textview.text = @"";
        textview.font = font;
        textview.delegate = self;
        textview.returnKeyType=UIReturnKeyDone;
        textview.backgroundColor = [UIColor whiteColor];
        self.mDisText = textview;
        [textview release];
        [self.view addSubview:mDisText];
    }
    
    {
        UITextField *textfield = [Instance addTextFileld:self frame:CGRectMake(0, 0, 320, 200)];
        [textfield setBorderStyle:UITextBorderStyleNone];
        textfield.backgroundColor = [UIColor whiteColor];
        textfield.placeholder = @"标题......";
        textfield.font = font;
        self.mNotifyTitle = textfield;
        [textfield release];
        [self.view addSubview:mNotifyTitle];
        [mNotifyTitle becomeFirstResponder];
        
    }
    

    //监听键盘高度的变换 

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];  
    
    // 键盘高度变化通知，ios5.0新增的    
#ifdef __IPHONE_5_0  
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];  
    if (version >= 5.0) {  
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];  
    }  
#endif  
}

#pragma mark 发送
-(void)SaveNotifyInfo
{
//    [Instance baiduApi:@"sendnotify" eventlabel:@"sendnotify"];
    
    //BaiduMobStat* statTracker = [BaiduMobStat defaultStat];[statTracker logEvent:@"sendnotify" eventLabel:@"sendnotify"];
     
    [self performSelector:@selector(NotityInformartion)];
    
}
-(void)GetChildsId
{

    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (CChild *obj in mChilds) {
        if (obj.isCheck == YES) {
            [array addObject:[NSNumber numberWithInt:obj.cID]];

        }
    }
    self.mChildID = array;
    [array release];
}
-(void)NotityInformartion
{
    NSString *NotityTitle = mNotifyTitle.text;
    NSString *NotityConten = mDisText.text;
    [self GetChildsId];

    if ([mChildID count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请选择幼儿" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alert.tag = SCHILD;
        alert.delegate = self;
        [alert show];
        [alert release];
        return;
    }
    if ([NotityTitle isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"标题不能为空" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alert.tag = STITLE;
        alert.delegate = self;
        [alert show];
        [alert release];
        return;
    }   
    if ([NotityConten isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"通知内容不能为空" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alert.tag = SCONTENT;
        alert.delegate = self;
        [alert show];
        [alert release];
        return;
    }

    
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/Notification/SendNotification"];
    

    
    NSMutableDictionary*mDict = [[NSMutableDictionary alloc] init];
    [mDict setObject:mChildID forKey:@"PersonIDsOfNotice"];
    [mDict setObject:NotityTitle forKey:@"Title"];
    [mDict setObject:NotityConten forKey:@"Content"];
    [mDict setObject:[Instance GetUseID] forKey:@"PublishPersonID"];
    
    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate = nil;
    mHttpRequest.Delegate = self;
    [self.view addSubview:mHttpRequest];
    [mHttpRequest CustomFormDataRequestDict:[mDict JSONRepresentation] tag:0 url:urlstr];
    [mDict release];
    [mHttpRequest release];
}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"网络错误，发送失败" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];

}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    [self navBack];
    [_Delegate ReloDataList];

}

#pragma mark 重置
-(void)resetNotity
{
    mCNameText.text= @"";
    mNotifyTitle.text = @"";
    mDisText.text = @"";
    
}

#pragma mark 响应输入
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
	textField.text = @"";	
	return NO;
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    textView.text = @"";
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField;              // called when 'return' key pressed. return NO to ignore.
{
//    textField.text ="\r\n";
    [textField.text stringByAppendingFormat:@"\r\n"];
    return NO;
}

-(void)ChangeRectOfView:(CGRect)keyboardRect
{
    CGRect rect = keyboardRect;
    rect.origin.x = 0.0f;
    rect.origin.y = 0.0f;
    rect.size.height = 416 - keyboardRect.size.height-25;
    [mDisText setFrame:rect];
    mNotifyTitle.frame = mDisText.frame;
    
    UIView *view = [self.view viewWithTag:IViewTag];
    rect = view.frame;
    rect.origin.y = mDisText.frame.size.height;
    [view setFrame:rect];
}

#pragma mark -  
#pragma mark Responding to keyboard events  
- (void)keyboardWillShow:(NSNotification *)notification {  
    
    /*  
     Reduce the size of the text view so that it's not obscured by the keyboard.  
     Animate the resize so that it's in sync with the appearance of the keyboard.  
     */  
    
    NSDictionary *userInfo = [notification userInfo];  
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];  
    
    CGRect keyboardRect = [aValue CGRectValue];  
    NSLog(@"keyboardRect width :%f  height :%f",keyboardRect.size.width,keyboardRect.size.height);
    [self InitTools];
    [self ChangeRectOfView:keyboardRect];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];  
    NSTimeInterval animationDuration;  
    [animationDurationValue getValue:&animationDuration];  
    
    // Animate the resize of the text view's frame in sync with the keyboard's appearance.  
}  


- (void)keyboardWillHide:(NSNotification *)notification {  
    
    NSDictionary* userInfo = [notification userInfo];  
    
    /*  
     Restore the size of the text view (fill self's view).  
     Animate the resize so that it's in sync with the disappearance of the keyboard.  
     */  
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];  
    NSTimeInterval animationDuration;  
    [animationDurationValue getValue:&animationDuration];  
    
}  
#pragma mark 警告响应
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == SCHILD) {
        [self SlectChild];
        return;
    }
    if (alertView.tag == STITLE) {
        [self SetnotityTitle];
        return;
    }
    if (alertView.tag == SCONTENT) {
        [self SetNotifyContent];
        return;
    }
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
