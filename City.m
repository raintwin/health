//
//  City.m
//  Health
//
//  Created by hq on 12-9-14.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "City.h"
#import "ProvinceCity.h"

@implementation City
@synthesize mCitys;
@synthesize provinceID;
@synthesize Delegate;
@synthesize isSeekFood;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
-(void)navBack
{

    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];
    [self performSelector:@selector(GetCity) withObject:nil afterDelay:0.1f];
}
-(void)GetCity
{
    NSMutableDictionary*mDict = [[NSMutableDictionary alloc] init];
    
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/UserManager/FetchRegionList"];

    
    
    [mDict setObject:[NSNumber numberWithInt:provinceID] forKey:@"regionID"];
    NSString *JSONString = [mDict JSONRepresentation];
    [mDict release];
    
    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate = self;
    [mHttpRequest CustomFormDataRequestDict:JSONString tag:0 url:urlstr];
    [self.view addSubview:mHttpRequest];
    [mHttpRequest release];
    
    
}
#pragma  mark requestFinished
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{

    NSArray *array = [[[ResultContent JSONValue] valueForKey:@"regionList"] retain];
    NSMutableArray *more = [[NSMutableArray alloc]init];
    
    for (int i = 0; i< [array count]; i++) {
        ProvinceCity *hobj = [[ProvinceCity alloc] init];
        hobj.provinceID = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"regionID"]] intValue];
        hobj.province   = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"regionName"]];
        
        
        
        
        [more addObject:hobj];
        [hobj release];
    }
    [array release];

    self.mCitys = more;
    [more release];

    if (isSeekFood) {
        ProvinceCity *hobj = [[ProvinceCity alloc] init];
        hobj.provinceID  = 0;
        hobj.province = @"不限";
        [mCitys insertObject:hobj atIndex:0];
        [hobj release];
    }
    [self.tableView reloadData];
}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{}

-(void)dealloc
{
    [mCitys release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mCitys = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [mCitys count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    ProvinceCity *pro = [mCitys objectAtIndex:[indexPath row]];
    cell.textLabel.text = pro.province;
    cell.textLabel.textAlignment = UITextAlignmentCenter;

    return cell;
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    ProvinceCity *pro = [mCitys objectAtIndex:[indexPath row]];    
    [tableView reloadData];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];

    if (isSeekFood) {
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        [mDict setObject:pro.province forKey:@"cityName"];
        [mDict setObject:[NSNumber numberWithInt:pro.provinceID] forKey:@"cityID"];
        [Delegate FrushDataOfCity:mDict];
        [mDict release];
        return;
    }

    NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
    [mDict setObject:pro.province forKey:@"cityName"];
    [mDict setObject:[NSNumber numberWithInt:pro.provinceID] forKey:@"cityID"];
    [Delegate FrushDataOfCity:mDict];
    [mDict release];
    [self.navigationController popViewControllerAnimated:YES];
//    NSString *documentDirectory = [Instance GetDocumentByString:@"initlalize"];
//    if ([[NSFileManager defaultManager] fileExistsAtPath:documentDirectory]) {
//        NSMutableDictionary *mDict = [NSMutableDictionary dictionaryWithContentsOfFile:documentDirectory];
//        [mDict setObject:pro.province forKey:@"cityName"];
//        [mDict setObject:[NSNumber numberWithInt:pro.provinceID] forKey:@"cityID"];
//        [mDict writeToFile:documentDirectory atomically:YES];
//    }   
    

}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/



@end
