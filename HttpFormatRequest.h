//
//  HttpFormatRequest.h
//  texthttp
//
//  Created by jiaodian on 12-10-26.
//
//

#import <UIKit/UIKit.h>
#import "DMAppDelegate.h"

@class DMViewController;

@protocol HttpFormatRequestDeleagte <NSObject>

-(void)HttpRequestFinish:(NSString*)ResultContent tag:(int)tag;
-(void)HttpRequestFailed:(NSString*)ResultContent tag:(int)tag;


@end

@interface HttpFormatRequest : UIView
{
    id<HttpFormatRequestDeleagte>Delegate;

}
@property (retain,nonatomic) DMAppDelegate *myDelegate;

-(void)CustomFormDataRequestDict:(NSString*)JSONString tag:(int)tag url:(NSString*)UrlString;
-(void)CustomFormDataRequestDownFile:(NSString*)mFileString tag:(int)tag url:(NSString*)UrlString;
-(void)CustomFormDataRequestSendFile:(NSString*)JSONString FilePath:(NSString*)filePath tag:(int)tag url:(NSString*)UrlString forkey:(NSString*)KeyString;
-(void)CustomFormDataRequestSendFiles:(NSString*)JSONString  tag:(int)tag url:(NSString*)UrlString;

@property(nonatomic,assign)id<HttpFormatRequestDeleagte>Delegate;
@property(nonatomic,retain)NSMutableArray *mFilePaths,*mRequestKeys;

@end

