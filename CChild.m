//
//  CChild.m
//  Health
//
//  Created by hq on 12-7-26.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "CChild.h"

@implementation CChild
@synthesize cID;
@synthesize fName,cName,cPhotoUrl;

@synthesize image;
@synthesize isLoad;
@synthesize isCheck;


-(void)dealloc
{
    [fName release]; fName = nil;
    [cName release]; cName = nil;
    [cPhotoUrl release]; cPhotoUrl = nil;
    [image release]; image = nil;
    [super dealloc];
}
@end

@implementation CCommont

@synthesize isExist;
@synthesize mComId;

@end

@implementation ChildOfPhoto

@synthesize mRecorder;

@end


@implementation ChildOfPhotoInfo
@synthesize childPhotoComment,publishDateTime,publishPersonName,picUrl,smallPicUrl;

@synthesize childPhotoID,publishPersonID;
@synthesize tag;

@synthesize bigphotoimage,smallphotoimage;
@synthesize isSmallLoad,isBigLoad;
-(void)dealloc
{
    [childPhotoComment release]; childPhotoComment = nil;
    [publishDateTime release],   publishDateTime   = nil;
    [publishPersonName release];  publishPersonName = nil;
    [picUrl release];               picUrl = nil;
    
    [smallphotoimage release];    smallphotoimage = nil;

    [smallPicUrl release];          smallPicUrl = nil;
    [bigphotoimage release];        bigphotoimage = nil;
    [super dealloc];
}
@end


@implementation ChildForPaper

@synthesize photo,childName;
@synthesize notReadCount,childID;
@synthesize isLoad;
@synthesize image;
-(void)dealloc
{
    [photo release];            photo = nil;
    [childName release];        childName = nil;
    [image release];            image = nil;
    [super dealloc];
}
@end

@implementation PaperInfo

@synthesize content,publishDateTime,publishPersonUserName,publishPhoto;
@synthesize messageid,publishPersonID;
@synthesize teacherOrParent;
-(void)dealloc
{
    [content release];  content = nil;
    [publishDateTime release];  publishDateTime = nil;
    [publishPersonUserName release];  publishPersonUserName = nil;
    [publishPhoto release];  publishPhoto = nil;
    
    [super dealloc];
}

@end
