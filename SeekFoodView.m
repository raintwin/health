//
//  SeekFoodView.m
//  Health
//
//  Created by hq on 12-9-15.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "SeekFoodView.h"
#import "Recipe.h"
#import "FoodInfoViewController.h"
//#import "ConditionView.h"
//#import "Province.h"
//#import "City.h"


#define CompareType  1010011
#define NutritionType 1010001
#define FoodType 1010002
#define ProvinceType 1010003
#define CityType 1010004
#define SexyType 1010005
#define AgeType 1010006
#define Weight  1010007

#define mPageCount 1000
#define ConditionFile @"Condition"
#define LineHeight 10
#define Heght 40
#define mTableHeight 35
#define mScrollTAG 1767545
@implementation SeekFoodView


@synthesize mFoodTextField;
@synthesize mealID;
@synthesize mTmepTextField;
@synthesize mConditionDict;
@synthesize delegate;
@synthesize mFoods;
@synthesize mSeekFoodResultTable;
@synthesize mResults;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        NSMutableArray *array = [[NSMutableArray alloc]init];
        self.mResults = array;
        [array release];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)RemarkDict
{
    [mConditionDict setObject:[NSNumber numberWithInt:[mBtnsView GetBtnValue]] forKey:@"genderID"];
    
    if (mTmepTextField !=nil && [mTmepTextField.text intValue]>0) {
        [mConditionDict setObject:mTmepTextField.text forKey:@"foodNutrientValue"];
    }
    else
    {
        [mConditionDict removeObjectForKey:@"foodNutrientValue"];
    }
    
    if (mFoodTextField !=nil && [mFoodTextField.text length]>0) {
        [mConditionDict setObject:mFoodTextField.text forKey:@"foodName"];
    }
    else
    {
        [mConditionDict removeObjectForKey:@"foodName"];

    }
    NSLog(@"mConditionDict :%@",[mConditionDict JSONRepresentation]);
    [mConditionDict writeToFile:[Instance GetDocumentByString:ConditionFile] atomically:YES];

}
#pragma mark seekFoodResult delegate
-(void)ReturnResult:(NSMutableArray*)array
{
    if ([array count] == 0) {
        return;
    }
    [self performSelector:@selector(backNavWithArray:) withObject:array afterDelay:0.01f];
}
-(void)backNavWithArray:(NSMutableArray*)array
{
    [delegate ReturnQuestResult:array condition:mConditionDict];
    [self.navigationController popViewControllerAnimated:NO];

}
-(void)backNav
{
    [self RemarkDict];
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - View lifecycle 选择按纽
-(UIButton*)SelfViewAddButton:(NSString*)title rect:(CGRect)Rect
{
    UIView *BackView = (UIView *)[self.view viewWithTag:mScrollTAG];
    if (BackView != nil) {
        UIButton *btn = [[UIButton alloc]initWithFrame:Rect];
        btn.tag = CompareType;
        [btn setTitle:title forState:0];
        [btn setBackgroundImage:[UIImage imageNamed:TWOENSURE] forState:0];
        [btn addTarget:self action:@selector(Condition:) forControlEvents:64];
        [BackView addSubview:btn];
        [btn release];
        return btn;
    }
    return nil;
}
#pragma mark 查询食物
-(void)QueryFood
{
    [self performSelector:@selector(hideKeyBoard)];

    [self RemarkDict];

    if ([[mConditionDict objectForKey:@"foodNutrientComparationID"] intValue] != 0 && [[mConditionDict objectForKey:@"foodNutrientNameID"]intValue] != 0){
        if([[mConditionDict objectForKey:@"foodNutrientValue"] floatValue] ==0.0 || [[mConditionDict objectForKey:@"foodNutrientValue"] length] == 0) {
            UIAlertView *alerview = [[UIAlertView alloc] initWithTitle:nil message:@"营养素值不值为空或者为零" delegate:self cancelButtonTitle:@"查询" otherButtonTitles:nil, nil];
            alerview.tag = 1010101;
            alerview.delegate = self;
            [alerview show];
            [alerview release];
            return;
        }
    }
    
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/InHomeArrangeMeal/FetchSelfArrangeMealFoodList"];
    
    
    
    [mConditionDict setObject:[NSNumber numberWithInt:mPageStart] forKey:@"PageStart"];
    [mConditionDict setObject:[NSNumber numberWithInt:mPageCount] forKey:@"PageCount"];
    
    
    NSString *JSONString = [mConditionDict JSONRepresentation];

    
    HttpFormatRequest *mHttpe = [[HttpFormatRequest alloc]init];
    mHttpe.Delegate = self;
    [mHttpe CustomFormDataRequestDict:JSONString tag:0 url:urlstr];
    [self.view addSubview:mHttpe];
    [mHttpe release];


}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (mTmepTextField != nil && alertView.tag == 1010101) {
        [mTmepTextField becomeFirstResponder];
    }
}
-(void)initmConditionDict
{
    [mConditionDict setObject:[NSNumber numberWithInt:mealID] forKey:@"mealID"];
    [mConditionDict setObject:[NSNumber numberWithInt:0] forKey:@"ageID"];
    [mConditionDict setObject:[NSNumber numberWithInt:0] forKey:@"genderID"];
    [mConditionDict setObject:[NSNumber numberWithInt:0] forKey:@"provinceID"];
    [mConditionDict setObject:[NSNumber numberWithInt:0] forKey:@"cityID"];
    [mConditionDict setObject:[NSNumber numberWithInt:0] forKey:@"foodKindIDBySSBT"];
    [mConditionDict setObject:[NSNumber numberWithInt:0] forKey:@"foodNutrientNameID"];
    [mConditionDict setObject:[NSNumber numberWithInt:0] forKey:@"foodNutrientComparationID"];
    [mConditionDict setObject:[NSNumber numberWithFloat:0] forKey:@"foodNutrientValue"];
    
}
#pragma mark 高级搜索
-(void)HeighSearch:sender
{

    [self performSelector:@selector(hideKeyBoard)];
    UIButton *tmpe = (UIButton*)sender;
    UIScrollView *scroler = (UIScrollView *)[self.view viewWithTag:mScrollTAG];
    NSLog(@"    BackView.contentOffset.y :%f",scroler.contentSize.height);
    if (scroler != nil) {
        if (scroler.contentOffset.y== 0) {
            [scroler setContentOffset:CGPointMake(0, 330) animated:YES];
            [tmpe setImage:[UIImage imageNamed:@"高级搜索上.png"] forState:UIControlStateNormal];
            [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"选择" target:self selector:@selector(ConfirmFood)];
        }
        else
        {
            [scroler setContentOffset:CGPointMake(0, 0) animated:YES];
            [tmpe setImage:[UIImage imageNamed:@"高级搜索下.png"] forState:UIControlStateNormal];
            [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"查询" target:self selector:@selector(QueryFood)];


        }
        [UIView commitAnimations];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(backNav)];
    NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
    self.mConditionDict = mDict;
    [mDict release];
    mPageStart = 0 ;
    
    [self initmConditionDict];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"viewblackgroup.jpg"]]];
    {        
        {
            UIImageView *backimg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 38)];
            [backimg setImage:[UIImage imageNamed:@"IMG_1489BG.png"]];
            [self.view addSubview:backimg];
            [backimg release];
        }
        {
            UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(10, 10, 78, 16)];
            [btn setImage:[UIImage imageNamed:@"高级搜索上.png"] forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(HeighSearch:) forControlEvents:64];
            [self.view addSubview: btn];
            [btn release];
        }
        
    }

    UIScrollView *indexView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 38, 320, 330)];
    indexView.tag = mScrollTAG;
    indexView.delegate = self;
    [indexView setBackgroundColor:[UIColor clearColor]];
    [indexView setPagingEnabled:NO];
    
    [indexView setScrollEnabled:NO];
    [indexView setContentSize:CGSizeMake(0, 330*2)];
    [self.view addSubview:indexView];
    [indexView release];


    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 330)];
    [backView setBackgroundColor:[UIColor clearColor]];
    [indexView addSubview:backView];
    [backView release];

    {
        UILabel *label = [Instance addLableRect:CGRectMake(10, 340, 100, 20) text:@"(全部)" textsize:15];
        [indexView addSubview:label];
        [label release];

        UITableView *table = [[UITableView alloc] initWithFrame:CGRectMake(0, 360,320, 300)];
        table.dataSource = self;
        table.delegate = self;
        [table setBackgroundColor:[UIColor clearColor]];
        self.mSeekFoodResultTable = table;
        [table release];
        [indexView addSubview:mSeekFoodResultTable];
    }
    

    /////////////----------------------------------
    

    {
        UILabel *label = [Instance addLableRect:CGRectMake(20, LineHeight, 100, 20) text:@"营养素" textsize:15];
        [backView addSubview:label];
        [label release];
        
        
        UILabel *nutrition = [Instance addLableRect:CGRectMake(100, LineHeight, 100, 20) text:@"不限" textsize:15];
        nutrition.tag = NutritionType;
        [backView addSubview:nutrition];
        [nutrition release];
        
        UILabel *compare = [Instance addLableRect:CGRectMake(100, LineHeight+Heght*1, 140, 20) text:@"不限" textsize:15];
        compare.tag = CompareType;
        [backView addSubview:compare];
        [compare release];

        // @"克/毫克/微克" 
        UITextField *foodName = [[UITextField alloc]initWithFrame:CGRectMake(100, LineHeight+Heght*2, 100, 25)];
        foodName.keyboardType = UIKeyboardTypeNumberPad;
        [foodName setBorderStyle:UITextBorderStyleRoundedRect];
        foodName.font = [UIFont systemFontOfSize:15];
        foodName.delegate = self;
        foodName.tag = Weight;
        self.mTmepTextField = foodName;
        [backView addSubview:mTmepTextField];
        [foodName release];

        UILabel *label1 = [Instance addLableRect:CGRectMake(205, LineHeight+Heght*2, 100, 20) text:@"克/毫克/微克" textsize:15];
        [backView addSubview:label1];
        [label1 release];
        {
            UIButton *btn = [self SelfViewAddButton:@"选择" rect:CGRectMake(260, LineHeight, 51, 28)];
            btn.tag = NutritionType;
        }
        {
            UIButton *btn = [self SelfViewAddButton:@"选择" rect:CGRectMake(260, LineHeight+Heght*1, 51, 28)];
            btn.tag = CompareType;
        }
        
    }
    
    {
        UILabel *label = [Instance addLableRect:CGRectMake(20, LineHeight+Heght*3, 100, 20) text:@"食物类别" textsize:15];
        [backView addSubview:label];
        [label release];
        
        UILabel *nutrition = [Instance addLableRect:CGRectMake(100, LineHeight+Heght*3, 150, 20) text:@"不限" textsize:15];
        nutrition.tag = FoodType;
        [backView addSubview:nutrition];
        [nutrition release];
        {
            UIButton *btn = [self SelfViewAddButton:@"选择" rect:CGRectMake(260, LineHeight+Heght*3, 51, 28)];
            btn.tag = FoodType;
        }


    }
    {
        UILabel *label = [Instance addLableRect:CGRectMake(20, LineHeight+Heght*4, 100, 20) text:@"适合省份" textsize:15];
        [backView addSubview:label];
        [label release];
        
        UILabel *nutrition = [Instance addLableRect:CGRectMake(100, LineHeight+Heght*4, 150, 20) text:@"不限" textsize:15];
        nutrition.tag = ProvinceType;
        [backView addSubview:nutrition];
        [nutrition release];
        
        {
            UIButton *btn = [self SelfViewAddButton:@"选择" rect:CGRectMake(260, LineHeight+Heght*4, 51, 28)];
            btn.tag = ProvinceType;
        }

        
    }  
    {
        UILabel *label = [Instance addLableRect:CGRectMake(20, LineHeight+Heght*5, 100, 20) text:@"适合城市" textsize:15];
        [backView addSubview:label];
        [label release];
        
        UILabel *nutrition = [Instance addLableRect:CGRectMake(100, LineHeight+Heght*5, 150, 20) text:@"不限" textsize:15];
        nutrition.tag = CityType;
        [backView addSubview:nutrition];
        [nutrition release];
        
        {
            UIButton *btn = [self SelfViewAddButton:@"选择" rect:CGRectMake(260, LineHeight+Heght*5, 51, 28)];
            [btn setEnabled:NO];
           btn.tag = CityType+1000;
        }

        
    }
    {
        UILabel *label = [Instance addLableRect:CGRectMake(20, LineHeight+Heght*6, 100, 20) text:@"适合性别" textsize:15];
        [backView addSubview:label];
        [label release];
        
        NSMutableArray *array = [[NSMutableArray alloc]initWithObjects:@"不限",@"男",@"女", nil];
        
        mBtnsView = [[MusButton alloc]initWithFrame:CGRectMake(100, LineHeight+Heght*6, 23, 23)];
        [mBtnsView setTitles:array];
        [backView addSubview:mBtnsView];
        [array release];
                
    }
    {
        UILabel *label = [Instance addLableRect:CGRectMake(20, LineHeight+Heght*7, 100, 20) text:@"适合年龄" textsize:15];
        [backView addSubview:label];
        [label release];
        
        UILabel *nutrition = [Instance addLableRect:CGRectMake(100, LineHeight+Heght*7, 100, 20) text:@"不限" textsize:15];
        [backView addSubview:nutrition];
        [nutrition release];
        
        nutrition.tag = AgeType;
        {
            UIButton *btn = [self SelfViewAddButton:@"选择" rect:CGRectMake(260, LineHeight+Heght*7, 51, 28)];
            btn.tag = AgeType;
        }

    }
    if ([Instance IsGetDocument:ConditionFile]) {
         NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:[Instance GetDocumentByString:ConditionFile]];
        
        
        [mConditionDict setObject:[dic objectForKey:@"ageID"] forKey:@"ageID"];
        [mConditionDict setObject:[dic objectForKey:@"genderID"] forKey:@"genderID"];
        [mConditionDict setObject:[dic objectForKey:@"foodKindIDBySSBT"] forKey:@"foodKindIDBySSBT"];
        [mConditionDict setObject:[dic objectForKey:@"foodNutrientComparationID"] forKey:@"foodNutrientComparationID"];
        [mConditionDict setObject:[dic objectForKey:@"foodNutrientNameID"] forKey:@"foodNutrientNameID"];
        [mConditionDict setObject:[dic objectForKey:@"provinceID"] forKey:@"provinceID"];
        [mConditionDict setObject:[dic objectForKey:@"cityID"] forKey:@"cityID"];
        
        if ([[dic objectForKey:@"foodName"] length]>0) {
            [mConditionDict setObject:[dic objectForKey:@"foodName"] forKey:@"foodName"];
        }
        if ([[dic objectForKey:@"foodNutrientValue"] length]>0) {
            [mConditionDict setObject:[dic objectForKey:@"foodNutrientValue"] forKey:@"foodNutrientValue"];
        }
        
        if ((UILabel *)[self.view viewWithTag:ProvinceType] != nil && [[dic objectForKey:@"provinceName"] length] >0) {
            ((UILabel *)[self.view viewWithTag:ProvinceType]).text = [dic objectForKey:@"provinceName"];
            [mConditionDict setObject:[dic objectForKey:@"provinceName"] forKey:@"provinceName"];

        }
        if ((UILabel *)[self.view viewWithTag:CityType] != nil && [[dic objectForKey:@"cityName"] length] >0) {
            ((UILabel *)[self.view viewWithTag:CityType]).text = [dic objectForKey:@"cityName"];
            [mConditionDict setObject:[dic objectForKey:@"cityName"] forKey:@"cityName"];

        }
        
        if ((UILabel *)[self.view viewWithTag:NutritionType] != nil && [[dic objectForKey:@"foodNutrientName"]length] >0) {
            ((UILabel *)[self.view viewWithTag:NutritionType]).text = [dic objectForKey:@"foodNutrientName"];
            [mConditionDict setObject:[dic objectForKey:@"foodNutrientName"] forKey:@"foodNutrientName"];

        }
        if ((UILabel *)[self.view viewWithTag:CompareType] != nil && [[dic objectForKey:@"foodNutrientComparation"] length] >0) {
            ((UILabel *)[self.view viewWithTag:CompareType]).text = [dic objectForKey:@"foodNutrientComparation"];  
            [mConditionDict setObject:[dic objectForKey:@"foodNutrientComparation"] forKey:@"foodNutrientComparation"];

        }
        if ((UILabel *)[self.view viewWithTag:AgeType] != nil && [[dic objectForKey:@"age"] length] >0) {
            ((UILabel *)[self.view viewWithTag:AgeType]).text = [dic objectForKey:@"age"];
            [mConditionDict setObject:[dic objectForKey:@"age"] forKey:@"age"];

                
        }
        if ((UILabel *)[self.view viewWithTag:FoodType] != nil && [[dic objectForKey:@"foodKind"] length] >0) {
            ((UILabel *)[self.view viewWithTag:FoodType]).text = [dic objectForKey:@"foodKind"];
            [mConditionDict setObject:[dic objectForKey:@"foodKind"] forKey:@"foodKind"];

        }
        if ([[mConditionDict objectForKey:@"provinceID"] intValue] == 0) 
            [((UIButton *)[self.view viewWithTag:CityType+1000]) setEnabled:NO];
        else  [((UIButton *)[self.view viewWithTag:CityType+1000]) setEnabled:YES];

        if (mFoodTextField !=nil && [[mConditionDict objectForKey:@"foodName"] length]>0) {
            mFoodTextField.text = [mConditionDict objectForKey:@"foodName"];
        }
        if (mTmepTextField !=nil && [[mConditionDict objectForKey:@"foodNutrientValue"] intValue]>0) {
            mTmepTextField.text = [mConditionDict objectForKey:@"foodNutrientValue"];
        }
        [mBtnsView SetBtn:[[mConditionDict objectForKey:@"genderID"] intValue]];
    }
        //查询
//        [NSThread detachNewThreadSelector:@selector(GetFoodList) toTarget:self withObject:nil];

    
    [self performSelector:@selector(GetFoodList) withObject:nil afterDelay:0.1f];
    [indexView setContentOffset:CGPointMake(0, 330)];
    
    
}
-(void)GetFoodList
{
    mPageStart = [mResults count]/mPageCount;
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/InHomeArrangeMeal/FetchSelfArrangeMealFoodList"];
    [mConditionDict setObject:[NSNumber numberWithInt:mPageStart] forKey:@"PageStart"];
    [mConditionDict setObject:[NSNumber numberWithInt:mPageCount] forKey:@"PageCount"];
    
    NSString *JSONString = [mConditionDict JSONRepresentation];
    
    HttpFormatRequest *mHttpe = [[HttpFormatRequest alloc]init];
    mHttpe.Delegate = self;
    [mHttpe CustomFormDataRequestDict:JSONString tag:0 url:urlstr];
    [self.view addSubview:mHttpe];
    [mHttpe release];
    
    
}
#pragma  mark requestFinished
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag{
    NSArray *array = [[[ResultContent JSONValue] valueForKey:@"foodList"] retain];
    [self performSelectorOnMainThread:@selector(LoadDataOfFood:) withObject:array waitUntilDone:YES];
    
    if ([array count] == 0) {
        isMore = NO;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"暂无数据" delegate:self cancelButtonTitle:@"查询" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    
    if ([mResults count] >0) {
        UIScrollView *scroller = (UIScrollView*)[self.view viewWithTag:mScrollTAG];
        if (scroller) {
            [scroller setContentOffset:CGPointMake(0, 330) animated:YES];
        }
        [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"选择" target:self selector:@selector(ConfirmFood)];
    }
    
    [self.mSeekFoodResultTable reloadData];
    NSLog(@"[mResults count] %d",[mResults count]);
    [array release];
}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag{
}
#pragma mark 返回选择食物
-(void)ConfirmFood
{


    
    if ([mResults count] == 0 ) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"请选择食物" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
        [alertView release];
        
//        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    else
    {
        NSMutableArray *array = [[NSMutableArray alloc] init];
        for (FoodResult *foodObj in mResults) {
            if (foodObj.isMark) {
                [array addObject:foodObj];
            }
        }
        if ([array count] == 0) {
            [array release];
            [self.navigationController popViewControllerAnimated:YES];
            return;
        }
        [delegate ReturnQuestResult:array condition:mConditionDict];
        [self.navigationController popViewControllerAnimated:NO];
        [array release];

    }    
}
-(BOOL)isExist:(int)foodNutDisButID
{
    BOOL mIsExist = YES;
    for (int i = 0; i<[mFoods count]; i++) {
        FoodResult *Class = [mFoods objectAtIndex:i];
        if (Class.foodNutrientDistributionID == foodNutDisButID) {
            return NO;
        }
    }
    return mIsExist;
}
-(void)LoadDataOfFood:(NSArray *)array
{
    [mResults removeAllObjects];
    for (int i = 0; i< [array count]; i++) {
        FoodResult *foodresult = [[FoodResult alloc]init];
        int foodNutDisButID = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"foodNutrientDistributionID"]] intValue];
        int foodKindID = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"foodKindIDBySSBT"]] intValue];
        foodresult.foodNutrientDistributionID = foodNutDisButID;
        foodresult.foodKindIDBySSBT = foodKindID;
        foodresult.foodKindNameBySSBT = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"foodKindNameBySSBT"]];
        foodresult.foodName = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"foodName"]];
        foodresult.foodPicUrl = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"foodPicUrl"]];
        foodresult.isMark = NO;
        
        if ([self isExist:foodNutDisButID]) {
            [mResults addObject:foodresult];
        }
        
        [foodresult release];
    }
    
}
#pragma mark 城市 省
-(void)Condition:sender
{
    
    UIButton *mTmep = (UIButton*)sender;
    if (mTmep.tag == ProvinceType) {
        Province *CtrView = [[Province alloc]initWithStyle:UITableViewStylePlain];
        CtrView.isSeekFood = YES;
        CtrView.Delagete = self;
        [self.navigationController pushViewController:CtrView animated:YES];
        [CtrView release];
        return;
    }
    if (mTmep.tag == CityType+1000) {
        City *CtrView = [[City alloc]initWithStyle:UITableViewStylePlain];
        CtrView.isSeekFood = YES;
        CtrView.Delegate = self;
        CtrView.provinceID = [[mConditionDict objectForKey:@"provinceID"] intValue];
        [self.navigationController pushViewController:CtrView animated:YES];
        [CtrView release];
        return;

    }
    ConditionView *CtrView = [[ConditionView alloc]initWithStyle:UITableViewStylePlain];
    CtrView.Delegate = self;
    CtrView.ConditionID = mTmep.tag;
    [self.navigationController pushViewController:CtrView animated:YES];
    [CtrView release];
    
}
-(void)FrushDataOfProvince:(NSMutableDictionary*)mDict
{
    if ((UILabel *)[self.view viewWithTag:ProvinceType] != nil ) {
        ((UILabel *)[self.view viewWithTag:ProvinceType]).text = [mDict objectForKey:@"provinceName"];
        [mConditionDict setObject:[mDict objectForKey:@"provinceID"] forKey:@"provinceID"];
        [mConditionDict setObject:[mDict objectForKey:@"provinceName"] forKey:@"provinceName"];
        
        if ([[mConditionDict objectForKey:@"provinceID"] intValue] == 0) {
            [((UIButton *)[self.view viewWithTag:CityType+1000]) setEnabled:NO];
            [mConditionDict removeObjectForKey:@"cityName"];

        }
        else  
        {
            [((UIButton *)[self.view viewWithTag:CityType+1000]) setEnabled:YES];
        }

    }
    [mConditionDict setObject:[NSNumber numberWithInt:0] forKey:@"cityID"];
    if ((UILabel *)[self.view viewWithTag:CityType] != nil ) {
        ((UILabel *)[self.view viewWithTag:CityType]).text = @"不限";
        [mConditionDict setObject:[NSNumber numberWithInt:0] forKey:@"cityID"];

    }
}
-(void)ReloDataProvince
{}
-(void)FrushDataOfCity:(NSMutableDictionary*)mDict
{
    if ((UILabel *)[self.view viewWithTag:CityType] != nil ) {
        ((UILabel *)[self.view viewWithTag:CityType]).text = [mDict objectForKey:@"cityName"];
        [mConditionDict setObject:[mDict objectForKey:@"cityID"] forKey:@"cityID"];
        [mConditionDict setObject:[mDict objectForKey:@"cityName"] forKey:@"cityName"];

    }


}
-(void)FrushData:(NSMutableDictionary*)mDict
{
    if ([[mDict objectForKey:@"Type"] intValue] == NutritionType) {
        if ((UILabel *)[self.view viewWithTag:NutritionType] != nil ) {
            ((UILabel *)[self.view viewWithTag:NutritionType]).text = [mDict objectForKey:@"value"];
            [mConditionDict setObject:[mDict objectForKey:@"ID"] forKey:@"foodNutrientNameID"];
            [mConditionDict setObject:[mDict objectForKey:@"value"] forKey:@"foodNutrientName"];

        }
    }
    if ([[mDict objectForKey:@"Type"] intValue] == CompareType) {
        if ((UILabel *)[self.view viewWithTag:CompareType] != nil ) {
            ((UILabel *)[self.view viewWithTag:CompareType]).text = [mDict objectForKey:@"value"];
            [mConditionDict setObject:[mDict objectForKey:@"ID"] forKey:@"foodNutrientComparationID"];
            [mConditionDict setObject:[mDict objectForKey:@"value"] forKey:@"foodNutrientComparation"];

        }
    }
    if ([[mDict objectForKey:@"Type"] intValue] == AgeType) {
        if ((UILabel *)[self.view viewWithTag:AgeType] != nil ) {
            ((UILabel *)[self.view viewWithTag:AgeType]).text = [mDict objectForKey:@"value"];
            [mConditionDict setObject:[mDict objectForKey:@"ID"] forKey:@"ageID"];
            [mConditionDict setObject:[mDict objectForKey:@"value"] forKey:@"age"];

        }
    }
    if ([[mDict objectForKey:@"Type"] intValue] == FoodType) {
        if ((UILabel *)[self.view viewWithTag:FoodType] != nil ) {
            ((UILabel *)[self.view viewWithTag:FoodType]).text = [mDict objectForKey:@"value"];
            [mConditionDict setObject:[mDict objectForKey:@"ID"] forKey:@"foodKindIDBySSBT"];
            [mConditionDict setObject:[mDict objectForKey:@"value"] forKey:@"foodKind"];

        }
    }
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
#pragma mark hideKeyBoard
-(void)hideKeyBoard
{
    if (mTmepTextField != nil) {
        [mTmepTextField resignFirstResponder];
    }
    if (mFoodTextField != nil) {
        [mFoodTextField resignFirstResponder];
    }
    
}
#pragma mark touchBegan
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self hideKeyBoard];

}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = scrollView.frame.size.height;
    // 根据当前的x坐标和页宽度计算出当前页数
    int currentPage = floor((scrollView.contentOffset.y - pageWidth / 2) / pageWidth) + 1;
    NSLog(@"currentPage  edn row %d",currentPage);
    if (scrollView.contentOffset.y == 0) {
        [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"查询" target:self selector:@selector(QueryFood)];

        NSLog(@"1 页");
        return;
    }
    if (scrollView.contentOffset.y  == 330) {
        [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"选择" target:self selector:@selector(ConfirmFood)];
        NSLog(@"2页");
        return;
    }
}


//-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
//{
//    [self hideKeyBoard];
//}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self hideKeyBoard];

}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [mResults count];
}
-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return mTableHeight;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    
    for (UIView *sub in [cell subviews]) {
        [sub removeFromSuperview];
    }
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 320-10, mTableHeight-2)];
    textLabel.textAlignment = UITextAlignmentCenter;
    textLabel.backgroundColor = [UIColor clearColor];
    textLabel.font = [UIFont boldSystemFontOfSize:15];
    textLabel.tag = 10001;
    [cell addSubview:textLabel];
    [textLabel release];
    
    UIImageView *imageLine = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 1)];
    [imageLine setImage:[UIImage imageNamed:@"xian_03.png"]];
    [cell addSubview:imageLine];
    [imageLine release];
    
    if ([mResults count] == 0 && isMore == NO) {
        textLabel.text = @"暂无数据";
        cell.textLabel.textAlignment = UITextAlignmentCenter;
        return cell;
    }
    
    if ([mResults count] == 0 && isMore == YES) {
        textLabel.text = @"Loading";
        textLabel.textAlignment = UITextAlignmentCenter;
        
        return cell;
    }

    if ([indexPath row]< [mResults count]) {
        FoodResult *foodresult = [mResults objectAtIndex:[indexPath row]];
        if (foodresult.isMark) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        
        textLabel.text = foodresult.foodName;
        textLabel.textAlignment = UITextAlignmentLeft;
        UIButton *imageMrak = [[UIButton alloc]initWithFrame:CGRectMake(293, 8, 23, 23)];
        imageMrak.tag = [indexPath row];
        [imageMrak setImage:[UIImage imageNamed:@"tu_04.png"] forState:0];
        [imageMrak addTarget:self action:@selector(FoodInformation:) forControlEvents:64];
        [cell addSubview:imageMrak];
        [imageMrak release];
    }
    
    // Configure the cell...
    
    return cell;
}
#pragma mark 选择食物
-(void)FoodInformation:sender
{
    [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"选择" target:self selector:@selector(ConfirmFood)];

    UIButton *tmpe = (UIButton*)sender;
    FoodResult *recipeObj = [mResults objectAtIndex:tmpe.tag];
    
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:tmpe.tag inSection:0];
    UITableViewCell *cell = [self.mSeekFoodResultTable cellForRowAtIndexPath:indexpath];
    
    
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        cell.accessoryType = UITableViewCellAccessoryNone;
        recipeObj.isMark = NO;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        recipeObj.isMark = YES;
    }
    
    [cell.contentView bringSubviewToFront:tmpe];
    
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    

    if ([mResults count] == 0 && isMore == NO) {
        return;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if ([indexPath row] == [mResults count]) {
        if ((UILabel*)[cell viewWithTag:10001]!=nil) {
            ((UILabel*)[cell viewWithTag:10001]).text = @"loading";
            ((UILabel*)[cell viewWithTag:10001]).textAlignment = UITextAlignmentCenter;
            
        }
//        mPageStart++;
        mPageStart = [mResults count]/mPageCount;

        isMore = YES;
        [self performSelector:@selector(SendQuest)];
    }
    else
    {
        FoodResult *recipeObj = [mResults objectAtIndex:[indexPath row]];
        
        FoodInfoViewController *detailViewController = [[FoodInfoViewController alloc] initWithStyle:UITableViewStylePlain];
        detailViewController.foodNutrientDistributionID = recipeObj.foodNutrientDistributionID;
        detailViewController.title = recipeObj.foodName;
        // ...
        [self.navigationController pushViewController:detailViewController animated:YES];
        [detailViewController release];
    }
}

-(void)dealloc
{
    [mTmepTextField release];
    [mFoodTextField release];
    [mConditionDict release];
    [mResults release];
    [mFoods release];
    [mBtnsView release];     mBtnsView = nil;
    [mSeekFoodResultTable release];

    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mTmepTextField = nil;
    self.mConditionDict = nil;
    self.mFoodTextField = nil;
    self.mFoods = nil;
    self.mResults = nil;
    self.mSeekFoodResultTable = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
