//
//  ChildListOfCom.h
//  Health
//
//  Created by hq on 12-6-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DownLoadTableImage.h"

@protocol ChildListOfComDelegate <NSObject>
@optional

-(void)SetPitchs:(NSMutableArray *)pitchs ;

@end


@interface ChildListOfCom : UIViewController<UITableViewDelegate,UITableViewDataSource,DownLoadImageDelegate,HttpFormatRequestDeleagte>
{
    id<ChildListOfComDelegate> _chliddelgelagte;
    
    BOOL isMark;
}

@property(nonatomic,retain) NSMutableArray *mChilds;

@property(nonatomic,retain)UITableView *mTable;
@property(nonatomic,retain) id<ChildListOfComDelegate>_chliddelgelagte;
@property(nonatomic,retain)NSMutableDictionary *mDownLoadImageDict;

@property(nonatomic,retain)NSMutableDictionary *mComInfo;

@end

