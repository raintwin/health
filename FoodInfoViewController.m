
//
//  FoodInfoViewController.m
//  Health
//
//  Created by hq on 12-6-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "FoodInfoViewController.h"
#import "Recipe.h"
#import "AnalysisViewController.h"

#define mTableRowCount 8
#define mTableHeight 46
#define mTableTextSize 14

@implementation FoodInfoViewController
@synthesize foodNutrientDistributionID;
@synthesize mFoodNutrientDetailInfo;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {

        
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)navBack
{

    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark 营养素
-(void)AnalysisView
{

    AnalysisViewController *detailViewController = [[AnalysisViewController alloc] initWithStyle:UITableViewStylePlain];
    detailViewController.mFoodAnalysisDict = mFoodNutrientDetailInfo;
    detailViewController.title = [NSString stringWithFormat:@"%@营养素",[mFoodNutrientDetailInfo objectForKey:@"foodName"]];
    // ...
    // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];

}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //BaiduMobStat* statTracker = [BaiduMobStat defaultStat];    [statTracker logEvent:@"lookFood" eventLabel:@"lookFood"];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];
    [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"营养素" target:self selector:@selector(AnalysisView)];
    
    NSMutableDictionary *mDict = [[NSMutableDictionary alloc] init];
    self.mFoodNutrientDetailInfo = mDict;
    [mDict release];

    [self.tableView setScrollEnabled:NO];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    

//    [NSThread detachNewThreadSelector:@selector(GetRecipe) toTarget:self withObject:nil];
    [self performSelector:@selector(GetRecipe) withObject:nil afterDelay:0.1f];
}
-(void)GetRecipe
{
    
    NSMutableDictionary*mDict = [[NSMutableDictionary alloc] init];
    
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/InSchoolArrangeMeal/TPFetchOneFoodNutrientDetailInfo"];
    
    
    [mDict setObject:[NSNumber numberWithInt:foodNutrientDistributionID] forKey:@"foodNutrientDistributionID"];
    
    NSString *JSONString = [mDict JSONRepresentation];
    [mDict release];
    
    
    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate = self;
    [mHttpRequest CustomFormDataRequestDict:JSONString tag:0 url:urlstr];
    [self.view addSubview:mHttpRequest];
    [mHttpRequest release];
    
}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    
    //---------------
    //    NSMutableDictionary *RecipeDict = [[NSMutableDictionary alloc] init];
    //    [RecipeDict setObject:[NSNumber numberWithInt:[[[ResultContent JSONValue] valueForKey:@"weekArrangeMealBasicID"] intValue]] forKey:@"weekArrangeMealBasicID"];
    //    [RecipeDict setObject:[NSNumber numberWithInt:[[[ResultContent JSONValue] valueForKey:@"dayOfWeek"] intValue]] forKey:@"dayOfWeek"];
    //    
    //    [RecipeDict release];
    
    //----------------- 
    
    
    
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"foodName"]] forKey:@"foodName"];  //0
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"foodKindName"]] forKey:@"foodKindName"];//1
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"eatFoodKindName"]] forKey:@"eatFoodKindName"];//2
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"fatFromKindName"]] forKey:@"fatFromKindName"];//3
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"energyFoodFromKindName"]] forKey:@"energyFoodFromKindName"];//4
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"proteinFromKindName"]] forKey:@"proteinFromKindName"];//5
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"productAddress"]] forKey:@"productAddress"];//6
    
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"foodish"]] forKey:@"foodish"];//7
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"energy"]] forKey:@"energy"];//8
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"water"]] forKey:@"water"];//9
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"portein"]] forKey:@"portein"]; //10
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"fat"]] forKey:@"fat"];//11
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"carbohydrate"]] forKey:@"carbohydrate"];   //12
    
    
    
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"fiber"]] forKey:@"fiber"]; //13
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"cholesterol"]] forKey:@"cholesterol"];//14
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"ash"]] forKey:@"ash"];//15
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"carotene"]] forKey:@"carotene"];//16
    
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitA"]] forKey:@"vitA"];//17
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"retinol"]] forKey:@"retinol"];//18
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"thiamin"]] forKey:@"thiamin"];//19
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"riboflavin"]] forKey:@"riboflavin"];//20
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"niacin"]] forKey:@"niacin"];//21
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitC"]] forKey:@"vitC"];//22
    
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitE"]] forKey:@"vitE"];//23
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"alpahE"]] forKey:@"alpahE"];//24
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"betaGamaE"]] forKey:@"betaGamaE"];//25
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"deltaE"]] forKey:@"deltaE"];//26
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"potassium"]] forKey:@"potassium"];//27
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"sodium"]] forKey:@"sodium"];//28
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"calcium"]] forKey:@"calcium"];//29
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"magnesium"]] forKey:@"magnesium"];//30
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"iron"]] forKey:@"iron"];//31
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"manganese"]] forKey:@"manganese"];//32
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"zinc"]] forKey:@"zinc"];//33
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"copper"]] forKey:@"copper"];//34
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"phosphorus"]] forKey:@"phosphorus"];//35
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"selenium"]] forKey:@"selenium"];//36
    [mFoodNutrientDetailInfo setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"picUrl"]] forKey:@"picUrl"];//37
    
    
    NSLog(@"foodName :%@",[[ResultContent JSONValue] valueForKey:@"foodName"]);
    [self.tableView reloadData]; 
}
#pragma  mark requestFinished

-(void)dealloc
{
    [mFoodNutrientDetailInfo release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mFoodNutrientDetailInfo = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  mTableHeight;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return mTableRowCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 46)];
        if ([indexPath row]%2 == 0)  [img setImage:[UIImage imageNamed:@"xia.jpg"]];        
        else  [img setImage:[UIImage imageNamed:@"shang.jpg"]];      
        [cell.contentView addSubview:img];
        [img release];
        
        
        {
            UILabel *label = [Instance addLable:CGRectMake(0, 0, 320, mTableHeight) tag:100011 size:mTableTextSize string:@""];
            [cell.contentView addSubview:label];
            [label release];
        }

    }
    cell.accessoryType = UITableViewCellAccessoryNone;
    int mIndexRow = [indexPath row];
    if (mIndexRow == 0) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"  食物      %@",[mFoodNutrientDetailInfo objectForKey:@"foodKindName"]];
    }
    if (mIndexRow == 1) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"  摄入的食物      %@",[mFoodNutrientDetailInfo objectForKey:@"eatFoodKindName"]];
    }
    if (mIndexRow == 2) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"  脂肪来源      %@",[mFoodNutrientDetailInfo objectForKey:@"fatFromKindName"]];
    }
    if (mIndexRow == 3) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"  能量来源      %@",[mFoodNutrientDetailInfo objectForKey:@"energyFoodFromKindName"]];
    }
    if (mIndexRow == 4) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"  蛋白质来源      %@",[mFoodNutrientDetailInfo objectForKey:@"proteinFromKindName"]];
    }
    if (mIndexRow == 5) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"  产地      %@",[mFoodNutrientDetailInfo objectForKey:@"productAddress"]];
    }
    if (mIndexRow == 6) {
        ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"  可食用部分      %@",[mFoodNutrientDetailInfo objectForKey:@"foodish"]];
    }

    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    
//    if ([indexPath row] == 7) {
//        AnalysisViewController *detailViewController = [[AnalysisViewController alloc] initWithStyle:UITableViewStylePlain];
//        detailViewController.mFoodAnalysisDict = mFoodNutrientDetailInfo;
//        detailViewController.title = [NSString stringWithFormat:@"%@营养素",[mFoodNutrientDetailInfo objectForKey:@"foodName"]];
//        // ...
//        // Pass the selected object to the new view controller.
//        [self.navigationController pushViewController:detailViewController animated:YES];
//        [detailViewController release];
//
//    }
}

@end
