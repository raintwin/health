//
//  ChildOneListView.h
//  Health
//
//  Created by hq on 12-6-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SendNotifyViewController.h"
#import "DownLoadTableImage.h"
#import "TableTemplateView.h"
@interface ChildOneListView : UIViewController<TableTemplateViewDelegate>
{

}
@property(nonatomic,retain)TableTemplateView *mTableTemplateView;
@property(nonatomic,assign)int TYPE,mChildID;
@end
