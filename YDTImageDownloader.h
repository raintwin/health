//
//  YDTImageDownloader.h
//  YDTClient
//
//  Created by  hq on 11-10-27.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol YDTImageDownloaderDelegate 
//- (void) FinishededImageLoad:(NSIndexPath*)indexPath Image:(UIImage*) image IsFailed:(BOOL) bFaild;
-(void)SetImageView:(UIImage*)image;
@end

@interface YDTImageDownloader : NSObject 
{
	int        nImageWidth;
	int		   nImageHeight;
	NSString* strImageURL;

	
	NSIndexPath *objIndexPathTo;
    id<YDTImageDownloaderDelegate> objDelegate;
    
    NSMutableData *objImageData;
    NSURLConnection *objConnection;
}


@property (nonatomic,assign) int ImageWidth;
@property (nonatomic,assign) int ImageHeight;

@property (nonatomic, retain) NSString* ImageURL;


@property (nonatomic, retain) NSIndexPath* IndexPathTo;
@property (nonatomic, assign) id<YDTImageDownloaderDelegate> Delegate;

@property (nonatomic,retain) NSMutableData* ImageData;
@property (nonatomic,retain) NSURLConnection* Connection;

@property(nonatomic,retain)UIImage *mImage;
@property(nonatomic,retain)UIImageView *mImageView;

-(BOOL) StartDownloadImage;
-(BOOL) CancelDownloadImage;

@end


