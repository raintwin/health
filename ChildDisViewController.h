//
//  ChildDisViewController.h
//  Health
//
//  Created by hq on 12-6-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChildListOfCom.h"
#import "JsonInstance.h"

@protocol ChildDisViewControllerDelegate <NSObject>

-(void)ReloDataList;

@end

@interface ChildDisViewController : UIViewController<ChildListOfComDelegate,UITextViewDelegate,UITextFieldDelegate,UIScrollViewDelegate,HttpFormatRequestDeleagte>
{
    
    BOOL OnSuccess;
    BOOL isSelect;
    
    id<ChildDisViewControllerDelegate>_Delegate;
}
@property(nonatomic,retain) NSMutableArray *mChilds,*mChildID;
@property(nonatomic,retain) UITextField *mCNameText;
@property(nonatomic,retain) UITextView *mDisText;

@property(nonatomic,assign)BOOL isDis, isAnd,isPhoto,isText,isTitle,isUsers;

@property(nonatomic,assign)id<ChildDisViewControllerDelegate>_Delegate;

@property(nonatomic,retain)NSMutableArray *mDisViews;

@property(nonatomic,assign)int EvaluateType;
@property(nonatomic,retain)NSMutableDictionary *mDateDict;
// tool icon

-(void)InitScrollerDis;
-(void)SetPitchs:(NSMutableArray *)childs;
@end

