//
//  SendNotifyViewController.h
//  Health
//
//  Created by hq on 12-6-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChildListViewController.h"
#import "JsonInstance.h"
#import "UIPlaceHolderTextView.h"
@protocol SendNotifyDelegate <NSObject>

-(void)ReloDataList;

@end

@interface SendNotifyViewController : UIViewController<ChileListDelegate,UITextViewDelegate,UITextFieldDelegate,UIAlertViewDelegate,HttpFormatRequestDeleagte>
{
    
    BOOL OnSuccess;
    BOOL isSelect;
    
    id<SendNotifyDelegate>_Delegate;
}
@property(nonatomic,retain) NSMutableArray *mChilds,*mChildID;
@property(nonatomic,retain) UITextField *mCNameText,*mNotifyTitle;
//@property(nonatomic,retain) UITextView *mDisText;

@property(nonatomic,assign)BOOL isAnd,isPhoto,isText,isTitle,isUsers;
@property(nonatomic,retain)UIPlaceHolderTextView *mDisText;
@property(nonatomic,assign)id<SendNotifyDelegate>_Delegate;


// tool icon


-(void)SetPitchs:(NSMutableArray *)childs;
@end

