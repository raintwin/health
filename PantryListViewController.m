//
//  PantryListViewController.m
//  Health
//
//  Created by dinghao on 13-4-27.
//
//

#import "PantryListViewController.h"
#import "SystemComPantryViewControl.h"


#define DataCount 200


@interface PantryListViewController ()

@property(nonatomic,retain) UITableView *tableview;
@property(nonatomic,retain)NSMutableArray *mDates;

@property(nonatomic,retain)NSMutableDictionary *requestListDict;
@property(nonatomic,retain)HttpFormatRequest *httprequest;

@end

@implementation PantryListViewController
@synthesize tableview;
@synthesize mDates;
@synthesize httprequest;
@synthesize requestListDict;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        indexPage = 0;
    }
    return self;
}
#pragma mark - View lifecycle
-(void)backNav
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.mDates = [[NSMutableArray alloc]init ];
    self.requestListDict = [[NSMutableDictionary alloc]init];
    
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(backNav)];




    [self.requestListDict setObject:[NSNumber numberWithInt:[[Instance GetUseID] intValue]] forKey:@"userId"];
    [self.requestListDict setObject:[NSNumber numberWithInt:indexPage] forKey:@"page"];
    [self.requestListDict setObject:[NSNumber numberWithInt:DataCount] forKey:@"everyPage"];
    
    [self.requestListDict setObject:NSLocalizedString(@"PORT_RecordMealList", nil) forKey:NSLocalizedString(@"PORT_ReqType", nil)];
    
    self.httprequest = [[HttpFormatRequest alloc]init];
    self.httprequest.Delegate = self;
    [self.view addSubview:self.httprequest];
    [self.httprequest CustomFormDataRequestDict:[self.requestListDict JSONRepresentation] tag:0 url:WEBSERVERURL];
        
    
    self.tableview = [[UITableView alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y , self.view.bounds.size.width, [Instance getScreenHeight]) style:UITableViewStylePlain];
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
//	self.tableview.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	self.tableview.showsVerticalScrollIndicator = YES;
	[self.view addSubview:self.tableview];

}
#pragma mark request
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{
    
}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    NSArray *array = [[[ResultContent JSONValue] valueForKey:@"list"] retain];
    if ([array count] > 0) {
        for (int i = 0; i<[array count]; i++) {
            MealList *list = [[MealList alloc]init];
            list.date = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"date"]];
            [mDates addObject:list];
            [list release];
            
        }
    }
    [array release];
    
    [self.tableview reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [mDates count];
}
-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.textLabel.text = @"";
        cell.imageView.image = nil;
    }
    cell.imageView.image = nil;
    
    if ([self.mDates count] == 0) {
        return cell;
    }
    MealList *list = [mDates objectAtIndex:[indexPath row]];
    
    cell.textLabel.text = list.date;
    cell.textLabel.textAlignment = UITextAlignmentLeft;
    
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.imageView.image = [UIImage imageNamed:@"碗.png"];
    return cell;
}

#pragma mark - Table view delegate
-(BOOL)isEqualDate:(NSString*)BaseDate
{
    NSLog(@"BaseDate :%@",BaseDate);
    if ([BaseDate isEqualToString:[Instance GetSystemDate]]) {
        return YES;
    }
    
    return NO;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    // Navigation logic may go here. Create and push another view controller.
    if ([self.mDates count] == 0) {
        return ;
    }
    
    
    
    NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
    MealList *list = [mDates objectAtIndex:[indexPath row]];
    
    [mDict setObject:NSLocalizedString(@"PORT_RecordMealDetail", nil) forKey:NSLocalizedString(@"PORT_ReqType", nil)];
    
    [mDict setObject:[NSNumber numberWithInt:[[Instance GetUseID]intValue]] forKey:@"userId"];
    [mDict setObject:list.date forKey:@"name"];
    
    SystemComPantryViewControl *detailViewController = [[SystemComPantryViewControl alloc] initWithNibName:@"SystemComPantryViewControl" bundle:nil];
    detailViewController.title =[NSString stringWithFormat:@"%@ 配餐",list.date];
    detailViewController.isRecordData = YES;
    detailViewController.requstRecordMealDict = mDict;
    
    
    
    [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview:detailViewController.view];
    CATransition *animationp = [CATransition animation];
    detailViewController.view.frame = [UIApplication sharedApplication].keyWindow.rootViewController.view.bounds;

    animationp.delegate = self;
    animationp.duration = 0.3f;
    animationp.timingFunction = UIViewAnimationCurveEaseInOut;
    animationp.fillMode = kCAFillModeForwards;
    animationp.type = kCATransitionPush;
    animationp.subtype = kCATransitionFromRight;
    [[UIApplication sharedApplication].keyWindow.rootViewController.view.layer addAnimation:animationp forKey:@"animation"];


    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
