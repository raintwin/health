//
//  PantrytMianViewController.m
//  Health
//
//  Created by jiaodian on 12-12-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>

#import "PantrytMianViewController.h"
#import "SystemSelectMealViewControl.h"
#import "SystemComPantryViewControl.h"
#import "OptionalMainViewController.h"
@interface PantrytMianViewController ()
@property(nonatomic,retain)UIViewController *sysViewController;
@property(nonatomic,retain)OptionalMainViewController *optionalViewController;
@property (nonatomic, retain)UIButton *saveButton,*sysButton,*optionalButton;

@end

@implementation PantrytMianViewController
@synthesize sysViewController;
@synthesize optionalViewController;
@synthesize saveButton,sysButton,optionalButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)dealloc
{
    [self.sysViewController release];
    [self.optionalViewController release];
    [self.saveButton release];
    [self.sysButton release];
    [self.optionalButton release];

    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.sysViewController = nil;
    self.optionalViewController = nil;
    self.saveButton = nil;
    self.sysButton = nil;
    self.optionalButton = nil;

}

#pragma mark - View lifecycle
-(void)backNav
{
    CATransition *animation = [CATransition animation];
    animation.duration = 0.4f;
    animation.timingFunction = UIViewAnimationCurveEaseInOut;
    animation.fillMode = kCAFillModeForwards;
    animation.type = kCATransitionPush;
    animation.subtype = kCATransitionFromLeft;
    [self.view.superview.layer addAnimation:animation forKey:@"animation"];
    [self.view removeFromSuperview];
    [self release];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    {
        self.sysButton= [Instance addUIButtonOfImage:@"cellheaderyellow.png" Title:@"系统推荐" rect:CGRectMake(0, 0, 160, 30)];
        [self.sysButton addTarget:self action:@selector(System) forControlEvents:64];
        [self.view addSubview:self.sysButton];
    }
    {
        self.optionalButton = [Instance addUIButtonOfImage:@"cellheadergray.png" Title:@"自选配餐" rect:CGRectMake(160, 0, 160, 30)];
        [self.optionalButton addTarget:self action:@selector(optional) forControlEvents:64];
        [self.view addSubview:self.optionalButton];
    }
    CGRect frame = self.view.bounds;
    frame.origin.y = 30 ;
    frame.size.height = frame.size.height - 30-44;
    {
        self.optionalViewController = [[OptionalMainViewController alloc]initWithNibName:@"OptionalMainViewController" bundle:nil];
        [self.optionalViewController.view setFrame:frame];
        self.title = @"自选配餐";
        
        self.optionalViewController.viewControllerDelegate = self;
        [self.view addSubview:self.optionalViewController.view];
        
        [self.optionalViewController.view setHidden:YES];
    }
    {
        self.sysViewController = [[SystemComPantryViewControl alloc]initWithNibName:@"SystemComPantryViewControl" bundle:nil];
        [self.sysViewController.view setFrame:frame];
        [self.view addSubview:self.sysViewController.view];
        
    }
    {
        self.saveButton = [[UIButton alloc]initWithFrame:CGRectMake(270, 7, 45, 30)];
        [self.saveButton setBackgroundImage:[UIImage imageNamed:@"blankbutton"] forState:UIControlStateNormal];
        [self.saveButton setTitle:@"保存" forState:0];
        [self.view addSubview:self.saveButton];
        [self.saveButton addTarget:self action:@selector(saveMealList) forControlEvents:64];
    }

}
-(void)saveMealList
{
    [(SystemComPantryViewControl*)self.sysViewController saveSysMealList];
}
-(void)System
{
    [self.sysButton setBackgroundImage:[UIImage imageNamed:@"cellheaderyellow.png"] forState:0];
    [self.optionalButton setBackgroundImage:[UIImage imageNamed:@"cellheadergray.png"] forState:0];

    [optionalViewController.view setHidden:YES];
    [sysViewController.view setHidden:NO];
    self.saveButton.hidden = NO;
}
-(void)optional
{
    [self.sysButton setBackgroundImage:[UIImage imageNamed:@"cellheadergray.png"] forState:0];
    [self.optionalButton setBackgroundImage:[UIImage imageNamed:@"cellheaderyellow.png"] forState:0];

    [optionalViewController.view setHidden:NO];
    [sysViewController.view setHidden:YES];
    self.saveButton.hidden = YES;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
