//
//  CChild.h
//  Health
//
//  Created by hq on 12-7-26.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CChild : NSObject

@property(nonatomic,retain) NSString *fName,*cName,*cPhotoUrl;
@property(nonatomic,assign) int cID;
@property(nonatomic,retain)UIImage *image;
@property(nonatomic,assign)BOOL isLoad;
@property(nonatomic,assign)BOOL isCheck;
@end

@interface CCommont : CChild
@property(nonatomic,assign)BOOL isExist;
@property(nonatomic,assign)int mComId;

@end

@interface ChildOfPhoto : CChild 
@property(nonatomic,assign)int mRecorder;
@end



@interface ChildOfPhotoInfo : CChild 
@property(nonatomic,retain)NSString *childPhotoComment,*publishDateTime,*publishPersonName,*picUrl,*smallPicUrl;
@property(nonatomic,assign)int childPhotoID,publishPersonID;
@property(nonatomic,assign)int tag;

@property(nonatomic,retain)UIImage *bigphotoimage,*smallphotoimage;

@property(nonatomic,assign)BOOL isSmallLoad,isBigLoad;

@end


@interface ChildForPaper : NSObject 

@property(nonatomic,retain)NSString *photo,*childName;
@property(nonatomic,assign)int notReadCount,childID;
@property(nonatomic,assign)BOOL isLoad;
@property(nonatomic,retain)UIImage *image;
@end


@interface PaperInfo : ChildForPaper 
@property(nonatomic,retain)NSString *content,*publishDateTime,*publishPersonUserName,*publishPhoto;
@property(nonatomic,assign)int messageid,publishPersonID;
@property(nonatomic,assign)BOOL teacherOrParent;
@end




