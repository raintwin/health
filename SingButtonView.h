//
//  SingButtonView.h
//  Health
//
//  Created by hq on 12-6-25.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SingButtonView : UIView
{
    
}
@property(nonatomic,assign)int Count;
@property(nonatomic,assign)int tagBtn;
@property(nonatomic,assign)int pressMun;

@property(nonatomic,retain)NSString *preImage,*nextImage;

-(void)setImage1:(NSString *)Image1  SetImage2:(NSString *)Image2 BtnTile:(NSMutableArray *)titles;
-(void)setImage:(NSString *)imagePath;

-(int)retunSelectBtn;
@end
