//
//  RegisterViewController.m
//  Health
//
//  Created by hq on 12-6-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "RegisterViewController.h"
#import "BulidController.h"
@implementation RegisterViewController
@synthesize nameStr;
@synthesize passwordStr;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        NSLog(@"登录");
//        [self.view setBackgroundColor:[UIColor blackColor]];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    
    UIImageView *img = [ViewTool addUIImageView:self.view imageName:@"img_44.jpg" type:@"" x:0 y:0 x1:320 y1:44];  
    UILabel *title = [ViewTool addUILableBold:img x:0 y:0 x1:320 y1:44 fontSize:20 lableText:@"登录"];
    title.textColor = [UIColor whiteColor];
    title.textAlignment = UITextAlignmentCenter;
    
    mytable = [[UITableView alloc] initWithFrame:CGRectMake(0, 44, self.view.frame.size.width, self.view.frame.size.height - 44) style:UITableViewStyleGrouped];
    [mytable setBackgroundColor:[UIColor clearColor]];
    [mytable setScrollEnabled:NO];
    mytable.delegate = self;
    mytable.dataSource = self;
    [self.view addSubview:mytable];
    [mytable release];
    

//    {
//        UIButton *btn = [ViewTool addUIButton:self.view imageName:@"nil.png" type:@"" x:0 y:0 x1:50 y1:50];
//        [btn setBackgroundColor:[UIColor redColor]];
//        [btn setTitle:@"家长" forState:0];
//        btn.tag = 1;
//        [btn addTarget:self action:@selector(test:) forControlEvents:64];
//        [self.view addSubview:btn];
//    }
//    
//    {
//        UIButton *btn = [ViewTool addUIButton:self.view imageName:@"nil.png" type:@"" x:100 y:0 x1:50 y1:50];
//        [btn setBackgroundColor:[UIColor blueColor]];
//        [btn setTitle:@"老师" forState:0];
//        btn.tag = 2;
//        [btn addTarget:self action:@selector(test:) forControlEvents:64];
//        [self.view addSubview:btn];
//    }
//    {
//        UIButton *btn = [ViewTool addUIButton:self.view imageName:@"nil.png" type:@"" x:200 y:0 x1:50 y1:50];
//        [btn setBackgroundColor:[UIColor greenColor]];
//        [btn setTitle:@"体验" forState:0];
//        btn.tag = 3;
//        [btn addTarget:self action:@selector(test:) forControlEvents:64];
//        [self.view addSubview:btn];
//    }

}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section    // fixed font style. use custom view (UILabel) if you want something different
{
    NSString *title = @"免费注册"; 
    if (section == 1) {
        return title;
    }
    else
    {
        return nil;
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0) {
        return 2;
    }
    else
        return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
 	static NSString * cellName = @"CityManagerViewController";
	
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier: cellName] autorelease];
    }
    if ([indexPath section] == 0) {
        
        if ([indexPath row] == 0) {
            cell.textLabel.text = @"帐 号 :";
            
            UITextField *nameText  = [[UITextField alloc] initWithFrame:CGRectMake(80, cell.frame.size.height/4, 200, cell.frame.size.height-20)];
            [nameText setBorderStyle:UITextBorderStyleNone];
            nameText.tag = 10000;
            nameText.clearButtonMode=UITextFieldViewModeWhileEditing;
            nameText.placeholder = @"帐号";
            nameText.delegate = self;
            nameText.returnKeyType=UIReturnKeyDone;
            [cell addSubview:nameText];
            [nameText release];

 
        }
        if ([indexPath row] == 1) {
            cell.textLabel.text = @"密 码 :";
            UITextField *passwordText  = [[UITextField alloc] initWithFrame:CGRectMake(80, cell.frame.size.height/4, 200, cell.frame.size.height-20)];
            passwordText.tag = 20000;
            [passwordText setBackgroundColor:[UIColor clearColor]];
            [passwordText setBorderStyle:UITextBorderStyleNone];
            passwordText.clearButtonMode=UITextFieldViewModeWhileEditing;

            passwordText.placeholder = @"密 码";
            passwordText.delegate = self;
            passwordText.returnKeyType=UIReturnKeyDone;
            [cell addSubview:passwordText];
            [passwordText release];

        }

    }
    if ([indexPath section] == 1) {
        if ([indexPath row] == 0) {
            cell.textLabel.text = @"手机用户快速注册"; }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }  
    if ([indexPath section] == 2) {
        if ([indexPath row] == 0) {
            cell.textLabel.text = @"随便逛逛"; }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    }  
  
    return cell;
}
- (BOOL)textFieldShouldClear:(UITextField *)textField              // called when clear button pressed. return NO to ignore (no notifications)
{
    textField.text = nil;
    [textField resignFirstResponder];
    return YES;

}
- (BOOL)textFieldShouldReturn:(UITextField *)textField              // called when 'return' key pressed. return NO to ignore.
{
    if (10000  == textField.tag) {
        nameStr = textField.text;
    }
    if (20000 == textField.tag ) {
        passwordStr = textField.text;
    }
    
    [textField resignFirstResponder];
    return YES;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES ];

    if ([indexPath section] == 0) {
        return;
    }
    if ([indexPath section] == 1) {
        if ([indexPath row] == 0) {
            LoginType = TEACHER;
        }
    }

    if ([indexPath section] == 2) {
        if ([indexPath row] == 0) {
            LoginType = PATRIARCH;
        }
    }
    

    BulidController *build = [[BulidController alloc]init];
    tabBarController = [build BuildsController];
    tabBarController.customizableViewControllers = nil;
    [tabBarController.view setFrame:[self.view bounds]];
    [[self.view superview] addSubview:tabBarController.view];
    
    
    [build release];

}
-(void)test:sender
{
    UIButton *tmp = sender;
    switch (tmp.tag) {
        case 1:
            NSLog(@"1");
            LoginType = PATRIARCH;
            break;
        case 2:
            NSLog(@"2");
            LoginType = TEACHER;
            break;
        case 3:
            NSLog(@"3");
            LoginType = PATRIARCH;
            break;
            
        default:
            break;
    }
    NSLog(@"家长");
    BulidController *build = [[BulidController alloc]init];
    tabBarController = [build BuildsController];
    tabBarController.customizableViewControllers = nil;
    [tabBarController.view setFrame:[self.view bounds]];
    [[self.view superview] addSubview:tabBarController.view];
    [build release];
}
-(void)dealloc
{
    
    [tabBarController release];
    [super dealloc];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        switch (buttonIndex) {
            case 0:
                NSLog(@"0");
                LoginType = PATRIARCH;
                break;
            case 1:
                NSLog(@"1");
                LoginType = TEACHER;
                
                break;
            case 2:
                NSLog(@"2");
                LoginType = EXPERIENCE;
                
                break;
            case 3:
                NSLog(@"3");
                break;
                
            default:
                break;
        }
        NSLog(@"%@",LoginType);
        NSLog(@"x -- %f",[self.view superview].frame.origin.x);

        NSLog(@"y -- %f",[self.view superview].frame.origin.y);
        
        NSLog(@"width -- %f",[self.view superview].frame.size.width);
        
        NSLog(@"height -- %f",[self.view superview].frame.size.height);

       
//        [dmvc release];
//        BulidController *build = [[BulidController alloc]init];
//        UITabBarController *tabBarController;
//        tabBarController = [build BuildsController];
//        [self.view addSubview:tabBarController.view];
    }
    [alertView release];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
