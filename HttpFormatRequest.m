//
//  HttpFormatRequest.m
//  texthttp
//
//  Created by jiaodian on 12-10-26.
//
//

#import "HttpFormatRequest.h"
#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "SVProgressHUD.h"

#define DeResultStuates           @"ResultStuates"
#define DeResultDescription       @"ResultDescription"
#define DeResultCode              @"ResultCode"

@interface HttpFormatRequest ()
@property(retain,nonatomic) UITapGestureRecognizer* GlobalsingleRecognizer;

@end

@implementation HttpFormatRequest
@synthesize Delegate;
@synthesize mFilePaths;
@synthesize mRequestKeys;
@synthesize myDelegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.myDelegate = (DMAppDelegate *) [[UIApplication sharedApplication] delegate];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
-(void)handleSingleTapFrom
{
    NSLog(@"touch ");
}
-(void)CustomAddGestureRecognizer
{
    UIView *tmpeView = [[[UIApplication sharedApplication].keyWindow rootViewController].view viewWithTag:1312313131];
    if (tmpeView != nil) {
        [tmpeView removeFromSuperview];
    }
    tmpeView = [[UIView alloc] initWithFrame:[[[UIApplication sharedApplication].keyWindow rootViewController].view bounds]];
    [tmpeView setBackgroundColor:[UIColor clearColor]];
    tmpeView.tag = 1312313131;
    [tmpeView setUserInteractionEnabled:YES];
    
    [[[UIApplication sharedApplication].keyWindow rootViewController].view addSubview:tmpeView];
    [tmpeView release];


}
-(void)CustomDeleteGestureRecognizer
{
//    [[UIApplication sharedApplication].keyWindow removeGestureRecognizer:GlobalsingleRecognizer];
//    GlobalsingleRecognizer = nil;
    
//    [self.myDelegate.viewController removeGestureRecognizer:self.GlobalsingleRecognizer];
//    //    [[UIApplication sharedApplication].keyWindow removeGestureRecognizer:self.GlobalsingleRecognizer]];
//    self.GlobalsingleRecognizer = nil;
    UIView *tmpeView = [[[UIApplication sharedApplication].keyWindow rootViewController].view viewWithTag:1312313131];
    if (tmpeView != nil) {
        [tmpeView removeFromSuperview];
    }

}

-(void)CustomFormDataRequestDict:(NSString*)JSONString tag:(int)tag url:(NSString*)UrlString
{
    
    NSLog(@"URL:%@",UrlString);
    NSLog(@"Foucion JSONString-- :%@",JSONString);
    
    [self CustomAddGestureRecognizer];

    
    NSURL *url = [NSURL URLWithString:UrlString];
    
    [SVProgressHUD show];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    request.tag = tag;
    [request setDelegate:self];
    [request setTimeOutSeconds:20];
    [request setPostValue:JSONString forKey:@"requestContent"];
    [request startAsynchronous];
}
-(void)CustomFormDataRequestSendFile:(NSString*)JSONString FilePath:(NSString*)filePath tag:(int)tag url:(NSString*)UrlString forkey:(NSString*)KeyString
{
    
    
    [self CustomAddGestureRecognizer];
    
    NSLog(@"Foucion JSONString-- :%@",JSONString);
    
    NSURL *url = [NSURL URLWithString:UrlString];
    
    [SVProgressHUD show];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    request.tag = FILESEND;
    [request setDelegate:self];
    [request setTimeOutSeconds:30];
    [request setPostValue:JSONString forKey:@"requestContent"];
    [request setFile:filePath forKey:KeyString];
    [request startAsynchronous];
}
-(void)CustomFormDataRequestSendFiles:(NSString*)JSONString  tag:(int)tag url:(NSString*)UrlString 
{
    
    
    [self CustomAddGestureRecognizer];
    
    NSLog(@"Foucion JSONString-- :%@",JSONString);
    
    NSURL *url = [NSURL URLWithString:UrlString];
    
    [SVProgressHUD show];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    request.tag = FILESEND;
    [request setDelegate:self];
    [request setTimeOutSeconds:30];
    [request setPostValue:JSONString forKey:@"requestContent"];
    for (int i = 0 ; i < [mFilePaths count]; i++) {
        [request setFile:[mFilePaths objectAtIndex:i] forKey:[mRequestKeys objectAtIndex:i]];
        
    }
    [request startAsynchronous];
}
-(void)CustomFormDataRequestDownFile:(NSString*)mFileString tag:(int)tag url:(NSString*)UrlString  
{
    
    NSLog(@"URL:%@",UrlString);
    
    [self CustomAddGestureRecognizer];
    
    [SVProgressHUD show];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:UrlString]];
    [request setDownloadDestinationPath:mFileString];
    [request setDelegate:self];
    [request setTimeOutSeconds:20];
    [request startAsynchronous];
    request.tag = FILEDOWNID;

}
- (void)requestFinished:(ASIHTTPRequest *)request
{
    [SVProgressHUD dismiss];
    [self CustomDeleteGestureRecognizer];

    NSData *responeseData = [request responseData];
    NSString *responseStr = [[NSString alloc] initWithData:responeseData encoding:NSUTF8StringEncoding];
    
    
    responseStr =   [responseStr stringByReplacingOccurrencesOfString: @"\r" withString:@""];
    responseStr =   [responseStr stringByReplacingOccurrencesOfString: @"\n" withString:@""];
    
    NSLog(@"---- responseStr %@",responseStr);
    if ([responseStr length] == 0) {
        [Delegate HttpRequestFinish:@"OK" tag:request.tag];
        return;
    }
    NSMutableDictionary *mDict = [responseStr JSONValue];
    [responseStr release];
    if (FILEDOWNID == request.tag) {
        
        [Delegate HttpRequestFinish:@"OK" tag:FILEDOWNID];
        
        return;
    }
    if (FILESEND == request.tag) {
        [Delegate HttpRequestFinish:[[mDict objectForKey:@"ResultContent"] JSONRepresentation] tag:FILESEND];

        return;
    }
    int ResultCode = [[[mDict objectForKey:DeResultStuates] objectForKey:DeResultCode] intValue];
    

    
    if (ResultCode == 0 ) {        
        [Delegate HttpRequestFinish:[[mDict objectForKey:@"ResultContent"] JSONRepresentation] tag:request.tag];

    }
    else if (ResultCode == 1 )
    {
        if (HTTPSIGN != request.tag) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:[[mDict objectForKey:DeResultStuates] objectForKey:DeResultDescription] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
        }
        [Delegate HttpRequestFinish:[[mDict objectForKey:DeResultStuates] objectForKey:DeResultDescription] tag:ResultCode];
        
        return;
    }
    


}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    [SVProgressHUD dismiss];
    [self CustomDeleteGestureRecognizer];

    [Delegate HttpRequestFailed:nil tag:request.tag];
    return;
}
-(void)dealloc
{
    [mFilePaths release];   mFilePaths = nil;
    [mRequestKeys release]; mRequestKeys =nil;
    [myDelegate release];   myDelegate = nil;
    [super dealloc];
}
@end
