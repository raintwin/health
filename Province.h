//
//  Province.h
//  Health
//
//  Created by hq on 12-9-13.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ProvinceDelegate <NSObject>
-(void)ReloDataProvince;
-(void)FrushDataOfProvince:(NSMutableDictionary*)mDict;
@end

@interface Province : UITableViewController<HttpFormatRequestDeleagte>
{
}
@property(nonatomic,retain)NSMutableArray *mProvinces;
@property(nonatomic,assign)BOOL isSeekFood;
@property(nonatomic,assign)id<ProvinceDelegate> Delagete;
@end

