//
//  AnalysisViewController.m
//  Health
//
//  Created by hq on 12-6-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "AnalysisViewController.h"
#import "FoodMatterInfoController.h"


#define mTableRowCount 29
#define mTableHeight 46
#define mTableTextSize 15
@interface AnalysisViewController ()

@property(nonatomic,retain) NSMutableArray *nutrientDetials;
@property(nonatomic,retain) NSMutableArray *nutrientNames;

@end

@implementation AnalysisViewController

@synthesize mFoodAnalysisDict;
@synthesize nutrientDetials;
@synthesize nutrientNames;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)navBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];


    self.nutrientDetials = [[NSMutableArray alloc]init];
    self.nutrientNames   = [[NSMutableArray alloc]init];
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];

    
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"energy"]];
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"portein"]];
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"fat"]];
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"carbohydrate"]];
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"fiber"]];
    
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"cholesterol"]];
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"ash"]];
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"carotene"]];
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"vitA"]];
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"retinol"]];
    
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"thiamin"]];
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"riboflavin"]];
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"niacin"]];
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"vitC"]];
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"vitE"]];
    
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"alpahE"]];
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"betaGamaE"]];
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"deltaE"]];
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"potassium"]];
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"sodium"]];
    
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"calcium"]];
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"magnesium"]];
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"iron"]];
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"manganese"]];
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"zinc"]];
    
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"copper"]];
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"phosphorus"]];
    [self.nutrientDetials addObject:[mFoodAnalysisDict objectForKey:@"selenium"]];
    
    [self.nutrientNames addObject:@"热量"];
    [self.nutrientNames addObject:@"蛋白质"];
    [self.nutrientNames addObject:@"脂肪"];
    [self.nutrientNames addObject:@"碳水化合物"];
    [self.nutrientNames addObject:@"膳食纤维"];
    [self.nutrientNames addObject:@"胆固醇"];
    [self.nutrientNames addObject:@"灰分"];
    [self.nutrientNames addObject:@"胡萝卜素"];
    [self.nutrientNames addObject:@"维生素A"];
    [self.nutrientNames addObject:@"视黄醇"];
    [self.nutrientNames addObject:@"硫胺素(维生素B1)"];
    [self.nutrientNames addObject:@"核黄素(维生素B2)"];
    [self.nutrientNames addObject:@"尼克酸(维生素PP,维生素B5)"];
    [self.nutrientNames addObject:@"维生素C"];
    [self.nutrientNames addObject:@"维生素E"];
    [self.nutrientNames addObject:@"维生素AlpahE"];//
    [self.nutrientNames addObject:@"维生素BetaGamaE"];
    [self.nutrientNames addObject:@"维生素DeltaE"];
    [self.nutrientNames addObject:@"钾(K)"];
    [self.nutrientNames addObject:@"钠(Na)"];
    [self.nutrientNames addObject:@"钙(Ca)"];
    [self.nutrientNames addObject:@"镁(Mg)"];
    [self.nutrientNames addObject:@"铁(Fe)"];
    [self.nutrientNames addObject:@"锰(Mn)"];
    [self.nutrientNames addObject:@"锌(Zn)"];
    [self.nutrientNames addObject:@"铜(Cu)"];
    [self.nutrientNames addObject:@"磷(P)"];
    [self.nutrientNames addObject:@"硒(Se)"];
    
}


-(void)dealloc
{
    [mFoodAnalysisDict release];
    [self.nutrientDetials release];
    
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mFoodAnalysisDict = nil;
    self.nutrientDetials   = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [nutrientNames count];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section;    // fixed font style. use custom view (UILabel) if you want something different
{
    return  @"营养素名称                  每百克含量";
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
{
    return mTableHeight-15;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        
        UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 46)];
        img.tag = 1000;
        [cell.contentView addSubview:img];
        [img release];
        
        
        {
            UILabel *label = [Instance addLable:CGRectMake(10, 0, 300, mTableHeight) tag:100011 size:mTableTextSize string:@""];
            [cell.contentView addSubview:label];
            [label release];
        }
        {
            UILabel *label = [Instance addLable:CGRectMake(200, 0, 160, mTableHeight) tag:100012 size:mTableTextSize string:@""];
            [cell.contentView addSubview:label];
            [label release];
        }

    }
    if ((UIImageView*)[cell.contentView viewWithTag:1000] !=nil) {
        UIImageView *imageView = (UIImageView*)[cell.contentView viewWithTag:1000];
        [imageView setImage:[UIImage imageNamed:[indexPath row]%2 == 0 ? @"xia.jpg" :@"shang.jpg"]];
    }

    // Configure the cell...
    UILabel *nutrientName = (UILabel *)[cell.contentView viewWithTag:100011];
    UILabel *nutrientDetial = (UILabel *)[cell.contentView viewWithTag:100012];
    nutrientName.text   = @"";
    nutrientDetial.text = @"";
    
    nutrientName.text   = [nutrientNames objectAtIndex:[indexPath row]];
    nutrientDetial.text = [nutrientDetials objectAtIndex:[indexPath row]];

    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    
//     FoodMatterInfoController *detailViewController = [[FoodMatterInfoController alloc] initWithNibName:@"FoodMatterInfoController" bundle:nil];
//    detailViewController.title = @"计量";
//     // ...
//     // Pass the selected object to the new view controller.
//     [self.navigationController pushViewController:detailViewController animated:YES];
//     [detailViewController release];
     
}

@end
