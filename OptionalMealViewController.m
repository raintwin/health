//
//  OptionalMealViewController.m
//  Health
//
//  Created by dinghao on 13-4-22.
//
//

#import "OptionalMealViewController.h"
#define TableViewHeiht 30
#define dafaultCount 3

#define requestMealDetial 1000
#define requestSaveDetial 1001

#define cellSizeWidth 300

#define BREAKFASTSTANDARD      @"breakfastStandard"
#define BREAKFASTBOTSTANDARD   @"breakfastBotStandard"
#define LUNCHSTANDARD          @"lunchStandard"
#define LUNCHBOTSTANDARD       @"lunchBotStandard"
#define DINNERSTANDARD         @"dinnerStandard"


#define HOT                 @"hot"
#define PROTEINMIN          @"proteinMin"
#define PROTEINMAX          @"proteinMax"
#define AXUNGEMIN           @"axungeMin"
#define AXUNGEMAX           @"axungeMax"
#define CARBOHYDRATEMIN     @"carbohydrateMin"
#define CARBOHYDRATEMAX     @"carbohydrateMax"
#define CALCIUM             @"calcium"
#define VITAMIN             @"vitamin"
    
@interface OptionalMealViewController ()

@property (nonatomic, retain) UITableView *tableview;
@property (nonatomic, retain) NSMutableArray *mealDatas;
@property (nonatomic, retain) HttpFormatRequest *httprequest;
@property (nonatomic, retain) NSMutableDictionary *requestDict;

@property (nonatomic, retain) NSMutableArray *NHots,*NProteins,*NAxunges,*NCarbohydrates,*NCalciums,*NZelCs;
@property (nonatomic, retain)UIButton *saveButton;
@end

@implementation OptionalMealViewController
@synthesize tableview;
@synthesize mealDatas;
@synthesize httprequest;
@synthesize requestDict;
@synthesize NHots,NProteins,NAxunges,NCarbohydrates,NCalciums,NZelCs;
@synthesize saveButton;

typedef enum _NutritionType {
	NHotType = 0,
    NProteinType,
    NAxungeType,
    NCarbohydrateType,
    NCalciumType,
    NZelCType,
} NutritionType;

typedef enum _ComponentCount {
	littleCount = 0,
    normalCount,
    moreCount,
    abundantCount,
} ComponentCount;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)dealloc
{
    [super dealloc];
    [self.tableview release];
    [self.mealDatas release];
    [self.httprequest release];
    [self.requestDict release];
    [self.NHots release];
    [self.NProteins release];
    [self.NAxunges release];
    [self.NCarbohydrates release];
    [self.NCalciums release];
    [self.NZelCs release];
    [self.saveButton release];
}
-(void)viewDidUnload
{
    [super viewDidUnload];
    self.tableview = nil;
    self.mealDatas = nil;
    self.httprequest = nil;
    self.requestDict = nil;
    self.NHots = nil;
    self.NProteins = nil;
    self.NAxunges = nil;
    self.NCarbohydrates = nil;
    self.NCalciums = nil;
    self.NZelCs = nil;
    self.saveButton = nil;
}
-(void)backNav
{
    [self saveTmpData];
    CATransition *animation = [CATransition animation];
    animation.duration = 0.4f;
    animation.timingFunction = UIViewAnimationCurveEaseInOut;
    animation.fillMode = kCAFillModeForwards;
    animation.type = kCATransitionPush;
    animation.subtype = kCATransitionFromLeft;
    [self.view.superview.layer addAnimation:animation forKey:@"animation"];
    [self.view removeFromSuperview];
    [self release];
}
typedef enum  {
	BreakfastType = 18,
    BreakfastDotType,
    LunchType,
    LunchDotType,
    DinnerType,
} mealType;
-(NSString *)setTitleText
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int type = [[defaults objectForKey:@"mealType"] intValue];
    switch (type) {
        case BreakfastType:
            return @"早餐";
            break;
        case BreakfastDotType:
            return @"早点";
        case LunchType:
            return @"午餐";
        case LunchDotType:
            return @"午点";
        case DinnerType:
            return @"晚餐";
        default:
            break;
    }
    return nil;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.mealDatas = [[NSMutableArray alloc]init];
    self.requestDict = [[NSMutableDictionary alloc]init];
    
    self.NHots = [[NSMutableArray alloc]init];
    self.NProteins = [[NSMutableArray alloc]init];
    self.NAxunges = [[NSMutableArray alloc]init];
    self.NCarbohydrates = [[NSMutableArray alloc]init];
    self.NCalciums = [[NSMutableArray alloc]init];
    self.NZelCs = [[NSMutableArray alloc]init];

    
    self.httprequest= [[HttpFormatRequest alloc]init];
    self.httprequest.Delegate = self;
    [self.view addSubview:self.httprequest];

    
    self.tableview = [[UITableView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height -44) style:UITableViewStylePlain];
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
	self.tableview.allowsSelection = NO;
	self.tableview.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	self.tableview.showsVerticalScrollIndicator = YES;
	[self.view insertSubview:self.tableview atIndex:0];

    {
        self.saveButton = [[UIButton alloc]initWithFrame:CGRectMake(270, 7, 45, 30)];
        [self.saveButton setBackgroundImage:[UIImage imageNamed:@"blankbutton"] forState:UIControlStateNormal];
        
        [self.saveButton setTitle:@"保存" forState:0];
        [self.view addSubview:self.saveButton];
        [self.saveButton addTarget:self action:@selector(saveMealList) forControlEvents:64];
        self.saveButton.hidden  = YES;
    }
    [self resetTmpData];
}
#pragma mark 减菜谱
-(void)reduceMeal:sender
{
    UIButton *button = sender;
    [self.mealDatas removeObjectAtIndex:button.tag];
    [self.NHots removeObjectAtIndex:button.tag];
    [self.NProteins removeObjectAtIndex:button.tag];
    [self.NAxunges removeObjectAtIndex:button.tag];
    [self.NCalciums removeObjectAtIndex:button.tag];
    [self.NCarbohydrates removeObjectAtIndex:button.tag];
    [self.NZelCs removeObjectAtIndex:button.tag];
    [self.tableview reloadData];
}
#pragma mark 添加菜谱
-(void)andMeal
{
    
    OptionalResultViewController *viewController = [[OptionalResultViewController alloc]initWithNibName:@"OptionalResultViewController" bundle:nil];
    [viewController.view setFrame:self.view.bounds];
    viewController.title = @"配餐";
    viewController.delegate = self;
    [self.navigationController pushViewController:viewController animated:YES];
}
#pragma mark delegate 

-(void)addMeals:(sysMeal*)meal;
{
    [self.mealDatas addObject:meal];
    [self.requestDict setObject:NSLocalizedString(@"PORT_SysMealDetail", nil) forKey:NSLocalizedString(@"PORT_ReqType", nil)];
    [self.requestDict setObject:meal.uType forKey:@"uType"];
    [self.requestDict setObject:[NSNumber numberWithInt:meal.ID] forKey:@"id"];
    [self.httprequest CustomFormDataRequestDict:[self.requestDict JSONRepresentation] tag:requestMealDetial url:WEBSERVERURL];
    [self.tableview reloadData];
}
-(NSString *)getMealType
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int type = [[defaults objectForKey:@"mealType"] intValue];
    switch (type) {
        case BreakfastType:
            return @"mealA";
            break;
        case BreakfastDotType:
            return @"mealB";
        case LunchType:
            return @"mealC";
        case LunchDotType:
            return @"mealD";
        case DinnerType:
            return @"mealE";
        default:
            break;
    }
    return nil;
}
-(NSString *)getStandardType
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int type = [[defaults objectForKey:@"mealType"] intValue];
    switch (type) {
        case BreakfastType:
            return BREAKFASTSTANDARD;
            break;
        case BreakfastDotType:
            return BREAKFASTBOTSTANDARD;
        case LunchType:
            return LUNCHSTANDARD;
        case LunchDotType:
            return LUNCHBOTSTANDARD;
        case DinnerType:
            return DINNERSTANDARD;
        default:
            break;

    }
    return nil;
}
-(void)saveMealList
{
    NSMutableArray *array = [[NSMutableArray alloc]init];
    for (sysMeal *meal in self.mealDatas) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:meal.name forKey:@"name"];
        [dict setObject:meal.uType forKey:@"uType"];
        [dict setObject:[NSNumber numberWithInt:meal.ID] forKey:@"id"];
        
        [array addObject:dict];
        [dict release];
    }
    [self.requestDict setObject:NSLocalizedString(@"PORT_SaveMealList", nil) forKey:NSLocalizedString(@"PORT_ReqType", nil)];
    [self.requestDict setObject:[NSNumber numberWithInt:[[Instance GetUseID] intValue]] forKey:@"userId"];
    [self.requestDict setObject:array forKey:[self getMealType]];
    [self.httprequest CustomFormDataRequestDict:[self.requestDict JSONRepresentation] tag:requestSaveDetial url:WEBSERVERURL];
    [array release];
}
-(void)saveTmpData
{
    if ([mealDatas count] >0) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];

        [self setOptionalFood:self.mealDatas dict:dict key:@"mealDatas"];
        [self setOptionalNutritiont:self.NHots dict:dict key:@"NHots"];
        [self setOptionalNutritiont:self.NProteins dict:dict key:@"NProteins"];
        [self setOptionalNutritiont:self.NCalciums dict:dict key:@"NCalciums"];
        [self setOptionalNutritiont:self.NAxunges dict:dict key:@"NAxunges"];
        [self setOptionalNutritiont:self.NCarbohydrates dict:dict key:@"NCarbohydrates"];
        [self setOptionalNutritiont:self.NZelCs dict:dict key:@"NZelCs"];

        [defaults setObject:dict forKey:[self getMealType]];
        [defaults synchronize];
    }
}
-(void)setOptionalNutritiont:(NSMutableArray *)arry dict:(NSMutableDictionary *)dictCollect key:(NSString *)stringKey
{
    NSMutableArray *optionalDatas = [[NSMutableArray alloc]init];
    for (FoodNutritiont *foodNut in arry) {
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        [mDict setObject:foodNut.nutritionAmount forKey:@"nutritionAmount"];
        [mDict setObject:foodNut.unit forKey:@"unit"];
        [mDict setObject:foodNut.nutritionName forKey:@"nutritionName"];
        [optionalDatas addObject:mDict];
        [mDict release];
    }
    [dictCollect setObject:optionalDatas forKey:stringKey];
    [optionalDatas release];
}
-(void)setOptionalFood:(NSMutableArray *)arry dict:(NSMutableDictionary *)dictCollect key:(NSString *)stringKey
{
    NSMutableArray *optionalDatas = [[NSMutableArray alloc]init];
    for (sysMeal *meal in arry) {
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        [mDict setObject:[NSNumber numberWithInt:meal.ID] forKey:@"id"];
        [mDict setObject:meal.uType forKey:@"uType"];
        [mDict setObject:meal.name forKey:@"name"];
        [optionalDatas addObject:mDict];
        [mDict release];
    }
    [dictCollect setObject:optionalDatas forKey:stringKey];
    [optionalDatas release];
}
-(void)resetTmpData
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dict = [defaults objectForKey:[self getMealType]];
    if (dict != nil) {
        [self getOptionalFood:(NSMutableArray *)[dict objectForKey:@"mealDatas"] lists:self.mealDatas];
        [self getOptionalNutritiont:(NSMutableArray *)[dict objectForKey:@"NHots"] lists:self.NHots];
        [self getOptionalNutritiont:(NSMutableArray *)[dict objectForKey:@"NProteins"] lists:self.NProteins];
        [self getOptionalNutritiont:(NSMutableArray *)[dict objectForKey:@"NCalciums"] lists:self.NCalciums];
        [self getOptionalNutritiont:(NSMutableArray *)[dict objectForKey:@"NCarbohydrates"] lists:self.NCarbohydrates];
        [self getOptionalNutritiont:(NSMutableArray *)[dict objectForKey:@"NAxunges"] lists:self.NAxunges];
        [self getOptionalNutritiont:(NSMutableArray *)[dict objectForKey:@"NZelCs"] lists:self.NZelCs];

    }
    [self.tableview reloadData];
}
-(void)getOptionalNutritiont:(NSMutableArray *)datas lists:(NSMutableArray *)lists
{
    for (NSDictionary *dict in datas) {
        FoodNutritiont *nutrition = [[FoodNutritiont alloc]init];
        nutrition.nutritionAmount = [dict objectForKey:@"nutritionAmount"];
        nutrition.nutritionName = [dict objectForKey:@"nutritionName"];
        nutrition.unit = [dict objectForKey:@"unit"];
        NSLog(@"nutrition.nutritionAmount :%@",nutrition.nutritionAmount);
        [lists addObject:nutrition];
        [nutrition release];

    }
}
-(void)getOptionalFood:(NSMutableArray *)datas lists:(NSMutableArray *)lists
{
    for (NSDictionary *dict in datas) {
        sysMeal *meal = [[sysMeal alloc]init];
        meal.ID = [[dict objectForKey:@"id"] intValue];
        meal.name = [dict objectForKey:@"name"];
        meal.uType = [dict objectForKey:@"uType"];

        [lists addObject:meal];
        [meal release];
        
    }
}
-(void)setsaveButton
{
    if ([self.mealDatas count] >0) 
        self.saveButton.hidden = NO;
    else
        self.saveButton.hidden = YES;


}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{
    if (tag == requestMealDetial) {
        [mealDatas removeLastObject];
        [self setsaveButton];

    }
}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    if (tag == requestSaveDetial) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"保存成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        return;
    }
    NSArray *nutritionList = [[[ResultContent JSONValue] valueForKey:@"nutrition"] retain];
    if ([nutritionList count] >0) {
        for (int i = 0 ; i < [nutritionList count]; i++) {
            FoodNutritiont *foodNut = [[FoodNutritiont alloc]init];
            
            foodNut.nutritionAmount = [[nutritionList objectAtIndex:i] valueForKey:@"amount"];
            foodNut.unit  = [[nutritionList objectAtIndex:i] valueForKey:@"unit"];
            foodNut.nutritionName = [[nutritionList objectAtIndex:i] valueForKey:@"name"];

            if ([foodNut.nutritionName isEqualToString:@"热量"]) {
                [self.NHots addObject:foodNut];
            }
            if ([foodNut.nutritionName isEqualToString:@"蛋白质"]) {
                [self.NProteins addObject:foodNut];
            }
            if ([foodNut.nutritionName isEqualToString:@"脂肪"]) {
                [self.NAxunges addObject:foodNut];
            }
            if ([foodNut.nutritionName isEqualToString:@"碳水化合物"]) {
                [self.NCarbohydrates addObject:foodNut];
            }
            if ([foodNut.nutritionName isEqualToString:@"钙"]) {
                [self.NCalciums addObject:foodNut];

            }
            if ([foodNut.nutritionName isEqualToString:@"维生素C"]){
                [self.NZelCs addObject:foodNut];
            }
            [foodNut release];
        }
    }
    [nutritionList release];
    [self setsaveButton];

    [self.tableview reloadData];
}
-(float)statisticsAmount:(NSMutableArray *)array
{
    float totality = 0.0f;
    for (FoodNutritiont *foodNut in array) {
        totality = totality + [foodNut.nutritionAmount floatValue];
    }
    return totality;
}
-(NSString *)getUnit:(NSMutableArray *)array
{
    for (FoodNutritiont *foodNut in array) {
        return foodNut.unit;
    }

    return nil;
}
- (NSString *)getNutritionValue:(ComponentCount)type
{
    switch (type) {
        case littleCount:
            return @"偏低";
            break;
        case normalCount:
            return @"正常";
            break;
        case moreCount:
            return @"偏高";

            break;
        case abundantCount:
            return @"充裕";

            break;
        default:
            break;
    }
    return nil;
}
-(void)setNutritionforCell:(UITableViewCell*)cell row:(int)row
{
    
    UIFont *font = [UIFont boldSystemFontOfSize:15];
    UILabel *labelName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, TableViewHeiht)];
    labelName.textAlignment = UITextAlignmentCenter;
    labelName.backgroundColor = [UIColor clearColor];
    labelName.font = font;
    labelName.text = @"";
    [cell.contentView addSubview:labelName];
    [labelName release];
    
    UILabel *labelAmont = [[UILabel alloc]initWithFrame:CGRectMake(120, 0, 110, TableViewHeiht)];
    labelAmont.textAlignment = UITextAlignmentLeft;
    labelAmont.backgroundColor = [UIColor clearColor];
    labelAmont.font = font;
    labelAmont.text = @"";
    [cell.contentView addSubview:labelAmont];
    [labelAmont release];
    
    UILabel *explainLabel = [[UILabel alloc]initWithFrame:CGRectMake(220, 0, 110, TableViewHeiht)];
    explainLabel.textAlignment = UITextAlignmentLeft;
    explainLabel.backgroundColor = [UIColor clearColor];
    explainLabel.font = font;
    explainLabel.text = @"";
    [cell.contentView addSubview:explainLabel];
    [explainLabel release];

    
    float totality = 0.0f;
    NSString *nutritionName = nil;
    NSString *Unit = nil;
    NSString *explain = nil;
    float totalityPercentage = 0.0f;
    float NHotsTotality = [self statisticsAmount:self.NHots];

    switch (row) {
        case NHotType:
            totality = [self statisticsAmount:self.NHots];
            Unit   = [self getUnit:self.NHots];
            nutritionName = @"热量";
            totalityPercentage = totality / [[self getNutritionStandard:[self getStandardType] element:HOT] floatValue] * 100;
            if (totalityPercentage < 90) {
                explain = [self getNutritionValue:littleCount];
            }
            else if (totalityPercentage > 90 && totalityPercentage < 120)
                explain = [self getNutritionValue:normalCount];
            else
                explain = [self getNutritionValue:moreCount];
            break;
        case NProteinType:
            totality = [self statisticsAmount:self.NProteins];
            Unit = [self getUnit:self.NProteins];
            nutritionName = @"蛋白质";

            
            if (totality < [[self getNutritionStandard:[self getStandardType] element:PROTEINMIN] floatValue]) {
                explain = [self getNutritionValue:littleCount];
            }
            else if ((totality > [[self getNutritionStandard:[self getStandardType] element:PROTEINMIN] floatValue]) && (totality < [[self getNutritionStandard:[self getStandardType] element:PROTEINMAX] floatValue]))
                explain = [self getNutritionValue:normalCount];
            else
                explain = [self getNutritionValue:abundantCount];
            
             break;
         case NAxungeType:
            totality = [self statisticsAmount:self.NAxunges];
            Unit = [self getUnit:self.NAxunges];
            nutritionName = @"脂肪";
            
            totalityPercentage = (totality * 9)/NHotsTotality * 100;

            if (totalityPercentage < 20) {
                explain = [self getNutritionValue:littleCount];
            }
            else if (totalityPercentage > 20 && totalityPercentage < 25)
                explain = [self getNutritionValue:normalCount];
            else
                explain = [self getNutritionValue:moreCount];
            break;
         case NCarbohydrateType:
            totality = [self statisticsAmount:self.NCarbohydrates];
            Unit = [self getUnit:self.NCarbohydrates];
            nutritionName = @"碳水化合物";
            
            totalityPercentage = (totality * 4)/NHotsTotality * 100 ;
            
            if (totalityPercentage < 60) {
                explain = [self getNutritionValue:littleCount];
            }
            else if (totalityPercentage > 60 && totalityPercentage < 70)
                explain = [self getNutritionValue:normalCount];
            else
                explain = [self getNutritionValue:moreCount];

            
            break;
         case NCalciumType:
            totality = [self statisticsAmount:self.NCalciums];
            Unit = [self getUnit:self.NCalciums];
            nutritionName = @"钙";

            totalityPercentage = totality / [[self getNutritionStandard:[self getStandardType] element:CALCIUM] floatValue] * 100;

            if (totalityPercentage < 80) {
                explain = [self getNutritionValue:littleCount];
            }
            else if (totalityPercentage > 80 && totalityPercentage < 100)
                explain = [self getNutritionValue:normalCount];
            else
                explain = [self getNutritionValue:abundantCount];
            
            break;
         case NZelCType:
            totality = [self statisticsAmount:self.NZelCs];
            Unit = [self getUnit:self.NZelCs];
            nutritionName = @"维生素C";
            totalityPercentage = totality / [[self getNutritionStandard:[self getStandardType] element:VITAMIN] floatValue] * 100;

            if (totalityPercentage < 80) {
                explain = [self getNutritionValue:littleCount];
            }
            else if (totalityPercentage > 80 && totalityPercentage < 100 )
                explain = [self getNutritionValue:normalCount];
            else
                explain = [self getNutritionValue:abundantCount];

            break;
         default:
         break;
         }
    labelName.text =nutritionName;
    labelAmont.text = [NSString stringWithFormat:@"%0.2f %@",totality,Unit];
    explainLabel .text = explain;
}

#pragma mark tableview;
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    
    return dafaultCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0) {
        if ([mealDatas count]<dafaultCount) {
            return dafaultCount;
        }
        return [mealDatas count];
    }
    if (section == 1) {
        return 6;
    }
    if (section == 2) {
        return 1;
    }
    return 0;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIImageView *headerImageView = [[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, TableViewHeiht)] autorelease];
    [headerImageView setImage:[UIImage imageNamed:@"cellheader.png"]];
    [headerImageView setUserInteractionEnabled:YES];
    UILabel *mLabel = [Instance addLableRect:CGRectMake(5, 0, 320, TableViewHeiht) text:@"" textsize:15];
    mLabel.textAlignment = UITextAlignmentCenter;
    [headerImageView addSubview:mLabel];
    [mLabel release];

    if (section == 0) {
        mLabel.text = @"菜谱";
        UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(280, 0, 26, 28)];
        [button setBackgroundImage:[UIImage imageNamed:@"addButton"] forState:0];
        [button addTarget:self  action:@selector(andMeal) forControlEvents:64];
        [headerImageView addSubview:button];
        [button release];
    }
    if (section == 1) {
        mLabel.text = @"营养合计";
    }
    if (section == 2) {
        mLabel.text = @"小知识";
    }
    return headerImageView;
}
-(float)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return TableViewHeiht;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if ([indexPath section] == 2) {
        return ([self getTipViewHeight].height+TableViewHeiht);
    }
    return TableViewHeiht;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];

    }
    for (UIView *view in [cell.contentView subviews]) {
        [view removeFromSuperview];
    }
    cell.textLabel.text = @"";

    if ([indexPath section] == 0) {
        if ([mealDatas count] == 0 || [indexPath row] >=[mealDatas count]) {
            return cell;
        }
        sysMeal *meal = [mealDatas objectAtIndex:[indexPath row]];
        cell.textLabel.text = meal.name;
        cell.textLabel.textAlignment = UITextAlignmentCenter;
        cell.textLabel.backgroundColor = [UIColor clearColor];
        UIButton *button = [ViewTool addUIButton:cell.contentView imageName:@"reduce.png" type:@"" x:250 y:0 x1:26 y1:28];
        button.tag = [indexPath row];
        [button addTarget:self action:@selector(reduceMeal:) forControlEvents:64];
    }
    if ([indexPath section] == 1) {
        if ([mealDatas count] == 0) {
            return cell;
        }
        [self setNutritionforCell:cell row:[indexPath row]];
        return cell;
    }
    if ([indexPath section] == 2) {
        CGRect frame  = cell.frame;
        frame.size = [self getTipViewHeight];
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.text = [self getTipText];
        cell.textLabel.font = [UIFont boldSystemFontOfSize:14];;
        cell.textLabel.frame = frame;
        return cell;
    }
    // Configure the cell...
    
    return cell;
}
#pragma mark getheadviewheight
-(CGSize)getTipViewHeight
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tiptext" ofType:@"plist"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        CGSize size = CGSizeMake(cellSizeWidth, 3000);
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:filePath];
        UIFont *font = [UIFont boldSystemFontOfSize:14];
        CGSize labelsize = [[dict objectForKey:[self getMealType]] sizeWithFont:font constrainedToSize:size lineBreakMode:UILineBreakModeClip];
        return labelsize;
    }
    return CGSizeMake(cellSizeWidth, TableViewHeiht);
}
-(NSString *)getTipText
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tiptext" ofType:@"plist"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:filePath];
        return [dict objectForKey:[self getMealType]];
    }
    return nil;
}
-(NSString *)getNutritionStandard:(NSString *)standard element:(NSString *)element
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tiptext" ofType:@"plist"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:filePath];
        return [[dict objectForKey:standard] objectForKey:element];
    }

    return nil;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
