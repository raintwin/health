//
//  FoodMenuAnalysisController.m
//  Health
//
//  Created by hq on 12-6-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "FoodMenuAnalysisController.h"
#import "FoodMatterInfoController.h"
#import "RecommendDinnerView.h"


#import "FoodView.h"
#import "HitFoodView.h"
#import "HitNutView.h"
#import "ProteinView.h"
#import "FatView.h"
#import "RatioView.h"

#define NUMBERS @"0123456789.\n"

@implementation FoodMenuAnalysisController
@synthesize mAnysisRecipeDict;
@synthesize mAnalysisResultDict;
@synthesize isDinner;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
//        [self.tableView setFrame:CGRectMake(0, 0, self.view.frame.size.width, 370)];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)navBack
{

    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark 晚餐推荐
-(void)RecommentRecipe
{

    RecommendDinnerView *ctrView = [[RecommendDinnerView alloc]initWithNibName:@"RecommendDinnerView" bundle:nil];
    ctrView.mAnalysisResultDict = mAnalysisResultDict;
    ctrView.title = @"晚餐推荐";
    [self.navigationController pushViewController:ctrView animated:YES];
    [ctrView release];
}
#pragma mark - View lifecycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    //BaiduMobStat* statTracker = [BaiduMobStat defaultStat];    [statTracker logEvent:@"mealAnysis" eventLabel:@"mealAnysis"];

    

    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];
//    if (!isDinner) {
//        [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:FOURENSURE title:@"晚餐推荐" target:self selector:@selector(RecommentRecipe)];
//    }
    
    
    NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
    self.mAnalysisResultDict = mDict;
    [mDict release];

    [self performSelector:@selector(GetRecipe) withObject:nil afterDelay:0.1f];
}
-(void)GetRecipe
{
    
    NSURL *url;
    NSMutableDictionary*mDict = [[NSMutableDictionary alloc] init];
    
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",                                                                                                                                                                                                                                                                                                                                                                                                                                                                              WEBDEFAULTSERVERURL,@"/InSchoolArrangeMeal/TPFetchOneDayMenuNutrimentAnalysisResult"];
    NSLog(@"urlstr ---:%@",urlstr);
    url = [NSURL URLWithString:urlstr];
    
    NSLog(@"mAnysisRecipeDict :%@",[mAnysisRecipeDict JSONRepresentation]);

    
    [mDict setObject:[NSNumber numberWithInt:[[mAnysisRecipeDict objectForKey:@"weekArrangeMealBasicID"] intValue]] forKey:@"weekArrangeMealBasicID"];
    NSLog(@"dayOfWeek :%@",[mAnysisRecipeDict objectForKey:@"dayOfWeek"]);
    [mDict setObject:[mAnysisRecipeDict objectForKey:@"dayOfWeek"] forKey:@"whichDay"];

    
    
    NSString *JSONString = [mDict JSONRepresentation];
    [mDict release];
    
    
    HttpFormatRequest *mHttpRequest = [[HttpFormatRequest alloc]init];
    mHttpRequest.Delegate = self;
    [mHttpRequest CustomFormDataRequestDict:JSONString tag:0 url:urlstr];
    [self.view addSubview:mHttpRequest];
    [mHttpRequest release];
    
}
#pragma  mark requestFinished

-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    [self performSelectorOnMainThread:@selector(FinishMict:) withObject:ResultContent waitUntilDone:YES];
    
}

-(void)FinishMict:(NSString *)ResultContent
{
    UIView *mTmpeView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 480)];
    [mTmpeView setUserInteractionEnabled:YES];
    [self.view addSubview:mTmpeView];
    [mTmpeView release];
    
    mIsDownOver = NO;
    NSMutableDictionary *mTmepDict = [[NSMutableDictionary alloc]init];

    [mTmepDict setObject:[NSNumber numberWithBool:[[[ResultContent JSONValue] valueForKey:@"dayOfWeek"] boolValue]] forKey:@"todayOrOther"];
    
    [SVProgressHUD show];
    //Begin 食物类别计量页面所需要的指标
    NSString *str = [NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"wheatFlourAndRice"]];
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"miscellaneousGrainCrop"]] forKey:@"miscellaneousGrainCrop"];//2
    [mTmepDict setObject:str forKey:@"wheatFlourAndRice"];//3
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"milkStuff"]] forKey:@"milkStuff"];//4
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"cakeFood"]] forKey:@"cakeFood"];//5
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"drySoyBeanFood"]] forKey:@"drySoyBeanFood"];//6
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"soyBeanProduct"]] forKey:@"soyBeanProduct"];//7
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"lightColoredVegetable"]] forKey:@"lightColoredVegetable"];//8
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"greenOrangeVegetable"]] forKey:@"greenOrangeVegetable"];//9
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"dryVegetable"]] forKey:@"dryVegetable"]; //10
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"seaVegetable"]] forKey:@"seaVegetable"];//11
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"saltVegetable"]] forKey:@"saltVegetable"];   //12
    
    
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"fruit"]] forKey:@"fruit"]; //13
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"solidShellFood"]] forKey:@"solidShellFood"];//14
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"milkFood"]] forKey:@"milkFood"];//15
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"eggFood"]] forKey:@"eggFood"];//16
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"meatFood"]] forKey:@"meatFood"];//17
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"liverFood"]] forKey:@"liverFood"];//18
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"fishAndShrimp"]] forKey:@"fishAndShrimp"];//19
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"starchAndSugur"]] forKey:@"starchAndSugur"];//20
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"cookingOil"]] forKey:@"cookingOil"];//21
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"soySauceOil"]] forKey:@"soySauceOil"];//22
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"cookingSalt"]] forKey:@"cookingSalt"];//23
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"otherFood"]] forKey:@"otherFood"];//24
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"betaGamaE"]] forKey:@"betaGamaE"];//25
    
    
    //End 食物类别计量页面所需要的指标
    
    
    
    //Begin 热量食物来源分布页面所需要的指标
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"grainEnergy"]] forKey:@"grainEnergy"];//30
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"soyBeanEnergy"]] forKey:@"soyBeanEnergy"];//31
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"otherPlantEnergy"]] forKey:@"otherPlantEnergy"];//32
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"animalEnergy"]] forKey:@"animalEnergy"];//33
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"oilEnergy"]] forKey:@"oilEnergy"];//34
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"sugarEnergy"]] forKey:@"sugarEnergy"];//35
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"potatoEnergy"]] forKey:@"potatoEnergy"];//36
    
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"grainEnergyPertentage"]] forKey:@"grainEnergyPertentage"];//40
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"soyBeanEnergyPertentage"]] forKey:@"soyBeanEnergyPertentage"];//41
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"otherPlantEnergyPertentage"]] forKey:@"otherPlantEnergyPertentage"];//42
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"animalEnergyPertentage"]] forKey:@"animalEnergyPertentage"];//43
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"oilEnergyPertentage"]] forKey:@"oilEnergyPertentage"];//44
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"sugarEnergyPertentage"]] forKey:@"sugarEnergyPertentage"];//45
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"potatoEnergyPertentage"]] forKey:@"potatoEnergyPertentage"];//46
    //End 热量食物来源分布页面所需要的指标
    
    
    //Begin 热量营养素来源分布页面所需要的指标
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"carbohydrateEnergy"]] forKey:@"carbohydrateEnergy"];//60
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"porteinEnergy"]] forKey:@"porteinEnergy"];//61
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"fatEnergy"]] forKey:@"fatEnergy"];//62
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"carbohydrateEnergyPertentage"]] forKey:@"carbohydrateEnergyPertentage"];//63
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"porteinEnergyPertentage"]] forKey:@"porteinEnergyPertentage"];//64
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"fatEnergyPertentage"]] forKey:@"fatEnergyPertentage"];//65
    //End 热量营养素来源分布页面所需要的指标
    
    
    
    //Begin 蛋白质来源分布页面所需要的指标
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"animalPortein"]] forKey:@"animalPortein"];//80
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"soyBeanPortein"]] forKey:@"soyBeanPortein"];//81
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"milletPortein"]] forKey:@"milletPortein"];//82
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"otherPortein"]] forKey:@"otherPortein"];//83
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"animalPorteinPertentage"]] forKey:@"animalPorteinPertentage"];//84
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"soyBeanPorteinPertentage"]] forKey:@"soyBeanPorteinPertentage"];//85
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"milletPorteinPertentage"]] forKey:@"milletPorteinPertentage"];//86
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"otherPorteinPertentage"]] forKey:@"otherPorteinPertentage"];//87
    
    //End 蛋白质来源分布页面所需要的指标
    
    
    //Begin 脂肪来源分布页面所需要的指标
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"animalFat"]] forKey:@"animalFat"];//90
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"plantFat"]] forKey:@"plantFat"];//91
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"animalFatPertentage"]] forKey:@"animalFatPertentage"];//92
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"plantFatPertentage"]] forKey:@"plantFatPertentage"];//93
    //End 脂肪来源分布页面所需要的指标
    
    
    //Begin 三餐比例及重要营养素比例页面所需要的指标
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"breakfastEnergy"]] forKey:@"breakfastEnergy"];//100
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"breakfastDessertEnergy"]] forKey:@"breakfastDessertEnergy"];//101
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"lunchEnergy"]] forKey:@"lunchEnergy"];//102
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"lunchDessertEnergy"]] forKey:@"lunchDessertEnergy"];//103
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"supperEnergy"]] forKey:@"supperEnergy"];//104
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"supperDessertEnergy"]] forKey:@"supperDessertEnergy"];//105
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"breakfastEnergyPertentage"]] forKey:@"breakfastEnergyPertentage"];//106
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"breakfastDessertEnergyPertentage"]] forKey:@"breakfastDessertEnergyPertentage"];//107
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"lunchEnergyPertentage"]] forKey:@"lunchEnergyPertentage"];//108
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"lunchDessertEnergyPertentage"]] forKey:@"lunchDessertEnergyPertentage"];//109
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"supperEnergyPertentage"]] forKey:@"supperEnergyPertentage"];//110
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"supperDessertEnergyPertentage"]] forKey:@"supperDessertEnergyPertentage"];//111
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"calciumPhosphorusRatio"]] forKey:@"calciumPhosphorusRatio"];//119
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"greenOrangeVegetableInVegetablePertentage"]] forKey:@"greenOrangeVegetableInVegetablePertentage"];//120
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"porteinFatCarbohydrateRatio"]] forKey:@"porteinFatCarbohydrateRatio"];//121
    //End 三餐比例及重要营养素比例页面所需要的指标
    
    
    //Begin 营养素分析结果页面所需要的指标
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"energyFullDayStandard"]] forKey:@"energyFullDayStandard"];//X130
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"energyInSchoolStandard"]] forKey:@"energyInSchoolStandard"];//X131
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"energyInSchoolSupply"]] forKey:@"energyInSchoolSupply"];//X132
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"energyInSchoolPercentage"]] forKey:@"energyInSchoolPercentage"];//X133
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"energyEvaluationValue"]] forKey:@"energyEvaluationValue"];//X134
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"carbohydrateEnergyFullDayStandard"]] forKey:@"carbohydrateEnergyFullDayStandard"];//X135
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"carbohydrateEnergyInSchoolStandard"]] forKey:@"carbohydrateEnergyInSchoolStandard"];//X136
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"carbohydrateEnergyInSchoolSupply"]] forKey:@"carbohydrateEnergyInSchoolSupply"];//X137
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"carbohydrateEnergyInSchoolPercentage"]] forKey:@"carbohydrateEnergyInSchoolPercentage"];//X138
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"carbohydrateEnergyEvaluationValue"]] forKey:@"carbohydrateEnergyEvaluationValue"];//X139
    
    
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"fatEnergyFullDayStandard"]] forKey:@"fatEnergyFullDayStandard"];//X140
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"fatEnergyInSchoolStandard"]] forKey:@"fatEnergyInSchoolStandard"];//X141
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"fatEnergyInSchoolSupply"]] forKey:@"fatEnergyInSchoolSupply"];//X142
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"fatEnergyInSchoolPercentage"]] forKey:@"fatEnergyInSchoolPercentage"];//X143
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"fatEnergyEvaluationValue"]] forKey:@"fatEnergyEvaluationValue"];//X144
    
    
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"porteinEnergyFullDayStandard"]] forKey:@"porteinEnergyFullDayStandard"];//X145
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"porteinEnergyInSchoolStandard"]] forKey:@"porteinEnergyInSchoolStandard"];//X146
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"porteinEnergyInSchoolSupply"]] forKey:@"porteinEnergyInSchoolSupply"];//X147
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"porteinEnergyInSchoolPercentage"]] forKey:@"porteinEnergyInSchoolPercentage"];//X148
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"porteinEnergyEvaluationValue"]] forKey:@"porteinEnergyEvaluationValue"];//X149
    
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"porteinFullDayStandard"]] forKey:@"porteinFullDayStandard"];//X150
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"porteinInSchoolStandard"]] forKey:@"porteinInSchoolStandard"];//X151
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"porteinInSchoolSupply"]] forKey:@"porteinInSchoolSupply"];//X152
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"porteinInSchoolPercentage"]] forKey:@"porteinInSchoolPercentage"];//X153
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"porteinEvaluationValue"]] forKey:@"porteinEvaluationValue"];//X154
    
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"animalPorteinFullDayStandard"]] forKey:@"animalPorteinFullDayStandard"];//X155
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"animalPorteinInSchoolStandard"]] forKey:@"animalPorteinInSchoolStandard"];//X156
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"animalPorteinInSchoolSupply"]] forKey:@"animalPorteinInSchoolSupply"];//X157
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"animalPorteinInSchoolPercentage"]] forKey:@"animalPorteinInSchoolPercentage"];//X158
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"animalPorteinEvaluationValue"]] forKey:@"animalPorteinEvaluationValue"];//X159
    
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"animalAndSoyBeanPorteinFullDayStandard"]] forKey:@"animalAndSoyBeanPorteinFullDayStandard"];//X160
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"animalAndSoyBeanPorteinInSchoolStandard"]] forKey:@"animalAndSoyBeanPorteinInSchoolStandard"];//X161
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"animalAndSoyBeanPorteinInSchoolSupply"]] forKey:@"animalAndSoyBeanPorteinInSchoolSupply"];//X162
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"animalAndSoyBeanPorteinInSchoolPercentage"]] forKey:@"animalAndSoyBeanPorteinInSchoolPercentage"];//X163
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"animalAndSoyBeanPorteinEvaluationValue"]] forKey:@"animalAndSoyBeanPorteinEvaluationValue"];//X164
    
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"calciumFullDayStandard"]] forKey:@"calciumFullDayStandard"];//X166
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"calciumInSchoolStandard"]] forKey:@"calciumInSchoolStandard"];//X166
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"calciumInSchoolSupply"]] forKey:@"calciumInSchoolSupply"];//X167
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"calciumInSchoolPercentage"]] forKey:@"calciumInSchoolPercentage"];//X168
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"calciumEvaluationValue"]] forKey:@"calciumEvaluationValue"];//X169
    
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitAFullDayStandard"]] forKey:@"vitAFullDayStandard"];//x170
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitAInSchoolStandard"]] forKey:@"vitAInSchoolStandard"];//x171
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitAInSchoolSupply"]] forKey:@"vitAInSchoolSupply"];//x172
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitAInSchoolPercentage"]] forKey:@"vitAInSchoolPercentage"];//x173
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitAEvaluationValue"]] forKey:@"vitAEvaluationValue"];//x174
    
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitB1FullDayStandard"]] forKey:@"vitB1FullDayStandard"];//x175
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitB1InSchoolStandard"]] forKey:@"vitB1InSchoolStandard"];//x176
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitB1InSchoolSupply"]] forKey:@"vitB1InSchoolSupply"];//x177
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitB1InSchoolPercentage"]] forKey:@"vitB1InSchoolPercentage"];//x178
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitB1EvaluationValue"]] forKey:@"vitB1EvaluationValue"];//x179
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitB2FullDayStandard"]] forKey:@"vitB2FullDayStandard"];//x180
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitB2InSchoolStandard"]] forKey:@"vitB2InSchoolStandard"];//x181
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitB2InSchoolSupply"]] forKey:@"vitB2InSchoolSupply"];//x182
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitB2InSchoolPercentage"]] forKey:@"vitB2InSchoolPercentage"];//x183
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitB2EvaluationValue"]] forKey:@"vitB2EvaluationValue"];//x184
    
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitCFullDayStandard"]] forKey:@"vitCFullDayStandard"];//x185
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitCInSchoolStandard"]] forKey:@"vitCInSchoolStandard"];//x186
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitCInSchoolSupply"]] forKey:@"vitCInSchoolSupply"];//x187
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitCInSchoolPercentage"]] forKey:@"vitCInSchoolPercentage"];//x188
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitCEvaluationValue"]] forKey:@"vitCEvaluationValue"];//x189
    
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitEFullDayStandard"]] forKey:@"vitEFullDayStandard"];//x190
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitEInSchoolStandard"]] forKey:@"vitEInSchoolStandard"];//x191
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitEInSchoolSupply"]] forKey:@"vitEInSchoolSupply"];//x192
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitEInSchoolPercentage"]] forKey:@"vitEInSchoolPercentage"];//x193
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitEEvaluationValue"]] forKey:@"vitEEvaluationValue"];//x194
    
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitPPFullDayStandard"]] forKey:@"vitPPFullDayStandard"];//x195
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitPPInSchoolStandard"]] forKey:@"vitPPInSchoolStandard"];//x196
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitPPInSchoolSupply"]] forKey:@"vitPPInSchoolSupply"];//x197
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitPPInSchoolPercentage"]] forKey:@"vitPPInSchoolPercentage"];//x198
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"vitPPEvaluationValue"]] forKey:@"vitPPEvaluationValue"];//x199
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"potassiumFullDayStandard"]] forKey:@"potassiumFullDayStandard"];//x200
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"potassiumInSchoolStandard"]] forKey:@"potassiumInSchoolStandard"];//x201
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"potassiumInSchoolSupply"]] forKey:@"potassiumInSchoolSupply"];//x202
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"potassiumInSchoolPercentage"]] forKey:@"potassiumInSchoolPercentage"];//x203
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"potassiumEvaluationValue"]] forKey:@"potassiumEvaluationValue"];//x204
    
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"magnesiumFullDayStandard"]] forKey:@"magnesiumFullDayStandard"];//x205
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"magnesiumInSchoolStandard"]] forKey:@"magnesiumInSchoolStandard"];//x206
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"magnesiumInSchoolSupply"]] forKey:@"magnesiumInSchoolSupply"];//x207
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"magnesiumInSchoolPercentage"]] forKey:@"magnesiumInSchoolPercentage"];//x208
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"magnesiumEvaluationValue"]] forKey:@"magnesiumEvaluationValue"];//x209
    
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"ironFullDayStandard"]] forKey:@"ironFullDayStandard"];//x210
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"ironInSchoolStandard"]] forKey:@"phosphorusInSchoolStandard"];//x211
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"ironInSchoolSupply"]] forKey:@"ironInSchoolSupply"];//x212
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"ironInSchoolPercentage"]] forKey:@"ironInSchoolPercentage"];//x213
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"ironEvaluationValue"]] forKey:@"ironEvaluationValue"];//x214
    
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"zincFullDayStandard"]] forKey:@"zincFullDayStandard"];//x215
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"zincInSchoolStandard"]] forKey:@"zincInSchoolStandard"];//x216
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"zincInSchoolSupply"]] forKey:@"zincInSchoolSupply"];//x217
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"zincInSchoolPercentage"]] forKey:@"zincInSchoolPercentage"];//x218
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"zincEvaluationValue"]] forKey:@"zincEvaluationValue"];//x219
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"phosphorusFullDayStandard"]] forKey:@"phosphorusFullDayStandard"];//x220
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"phosphorusInSchoolStandard"]] forKey:@"phosphorusInSchoolStandard"];//x221
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"phosphorusInSchoolSupply"]] forKey:@"phosphorusInSchoolSupply"];//x222
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"phosphorusInSchoolPercentage"]] forKey:@"phosphorusInSchoolPercentage"];//x223
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"phosphorusEvaluationValue"]] forKey:@"phosphorusEvaluationValue"];//x224
    
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"seleniumFullDayStandard"]] forKey:@"seleniumFullDayStandard"];//x225
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"seleniumInSchoolStandard"]] forKey:@"seleniumInSchoolStandard"];//x226
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"seleniumInSchoolSupply"]] forKey:@"seleniumInSchoolSupply"];//x227
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"seleniumInSchoolPercentage"]] forKey:@"seleniumInSchoolPercentage"];//x228
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"seleniumEvaluationValue"]] forKey:@"seleniumEvaluationValue"];//x229
    
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"calciumPhosphorusRatioFullDayStandard"]] forKey:@"calciumPhosphorusRatioFullDayStandard"];//x230
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"calciumPhosphorusRatioInSchoolStandard"]] forKey:@"calciumPhosphorusRatioInSchoolStandard"];//x231
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"calciumPhosphorusRatioInSchoolSupply"]] forKey:@"calciumPhosphorusRatioInSchoolSupply"];//x232
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"calciumPhosphorusRatioInSchoolPercentage"]] forKey:@"calciumPhosphorusRatioInSchoolPercentage"];//x233
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"calciumPhosphorusRatioEvaluationValue"]] forKey:@"calciumPhosphorusRatioEvaluationValue"];//x234
    
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"caroteneFullDayStandard"]] forKey:@"caroteneFullDayStandard"];//x235
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"caroteneInSchoolStandard"]] forKey:@"caroteneInSchoolStandard"];//x236
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"caroteneInSchoolSupply"]] forKey:@"caroteneInSchoolSupply"];//x237
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"caroteneInSchoolPercentage"]] forKey:@"caroteneInSchoolPercentage"];//x238
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"caroteneEvaluationValue"]] forKey:@"caroteneEvaluationValue"];//x239
    
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"fiberFullDayStandard"]] forKey:@"fiberFullDayStandard"];//x240
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"fiberInSchoolStandard"]] forKey:@"fiberInSchoolStandard"];//x241
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"fiberInSchoolSupply"]] forKey:@"fiberInSchoolSupply"];//x242
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"fiberInSchoolPercentage"]] forKey:@"fiberInSchoolPercentage"];//x243
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"fiberEvaluationValue"]] forKey:@"fiberEvaluationValue"];//x244
    
    
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"cholesterolFullDayStandard"]] forKey:@"cholesterolFullDayStandard"];//x245
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"cholesterolInSchoolStandard"]] forKey:@"cholesterolInSchoolStandard"];//x246
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"cholesterolInSchoolSupply"]] forKey:@"cholesterolInSchoolSupply"];//x247
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"cholesterolInSchoolPercentage"]] forKey:@"cholesterolInSchoolPercentage"];//x248
    [mTmepDict setObject:[NSString stringWithFormat:@"%@",[[ResultContent JSONValue] valueForKey:@"cholesterolEvaluationValue"]] forKey:@"cholesterolEvaluationValue"];//x249
    
    
    
    //End 营养素分析结果页面所需要的指标
    
    
    for (NSString *KEY in mTmepDict) {
        
        NSString *KeyStr = [NSString stringWithFormat:@"%@",[mTmepDict objectForKey:KEY]];
        if ([self isFloat:KeyStr]) {
            [mAnalysisResultDict setObject:[NSString stringWithFormat:@"%0.2f",[[mTmepDict objectForKey:KEY] floatValue]] forKey:KEY];
        }
        else {
            [mAnalysisResultDict setObject:[NSString stringWithFormat:@"%@",[mTmepDict objectForKey:KEY]] forKey:KEY];
        }
    }
    [mTmepDict release];
    [self.tableView reloadData];
    [SVProgressHUD dismiss];
    mIsDownOver = YES;
    [mTmpeView removeFromSuperview];
}
-(BOOL)isFloat:(NSString*)string
{
    NSCharacterSet *cs;    
    cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS] invertedSet];        
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];        
    BOOL basicTest = [string isEqualToString:filtered];
    return basicTest;
}

-(void)dealloc
{
    [mAnalysisResultDict release];
    [mAnysisRecipeDict release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mAnysisRecipeDict = nil;
    self.mAnalysisResultDict  = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  40;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];

    NSArray *arrays = [[NSArray alloc]initWithObjects:@"食物类别计量",@"热量食物来源分布",@"热量营养素来源分布",@"蛋白质来源分布",@"脂肪来源分布",@"三餐比例及重要营养比例",@"营养素计量", nil ];

    cell.textLabel.text  = [arrays objectAtIndex:[indexPath row]];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [arrays release];
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate
#define FoodViewTag     1001
#define HitFoodViewTag  1002
#define HitNutViewTag   1003
#define ProteinViewTag  1004
#define FatViewTag      1005
#define RatioViewTag    1006
#define NutViewTag      1007
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    //Begin 食物类别计量页面所需要的指标          1001
    //Begin 热量食物来源分布页面所需要的指标       1002
    //Begin 热量营养素来源分布页面所需要的指标      1003
    //Begin 蛋白质来源分布页面所需要的指标         1004
    //Begin 脂肪来源分布页面所需要的指标           1005
    //Begin 三餐比例及重要营养素比例页面所需要的指标 1006
    //Begin 营养素分析结果页面所需要的指标         1007
    
//   NSArray *arrays = [[NSArray alloc]initWithObjects:@"食物类别计量",@"热量食物来源分布",@"热量营养素来源分布",@"蛋白质来源分布",@"脂肪来源分布",@"三餐比例及重要营养比例",@"营养素计量", nil ];
//
    
    if (!mIsDownOver) {
        return;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([indexPath row] == 0) {
        FoodView *fctr = [[FoodView alloc]initWithStyle:UITableViewStylePlain];
        fctr.title = @"食物类别计量";
        fctr.mAnalysisResultDict = mAnalysisResultDict;
        [self.navigationController pushViewController:fctr animated:YES];
        [fctr release];
    }
    if ([indexPath row] == 1) {
        HitFoodView *fctr = [[HitFoodView alloc]initWithStyle:UITableViewStylePlain];
        fctr.title = @"热量食物来源分布";
        fctr.mAnalysisResultDict = mAnalysisResultDict;
        [self.navigationController pushViewController:fctr animated:YES];
        [fctr release];
    }
    if ([indexPath row] == 2) {
        HitNutView *fctr = [[HitNutView alloc]initWithStyle:UITableViewStylePlain];
        fctr.title = @"热量营养素来源分布";
        fctr.mAnalysisResultDict = mAnalysisResultDict;
        [self.navigationController pushViewController:fctr animated:YES];
        [fctr release];
    }
    if ([indexPath row] == 3) {
        ProteinView *fctr = [[ProteinView alloc]initWithStyle:UITableViewStylePlain];
        fctr.title = @"蛋白质来源分布";
        fctr.mAnalysisResultDict = mAnalysisResultDict;
        [self.navigationController pushViewController:fctr animated:YES];
        [fctr release];
    }
    if ([indexPath row] == 4) {
        FatView *fctr = [[FatView alloc]initWithStyle:UITableViewStylePlain];
        fctr.title = @"脂肪来源分布";
        fctr.mAnalysisResultDict = mAnalysisResultDict;
        [self.navigationController pushViewController:fctr animated:YES];
        [fctr release];
    }
    if ([indexPath row] == 5) {
        RatioView *fctr = [[RatioView alloc]initWithStyle:UITableViewStylePlain];
        fctr.title = @"三餐比例及重要营养比例";
        fctr.mAnalysisResultDict = mAnalysisResultDict;
        [self.navigationController pushViewController:fctr animated:YES];
        [fctr release];
    }
    if ([indexPath row] == 6) {
        FoodMatterInfoController *fctr = [[FoodMatterInfoController alloc]initWithStyle:UITableViewStylePlain];
        fctr.title = @"营养素计量";
        fctr.mAnalysisResultDict = mAnalysisResultDict;
        [self.navigationController pushViewController:fctr animated:YES];
        [fctr release];
    }
}

@end
