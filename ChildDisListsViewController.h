//
//  ChildDisListsViewController.h
//  Health
//
//  Created by hq on 12-7-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChildDisListsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,UIAlertViewDelegate,HttpFormatRequestDeleagte>

{
    
    int mType,mSelectRow;
}
-(void)GoChildDisViewController:(int)row;
-(int)GetEvaluateType:(int)num;
@property(nonatomic,retain)NSMutableDictionary *mDateDict;
@property(nonatomic,retain)HttpFormatRequest *mHttpFormatRequestView;
@end
