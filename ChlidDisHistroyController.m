//
//  ChlidDisHistroyController.m
//  Health
//
//  Created by hq on 12-6-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ChlidDisHistroyController.h"
#import "SendNotifyViewController.h"
#import "ChildOneListView.h"
#import "JSON.h"
#import "Display.h"
#import "DownLoadTableImage.h"


#define TABLAHIGHT 40

#define ePageCount 1000

#define KPageCount   @"PageCount"
#define KUserID      @"UserID"
#define KPageStart   @"PageStart"
#define KChildID     @"ChildID"

#define KID             @"ID"
#define KPhoto          @"photo"
#define KPublishTime    @"publishTime"
#define KTitle          @"title"

#define ERIGHTICON         @"二字按钮.png"

@implementation ChlidDisHistroyController
@synthesize mDownLoadImageDict;
@synthesize TYPE;

@synthesize mTableTemplateView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        mPageStart = 0;
        TYPE = -1;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)CancelDownLoad
{
    for (int i = 0; i<[mTableTemplateView.requestDatas count]; i++) {
        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:i inSection:0];
        DownLoadTableImage *imageDownLoad = [mDownLoadImageDict objectForKey:indexpath];
        if (imageDownLoad != nil) {
            [imageDownLoad CancelDownloadImage];
        }
        
    }
    [mDownLoadImageDict removeAllObjects];
}
-(void)navBack
{

    
    [self CancelDownLoad];
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    if (buttonIndex == 0) {
        //        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSMutableDictionary *mDictionary = [[NSMutableDictionary alloc]init];
    self.mDownLoadImageDict = mDictionary;
    [mDictionary release];
    
    [ViewTool addUIImageViewWithAutoSize:self.view imageName:@"viewblackgroup.jpg" type:@"" x:0 y:0 x1:0 y1:0];

    
    
    
    
 
    
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];
    [HealthHeaders SVProgressView:self.view];    
    
    {
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        [mDict setObject:[Instance GetUseID] forKey:@"ID"];

        TableTemplateView *table = [[TableTemplateView alloc]initWithFrame:CGRectMake(0, 0, 320, 372)];
        [table setTableHeightForRow:TABLAHIGHT];
        [table setUrlString:[NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/ChildRepresentation/TeacherFetchChildRepresentationRecordsOfEveryBaby"]];
        table.DataDict = mDict;
        table.delegate = self;
        table.isLoadImage = YES;
        self.mTableTemplateView = table;
        [table release];
        
        [self.view addSubview:mTableTemplateView];
        [mDict release];
        [mTableTemplateView GetDataForHttp];
    } 
}
-(void)ReturnHttpData:(NSString*)ResultContent container:(NSMutableArray*)arrays tag:(int)tag isFinish:(BOOL)isFinish;
{
    NSLog(@"ResultContent--:%@",ResultContent);
    
    NSArray *array = [[[ResultContent JSONValue] valueForKey:@"childRepresentationRecordsOfEveryBody"] retain];
    
    if ([array count]<ePageCount) {
        mTableTemplateView.isLoadOver = YES;
    }
    else
        mTableTemplateView.isLoadOver = NO;
    
    
    for (int i = 0; i< [array count]; i++) {
        Display *nobj = [[Display alloc] init];
        nobj.childID = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childID"]] intValue];
        nobj.childPhoto = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childPhoto"]]; 
        
        nobj.childName = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childName"]];
        nobj.inSchool = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childRepresentationRecordsInSchool"]] intValue];
        nobj.inHome = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childRepresentationRecordsInHome"]] intValue];
        nobj.image = nil;
        nobj.isLoad = NO;
        [arrays addObject:nobj];
        [nobj release];
    }
    [array release];
    [mTableTemplateView setRequestDatas:arrays];
     
//-----
    if ([arrays count] == 0 && isFinish == NO) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"加载失败" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alertView.delegate = self;
        [alertView show];
        [alertView release];
    }
    if ([arrays count] == 0 && isFinish == YES) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"暂无幼儿" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alertView.delegate = self;
        [alertView show];
        [alertView release];
    }
}
-(void)SetTableData:(UITableViewCell*)cell indexpath:(NSIndexPath*)indexPath datas:(NSMutableArray *)mDatas;
{

    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];

    cell.textLabel.text = @"";
    cell.textLabel.font = [UIFont boldSystemFontOfSize:15];
    UIImage *image =[UIImage imageNamed:@"acquiesce.jpg"];
    
    cell.imageView.image = image;
    Display *obj = [mDatas objectAtIndex:[indexPath row]];
    {
        if (!obj.isLoad) {
            if (mTableTemplateView.mTableView.dragging == NO && mTableTemplateView.mTableView.decelerating == NO) {
                if ([obj.childPhoto length] != 0) {
                    [self StartDownLoadImage:obj.childPhoto IndexPath:indexPath];
                }
                else
                {
                    obj.image = [UIImage imageNamed:@"acquiesce.jpg"];
                    obj.isLoad = YES;
                }
            }
        }
        else
        {
            cell.imageView.image = obj.image;
        }
    }
    if (TYPE == INSCHOOL) {
        cell.textLabel.text = [NSString stringWithFormat:@"%@   记录数:%d ",obj.childName,obj.inSchool];
    }
    if (TYPE == INHOME) {
        cell.textLabel.text = [NSString stringWithFormat:@"%@   记录数:%d ",obj.childName,obj.inHome];
    }
}
-(void)TableDidSelect:(NSIndexPath*)indexPath Datas:(NSMutableArray*)mDatas;
{
    Display *obj = [mTableTemplateView.requestDatas objectAtIndex:[indexPath row]];
    ChildOneListView *nifCtr = [[ChildOneListView alloc] initWithNibName:@"ChildOneListView" bundle:nil];
    nifCtr.title = @"个人列表";
    //    nifCtr.mDisChildId = obj.childID;
    nifCtr.TYPE = TYPE;
    nifCtr.mChildID = obj.childID;
    [self.navigationController pushViewController:nifCtr animated:YES];
    [nifCtr release];
}

#pragma mark  load imageview 
-(void)StartDownLoadImage:(NSString*)url IndexPath:(NSIndexPath*)indexpath 
{
    DownLoadTableImage *imageDownLoad = [mDownLoadImageDict objectForKey:indexpath];
    if (imageDownLoad == nil) {
        imageDownLoad = [[DownLoadTableImage alloc] init];
        imageDownLoad.ImageURL = url;
        imageDownLoad.ImageHeight = 40;
        imageDownLoad.ImageWidth = 40;
        imageDownLoad.IndexPathTo = indexpath;
        imageDownLoad.Delegate =self;
        [mDownLoadImageDict setObject:imageDownLoad forKey:indexpath];
        [imageDownLoad StartDownloadImage];
        [imageDownLoad release];
    }
}
#pragma mark finish download image
-(void)FinishDownLoadImage:(NSIndexPath*)indexpath Image:(UIImage*)image IsFailed:(BOOL)isFaild
{
    DownLoadTableImage *imageDownLoad = [mDownLoadImageDict objectForKey:indexpath];
    
    if (imageDownLoad != nil) {
        UITableViewCell *cell = [mTableTemplateView.mTableView cellForRowAtIndexPath:indexpath];
        Display *obj = [mTableTemplateView.requestDatas objectAtIndex:[indexpath row]];
        if (isFaild) {
            obj.image = [UIImage imageNamed:@"acquiesce.jpg"];
            obj.isLoad = NO;
        }
        else
        {
            obj.image = image;
            obj.isLoad = YES;
        }
        cell.imageView.image =  obj.image ;
    }
}
- (void)loadImagesForOnscreenRows
{
    NSArray *visiblePaths = [mTableTemplateView.mTableView indexPathsForVisibleRows];
    for (NSIndexPath *indexPath in visiblePaths)
    {
        if ([indexPath row]<[mTableTemplateView.requestDatas count]) {
            Display *item = [mTableTemplateView.requestDatas objectAtIndex:indexPath.row];
            
            if (!item.isLoad ) // avoid the app icon download if the app already has an icon
            {
                [self StartDownLoadImage:item.childPhoto IndexPath:indexPath];
            }
            
        }
    }
}
-(void)TableDownLoadCellImage
{
    [self loadImagesForOnscreenRows];
}

//------------------ 
-(void)dealloc
{
    [mDownLoadImageDict release];
    [mTableTemplateView release]; 
    
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mTableTemplateView = nil;
    self.mDownLoadImageDict = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
