	//
	//  MyScrollView.h
	//  PhotoBrowserEx
	//
	//  Created by  on 10-6-12.
	//  Copyright 2010 __MyCompanyName__. All rights reserved.
	//

#import <UIKit/UIKit.h>

@protocol myscrollViewDelegate;
@interface MyScrollView : UIScrollView <UIScrollViewDelegate>
{
	UIImage *image;
	UIImageView *imageView;
	int mytag;
    
    NSString *shuoming;
	UILabel *textview;
}

@property (nonatomic, retain) UIImage *image;
@property (nonatomic,retain)UIImageView *imageView;
@property (nonatomic,retain)   NSString *shuoming;
@property(nonatomic,assign) id<myscrollViewDelegate> viewdelegate;
@property int mytag;
@end

@protocol myscrollViewDelegate <NSObject>
-(void)MyScrollViewSingTouch;

@optional
-(void) setNormalSize;
-(void) setNotNormalSize ;
-(void)setsizeimage;

@end

