//
//  RatioView.m
//  Health
//
//  Created by hq on 12-9-13.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "RatioView.h"
#import "NTChartView.h"
#import "PieChartView.h"

#define mTableHeight 40
#define mTableTextSize 13
#define mTablePieHeight 320
@implementation RatioView
@synthesize mAnalysisResultDict;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)navBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];
    [self.tableView setShowsVerticalScrollIndicator:NO];

}

-(void)dealloc
{
    [mAnalysisResultDict release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mAnalysisResultDict = nil;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
        if (section == 1) {
            return 5;
        }
        if (section == 0) {
            return 1;
        }
    return 0;
}
-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath section] == 1) {
        return mTableHeight;
    }
    if ([indexPath section] == 0) {
        return mTablePieHeight;
    }
    return  0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    int mIndexRow = [indexPath row];
    int mIndexSection = [indexPath section];
    for (UIView *sub in [cell.contentView subviews]) {
        [sub removeFromSuperview];
    }
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    // Configure the cell...
    if ([indexPath section] == 1) {
        {
            UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 46)];
            [img setImage:[UIImage imageNamed:@"shang.jpg"]];  
            img.tag = 121211;
            [cell.contentView addSubview:img];
            [img release];
            
        }
        
        {
            UILabel *label = [Instance addLable:CGRectMake(10, 0, 300, mTableHeight) tag:100011 size:mTableTextSize string:@""];
            [cell.contentView addSubview:label];
            [label release];
        }
        {
            UILabel *label = [Instance addLable:CGRectMake(170, 0, 300, mTableHeight) tag:100022 size:mTableTextSize string:@""];
            [cell.contentView addSubview:label];
            [label release];
        }
        ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";
        if (mIndexRow == 0) {
            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"早餐热量      %@千卡",[mAnalysisResultDict objectForKey:@"breakfastEnergy"]]; //100
            ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"早点热量      %@千卡",[mAnalysisResultDict objectForKey:@"breakfastDessertEnergy"]];//101
            
        }
        if (mIndexRow == 1) {
            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"午餐热量      %@千卡",[mAnalysisResultDict objectForKey:@"lunchEnergy"]];//102
            ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"午点热量      %@千卡",[mAnalysisResultDict objectForKey:@"lunchDessertEnergy"]];//103
            
        }
//        if (mIndexRow == 2) {
//            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"晚餐热量      %@千卡",[mAnalysisResultDict objectForKey:@"supperEnergy"]];//104
//            ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"晚点热量      %@千卡",[mAnalysisResultDict objectForKey:@"supperDessertEnergy"]];//105
//            
//        }

        
        if (mIndexRow == 2) {
            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"钙磷比例      %@",[mAnalysisResultDict objectForKey:@"calciumPhosphorusRatio"]];//119
        }
        if (mIndexRow == 3) {
            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"绿橙蔬菜占总蔬菜百分比      %0.2f%%",[[mAnalysisResultDict objectForKey:@"greenOrangeVegetableInVegetablePertentage"] floatValue]];//120
        }
        if (mIndexRow == 4) {
            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"蛋白质脂肪碳水化合物比例      %@",[mAnalysisResultDict objectForKey:@"porteinFatCarbohydrateRatio"]];//121
        }

    }
    if (mIndexSection == 0) {
        /// ----------------- 
        
        NSMutableArray *array = [[NSMutableArray alloc]init];
        
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        // 1 
        [mDict setObject:[mAnalysisResultDict objectForKey:@"breakfastEnergyPertentage"] forKey:@"value"];
        [mDict setObject:@"早餐热量" forKey:@"title"];
        [array addObject:mDict];
        [mDict release];
        //2
        mDict = [[NSMutableDictionary alloc]init];
        [mDict setObject:[mAnalysisResultDict objectForKey:@"breakfastDessertEnergyPertentage"] forKey:@"value"];
        [mDict setObject:@"早点热量" forKey:@"title"];
        [array addObject:mDict];
        [mDict release];
        //3
        mDict = [[NSMutableDictionary alloc]init];
        [mDict setObject:[mAnalysisResultDict objectForKey:@"lunchEnergyPertentage"] forKey:@"value"];
        [mDict setObject:@"午餐热量" forKey:@"title"];
        [array addObject:mDict];
        [mDict release];
        //4
        mDict = [[NSMutableDictionary alloc]init];
        [mDict setObject:[mAnalysisResultDict objectForKey:@"lunchDessertEnergyPertentage"] forKey:@"value"];
        [mDict setObject:@"午点热量" forKey:@"title"];
        [array addObject:mDict];
        [mDict release];
        
//        mDict = [[NSMutableDictionary alloc]init];
//        [mDict setObject:[mAnalysisResultDict objectForKey:@"supperEnergyPertentage"] forKey:@"value"];
//        [mDict setObject:@"晚餐热量" forKey:@"title"];
//        [array addObject:mDict];
//        [mDict release];
//        
//        mDict = [[NSMutableDictionary alloc]init];
//        [mDict setObject:[mAnalysisResultDict objectForKey:@"supperDessertEnergyPertentage"] forKey:@"value"];
//        [mDict setObject:@"晚点热量 " forKey:@"title"];
//        [array addObject:mDict];
//        [mDict release];
        
        
        PieChartView *piechart = [[PieChartView alloc]initWithFrame:CGRectMake(0, 20, 320, mTablePieHeight) value:array];
        [cell.contentView addSubview:piechart];
        [array release];
        [piechart release];

    }

    return cell;
}


@end
