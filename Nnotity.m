//
//  Nnotity.m
//  Health
//
//  Created by hq on 12-8-3.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "Nnotity.h"

@implementation Nnotity
@synthesize nPhoto,nPublishTime,nTitle,nName;
@synthesize nID;
@synthesize image;
@synthesize isLoad;
-(void)dealloc
{

    [nPhoto release]; nPhoto = nil;
    [nPublishTime release]; nPublishTime = nil;
    [nTitle release]; nTitle = nil;
    [image release]; image = nil;
    [nName release]; nName = nil;
    [super dealloc];
}
@end
