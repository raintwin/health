    //
//  SystemSelectMealViewControl.m
//  Health
//
//  Created by jiaodian on 12-12-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "SystemSelectMealViewControl.h"
#import "SystemComPantryViewControl.h"
#import "PantrytMianViewController.h"
@interface SystemSelectMealViewControl ()

@end

#define TableViewHeiht 40
@implementation SystemSelectMealViewControl
@synthesize mTableView;
@synthesize mSysComPantryMeals;
@synthesize mSuperView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark - View lifecycle
-(void)backNav
{

    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark mSystemComPantryViewControl Delegate
-(void)SetNavBarHidden:(BOOL)isBOOL
{
    [mSuperView.navigationController setNavigationBarHidden:!isBOOL];
    [self.navigationController setNavigationBarHidden:isBOOL];

}
#pragma mark 确定
-(void)GoSystemComPantry
{
    NSLog(@"mSysComPantryMeals :%@",[mSysComPantryMeals JSONRepresentation]);
    BOOL isFlag = YES;
    NSMutableArray *array = [[NSMutableArray alloc]init];

    for (NSMutableDictionary *mDict in mSysComPantryMeals) {
        if ([[mDict objectForKey:@"flag"] boolValue]) {
            isFlag = NO;
            [array addObject:mDict];
        }
    }
    if (isFlag) {
        UIAlertView *alerview = [[UIAlertView alloc]initWithTitle:@"提示" message:@"请选择餐次" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alerview show];
        [alerview release];
        [array release];
        return;
    
    }
   
    [mSuperView.navigationController setNavigationBarHidden:YES];
    [self.navigationController setNavigationBarHidden:NO];
    
    SystemComPantryViewControl *mSystemComPantryViewControl = [[SystemComPantryViewControl alloc]initWithNibName:@"SystemComPantryViewControl" bundle:nil];
    mSystemComPantryViewControl.Delegate = self;
//    mSystemComPantryViewControl.mSysComMealas = array;
    [self.navigationController pushViewController:mSystemComPantryViewControl animated:YES];


}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(backNav)];
//    [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"确定" target:self selector:@selector(GoSystemComPantry)];
    {
        NSMutableArray *array = [[NSMutableArray alloc]init];
        self.mSysComPantryMeals = array;
        [array release];
    }
    {
        for (int i = 1; i<=6; i++) {
            NSArray *array = [[NSArray alloc] initWithObjects:@"早餐",@"早点",@"午餐",@"午点",@"晚餐",@"晚点", nil];

            NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
            [mDict setObject:[array objectAtIndex:i-1] forKey:@"mealName"];
            [mDict setObject:[NSNumber numberWithInt:i] forKey:@"meal"];
            [mDict setObject:[NSNumber numberWithInt:YES] forKey:@"flag"];
            [mSysComPantryMeals addObject:mDict];
            [array release];
            [mDict release];
            
        }
    }
    {
        UITableView *tableview = [[UITableView alloc]initWithFrame:self.view.frame style:UITableViewStyleGrouped];
        tableview.delegate = self;
        tableview.dataSource = self;
        
        self.mTableView = tableview;
        [tableview release];
        
        [self.view addSubview:mTableView];
    }
}
-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TableViewHeiht;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.textLabel.text = @"";
        cell.imageView.image = nil;
        
    }
    
    cell.imageView.image = nil;
    [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    cell.textLabel.textAlignment = UITextAlignmentLeft;

    if ([indexPath section] == 0) {
        NSMutableDictionary *mDict = [mSysComPantryMeals objectAtIndex:[indexPath row]];
        cell.textLabel.text =[mDict objectForKey:@"mealName"];
    }
    if ([indexPath section] == 1) {
        cell.textLabel.text = @"确定";
        cell.textLabel.textAlignment = UITextAlignmentCenter;
        [cell setAccessoryType:UITableViewCellAccessoryNone];

    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if ([ indexPath section] == 0) {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        NSMutableDictionary *mDict = [mSysComPantryMeals objectAtIndex:[indexPath row]];
        
        if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
            cell.accessoryType = UITableViewCellAccessoryNone;
            [mDict setObject:[NSNumber numberWithInt:NO] forKey:@"flag"];
            
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [mDict setObject:[NSNumber numberWithInt:YES] forKey:@"flag"];
            
        }
        return;
    }
    if ([indexPath section] == 1) {
        [self GoSystemComPantry];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0) {
         return [mSysComPantryMeals count];
    }
    if (section == 1) {
        return 1;
    }
    return 0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}
-(void)dealloc
{
    [mSuperView release];
    [mSysComPantryMeals release];
    [mTableView release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mSuperView = nil;
    self.mSysComPantryMeals = nil;
    self.mTableView = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
