//
//  NotifyListViewController.m
//  Health
//
//  Created by hq on 12-6-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>

#import "NotifyListViewController.h"
#import "SendNotifyViewController.h"
#import "InFoViewController.h"
#import "Nnotity.h"
#import "DownLoadTableImage.h"
#import "LoginViewController.h"
#import "TableTempView.h"
#define TABLAHIGHT 80
#define TABLAMOREHEGIHT 52

#define ePageCount 10

#define KPageCount   @"PageCount"
#define KUserID      @"UserID"
#define KPageStart   @"PageStart"
#define KChildID     @"ChildID"

#define KID             @"ID"
#define KPhoto          @"photo"
#define KPublishTime    @"publishTime"
#define KTitle          @"title"

#define ERIGHTICON         @"二字按钮.png"

@implementation NotifyListViewController
@synthesize mDownLoadImageDict;


@synthesize mTableTemplateView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)CancelDownLoad
{

    for (int i = 0; i<[mTableTemplateView.requestDatas count]; i++) {
        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:i inSection:0];
        DownLoadTableImage *imageDownLoad = [mDownLoadImageDict objectForKey:indexpath];
        if (imageDownLoad != nil) {
            [imageDownLoad CancelDownloadImage];
        }
        
    }
    [mDownLoadImageDict removeAllObjects];
}
-(void)navBack
{
    [self CancelDownLoad];   
    [self.navigationController popViewControllerAnimated:YES];

}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    if (buttonIndex == 0) {
//        [self.navigationController popViewControllerAnimated:YES];
    }
}
#pragma mark 增加通知
-(void)SendNotity
{

    SendNotifyViewController *svctr = [[SendNotifyViewController alloc]initWithNibName:@"SendNotifyViewController" bundle:nil];
    
    svctr.isAnd   = YES;
    svctr.isText  = YES;
    svctr.isTitle = YES;

    svctr._Delegate = self;
    svctr.title = @"发送通知";
    [self.navigationController pushViewController:svctr animated:YES];
    [svctr release];

}
#pragma mark sendnotify delegate
-(void)ReloDataList
{
    [self CancelDownLoad];
    [mTableTemplateView.requestDatas removeAllObjects];
    [mTableTemplateView GetDataForHttp];
   
}
- (void)viewDidLoad
{
    [super viewDidLoad];
//    [Instance baiduApi:@"looknotify" eventlabel:@"sendnotify"];

    
    //BaiduMobStat* statTracker = [BaiduMobStat defaultStat];    [statTracker logEvent:@"looknotify" eventLabel:@"looknotify"];
    
    NSMutableDictionary *mDictionary = [[NSMutableDictionary alloc]init];
    self.mDownLoadImageDict = mDictionary;
    [mDictionary release];
    
    [ViewTool addUIImageViewWithAutoSize:self.view imageName:@"viewblackgroup.jpg" type:@"" x:0 y:0 x1:0 y1:0];
    
    if (![Instance GetUseType]) {
        [Instance setRightImageAndNameForNavigationItem:self.navigationItem Image:TWOENSURE title:@"增加" target:self selector:@selector(SendNotity)];

    }
    
    
    NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
    NSString *urlstr;

    if(![Instance GetUseType])
    {
        urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/Notification/TeacherFetchNotificationList"];
        [mDict setObject:[Instance GetUseID] forKey:KUserID];
    }
    else
    {
        urlstr = [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/Notification/ParentFetchNotificationList"];
        [mDict setObject:[Instance GetUseID] forKey:KUserID];
        [mDict setObject:[Instance GetUseID] forKey:KChildID];

    }

    CGRect frame = [[UIScreen mainScreen] bounds];
    frame.origin.y = 20;
    frame.size.height = frame.size.height -20-50-44;

    // Do any additional setup after loading the view from its nib.
    {
        TableTempView *table = [[[TableTempView alloc]initWithFrame:CGRectMake(0, 0, 320, frame.size.height)] autorelease];
        [table setTableHeightForRow:TABLAHIGHT];
        table.tag = 1002010;
        [table setUrlString:urlstr];
        table.DataDict = mDict;
        table.isLoadImage = YES;
        table.delegate = self;
        table.commitEditing = YES;
        
        self.mTableTemplateView = table;
        [self.view addSubview:mTableTemplateView];
        [mDict release];
        [mTableTemplateView GetDataForHttp];

    }    
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];
    
}

-(void)ReturnHttpData:(NSString*)ResultContent container:(NSMutableArray*)arrays tag:(int)tag isFinish:(BOOL)isFinish;
{

    NSLog(@"ResultContent--:%@",ResultContent);
    
    NSArray *array = [[[ResultContent JSONValue] valueForKey:@"notificationList"] retain];
    
    if ([array count]<ePageCount) {
        mTableTemplateView.isLoadOver = YES;
    }
    else
        mTableTemplateView.isLoadOver = NO;
    
    
    for (int i = 0; i< [array count]; i++) {
        Nnotity *items = [[Nnotity alloc] init];
        items.nID = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:KID]] intValue];
        items.nPhoto = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:KPhoto]]; 
        
        items.nPublishTime = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:KPublishTime]];
        items.nTitle = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:KTitle]];
        items.isLoad = NO;
        items.image = [UIImage imageNamed:@"acquiesce.jpg"];
        [arrays addObject:items];
        [items release];
    }
    [array release];
    [mTableTemplateView setRequestDatas:arrays];
}
-(void)SetTableData:(UITableViewCell*)cell indexpath:(NSIndexPath*)indexPath datas:(NSMutableArray *)mDatas;
{

    Nnotity *items = [mDatas objectAtIndex:[indexPath row]];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    if (!items.isLoad) {
        if (mTableTemplateView.mTableView.dragging == NO && mTableTemplateView.mTableView.decelerating == NO) {
            if ([items.nPhoto length] != 0) {
                [self StartDownLoadImage:items.nPhoto IndexPath:indexPath];
            }
            else
            {
                items.image = [UIImage imageNamed:@"acquiesce.jpg"];
                items.isLoad = YES;
            }
        }
    }
    else
    {
        UIImageView *img =(UIImageView *)[cell.contentView viewWithTag:110000];
        [img setImage:items.image];
    }
    cell.imageView.image = items.image;

    cell.textLabel.text = items.nTitle;
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.detailTextLabel.text = items.nPublishTime;
      
}
-(void)TableDidSelect:(NSIndexPath*)indexPath Datas:(NSMutableArray*)mDatas;
{
    Nnotity *items = [mDatas objectAtIndex:[indexPath row]];
    InFoViewController *nifCtr = [[InFoViewController alloc] initWithNibName:@"InFoViewController" bundle:nil];
    nifCtr.title = @"通知";
    nifCtr.USERID = items.nID;
    nifCtr.CONTEXTTYPE = CONTEXT_NOTITY;
    [self.navigationController pushViewController:nifCtr animated:YES];
    [nifCtr release];
}
#pragma mark LoadData

#pragma mark table 
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

        Nnotity *items = [mTableTemplateView.requestDatas objectAtIndex:[indexPath row]];
        InFoViewController *nifCtr = [[InFoViewController alloc] initWithNibName:@"InFoViewController" bundle:nil];
        nifCtr.title = @"通知";
        nifCtr.USERID = items.nID;
        nifCtr.CONTEXTTYPE = CONTEXT_NOTITY;
        [self.navigationController pushViewController:nifCtr animated:YES];
        [nifCtr release];
}

#pragma mark  load imageview 
-(void)StartDownLoadImage:(NSString*)url IndexPath:(NSIndexPath*)indexpath 
{
    DownLoadTableImage *imageDownLoad = [mDownLoadImageDict objectForKey:indexpath];
    if (imageDownLoad == nil) {
        imageDownLoad = [[DownLoadTableImage alloc] init];
        imageDownLoad.ImageURL = url;
        imageDownLoad.ImageHeight = 65;
        imageDownLoad.ImageWidth = 65;
        imageDownLoad.IndexPathTo = indexpath;
        imageDownLoad.Delegate =self;
        [mDownLoadImageDict setObject:imageDownLoad forKey:indexpath];
        [imageDownLoad StartDownloadImage];
        [imageDownLoad release];
    }
}
#pragma mark finish download image
-(void)FinishDownLoadImage:(NSIndexPath*)indexpath Image:(UIImage*)image IsFailed:(BOOL)isFaild
{
    DownLoadTableImage *imageDownLoad = [mDownLoadImageDict objectForKey:indexpath];

    if (imageDownLoad != nil) {
        UITableViewCell *cell = [mTableTemplateView.mTableView cellForRowAtIndexPath:indexpath];
        Nnotity *items = [mTableTemplateView.requestDatas objectAtIndex:[indexpath row]];
        if (isFaild) {
            items.image = [UIImage imageNamed:@"acquiesce.jpg"];
            items.isLoad = NO;
        }
        else
        {
            items.image = image;
            items.isLoad = YES;
        }
        cell.imageView.image = items.image;
        
    }
}
- (void)loadImagesForOnscreenRows
{

        NSArray *visiblePaths = [mTableTemplateView.mTableView indexPathsForVisibleRows];
        for (NSIndexPath *indexPath in visiblePaths)
        {
            if ([indexPath row]<[mTableTemplateView.requestDatas count]) {
                Nnotity *item = [mTableTemplateView.requestDatas objectAtIndex:indexPath.row];
                
                if (!item.isLoad ) // avoid the app icon download if the app already has an icon
                {
                    [self StartDownLoadImage:item.nPhoto IndexPath:indexPath];
                }

            }
        }
}
-(void)deselectRow:(NSIndexPath *)indexPath;
{
    Nnotity *items = [mTableTemplateView.requestDatas objectAtIndex:[indexPath row]];

    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:[NSNumber numberWithInt:items.nID] forKey:@"notificationId"];
    [dict setObject:[NSNumber numberWithInt:[[Instance GetUseID]intValue]] forKey:@"userId"];
    [dict setObject:NSLocalizedString(@"PORT_DelRow", nil) forKey:NSLocalizedString(@"PORT_ReqType", nil)];
    [self.mTableTemplateView deleteRow:dict];
    [dict release];

}
-(void)TableDownLoadCellImage
{
    [self loadImagesForOnscreenRows];
}
#pragma mark -
#pragma mark Deferred image loading (UIScrollViewDelegate)
#pragma mark -
// Load images for all onscreen rows when scrolling is finished

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	if (!decelerate)
	{
        [self loadImagesForOnscreenRows];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadImagesForOnscreenRows];
}
-(void)dealloc
{
    [mDownLoadImageDict release];
    [mTableTemplateView release];
   [super dealloc];
}
- (void)viewDidUnload
{
    self.mDownLoadImageDict = nil;
    self.mTableTemplateView = nil;

    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
