//
//  FatView.m
//  Health
//
//  Created by hq on 12-9-13.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "FatView.h"
#import "PieChartView.h"
#define mTableHeight 40
#define mTableTextSize 13
#define mTablePieHeight 320
@implementation FatView
@synthesize mAnalysisResultDict;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)navBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];
    [self.tableView setShowsVerticalScrollIndicator:NO];

}

-(void)dealloc
{
    [mAnalysisResultDict release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mAnalysisResultDict = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
//    if (section == 0) {
//        return 1;
//    }
//    if (section == 1) {
//        return 1;
//    }
    
    return 1;
}
-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath section] == 1) {
        return mTableHeight;
    }
    if ([indexPath section] == 0) {
        return mTablePieHeight;
    }
    return  0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    int mIndexRow = [indexPath row];
    int mIndexSection = [indexPath section];
    for (UIView *sub in [cell.contentView subviews]) {
        [sub removeFromSuperview];
    }
    if ([indexPath section] == 1 ) {
        {
            UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 46)];
            [img setImage:[UIImage imageNamed:[indexPath row]%2 == 0 ? @"xia.jpg":@"shang.jpg"]];  
            img.tag = 121211;
            [cell.contentView addSubview:img];
            [img release];
            
        }
        
        {
            UILabel *label = [Instance addLable:CGRectMake(10, 0, 160, mTableHeight) tag:100011 size:mTableTextSize string:@""];
            [cell.contentView addSubview:label];
            [label release];
        }
        {
            UILabel *label = [Instance addLable:CGRectMake(170, 0, 300, mTableHeight) tag:100022 size:mTableTextSize string:@""];
            [cell.contentView addSubview:label];
            [label release];
        }
        
        if (mIndexRow == 0) {
            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"动物脂肪      %@克",[mAnalysisResultDict objectForKey:@"animalFat"]]; //90
            ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"植物脂肪      %@克",[mAnalysisResultDict objectForKey:@"plantFat"]];//91

        }
//        if (mIndexRow == 1) {
//        }
//        if (mIndexRow >=2) {
//            [((UILabel *)[cell.contentView viewWithTag:100011]) setFrame:CGRectMake(10, 0, 320, mTableHeight)];
//        }
//        if (mIndexRow == 2) {
//            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"动物脂肪占总脂肪摄入量的百分比      %0.2f%%",[[mAnalysisResultDict objectForKey:@"animalFatPertentage"] floatValue]];//92
//        }
//        if (mIndexRow == 3) {
//            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"植物脂肪占总脂肪摄入量的百分比      %0.2f%%",[[mAnalysisResultDict objectForKey:@"plantFatPertentage"] floatValue]];//93
//        }

    }
    if (mIndexSection == 0 ) {
        
        NSMutableArray *array = [[NSMutableArray alloc]init];
        
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        
        [mDict setObject:[mAnalysisResultDict objectForKey:@"animalFatPertentage"] forKey:@"value"];
        [mDict setObject:@"动物脂肪" forKey:@"title"];
        [array addObject:mDict];
        [mDict release];
        
        mDict = [[NSMutableDictionary alloc]init];
        [mDict setObject:[mAnalysisResultDict objectForKey:@"plantFatPertentage"] forKey:@"value"];
        [mDict setObject:@"植物脂肪" forKey:@"title"];
        [array addObject:mDict];
        [mDict release];
        
        
        
        PieChartView *piechart = [[PieChartView alloc]initWithFrame:CGRectMake(0, 20, 320, mTablePieHeight) value:array];
        [cell.contentView addSubview:piechart];
        [array release];
        [piechart release];
        
    }
    // Configure the cell...
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end
