//
//  HitNutView.m
//  Health
//
//  Created by hq on 12-9-12.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "HitNutView.h"
#import "PieChartView.h"
#define mTableHeight 40
#define mTableTextSize 13
#define mTablePieHeight 280
@implementation HitNutView
@synthesize mAnalysisResultDict;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)navBack
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];
    [self.tableView setShowsVerticalScrollIndicator:NO];

}
-(void)dealloc
{
    [mAnalysisResultDict release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mAnalysisResultDict = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0) {
        return 1;
    }
    if (section == 1) {
        return 2;
    }
    return 0;
}
-(float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath section] == 0) {
        return mTablePieHeight;

    }
    if ([indexPath section] == 1) {
        return mTableHeight;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    for (UIView *sub in [cell.contentView subviews]) {
        [sub removeFromSuperview];
    }
    int mIndexRow = [indexPath row];
    int mIndexSection = [indexPath section];

    if ([indexPath section] == 0) {
        NSMutableArray *array = [[NSMutableArray alloc]init];
        
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        
        [mDict setObject:[mAnalysisResultDict objectForKey:@"carbohydrateEnergyPertentage"] forKey:@"value"];
        [mDict setObject:@"碳水热" forKey:@"title"];
        [array addObject:mDict];
        [mDict release];
        
        mDict = [[NSMutableDictionary alloc]init];
        [mDict setObject:[mAnalysisResultDict objectForKey:@"porteinEnergyPertentage"] forKey:@"value"];
        [mDict setObject:@"蛋白质热" forKey:@"title"];
        [array addObject:mDict];
        [mDict release];
        
        mDict = [[NSMutableDictionary alloc]init];
        [mDict setObject:[mAnalysisResultDict objectForKey:@"fatEnergyPertentage"] forKey:@"value"];
        [mDict setObject:@"脂肪热" forKey:@"title"];
        [array addObject:mDict];
        [mDict release];
        
        PieChartView *piechart = [[PieChartView alloc]initWithFrame:CGRectMake(0, 20, 320, 280) value:array];
        [cell.contentView addSubview:piechart];
        [array release];
        [piechart release];

//        if (mIndexRow >=2) {
//            [((UILabel *)[cell.contentView viewWithTag:100011]) setFrame:CGRectMake(10, 0, 320, mTableHeight)];
//            ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//105
//        }
//        if (mIndexRow == 2) {
//            [((UILabel *)[cell.contentView viewWithTag:100011]) setFrame:CGRectMake(10, 0, 320, mTableHeight)];
//            
//            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"碳水热占总热量摄入量的百分比      %0.2f%%",[[mAnalysisResultDict objectForKey:@"carbohydrateEnergyPertentage"] floatValue]];//63
//        }
//        
//        if (mIndexRow == 3) {
//            [((UILabel *)[cell.contentView viewWithTag:100011]) setFrame:CGRectMake(10, 0, 320, mTableHeight)];
//            
//            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"蛋白质热占总热量摄入量的百分比      %0.2f%%",[[mAnalysisResultDict objectForKey:@"porteinEnergyPertentage"] floatValue]];//64
//        }
//        if (mIndexRow == 4) {
//            [((UILabel *)[cell.contentView viewWithTag:100011]) setFrame:CGRectMake(10, 0, 320, mTableHeight)];
//            
//            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"脂肪热占总热量摄入量的百分比      %0.2f%%",[[mAnalysisResultDict objectForKey:@"fatEnergyPertentage"] floatValue]];//65
//        }

    }
    
    
    if (mIndexSection == 1 ) {
        
        
        {
            UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 46)];
            [img setImage:[UIImage imageNamed:[indexPath row]%2 == 0 ? @"xia.jpg":@"shang.jpg"]];  
            img.tag = 121211;
            [cell.contentView addSubview:img];
            [img release];
            
        }
        
        {
            UILabel *label = [Instance addLable:CGRectMake(10, 0, 160, mTableHeight) tag:100011 size:mTableTextSize string:@""];
            label.tag = 100011;
            [cell.contentView addSubview:label];
            [label release];
        }
        {
            UILabel *label = [Instance addLable:CGRectMake(170, 0, 300, mTableHeight) tag:100022 size:mTableTextSize string:@""];
            label.tag = 100022;
            [cell.contentView addSubview:label];
            [label release];
        }
        
        if ([indexPath row]%2 == 0)        
            ((UIImageView *)[cell.contentView viewWithTag:121211]).image = [UIImage imageNamed:@"xia.jpg"];
        
        if (mIndexRow == 0) {
            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"碳水热      %@千卡",[mAnalysisResultDict objectForKey:@"carbohydrateEnergy"]]; //60
            ((UILabel *)[cell.contentView viewWithTag:100022]).text = [NSString stringWithFormat:@"蛋白质热      %@千卡",[mAnalysisResultDict objectForKey:@"porteinEnergy"]];//61
            
        }
        if (mIndexRow == 1) {
            ((UILabel *)[cell.contentView viewWithTag:100011]).text = [NSString stringWithFormat:@"脂肪热      %@千卡",[mAnalysisResultDict objectForKey:@"fatEnergy"]];//62
            ((UILabel *)[cell.contentView viewWithTag:100022]).text = @"";//61
            
        }

         
    }

    // Configure the cell...
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end
