//
//  ConditionView.h
//  Health
//
//  Created by hq on 12-9-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ConditionDelegate <NSObject>

-(void)FrushData:(NSMutableDictionary*)mDict;

@end
@interface ConditionView : UITableViewController
{
    id<ConditionDelegate>Delegate;
}
@property(nonatomic,assign)int  ConditionID;
@property(nonatomic,retain)id<ConditionDelegate>Delegate;
@end
