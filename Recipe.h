//
//  Recipe.h
//  Health
//
//  Created by hq on 12-9-7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Recipe : NSObject
@property(nonatomic,retain)NSString *dateForFindCondition,*dateForRepresentation,*dayOfWeek;
@end



#pragma mark 餐次
@interface RecipeClasses : NSObject 
@property(nonatomic,retain)NSString *mealName,*mealTime;
@property(nonatomic,assign)BOOL arrangeMealOrNot;
@property(nonatomic,assign)int mealType;
@end




#pragma mark 餐次 名称 餐名
@interface RecipeClassesName : NSObject 
@property(nonatomic,retain)NSString *menuName,*menuPicUrl;
@property(nonatomic,assign)int menuID,menuWeight;
@end




#pragma mark 食谱 名细
@interface RecipeMenuList : NSObject 
@property(nonatomic,retain)NSString *foodKindName,*foodName,*foodPicUrl;
@property(nonatomic,assign)int foodNutrientDistributionID,foodWeight;
@end

#pragma mark 食物类别
@interface FoodResult : NSObject 
@property(nonatomic,assign)int foodNutrientDistributionID,foodKindIDBySSBT;
@property(nonatomic,retain)NSString *foodName,*foodKindNameBySSBT,*foodPicUrl;
@property(nonatomic,assign)BOOL isMark;
@end

#pragma mark 推荐菜谱

@interface recommendRecipe : NSObject 

@property(nonatomic,retain)NSString *menuName,*menuPicUrl;
@property(nonatomic,assign)int menuID,mealId;
@property(nonatomic,assign)BOOL flag;


@end



#pragma mark 推存食谱 日期类

@interface DateTableClass : NSObject
@property(nonatomic,retain)NSString *mDate;
@property(nonatomic,assign)int ID;
@end




#pragma mark mealTable
@interface MealTableClass : NSObject
@property(nonatomic,assign)int ID,FatherID,MealType;
@end


#pragma mark 保存全部
@interface SaveAllClass : NSObject 
@property(nonatomic,retain)NSMutableArray *mFoods,*mRecommnectFoods,*mRecommentRecipes;
@property(nonatomic,assign)BOOL isDiv;
@property(nonatomic,assign)int mealID;
@end


#pragma mark 配餐
@interface sysMeal : NSObject
@property(nonatomic,retain)NSString *name,*uType;
@property(nonatomic,assign)int ID;
@end


#pragma mark 菜谱成份
@interface FoodNutritiont : NSObject
@property(nonatomic,retain)NSString *foodName,*nutritionName,*foodAmount,*nutritionAmount,*unit;
@end



#pragma mark 历史记录列表
@interface MealList : NSObject
@property(nonatomic,retain)NSString *date;
@end
