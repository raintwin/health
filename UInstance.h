//
//  Instance.h
//  Health
//
//  Created by hq on 12-7-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
@interface UInstance : NSObject
//------------------
+(NSString*)GetDocument;
+(NSString*)GetDocumentByString:(NSString*)string;

////////////
+(NSString*)GetUseID;
+(NSString*)GetChildID;
+(int)GetUseType;


+(void) setLeftImageBtnForNavigationItem:(UINavigationItem*)item Image:(NSString*) image target:(id) target selector:(SEL) selector;
+(void)setRightImageBtnForNavigationItem:(UINavigationItem*)item Image:(NSString*) image target:(id) target selector:(SEL) selector;

+(void) setLeftImageAndNameForNavigationItem:(UINavigationItem*)item Image:(NSString*) image title:(NSString*)title target:(id)target selector:(SEL) selector;
+(void)setRightImageAndNameForNavigationItem:(UINavigationItem*)item Image:(NSString*) image title:(NSString*)title target:(id)target selector:(SEL) selector;
// --------------- UITextField
+(UITextField*)addTextFileld:(id)trage originX:(float)x originY:(float)y width:(float)width height:(float)height;
+(UITextField*)addTextFileld:(id)trage frame:(CGRect)frame;

// --------------- UITextView
+(UITextView*)addTextView:(id)trage originX:(float)x originY:(float)y width:(float)width height:(float)height;
+(UITextView*)addTextView:(id)trage frame:(CGRect)frame;


//------------ uilabel 
+(UILabel*)addLablePoint:(CGPoint)point text:(NSString*)text textsize:(float)textsize;
+(UILabel*)addLableRect:(CGRect)rect  text:(NSString*)text textsize:(float)textsize;


//------------ button
+(UIButton *)addUIButton:(UIView *)fview Title:(NSString*)Title  rect:(CGRect)rect;
@end



@interface HealthHeaders : NSObject

+(BOOL)DisposeRequest:(ASIHTTPRequest *)request;
@end
