//
//  StringTool.m
//  欧神诺ShowSide
//
//  Created by apple on 11-7-2.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "StringTool.h"


@implementation StringTool
+(NSArray *)fenxiStringArray:(NSString *)string separate:(NSString *)sp {
	NSArray *array = [string componentsSeparatedByString:sp];
	NSString *temString = @"";
	int temCount= 0;
	for(NSString *s in array) {
		if (temCount==0) {
			if (!([s isEqualToString:@""]||s.length<2||s==nil)){
				temString = [NSString stringWithFormat:@"%@",s];	
				temCount++;
			} 
		}else {
			if (!([s isEqualToString:@""]||s.length<2||s==nil)){
				temString = [NSString stringWithFormat:@"%@,%@",temString,s];
			} 
		}
		
	}
	return [temString componentsSeparatedByString:sp];
	
}
@end
