//
//  ChildPhotoMainView.m
//  Health
//
//  Created by hq on 12-9-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ChildPhotoMainView.h"
#import "PhotoFolderViewController.h"
#import "ChildListPhotoViewCtr.h"
#import "SendPhotoViewController.h"

#import "PhotoList.h"
@implementation ChildPhotoMainView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)backNav
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(backNav)];
    [Instance setRightImageBtnForNavigationItem:self.navigationItem Image:@"camera.png" target:self selector:@selector(TakePhoto)];
    // Do any additional setup after loading the view from its nib.
    [ViewTool addUIImageView:self.view imageName:@"photoviewbg.png" type:@"" x:0 y:0 x1:320 y1:368];
    if ([Instance GetUseType] == LOGIN_NOBIND_P) {
        {
            UIButton *botton = [ViewTool addUIButton:self.view imageName:@"在家.png" type:@"" x:34 y:239 x1:109 y1:112];
            [botton addTarget:self action:@selector(InHomePhoto) forControlEvents:64];
            
        }
        return;
    }
    if ([Instance GetUseType] == LOGIN_BIND_P || [Instance GetUseType] == LOGIN_TEACH) {
        {
            UIButton *botton = [ViewTool addUIButton:self.view imageName:@"在家.png" type:@"" x:34 y:239 x1:109 y1:112];
            [botton addTarget:self action:@selector(InHomePhoto) forControlEvents:64];
            
        }
        {
            UIButton *botton = [ViewTool addUIButton:self.view imageName:@"在园.png" type:@"" x:32+150 y:239 x1:109 y1:112];
            [botton addTarget:self action:@selector(InSchoolPhoto) forControlEvents:64];
            
        }
    }
}
-(void)TakePhoto
{
    SendPhotoViewController *detailViewController = [[SendPhotoViewController alloc] initWithNibName:@"SendPhotoViewController" bundle:nil];
    
    detailViewController.title = @"发照片";
    
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];

}
-(void)InHomePhoto
{
    int     mPageStart = 0;
    int     mPageCount = 1000;

    if ([Instance GetUseType] == LOGIN_NOBIND_P) {
//        NSMutableDictionary *mDic = [[NSMutableDictionary alloc] init];
//        [mDic setObject:[NSNumber numberWithInt:[[Instance GetChildID] intValue]] forKey:@"ID"];
//        [mDic setObject:[NSNumber numberWithInt:mPageCount] forKey:@"PageCount"];
//        [mDic setObject:[NSNumber numberWithInt:mPageStart] forKey:@"PageStart"];
//        [mDic setObject:[NSNumber numberWithBool:false] forKey:@"inSchoolOrInHome"];
//
//        PhotoList *mPhotoList = [[PhotoList alloc]initWithStyle:UITableViewStylePlain];
//        mPhotoList.mDictParameter = mDic;
//        [self.navigationController pushViewController:mPhotoList animated:YES];
//        [mDic release];
//        [mPhotoList release];
        
        PhotoFolderViewController *detailViewController = [[PhotoFolderViewController alloc] initWithNibName:@"PhotoFolderViewController" bundle:nil];
        detailViewController.title = @"在家照片记录";
        detailViewController.isInhomeOrSchool = NO;
        detailViewController.mChildID  =[[Instance GetChildID] intValue];
        detailViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self.navigationController pushViewController:detailViewController animated:YES];
        [detailViewController release];
    }
    if ([Instance GetUseType] == LOGIN_BIND_P) {
        
        
        NSMutableDictionary *mDic = [[NSMutableDictionary alloc] init];
        [mDic setObject:[NSNumber numberWithInt:[[Instance GetChildID] intValue]] forKey:@"ID"];
        [mDic setObject:[NSNumber numberWithInt:mPageCount] forKey:@"PageCount"];
        [mDic setObject:[NSNumber numberWithInt:mPageStart] forKey:@"PageStart"];
        [mDic setObject:[NSNumber numberWithBool:false] forKey:@"inSchoolOrInHome"];
        
        PhotoList *mPhotoList = [[PhotoList alloc]initWithStyle:UITableViewStylePlain];
        mPhotoList.mDictParameter = mDic;
        [self.navigationController pushViewController:mPhotoList animated:YES];
        [mDic release];
        [mPhotoList release];
        
        
//        PhotoFolderViewController *detailViewController = [[PhotoFolderViewController alloc] initWithNibName:@"PhotoFolderViewController" bundle:nil];
//        detailViewController.title = @"在家照片记录";
//        detailViewController.isInhomeOrSchool = NO;
//        detailViewController.mChildID  =[[Instance GetChildID] intValue];
//        detailViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//        [self.navigationController pushViewController:detailViewController animated:YES];
//        [detailViewController release];
    }
    
    
    if ([Instance GetUseType] == LOGIN_TEACH) {
        ChildListPhotoViewCtr *detailViewController = [[ChildListPhotoViewCtr alloc] initWithNibName:@"ChildListPhotoViewCtr" bundle:nil];
        detailViewController.title = @"幼儿在家照片记录";
        detailViewController.isInhomeOrSchool = NO;
        [self.navigationController pushViewController:detailViewController animated:YES];
        [detailViewController release];
    }

}
-(void)InSchoolPhoto
{
    if ([Instance GetUseType] == LOGIN_TEACH) {
        ChildListPhotoViewCtr *detailViewController = [[ChildListPhotoViewCtr alloc] initWithNibName:@"ChildListPhotoViewCtr" bundle:nil];
        detailViewController.title = @"在园照片记录";
        detailViewController.isInhomeOrSchool = YES;
        
        [self.navigationController pushViewController:detailViewController animated:YES];
        [detailViewController release];
        
    }
    
    if ([Instance GetUseType] == LOGIN_BIND_P) {
        PhotoFolderViewController *detailViewController = [[PhotoFolderViewController alloc] initWithNibName:@"PhotoFolderViewController" bundle:nil];
        detailViewController.title = @"在园照片记录";
        detailViewController.isInhomeOrSchool = YES;
        detailViewController.mChildID  =[[Instance GetChildID] intValue];
        detailViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self.navigationController pushViewController:detailViewController animated:YES];
        [detailViewController release];
    }
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
