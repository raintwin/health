//
//  OptionalMainViewController.m
//  Health
//
//  Created by dinghao on 13-4-22.
//
//

#import "OptionalMainViewController.h"
#import "OptionalMealViewController.h"
@interface OptionalMainViewController ()

@end

typedef enum  {
	BreakfastType = 18,
    BreakfastDotType,
    LunchType,
    LunchDotType,
    DinnerType,
} mealType;

@implementation OptionalMainViewController
@synthesize viewControllerDelegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [ViewTool addUIImageView:self.view imageName:@"matchbackgroup.jpg" type:@"" x:0 y:0 x1:320 y1:386];
    {
        UIButton *button = [ViewTool addUIButton:self.view imageName:@"breakfast.png" type:@"" x:30 y:70 x1:127 y1:67];
        button.tag = BreakfastType;
        [button addTarget:self action:@selector(setButton:) forControlEvents:64];
    }
    {
        UIButton *button = [ViewTool addUIButton:self.view imageName:@"lunch.png" type:@"" x:5 y:160 x1:152 y1:53];
        button.tag = LunchType;
        [button addTarget:self action:@selector(setButton:) forControlEvents:64];
    }
    {
        UIButton *button = [ViewTool addUIButton:self.view imageName:@"dinner.png" type:@"" x:10 y:230 x1:147 y1:56];
        button.tag = DinnerType;
        [button addTarget:self action:@selector(setButton:) forControlEvents:64];
    }
    {
        UIButton *button = [ViewTool addUIButton:self.view imageName:@"breakfastdot.png" type:@"" x:167 y:110 x1:146 y1:59];
        button.tag = BreakfastDotType;
        [button addTarget:self action:@selector(setButton:) forControlEvents:64];
    }
    {
        UIButton *button =[ViewTool addUIButton:self.view imageName:@"lunchdot.png" type:@"" x:167 y:200 x1:142 y1:49];
        button.tag = LunchDotType;
        [button addTarget:self action:@selector(setButton:) forControlEvents:64];
    }
}
-(void)setButton:sender
{
    UIButton *button = sender;
    NSLog(@"button .tag = %d",button.tag);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithInt:button.tag] forKey:@"mealType"];
    [defaults synchronize];
    
    ;
    OptionalMealViewController *viewController = [[OptionalMealViewController alloc]initWithNibName:@"OptionalMealViewController" bundle:nil];
    [viewControllerDelegate.navigationController pushViewController:viewController animated:YES];
    return;
    
    [viewController.view setFrame:[UIApplication sharedApplication].keyWindow.rootViewController.view.bounds];
    NSLog(@"bounds 11: %@",NSStringFromCGRect([UIApplication sharedApplication].keyWindow.rootViewController.view.bounds));
    [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview:viewController.view];
    
    CATransition *animation = [CATransition animation];
    animation.delegate = self;
    animation.duration = 0.3f;
    animation.timingFunction = UIViewAnimationCurveEaseInOut;
    animation.fillMode = kCAFillModeForwards;
    animation.type = kCATransitionPush;
    animation.subtype = kCATransitionFromRight;
    [[UIApplication sharedApplication].keyWindow.rootViewController.view.layer addAnimation:animation forKey:@"animation"];

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
