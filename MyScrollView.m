	//
	//  MyScrollView.m
	//  PhotoBrowserEx
	//
	//  Created by on 10-6-12.
	//  Copyright 2010 __MyCompanyName__. All rights reserved.
	//

#import "MyScrollView.h"


@implementation MyScrollView


@synthesize image;
@synthesize viewdelegate;
@synthesize mytag;
@synthesize  shuoming;
@synthesize imageView;
#pragma mark -
#pragma mark === Intilization ===
#pragma mark -
- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame]))
	{
		self.delegate = self;
		self.minimumZoomScale = 0.5;
		self.maximumZoomScale = 2.5;
		self.showsVerticalScrollIndicator = NO;
		self.showsHorizontalScrollIndicator = NO;
		
		imageView  = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
			//		if (shuoming!=nil &&shuoming.length>0) {
			//			NSLog(@"addUILable");
			//			textview = [ViewTool addUILable:self  x:2 y:550 x1:460 y1:220 fontSize:16 lableText:shuoming];
			//			textview.textColor = [UIColor blackColor];
			//			textview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];	
			//			textview.numberOfLines =20;
			//		}
		[self addSubview:imageView];
        
 
    }
    return self;    
}

- (void)setImage:(UIImage *)img
{
	imageView.image = img;
//    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
	 
}
-(void)setsizeimage
{
}
#pragma mark -
#pragma mark === UIScrollView Delegate ===
#pragma mark -
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{	 	
	[self.viewdelegate setNotNormalSize];
	return imageView;
}



- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
{
		//NSLog(@"%s", _cmd);
	NSLog(@"DFSADFASDFASFASDFDSADF SDFS D  ");
	[self.viewdelegate setNotNormalSize];
	CGFloat zs = scrollView.zoomScale;
	zs = MAX(zs, 1.0);
	zs = MIN(zs, 2.0);	
	
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];		
	scrollView.zoomScale = zs;	
	[UIView commitAnimations];
	
	if (zs==1.0) {
		[self.viewdelegate setNormalSize];
	}else {
		[self.viewdelegate setNotNormalSize];
	}
}

#pragma mark -
#pragma mark === UITouch Delegate ===
#pragma mark -
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{   
    
	if (shuoming!=nil &&shuoming.length>0&&textview==nil) {
		NSLog(@"addUILable");
		textview = [ViewTool addUILable:self.superview  x:0+self.frame.origin.x y:612 x1:1024 y1:110 fontSize:16 lableText:[NSString stringWithFormat:@"       %@",shuoming]];
		textview.textColor = [UIColor whiteColor];
		textview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];	
		textview.numberOfLines =20;
		textview.textAlignment =UITextAlignmentLeft;
	}
	
	else if (shuoming!=nil &&shuoming.length>0&&textview!=nil) {
		if (textview.hidden==YES) {
			textview.hidden=NO;
		}else {
			textview.hidden=YES;
		}
	}
	
	UITouch *touch = [touches anyObject];

	if ([touch tapCount] == 2) 
	{
        [NSObject cancelPreviousPerformRequestsWithTarget:self];
        
//			//NSLog(@"double click");
		

	}
    else {
        [self performSelector:@selector(SingTouch:) withObject:touch afterDelay:0.3f];

        }

}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
	
}
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if ([touch tapCount] == 2) {
        [self performSelector:@selector(DoubleTouch)];
    }
}
-(void)SingTouch:touch
{
    if ([touch tapCount] != 2) {
        if (self.viewdelegate == nil) {
            return;
        }
        [self.viewdelegate MyScrollViewSingTouch];
    }
}
-(void)DoubleTouch
{
    CGFloat zs = self.zoomScale;
    zs = (zs == 1.0) ? 2.0 : 1.0;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];			
    self.zoomScale = zs;	
    [UIView commitAnimations];
    if (zs==1.0) {
        [self.viewdelegate setNormalSize];
        textview.hidden = NO;
    }else {
        [self.viewdelegate setNotNormalSize];
        textview.hidden = YES;
    }
}
#pragma mark -
#pragma mark === dealloc ===
#pragma mark -
- (void)dealloc
{
	[image release];
	[imageView release];
	
    [super dealloc];
}


@end
