//
//  ChildOneListView.m
//  Health
//
//  Created by hq on 12-6-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ChildOneListView.h"
#import "SendNotifyViewController.h"
#import "InFoViewController.h"
#import "ASIFormDataRequest.h"
#import "Display.h"
#import "DownLoadTableImage.h"
#import "DisViewShow.h"
#define TABLAHIGHT 50
//#define TABLAMOREHEGIHT 52

#define ePageCount 8

#define KPageCount   @"PageCount"
#define KUserID      @"UserID"
#define KPageStart   @"PageStart"


#define ERIGHTICON         @"二字按钮.png"

@implementation ChildOneListView
@synthesize TYPE;
@synthesize mChildID;
@synthesize mTableTemplateView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

        mChildID = -1;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)navBack
{

    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    if (buttonIndex == 0) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    

    
    [ViewTool addUIImageViewWithAutoSize:self.view imageName:@"viewblackgroup.jpg" type:@"" x:0 y:0 x1:0 y1:0];
    // Do any additional setup after loading the view from its nib.
    
    [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];

    
    
    {
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];

        
        
        NSString *urlstr;
        if (TYPE == INSCHOOL) {
            urlstr= [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/ChildRepresentation/TPFetchChildRepresentationRecordsOfOneBabyInSchool"];
        }
        if (TYPE == INHOME) {
            urlstr= [NSString stringWithFormat:@"%@%@",WEBDEFAULTSERVERURL,@"/ChildRepresentation/TPFetchChildRepresentationRecordsOfOneBabyInHome"];
        }
        
        [mDict setObject:[NSNumber numberWithInt:mChildID] forKey:@"ID"];

        // Do any additional setup after loading the view from its nib.
        {
            TableTemplateView *table = [[TableTemplateView alloc]initWithFrame:CGRectMake(0, 0, 320, 372)];
            table.isCase = NO;
            [table setTableHeightForRow:TABLAHIGHT];
            [table setUrlString:urlstr];
            table.DataDict = mDict;
            table.delegate = self;
            self.mTableTemplateView = table;
            [self.view addSubview:mTableTemplateView];
            [mDict release];
            [table release];
            [mTableTemplateView GetDataForHttp];
        }    
        
        [Instance setLeftImageBtnForNavigationItem:self.navigationItem Image:BACKICON target:self selector:@selector(navBack)];    

    }
}

#pragma  mark requestFinished

-(void)ReturnHttpData:(NSString*)ResultContent container:(NSMutableArray*)arrays tag:(int)tag isFinish:(BOOL)isFinish;
{
    
    NSArray *array;
    if (TYPE == INSCHOOL) {
        array = [[[ResultContent JSONValue] valueForKey:@"childRepresentationRecordsOfOneBabyInSchool"] retain];
        
    }
    else if (TYPE == INHOME ) {
        array = [[[ResultContent JSONValue] valueForKey:@"childRepresentationRecordsOfOneBabyInHome"] retain];
        
    }
    else
        return;
    
    if ([array count]<ePageCount) {
        mTableTemplateView.isLoadOver = YES;
    }
    else
        mTableTemplateView.isLoadOver = NO;

    
    
    for (int i = 0; i< [array count]; i++) {
        DisplayInfo *nobj = [[DisplayInfo alloc] init];
        nobj.mChildOfDisID = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childRepresentationID"]] intValue];
        nobj.mDisComment = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childRepresentationComment"]]; 
        nobj.mDisType = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"childRepresentationType"]] intValue];
        
        nobj.mDisDate = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"publishDateTime"]];
        nobj.mDiserID = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"publishPersonID"]] intValue];
        nobj.mDiserName = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"publishPersonName"]];
        
        [arrays addObject:nobj];
        [nobj release];
    }
    [array release];
    [mTableTemplateView setRequestDatas:arrays];
    
    //-----
    if ([arrays count] == 0) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"暂无记录" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alertView.delegate = self;
        [alertView show];
        [alertView release];
    }

}
-(void)SetTableData:(UITableViewCell*)cell indexpath:(NSIndexPath*)indexPath datas:(NSMutableArray *)mDatas;
{
    for (UIView *ctr  in [cell.contentView subviews]) {
        [ctr removeFromSuperview];
    }
    cell.imageView.image = nil;
    cell.textLabel.text = nil;

    NSInteger row = [indexPath row];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    cell.imageView.image = nil;
        
        DisplayInfo *obj = [mTableTemplateView.requestDatas objectAtIndex:row];
        
        {
            UILabel *label = [Instance addLableRect:CGRectMake(50, 3, 200, 20) text:@"" textsize:14];
            label.font = [UIFont boldSystemFontOfSize:15];
            label.tag = 200001;
            label.text = obj.mDisComment;
            [cell.contentView addSubview:label];
            [label release];
        }
        {
            UILabel *label = [Instance addLableRect:CGRectMake(50, 25, 200, 20) text:@"" textsize:12];
            label.tag = 200003;
            label.text = obj.mDisDate;
            label.textColor = HEXCOLOR(0xafabab);
            [cell.contentView addSubview:label];
            [label release];
        }
        cell.imageView.image = [UIImage imageNamed:[self CommonTypeImage:obj.mDisType]];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
}
-(void)TableDidSelect:(NSIndexPath*)indexPath Datas:(NSMutableArray*)mDatas;
{
    DisplayInfo *obj = [mTableTemplateView.requestDatas objectAtIndex:[indexPath row]];
    DisViewShow *nifCtr = [[DisViewShow alloc] initWithNibName:@"DisViewShow" bundle:nil];
    nifCtr.title = @"个人表现";
    nifCtr.mChildOfDisID = obj.mChildOfDisID;
    nifCtr.mChildID = mChildID;
    nifCtr.TYPE = TYPE;
    [self.navigationController pushViewController:nifCtr animated:YES];
    [nifCtr release];
}
-(void)TableDownLoadCellImage
{}


-(NSString*)CommonType:(int)tag
{
    switch (tag) {
            case 0:
            return  @"日表现";
            break;
            case 1:
            return @"周表现";
            break;
            case 2:
            return @"学期表现";
            break;
        default:
            break;
    }
    return nil;
}
-(NSString*)CommonTypeImage:(int)tag
{
    switch (tag) {
        case 0:
            return  @"日评价.png";
            break;
        case 1:
            return @"周评价.png";
            break;
        case 2:
            return @"期末评价.png";
            break;
        default:
            break;
    }
    return nil;
}

-(void)dealloc
{
//    [mDownLoadImageDict release];
    [mTableTemplateView release]; 
    
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mTableTemplateView = nil;
//    self.mDownLoadImageDict = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
