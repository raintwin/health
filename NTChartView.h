//
//  NTChartView.h
//  chart
//
//  Created by lee jory on 09-10-15.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NTChartView : UIView {
	
	//组数据，只实现一个组
	NSArray *groupData;
	
	//最大值，最小值, 列宽度, 
	float maxValue,minValue,columnWidth,sideWidth,maxScaleValue,maxScaleHeight;
	
	
}

@property(retain,nonatomic) NSArray *groupData;
@end
